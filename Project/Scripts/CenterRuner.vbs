'Create QTP object
Set QTP = CreateObject("QuickTest.Application")
QTP.Launch
QTP.Visible = TRUE
 
 WScript.echo "ST" 
'Open QTP Test
Test_Result_Path ="D:\SCB_CoMarch\Project\Scripts\Co-March-Web_CenterRun"
'Test_Result_Path =WScript.Arguments.Item(3)
QTP.Open Test_Result_Path, TRUE 'Set the QTP test path
  
'Set Result location
Set qtpResultsOpt = CreateObject("QuickTest.RunResultsOptions")
'qtpResultsOpt.ResultsLocation = "Result path" 'Set the results location


 
'Set the Test Parameters
Set qtpParams = QTP.Test.ParameterDefinitions.GetParameters()

'WScript.echo "Parameter1(ScenarioFile)=" & WScript.Arguments.Item(0)
'WScript.echo "Parameter2(Browser)=" & WScript.Arguments.Item(1)
 
'Set the value for test environment through command line
On Error Resume Next
ParamName1 = "Param1"
qtpParams.Item(ParamName1).Value = WScript.Arguments.Item(0)
WScript.echo "Parameter1(ScenarioFile)=" & qtpParams.Item(ParamName1).Value
ParamName2 = "Param2"
qtpParams.Item(ParamName2).Value = WScript.Arguments.Item(1)
WScript.echo "Parameter2(Browser)=" & qtpParams.Item(ParamName2).Value
ParamName3 = "Param3"
qtpParams.Item(ParamName3).Value = WScript.Arguments.Item(2)
WScript.echo "Parameter3(SheetName)=" & qtpParams.Item(ParamName3).Value
ParamName4 = "Param4"
qtpParams.Item(ParamName4).Value = WScript.Arguments.Item(3)
WScript.echo "Parameter4(RunMode)=" & qtpParams.Item(ParamName4).Value
On Error GoTo 0
 
  
'Run QTP test
QTP.Test.Run qtpResultsOpt, FALSE, qtpParams

 WScript.echo "DN" 
  
'Close QTP
'QTP.Test.Close
'QTP.Quit

