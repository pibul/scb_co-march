' Description:
'   Reads a Microsoft Excel file.
' Parameters:
'   strFile - [in] The name of the Excel file to read.
' Returns:
'   A two-dimension array of cell values, if successful.
'   Null on error
Option Explicit

Function ReadScenarioFile(ByVal strFile, ByVal strSheet)

  ' Local variable declarations
  Dim objExcel, objSheet, objCells, i, objWorkbook
  Dim nUsedRows, nUsedCols, nTop, nLeft, nRow, nCol
  Dim arrSheet()
  Dim activeSheetNo
  activeSheetNo = 0

  ' Default return value
  ReadScenarioFile = Null

  ' Create the Excel object
  On Error Resume Next
  Set objExcel = CreateObject("Excel.Application")
  If (Err.Number <> 0) Then
    Exit Function
  End If

  ' Don't display any alert messages
  objExcel.DisplayAlerts = 0  

  ' Open the document as read-only
  On Error Resume Next
  'Call objExcel.Workbooks.Open(strFile, False, True)
  Set objWorkbook = objExcel.Workbooks.Open (strFile)
  
  For i = 1 To objWorkbook.Sheets.Count
	if (Lcase(objWorkbook.Sheets(i).Name) = Lcase(strSheet)) Then
	WScript.Echo "Found Sheet" & i & "=" & objWorkbook.Sheets(i).Name
	activeSheetNo = i
	End If	
  Next 
  
  If (Err.Number <> 0) Then
	WScript.Echo "error"
    Exit Function
  End If
  
  If (activeSheetNo = 0) Then
	WScript.Echo "Not found sheet " & strSheet
    Exit Function
  End If


  ' If you wanted to read all sheets, you could call
  ' objExcel.Worksheets.Count to get the number of sheets
  ' and the loop through each one. But in this example, we
  ' will just read the first sheet.
  Set objSheet = objExcel.ActiveWorkbook.Worksheets(activeSheetNo)

  ' Get the number of used rows
  nUsedRows = objSheet.UsedRange.Rows.Count

  ' Get the number of used columns
  nUsedCols = objSheet.UsedRange.Columns.Count

  ' Get the topmost row that has data
  nTop = objSheet.UsedRange.Row

  ' Get leftmost column that has data
  nLeft = objSheet.UsedRange.Column

  ' Get the used cells
  Set objCells = objSheet.Cells

  ' Dimension the sheet array
  ReDim arrSheet(nUsedRows - 1, nUsedCols - 1)

  ' Loop through each row
  For nRow = 0 To (nUsedRows - 1)
    ' Loop through each column
    For nCol = 0 To (nUsedCols - 1)
  ' Add the cell value to the sheet array
  arrSheet(nRow, nCol) = objCells(nRow + nTop, nCol + nLeft).Value
    Next
  Next

  ' Close the workbook without saving
  Call objExcel.ActiveWorkbook.Close(False)

  ' Quit Excel
  objExcel.Application.Quit

  ' Return the sheet data to the caller
  ReadScenarioFile = arrSheet

End Function



Sub ExcelDumper(scInputFile, scSheetName, scDestFile)

  ' Local variable declarations
  Dim strFile, arrSheet, i, j, varCell, strFormat
  Dim inxTestCaseNameidx, inxRunStatus, inxTestCaseFilePath
  Dim testscenarioDirPath, testcaseDirPath
  testscenarioDirPath = "D:\SCB_CoMarch\Project\TestScenarios"
  testcaseDirPath = "D:\SCB_CoMarch\Project_20180203\TestCases"
  inxTestCaseNameidx = -1
  inxRunStatus = -1
  inxTestCaseFilePath = -1
  
  ' Prompt for the Excel file to read  
  If IsNull(scInputFile) Then Exit Sub

  ' Read the Excel file
  arrSheet = ReadScenarioFile(scInputFile,scSheetName)
  If IsNull(arrSheet) Then Exit Sub
  
  'Exit Sub
  
  For i = 0 To UBound(arrSheet, 2)
    if (Lcase(arrSheet(0, i)) = Lcase("TestCaseName")) Then
		inxTestCaseNameidx = i
	ElseIf (Lcase(arrSheet(0, i)) = Lcase("RunStatus")) Then 
		inxRunStatus = i
	ElseIf (Lcase(arrSheet(0, i)) = Lcase("TestCaseFilePath")) Then 
		inxTestCaseFilePath = i	
	End If    
  Next
  
  
  if  (inxTestCaseNameidx = -1) Or (inxRunStatus = -1) Or (inxTestCaseFilePath = -1) Then
    WScript.echo "Incomplete Header"
	Exit Sub
  End If
  
  

  Dim varTestCaseName, varTestCaseFile, varRunStatus, objSheet
  Dim curOpenFile, objTestCaseExcel, objTestCaseWorkbook, CheckStatus, curOpenFilePath
  Dim desTestCaseFilePath, desTestCaseExcel, desTestCaseWorkbook, desSheet
  Dim CopyExcelRange
  
  CopyExcelRange = "A1:FZ1000"
  desTestCaseFilePath = testcaseDirPath & "\" & scDestFile
  CheckStatus = -1
  
  Set objTestCaseExcel = CreateObject("Excel.Application")
  ' Dump the worksheet to the command line
  
  Set desTestCaseExcel = CreateObject("Excel.Application")
  Set desTestCaseWorkbook = desTestCaseExcel.Workbooks.Open (desTestCaseFilePath) 
  desTestCaseWorkbook.Application.Visible = False
  'desTestCaseWorkbook.Application.DisplayAlerts = False
  
  For i = 1 To UBound(arrSheet, 1)
  
	varTestCaseName = arrSheet(i, inxTestCaseNameidx)
	varTestCaseFile = arrSheet(i, inxTestCaseFilePath)
	varRunStatus = arrSheet(i, inxRunStatus)
    
	  if (Lcase(varTestCaseName) = Lcase("endofrow")) Then
		Exit Sub
	  Elseif (Lcase(varRunStatus) = Lcase("Yes")) And  (Lcase(varTestCaseName) <> Lcase("LaunchWeb_EN")) And (Lcase(varTestCaseName) <> Lcase("LaunchWeb_TH")) And  (Lcase(Trim(varTestCaseName)) <> Lcase("CloseBrowser")) Then
	  
	  
	  
		WScript.echo varTestCaseName & "," & varRunStatus & "," &  varTestCaseFile

		if curOpenFile <> varTestCaseFile then
		
		'------Close Old File------------ Start
			if curOpenFile <> "" then
				WScript.echo "Close old file " & curOpenFile
				
				' Close the workbook without saving
				'Call objTestCaseExcel.ActiveWorkbook.Close(False)
				objTestCaseWorkbook.Close
				set objTestCaseWorkbook = Nothing
				
			end if
		'------Close Old File------------ End 
			
			
		'-----Open New File------------- Start
			WScript.echo "Open new file " & varTestCaseFile
			curOpenFile = arrSheet(i, inxTestCaseFilePath)
			curOpenFilePath  = testcaseDirPath &"\"&curOpenFile
			
			' Open the document as read-only
			On Error Resume Next
			Set objTestCaseWorkbook = objTestCaseExcel.Workbooks.Open (curOpenFilePath) 
			objTestCaseExcel.Application.Visible = False
			objTestCaseExcel.Application.DisplayAlerts = False
			  
		  If (Err.Number <> 0) Then
			 WScript.echo "File : " & curOpenFile &" - Not Found !"
			 CheckStatus = 0
		  else
			CheckStatus = 1
			WScript.echo "File : " & curOpenFile &" - OK"
		  End If

		'-----Open New File-------------End 
			
		end if
		
		  if CheckStatus = 1 then
		
			Err.clear
			Set objSheet = objTestCaseExcel.ActiveWorkbook.Worksheets(varTestCaseName)
			If (Err.Number <> 0) Then
				WScript.echo "Sheet : " & varTestCaseName&" - Not Found at Source !"
			Else 
				WScript.echo "Sheet : " & varTestCaseName &" - OK at Source"
				
				Err.clear
				Set desSheet = desTestCaseExcel.ActiveWorkbook.Worksheets(varTestCaseName)
				If (Err.Number <> 0) Then
					WScript.echo "Sheet : " & varTestCaseName&" - Not Found at Destination ! "
				Else 
					WScript.echo "Sheet : " & varTestCaseName &" - OK at Destination"
					objSheet.Range(CopyExcelRange).Copy
					desSheet.Range(CopyExcelRange).PasteSpecial
					desTestCaseWorkbook.Save
					WScript.echo "Copy done"
				End If
			
			End If
		  
		  elseif CheckStatus = 0 then 
			  WScript.echo "Cannot proceed for this file !"
		  End if
		
	  End If
	  


  Next
  
  'Close the workbook without saving	
  
   objTestCaseWorkbook.Close
   set objTestCaseWorkbook = Nothing
   objTestCaseExcel.Application.Quit
   Set objTestCaseExcel = Nothing
   
   desTestCaseWorkbook.Save
   desTestCaseWorkbook.Close
   set desTestCaseWorkbook = Nothing
   desTestCaseExcel.Application.Quit
   Set desTestCaseExcel = Nothing

 
End Sub


'Main Script


WScript.echo "Start"

Dim paramScenarioFile, paramTestDataSheet, paramDestinationFile


paramScenarioFile = "D:\SCB_CoMarch\Project\TestScenarios\TestScenario_CoMarch_Web_Payment_Direct_Credit.xls"
paramTestDataSheet = "CoMarch_Web_EN"
paramDestinationFile = "TestCase_BirdTest.xls"


Call ExcelDumper(paramScenarioFile, paramTestDataSheet,paramDestinationFile)

'Dim objExcel, objWorkbook, objSheet, objCells, i

'create the excel object
	'Set objExcel = CreateObject("Excel.Application") 

'view the excel program and file, set to false to hide the whole process
	'objExcel.Visible = True 

'open an excel file (make sure to change the location) .xls for 2003 or earlier
	'Set objWorkbook = objExcel.Workbooks.Open("D:\SCB_CoMarch\Project\TestScenarios\TestScenario_CoMarch_Web_Payment_Direct_Credit.xls")

'For i = 1 To objWorkbook.Sheets.Count
'  WScript.Echo "Sheet" & i & "=" & objWorkbook.Sheets(i).Name
'Next 

'set a cell value at row 3 column 5
	'objExcel.Cells(3,5).Value = "new value"

'change a cell value
	'objExcel.Cells(3,5).Value = "something different"
	
'delete a cell value
	'objExcel.Cells(3,5).Value = ""

'get a cell value and set it to a variable
	'r3c5 = objExcel.Cells(3,5).Value

'save the existing excel file. use SaveAs to save it as something else
	'objWorkbook.Save

'close the workbook
	'objWorkbook.Close 

'exit the excel program
	'objExcel.Quit

'release objects
	'Set objExcel = Nothing
	'Set objWorkbook = Nothing

WScript.echo "Done"