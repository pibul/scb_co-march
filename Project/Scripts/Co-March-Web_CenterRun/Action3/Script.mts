﻿	
PathLibrary = "..\..\Share\Library\"
LoadFunctionLibrary(PathLibrary & "DecideActivity.txt")
LoadFunctionLibrary(PathLibrary & "ExcelHandlingFunctions.txt")
LoadFunctionLibrary(PathLibrary & "InitializeTestExecution.txt")
LoadFunctionLibrary(PathLibrary & "LibCommon.vbs")
LoadFunctionLibrary(PathLibrary & "ReportingFunctions.txt")
LoadFunctionLibrary(PathLibrary & "StartFramework.txt")

'Loading function library for Web
LoadFunctionLibrary(PathLibrary & "Web_Business.txt")
LoadFunctionLibrary(PathLibrary & "Web_Common.txt")

'Loading function library for Mobile
LoadFunctionLibrary(PathLibrary & "MCActivity_Business.txt")
LoadFunctionLibrary(PathLibrary & "MCActivity_IOS.txt")
LoadFunctionLibrary(PathLibrary & "MCActivity_Common.txt")
LoadFunctionLibrary(PathLibrary & "TerminalActivity.txt")

Call DefineExternalParam()


Dim param_browser, param_scenario_file, param_sheet_name

param_browser = Parameter("browser")
param_scenario_file = Parameter("scenario_file")	
param_sheet_name = Parameter("sheet_name")	


call logging("Browser = " & param_browser,c_DEBUG,1)
call logging("Scenario File = " & param_scenario_file,c_DEBUG,1)
call logging("Scenario Sheet = " & param_sheet_name,c_DEBUG,1)

'Define mode of executing
Environment("ExecutionType") = "Local"

'Below statment
Environment("RootProject") = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")

'Device("Device").OpenViewer
call logging("Start " & sMODULE,c_DEBUG,1)



'Define Test Scenario file path
Environment("TestScenario") = "TestScenarios\"&param_scenario_file
Environment("TestProfilePath") =Environment("RootProject")&"\Profile\Profile.xls"




'Define Browser
'LanuchBrowser =array("chrome")
Environment("DefaultBrowser") =  param_browser

'Define Test Scenario sheet
'TestScenarioSheet =  array("CoMarch_Web_EN" , "CoMarch_Web_TH")
Dim TestScenarioSheet

If param_sheet_name = "CoMarch_Web_EN" Then
	TestScenarioSheet =  array("CoMarch_Web_EN")
ElseIf param_sheet_name = "CoMarch_Web_TH" Then
	TestScenarioSheet =  array("CoMarch_Web_TH")
ElseIf param_sheet_name = "ALL" Then
	TestScenarioSheet  =  array("CoMarch_Web_EN","CoMarch_Web_TH")
ElseIf param_sheet_name = "CoMarch_Mobile_EN" Then
	TestScenarioSheet =  array("CoMarch_Mobile_EN")
ElseIf param_sheet_name = "CoMarch_Mobile_TH" Then
	TestScenarioSheet =  array("CoMarch_Mobile_TH")
ElseIf param_sheet_name = "ALLMOBILE" Then
	TestScenarioSheet =  array("CoMarch_Mobile_EN,CoMarch_Mobile_TH")	
End If

'TestScenarioSheet =  param_sheet_name

	'Start executing G-Able Framework
	For i = 0 To Ubound(TestScenarioSheet) Step 1
	    print "Number of round : " &i
	    print "TestScenarioSheet : " &TestScenarioSheet(i)
	    SwitchTestCaseProfile Environment("TestProfilePath"),Environment("TestScenario"),TestScenarioSheet(i) 
		Call InitializeQTP(TestScenarioSheet(i))	
	Next
	
	'print "Number of round : " &i
	'print "TestScenarioSheet : " &TestScenarioSheet
	'SwitchTestCaseProfile Environment("TestProfilePath"),Environment("TestScenario"),TestScenarioSheet
	'Call InitializeQTP(TestScenarioSheet)
	

call logging("Finish " & sMODULE,c_DEBUG,1)



