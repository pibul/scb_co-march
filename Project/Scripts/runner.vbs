'Create QTP object
Set QTP = CreateObject("QuickTest.Application")
QTP.Launch
QTP.Visible = TRUE
 
'Open QTP Test
Test_Result_Path ="D:\SCB_CoMarch\Project\Scripts\Co-March-Web_Center"
QTP.Open Test_Result_Path, TRUE 'Set the QTP test path
QTP.Visible = False
QTP.Test.Run 

'Set Result location
'Set qtpResultsOpt = CreateObject("QuickTest.RunResultsOptions")
'qtpResultsOpt.ResultsLocation = "Result path" 'Set the results location
 
'Run QTP test
'QTP.Test.Run qtpResultsOpt
 
'Close QTP
'QTP.Test.Close
'QTP.Quit