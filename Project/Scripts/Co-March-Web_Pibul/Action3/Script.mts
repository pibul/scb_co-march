﻿
'Define mode of executing
Environment("ExecutionType") = "Local"

'Below statment
Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")

'Device("Device").OpenViewer
call logging("Start " & sMODULE,c_DEBUG,1)

'Define Test Scenario file path
Environment("TestScenario") = "TestScenarios\TestScenario_CoMarch_Web_Payment_Direct_Credit.xls"

'Define Browser
LanuchBrowser =array("ie")

'Define Test Scenario sheet
TestScenarioSheet =  array("CoMarch_Web_EN" , "CoMarch_Web_TH")

For j = 0 To Ubound(LanuchBrowser) Step 1
	
	Environment("DefaultBrowser") =  LanuchBrowser(j)
	
	'Start executing G-Able Framework
	For i = 0 To Ubound(TestScenarioSheet) Step 1
	    print "Number of round : " &i
	    print "TestScenarioSheet : " &TestScenarioSheet(i)
		Call InitializeQTP(TestScenarioSheet(i))
		
	Next
Next

call logging("Finish " & sMODULE,c_DEBUG,1)


