﻿If Environment("ReplaceProfile") = "ON" Then

'Define mode of executing
Environment("ExecutionType") = "Local"

'Below statment
Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")

'Device("Device").OpenViewer
call logging("Start " & sMODULE,c_DEBUG,1)

'Define Test Scenario file path
Environment("TestScenario") = "TestScenarios\TestScenario_Run_Payment.xls"
Environment("TestProfilePath") =Environment("RootProject")&"\Profile\Profile.xls"

'Define Browser
LanuchBrowser =array("chrome")

'Define Test Scenario sheet
'TestScenarioSheet =  array("CoMarch_Web_EN" , "CoMarch_Web_TH")
TestScenarioSheet =  array("CoMarch_Web_EN")

'For j = 0 To Ubound(LanuchBrowser) Step 1
	
	'Environment("DefaultBrowser") =  LanuchBrowser(j)
	
	'Start executing G-Able Framework
	For i = 0 To Ubound(TestScenarioSheet) Step 1
	    print "Number of round : " &i
	    print "TestScenarioSheet : " &TestScenarioSheet(i)
	    SwitchTestCaseProfile Environment("TestProfilePath"),Environment("TestScenario"),TestScenarioSheet(i) 
		'Call InitializeQTP(TestScenarioSheet(i))
		'call WB_CloseBrowser(LanuchBrowser(j))
	Next
'Next

call logging("Finish " & sMODULE,c_DEBUG,1)

End If
