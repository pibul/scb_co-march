﻿ 
'Define mode of executing
Environment("ExecutionType") = "Local"

'Below statment
Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")

If repositoriescollection.Count > 0 Then
	repositoriescollection.RemoveAll	
End If

Environment("ObjectRepository") = Environment("RootProject") & "\Share\ObjectRepository\TestObject_2.tsr"
'Add runtime repository file
repositoriescollection.Add Environment("ObjectRepository")

'Device("Device").OpenViewer
call logging("Start " & sMODULE,c_DEBUG,1)

'Define Test Scenario file path
Environment("TestScenario") = "TestScenarios\TestScenario_BirdTest.xls"

'Define Test Scenario sheet
TestScenarioSheet = "Co-March_TH"


'Start executing G-Able Framework
For Iterator = 1 To 1 Step 1
    print "Number of round : " &Iterator
	Call InitializeQTP(TestScenarioSheet)
	
Next

call logging("Finish " & sMODULE,c_DEBUG,1)


