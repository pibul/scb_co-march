﻿'
'Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")
'
'Call CountCreatePayment("xxx")
'ExitTest
'
'systemutil.Run "chrome.exe","https://ccbuat.se.scb.co.th"
'
'Browser("Co-MarchWeb").Page("Common").WebEdit("Login_Username_Inp").Set "supall8259"
'Browser("Co-MarchWeb").Page("Common").WebButton("Login_UserNext_btn").Click
'Browser("Co-MarchWeb").Page("Common").WebEdit("Login_Password_Inp").Set "CS@2018k"
'Browser("Co-MarchWeb").Page("Common").WebButton("Login_UserNext_btn").Click
'
'
'Call WB_BIZ_GetTransactionFee("CenterOneAutoPayment0405171651;Variable")
'
'print "Test"


Call DefineExternalParam()

'Define mode of executing
Environment("ExecutionType") = "Local"

'Below statment
Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")

'Device("Device").OpenViewer
call logging("Start " & sMODULE,c_DEBUG,1)

'Define Test Scenario file path
Environment("TestScenario") = "TestScenarios\TestScenario_CoMarch_Web_Add_to_Package_Lumpsum_Package.xls"
Environment("TestProfilePath") =Environment("RootProject")&"\Profile\Profile.xls"
'Define Browser
LanuchBrowser =array("ie")

'Define Test Scenario sheet
TestScenarioSheet =  array("CoMarch_Web_EN" , "CoMarch_Web_TH")

For j = 0 To Ubound(LanuchBrowser) Step 1
	
	Environment("DefaultBrowser") =  LanuchBrowser(j)
	
	'Start executing G-Able Framework
	For i = 0 To Ubound(TestScenarioSheet) Step 1
		call logging("Number of round : " &i,c_DEBUG,3)
		call logging("TestScenarioSheet : " &TestScenarioSheet(i),c_DEBUG,3)	    
	    SwitchTestCaseProfile Environment("TestProfilePath"),Environment("TestScenario"),TestScenarioSheet(i) 
		Call InitializeQTP(TestScenarioSheet(i))
		
	Next
Next

call logging("Finish " & sMODULE,c_DEBUG,1)


