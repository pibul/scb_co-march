'Option Explicit
  
Public Function MC_UtilityActivity(functionName,ExpectedResult,Param1,Param2,Param3,Param4,Param5)
	Dim aResult(0,1)   
	Dim addResult
	addResult="no"
	
	If (LCase(Param1)  <> "n/a") and  (LCase(Param1)  <> "na") and  (LCase(Param1) <> "")  Then 
	    
		If Param1 = "{TeseCaseName}" Then
			Param1 = Environment("TestCaseName1")			
		End If		
		
		call logging("functionName " & functionName,c_DEBUG,1)
		
			OSType = Environment("OSType")
			Device_ID = Environment("Device_ID")
			If Err.Number <> 0 Then
				call logging(Err.Description,c_DEBUG,3)
				Err.Clear
			End If	
			Select Case Trim(functionName)   
				
				Case "MC_GetOTP"	
					strValueToEnter=Param1
					return1=MC_GetOTP(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_SetOTP"	
					strValueToEnter=Param1
					return1=MC_SetOTP(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	

				Case "MC_CheckAccountNotInList"			
					strValueToEnter=Param1
					MC_FunctionName="MC_CheckAccountNotInList_"&OSType&"(strValueToEnter)"
					return1=Eval(MC_FunctionName)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
				
				Case "MC_Business_FastEasyPinLogin"	
					strValueToEnter=Param1
					return1=MC_Business_FastEasyPinLogin(strValueToEnter,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
	
				Case "MC_ClickOnText"	
					strValueToEnter=Param1
					return1=MC_ClickOnText(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_EnterKey"	
					strValueToEnter=Param1
					return1=MC_EnterKey(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_RefreshSummaryPage"	
					strValueToEnter=Param1
					return1=MC_RefreshSummaryPage(OSType,strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_Business_PaymentSelectAccount"	
					strValueToEnter=Param1
					return1=MC_Business_PaymentSelectAccount(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
				
				Case "MC_Business_ConfirmDuplicate"	
					strValueToEnter=Param1
					return1=MC_Business_ConfirmDuplicate(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_CheckCashBack"	
					strValueToEnter=Param1
					return1=MC_CheckCashBack(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_ChooseAccountTransferFrom"	
					strValueToEnter=Param1
					MC_FunctionName="MC_ChooseAccountTransferFrom_"&OSType&"(strValueToEnter,Device_ID)"
					return1=Eval(MC_FunctionName)
'					return1=MC_ChooseAccountTransferFrom(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_ChooseClientInvestmentFrom"	
					strValueToEnter=Param1
					MC_FunctionName="MC_ChooseClientInvestmentFrom_"&OSType&"(strValueToEnter,Device_ID)"
					return1=Eval(MC_FunctionName)
'					return1=MC_ChooseClientInvestmentFrom(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_ChooseAccountInvestmentFrom"	
					strValueToEnter=Param1
					MC_FunctionName="MC_ChooseAccountInvestmentFrom_"&OSType&"(strValueToEnter,Device_ID)"
					return1=Eval(MC_FunctionName)
'					return1=MC_ChooseAccountInvestmentFrom(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_InputTopupAmount"	
					strValueToEnter=Param1
					return1=MC_InputTopupAmount(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_WaitIdle"
					strValueToEnter=Param1
					return1=MC_WaitIdle(strValueToEnter)
					addResult="no"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_Business_CheckEditFavoritePayment"
					strValueToEnter=Param1
					return1=MC_Business_CheckEditFavoritePayment(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1				
	
				Case "MC_PlusNumber"
					strValueToEnter=Param1
					return1=MC_PlusNumber(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
	
				Case "MC_MinusNumber"
					strValueToEnter=Param1
					return1=MC_MinusNumber(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
	
				Case "MC_MultiplyNumber"
					strValueToEnter=Param1
					return1=MC_MultiplyNumber(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
					
				Case "MC_DivideNumber"
					strValueToEnter=Param1
					return1=MC_DivideNumber(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
					
				Case "MC_Back"
					strValueToEnter=Param1
					return1=MC_Back()
					addResult="no"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_Find_ShareApp"
					strValueToEnter=Param1
					MC_FunctionName="MC_Find_ShareApp_"&OSType&"(strValueToEnter)"
					return1=Eval(MC_FunctionName)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_SetPin"
					strValueToEnter=Param1
					return1=MC_SetPin(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
	
				Case "MC_SwitchDevice"	
					strValueToEnter=Param1
					return1=MC_SwitchDevice(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_ReinstallApp"	
					strValueToEnter=Param1
					return1=MC_ReinstallApp(strValueToEnter,Device_ID,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_UninstallApp"	
					strValueToEnter=Param1
					return1=MC_UninstallApp(strValueToEnter,Device_ID,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_LaunchApp"	
					strValueToEnter=Param1
					return1=MC_LaunchApp(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_RestartApp"	
					strValueToEnter=Param1
					return1=MC_RestartApp(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_CloseApp"	
					strValueToEnter=Param1
					return1=MC_CloseApp(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_TurnOnOff3G"	
					strValueToEnter=Param1
					return1=MC_TurnOnOff3G(strValueToEnter,Device_ID)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_GetBalance"		
					strValueToEnter=Param1
					return1=MC_GetBalance(strValueToEnter,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
	
				Case "MC_GetBalanceRemain"	
					strValueToEnter=Param1
					return1=MC_GetBalanceRemain(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_ClickMainMenu"	
					strValueToEnter=Param1
					return1=MC_ClickMainMenu(strValueToEnter,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
				
				Case "MC_IOSSkipStep"
					skipstep=Param1				
					return1=MC_IOSSkipStep(OSType,skipstep)
					addResult="no"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_changelimitadmin"	
					strValueToEnter=Param1
					return1=MC_changelimitadmin(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_EasynetLogin"	
					strValueToEnter=Param1
					return1=MC_EasynetLogin(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_PurchaseFund"	
					strValueToEnter=Param1
					return1=MC_PurchaseFund(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

				Case "MC_RedeemFund"	
					strValueToEnter=Param1
					return1=MC_RedeemFund(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case "MC_SwitchFund"	
					strValueToEnter=Param1
					return1=MC_SwitchFund(strValueToEnter)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
				Case Else
					'Test Call framework
					actionResult=UtilityActivity(functionName,ExpectedResult,Param1,Param2,Param3,Param4,Param5)   
					
					'Remark because to use Business function, must remark reporter of Utility function
					'          Reporter.ReportEvent micFail,"Test Aborted. The Activity name is —>"&functionName,"Input a valid function name in the Activity column."
	'				aResult(0,0)=-1     
	'				aResult(0,1)=-1
	'				UtilityActivity=aResult
	'				Exit Function
			
			End Select    
  	End If	
	aResult(0,0)=addResult     
	aResult(0,1)=returnResult  
	MC_UtilityActivity=aResult

 End Function    


Public Function MobileActivity(appdataSource,UIName,functionName,ExpectedResult,captureScreen,Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param11,Param12,oParent)
   
     Dim aResult(0,1)
     Dim addResult
     addResult="no"

	Const sModule = "MobileActivity"
	call logging("sModule " & sMODULE,c_DEBUG,2)	

	UIName = lcase(UIName)
	strChildobject  = objDictChild.Item(UIName)
	strParentObject = objDictParent.Item(UIName)

 	If  (LCase(Param1)  <> "n/a") and  (LCase(Param1)  <> "na")   Then
 	
		OSType = Environment("OSType")
		Device_ID = Environment("Device_ID")
		If Err.Number <> 0 Then
			call logging(Err.Description,c_DEBUG,3)
			Err.Clear
		End If			
  		Select Case Trim(functionName)
  		
			Case"MC_SetTextOnEdit"
			
				MC_FunctionName="MC_SetTextOnEdit_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				strValueToEnter=Param1
				return1=Eval(MC_FunctionName)	
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case"MC_ClearAndSetText"
			
				strValueToEnter=Param1
				return1=MC_ClearAndSetText(strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case"MC_SetFocusText"
			
				strValueToEnter=Param1
				return1=MC_SetFocusText(strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_GetValueOnLabel"

				strValueToEnter=Param1
				return1=MC_GetValueOnLabel(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case"MC_WaitHideObject"
			
				strValueToEnter=Param1
				return1=MC_WaitHideObject(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_ClickOnButton"
			
				MC_FunctionName="MC_ClickOnButton_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				strValueToEnter=Param1
				return1=Eval(MC_FunctionName)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	
				
			Case "MC_CheckTextOnLabel"
			
				strValueToEnter=Param1
				return1=MC_CheckTextOnLabel(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	
				
			Case "MC_WaitObject"	
			
				strValueToEnter=Param1
				return1=MC_WaitObject(strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	
			
			Case "MC_CloseAllApp"	
				strValueToEnter=Param1
				return1=MC_CloseAllApp(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	
			
			Case "MC_SwipeRight"	
				strValueToEnter=Param1
				return1=MC_SwipeRight(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	

			Case "MC_SwipeLeft"	
				strValueToEnter=Param1
				return1=MC_SwipeLeft(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	

			Case "MC_SwipeUp"	
				strValueToEnter=Param1
				return1=MC_SwipeUp(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_SwipeDown"	
				strValueToEnter=Param1
				return1=MC_SwipeDown(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
			
			Case "MC_ChooseAddAccount"	
				strValueToEnter=Param1
				return1=MC_ChooseAddAccount(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	

			Case "MC_CompareTextContain"	
				MC_FunctionName="MC_CompareTextContain_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				strValueToEnter=Param1
				return1=Eval(MC_FunctionName)	
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_SwipeLeftFindObj"	
				strValueToEnter=Param1
				return1=MC_SwipeLeftFindObj(strValueToEnter,UIName,ExpectedResult,OSType)			
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_SwipeRightFindObj"	
				strValueToEnter=Param1
				return1=MC_SwipeRightFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1	
				
			Case "MC_SwipeUpFindObj"	
				strValueToEnter=Param1
				return1=MC_SwipeUpFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_SwipeDownFindObj"	
				strValueToEnter=Param1
				return1=MC_SwipeDownFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_SwipeUpRelateObj"	
				strValueToEnter=Param1
				return1=MC_SwipeUpRelateObj(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_ChooseQuickAccount"	
				strValueToEnter=Param1
				return1=MC_ChooseQuickAccount(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_Find_AccountNO"	
				strValueToEnter=Param1
				MC_FunctionName="MC_Find_AccountNO_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				return1=Eval(MC_FunctionName)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1


			Case "MC_Find_CreditCardNO"	
				strValueToEnter=Param1
				return1=MC_Find_CreditCardNO(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_Find_LoansNO"	
				strValueToEnter=Param1
				return1=MC_Find_LoansNO(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1		
				
			Case "MC_CompareNumber"	
				strValueToEnter=Param1
				return1=MC_CompareNumber(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_ChooseAccountTransferTo"	
				strValueToEnter=Param1
				MC_FunctionName="MC_ChooseAccountTransferTo_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				return1=Eval(MC_FunctionName)
'				return1=MC_ChooseAccountTransferTo(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_SetNumberOnEdit"	
				MC_FunctionName="MC_SetNumberOnEdit_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult,Device_ID)"
				strValueToEnter=Param1
				return1=Eval(MC_FunctionName)	
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_SetToggle"	
				strValueToEnter=Param1
				return1=MC_SetToggle(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_ChooseItemFromRadio"	
				strValueToEnter=Param1
				return1=MC_ChooseItemFromRadio(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_ChooseItemFromList"	
				strValueToEnter=Param1
				return1=MC_ChooseItemFromList(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_CheckTypeObject"	
				strValueToEnter=Param1
				return1=MC_CheckTypeObject(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_CompareDate"	
				strValueToEnter=Param1
				return1=MC_CompareDate(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_CheckEnable"	
				strValueToEnter=Param1
				return1=MC_CheckEnable(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_CheckIsFocus"	
				strValueToEnter=Param1
				return1=MC_CheckIsFocus(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_CheckVisibleObject"	
				strValueToEnter=Param1
				return1=MC_CheckVisibleObject(strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_CheckGreeting"	
				strValueToEnter=Param1
				return1=MC_CheckGreeting(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_SetSliderBar"	
				strValueToEnter=Param1
				MC_FunctionName="MC_SetSliderBar_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				return1=Eval(MC_FunctionName)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_GetLimitValue"			
				strValueToEnter=Param1
				return1=MC_GetLimitValue(strValueToEnter,UIName,ExpectedResult)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_DeleteThisItem"
			
				MC_FunctionName="MC_DeleteThisItem_"&OSType&"(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)"
				strValueToEnter=Param1
				return1=Eval(MC_FunctionName)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1
				
			Case "MC_SetQuickMenuOff"	
				
				strValueToEnter=Param1
				return1=MC_SetQuickMenuOff(strValueToEnter,UIName,ExpectedResult,OSType)
				addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
				returnResult=return1

			Case "MC_ChooseItemFromRadioPromptpay"	
					strValueToEnter=Param1
					return1=MC_ChooseItemFromRadioPromptpay(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1	
					
			Case "MC_CheckLanguage"	
					strValueToEnter=Param1
					return1=MC_CheckLanguage(strValueToEnter,UIName,ExpectedResult,OSType)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					
			Case "MC_ClickAndCheckObj"	
					strValueToEnter=Param1
					return1=MC_ClickAndCheckObj(strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

			Case "MC_WaitUntilFindObj"	
					strValueToEnter=Param1
					return1=MC_WaitUntilFindObj(strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

			Case "MC_HighlightObject"	
					strValueToEnter=Param1
					return1=MC_HighlightObject(strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
				
			Case "MC_CheckProperty"
					strValueToEnter=Param1
					return1=MC_CheckProperty(strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1

			Case "MC_ClearValueOnEdit"
					strValueToEnter=Param1
					return1=MC_ClearValueOnEdit(strValueToEnter,UIName,ExpectedResult)
					addResult="yes"  'The flag is set to add result to the dictionary object. Only when the flag is set to yes'Results' will be added.
					returnResult=return1
					

			Case Else
				aResult(0,0)=-1
				aResult(0,1)=-1
				MobileActivity = aResult
				
				'Reporter.ReportEvent micFail,"Test Aborted. The Activity name is —> " & functionName,"Input a valid function name in the Activity column."
				
				Exit Function
 		End Select

	 
	End If 

	aResult(0,0)=addResult
	aResult(0,1)=returnResult
 
 	MobileActivity = aResult

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ClickOnButton_ANDROID()
'*  Purpose:        To click on button
'*  Arguments:      strParentObject,strChildobject,ValueToEnter,UIName
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ClickOnButton_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity ="click on button"
	Const sModule = "MC_ClickOnButton"
	call logging("sModule " & sMODULE,c_DEBUG,2)
	Call logging("strValueToEnter : " & strValueToEnter,c_DEBUG,3)
	
	Mobile_Object_t = Create_OR_Object(UIName,"")
 	set objTestObject = Eval(Mobile_Object_t)
 					
	If lcase(strValueToEnter)="yes" or (Len(strValueToEnter)) = 0 or lcase(objTestObject.GetRoProperty("class"))="view" Then
			Click_Type = "yes"			
		 	get_mcindex = MapUIName_ANDROID(UIName)		 	
			If  get_mcindex <>"::N/A" Then
					objTestObject.SetTOProperty "mcindex",get_mcindex
					call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 				
			End If									
	Else
			Click_Type = "text"		
			If InStr(Mobile_Object_t,".Page(") > 0 Then
				objTestObject.SetTOProperty "innertext",strValueToEnter		
			Else
				If lcase(UIName)= "newbillpay_servicenamesearch_btn" Then
					replace_text="(?i).*"&strValueToEnter&".*"
				Else
					replace_text = strValueToEnter
				End If
				objTestObject.SetTOProperty "text",replace_text	
			End If 						
	End If
	
	If objTestObject.exist(3) Then
		If InStr(Mobile_Object_t,".Page(") > 0 Then
			objTestObject.Click
		Else
					If InStr(lcase(Mobile_Object_t),"accsum_invest") > 0 _
					or InStr(lcase(Mobile_Object_t),"invest_fundsname_txt") > 0 _
					or InStr(lcase(Mobile_Object_t),"lifestyle_donation_txt") > 0 Then
					
						objTestObject.Tap
						wait 1
						objTestObject.RefreshObject
						If objTestObject.Exist(1) Then
							objTestObject.Tap
						End If
						
					Else
			objTestObject.Tap
					End If			
		End If 
		iRet = CheckError()	
	End If
	
	If iRet = True Then
		If Click_Type = "yes" Then
			descript="click done"
		Else
			descript="click on text <b>"&strValueToEnter&"</b> done"
		End If
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
  	End If
  	
	set objTestObject = Nothing
	MC_ClickOnButton_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ClickAndCheckObj()
'*  Purpose:        click and check expect object
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ClickAndCheckObj(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity ="click and check expect object"
	Const sModule = "MC_ClickAndCheckObj"
	call logging("sModule " & sMODULE,c_DEBUG,2)
	Call logging("strValueToEnter : " & strValueToEnter,c_DEBUG,3)
	
	If instr(strValueToEnter,";")>0 Then
		
		splitvar = split(strValueToEnter,";")
		ClickVal = splitvar(0)
		ExpectVal = splitvar(1)
		
		Mobile_Object_t = Create_OR_Object(UIName,"")
	 	set objTestObject = Eval(Mobile_Object_t)
	 	
		Expect_Object_t = Create_OR_Object(ExpectedResult,"")
	 	set ExpectObject = Eval(Expect_Object_t)
	 	
		If lcase(ClickVal)="yes" Then
				Click_Type = "yes"			
			 	get_mcindex = MapUIName_ANDROID(UIName)		 	
				If  get_mcindex <>"::N/A" Then
						objTestObject.SetTOProperty "mcindex",get_mcindex
						call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 				
				End If									
		Else
				Click_Type = "text"		
				If InStr(Mobile_Object_t,".Page(") > 0 Then
					objTestObject.SetTOProperty "innertext",ClickVal		
				Else
					objTestObject.SetTOProperty "text",ClickVal	
				End If 						
		End If
		
		If lcase(ExpectVal)<>"yes" Then
				Click_Type = "text"		
				If InStr(Expect_Object_t,".Page(") > 0 Then
					ExpectObject.SetTOProperty "innertext",ExpectVal		
				Else
					ExpectObject.SetTOProperty "text",ExpectVal	
				End If 	
		End If
		
		If objTestObject.exist(3) Then		
			If InStr(Mobile_Object_t,".Page(") > 0 Then
				objTestObject.Click
			Else
				objTestObject.Tap		
			End If 
			
			If ExpectObject.Exist(10) Then
				descript="Found expect object <b>["&Expect_Object_t&"]</b>"
				call logging("Found expect object ["&Expect_Object_t&"]",c_DEBUG,3) 
				iRet = True		
			Else
				descript="not found expect object <b>["&Expect_Object_t&"]</b>"
				call logging("not found expect object ["&Expect_Object_t&"]",c_DEBUG,3) 
				iRet = False	
			End If	
		Else
			iRet = False
			Environment("TS_ErrorFound") = 1
		    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
		    descript = Environment("TS_ErrorDescption")
		End If
	Else
		descript="invalid input data"
		call logging(descript,c_DEBUG,3) 
		iRet = False	
  	End If
	
	set objTestObject = Nothing
	MC_ClickAndCheckObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetValueOnLabel()
'*  Purpose:        Get value text from object label     
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_GetValueOnLabel(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Activity ="get text on label"
	Const sModule = "MC_GetValueOnLabel"	
	Call logging("sModule " & sMODULE,c_DEBUG,2)
	
	Mobile_Object_t = Create_OR_Object(UIName,"")
	set objTestObject = Eval(Mobile_Object_t)
	
	If OSType="ANDROID" Then
		get_mcindex = MapUIName_ANDROID(UIName)
	Else
		get_mcindex = MapUIName_IOS(UIName)
	End If
	
	If  get_mcindex <>"::N/A" Then
		objTestObject.SetTOProperty "mcindex",get_mcindex
		call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 	
	End If	
				
	If InStr(Mobile_Object_t,".Page(") > 0 Then
		Environment(strValueToEnter) = objTestObject.GetROProperty("innertext")	
	Else
		Environment(strValueToEnter) = objTestObject.GetROProperty("text")
	End If
	
	iRet = CheckError()			
	If iRet = True Then
		descript="success get value : <b>"&Environment(strValueToEnter)&"</b>"
		call logging("success get value in Environment("&strValueToEnter&"): "&Environment(strValueToEnter),c_DEBUG,3) 
	Else
		Environment(strValueToEnter) = "::N/A"
		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3) 
	End If	
	set objTestObject=Nothing
	MC_GetValueOnLabel = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CheckTextOnLabel()
'*  Purpose:        To compare text Excel Data and App Label text is match whole text 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckTextOnLabel(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Activity ="check text on label"
	Const sModule = "MC_CheckTextOnLabel"	
	call logging("sModule " & sMODULE,c_DEBUG,2)
	
	call logging("UIName : " & UIName,c_DEBUG,3)

	Mobile_Object_t = Create_OR_Object(UIName,"")
	set objTestObject = Eval(Mobile_Object_t)
	call logging("Object : " & Mobile_Object_t,c_DEBUG,3)
		
	If OSType="ANDROID" Then
		get_mcindex = MapUIName_ANDROID(UIName)
	Else
		get_mcindex = MapUIName_IOS(UIName)
	End If
	
	If  get_mcindex <>"::N/A" Then
		objTestObject.SetTOProperty "mcindex",get_mcindex
		call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 	
	End If	
		
	Flag = CheckObjectExist(objTestObject)
	
	If Flag=1 Then
	       		
		If InStr(Mobile_Object_t,".Page(") = 0 Then
				ROProperty="text"
		Else
				ROProperty="innertext"			
		End If
		
		get_text = objTestObject.GetROProperty(ROProperty)
		CompareVal = StrComp(get_text, strValueToEnter, vbTextCompare)
		If CompareVal = 0 Then
			iRet = True
		Else
			iRet = False
		End If
	Else
		iRet = False
	End If	
		If iRet = True Then
			descript ="Excel Data : <b>["&strValueToEnter&"]</b> match app label : <b>["&get_text&"]</b>"
			call logging("Excel Data : ["&strValueToEnter&"] match app label : ["&get_text&"]",c_DEBUG,2)
		Else
			If Flag=-1 Then
				descript="not found object : "&Mobile_Object_t
				call logging(descript,c_DEBUG,2)
			Else
				descript ="Excel Data : <b>["&strValueToEnter&"]</b> <font color=""red"">not match </font> app label : <b>["&get_text&"]</b>"
				call logging("Excel Data : ["&strValueToEnter&"] not match app label : ["&get_text&"]",c_DEBUG,2)
			End If
		End If
		
	set objTestObject=Nothing
	MC_CheckTextOnLabel = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToValidate,ExpectedResult)
		
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SetTextOnEdit_ANDROID()
'*  Purpose:        To input any text on Text box     
'*  Arguments:      strParentObject,strChildobject,ValueToEnter,UIName
'* 	Return Value:   SetTextOnEdit
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetTextOnEdit_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "set text on textbox"
   	Const sModule = "SetTextOnEdit"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	
	If objTestObject.exist(3) Then	
		objTestObject.Set strValueToEnter
		If InStr(mobile_object_t,".Page(") > 0  and lcase(UIName)<>"register_otp_data" _
		and objTestObject.GetROProperty("value") <> strValueToEnter Then	
			'Check input value if miss last char ,add double last char
			strValueToEnter = strValueToEnter&right(strValueToEnter,1)
			objTestObject.Set strValueToEnter	
		End If
		iRet = CheckError()
	End If	

	If iRet = True Then
		descript = "enter text <b>["& strValueToEnter & "]</b> completely"
		call logging("enter text ["& strValueToEnter & "] completely ",c_DEBUG,3)
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
  	End If		
    set objTestObject= Nothing
	MC_SetTextOnEdit_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ClearValueOnEdit()
'*  Purpose:        Clear textbox on Text box     
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   SetTextOnEdit
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ClearValueOnEdit(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "clear value on textbox"
   	Const sModule = "MC_ClearValueOnEdit"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	
	If objTestObject.exist(3) Then
		If InStr(mobile_object_t,".Page(") > 0  Then
			ROProperty = "innertext"
		Else
			ROProperty = "text"
		End If
		
		'Check textbox value
		If len(objTestObject.GetROProperty(ROProperty))>0 Then
				FoundText = objTestObject.GetROProperty(ROProperty)
				call logging("Found text ["& FoundText & "]",c_DEBUG,3)
				NumText = len(objTestObject.GetROProperty(ROProperty))			
				If InStr(mobile_object_t,".Page(") > 0  Then
					objTestObject.Click
				Else
					objTestObject.Tap
				End If
				
				MC_EnterKey("key;End")
				For r_round = 1 To NumText Step 1
					MC_EnterKey("key;BackSpace")
				Next
		End If
		iRet = CheckError()	
	End If

	If iRet = True Then	
		descript = "clear text on <b>["& UIName & "]</b> completely"
		call logging("clear text on ["& UIName & "] completely ",c_DEBUG,3)
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
  		call logging(descript,c_DEBUG,3)
  	End If	
  		
    set objTestObject= Nothing
	MC_ClearValueOnEdit = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function


'**********************************************************************************************************************************
'*  Function Name : MC_ClearAndSetText()
'*  Purpose:        Clear textbox and input any text on Text box     
'*  Arguments:      strParentObject,strChildobject,ValueToEnter,UIName
'* 	Return Value:   SetTextOnEdit
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ClearAndSetText(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "clear and set text on textbox"
   	Const sModule = "MC_ClearAndSetText"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	
	If objTestObject.exist(3) Then
		If InStr(mobile_object_t,".Page(") > 0  Then
			ROProperty = "innertext"
			'Check textbox value
			If len(objTestObject.GetROProperty(ROProperty))>0 Then
					NumText = len(objTestObject.GetROProperty(ROProperty))
					objTestObject.Click
					MC_EnterKey("key;End")
					For r_round = 1 To NumText Step 1
						MC_EnterKey("key;BackSpace")
					Next
			End If
		    
			objTestObject.Set strValueToEnter
				
			If lcase(UIName)<>"register_otp_data" Then
				'Check input value if miss last char ,add double last char
				If objTestObject.GetROProperty("value")<>strValueToEnter Then
					SetVal=strValueToEnter&right(strValueToEnter,1)
					objTestObject.Set SetVal
				End If 
			End If
		Else
			ROProperty = "text"
			objTestObject.Tap
			objTestObject.SetFocus
			'Check textbox value
			If len(objTestObject.GetROProperty(ROProperty)) > 0 Then
				NumText = len(objTestObject.GetROProperty(ROProperty))
				MC_EnterKey("key;End")
				For r_round = 1 To NumText Step 1
				MC_EnterKey("key;BackSpace")
				Next
			End If		
				objTestObject.Set strValueToEnter		
		End If
		iRet = CheckError()	
	End If

	If iRet = True Then	
		descript = "enter text <b>["& strValueToEnter & "]</b> completely"
		call logging("enter text ["& strValueToEnter & "] completely ",c_DEBUG,3)
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
  		call logging(descript,c_DEBUG,3)
  	End If	
  		
    set objTestObject= Nothing
	MC_ClearAndSetText = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SetNumberOnEdit_ANDROID()
'*  Purpose:        To input number on Text box     
'*  Arguments:      strParentObject,strChildobject,ValueToEnter,UIName
'* 	Return Value:   SetNumberOnEdit
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetNumberOnEdit_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult,Device_ID)
	On Error Resume Next
	Activity = "set number on textbox"
   	Const sModule = "MC_SetNumberOnEdit"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	
	If objTestObject.exist(3) Then
		If InStr(mobile_object_t,".Page(") > 0 Then
			ROProperty = "innertext"
		Else
			ROProperty = "text"
		End If 
			
		NumText = len(objTestObject.GetROProperty(ROProperty))
		If NumText > 0 Then
			If InStr(mobile_object_t,".Page(") > 0 Then
				objTestObject.Click
			Else
				objTestObject.SetFocus
			End If
				MC_EnterKey("key;End")
				For r_round = 1 To NumText Step 1
					MC_EnterKey("key;BackSpace")
				Next
		End If
	
		If InStr(mobile_object_t,".Page(") > 0 Then
			objTestObject.Click
		Else
			objTestObject.Tap
			objTestObject.SetFocus
		End If	
		
		SetVal=FormatNumber(strValueToEnter,,,,0)
		wait 2
		iRet = CheckError()	
		If iRet = True Then	
			SDKPath=Environment("RootProject") & "\SDK-tools"
			Set oShell = CreateObject ("WSCript.shell")
			For Iterator = 1 To len(SetVal) Step 1
					 SetVal_Mid = mid(SetVal,Iterator,1)
					 Select Case Trim(SetVal_Mid)
			
					Case"0"
						SetVal_Mid = 7
					Case"1"
						SetVal_Mid = 8
					Case"2"
						SetVal_Mid = 9
					Case"3"
						SetVal_Mid = 10
					Case"4"
						SetVal_Mid = 11
					Case"5"
						SetVal_Mid = 12
					Case"6"
						SetVal_Mid = 13
					Case"7"
						SetVal_Mid = 14
					Case"8"
						SetVal_Mid = 15		
					Case"9"
						SetVal_Mid = 16	
					Case"."
						SetVal_Mid = 56						
					End Select
					 Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell input keyevent "&SetVal_Mid&" & Exit"
					oShell.Run Command,0,False
					wait 1
			Next
			Set oShell = Nothing 
			iRet = True	
			inputVal=FormatNumber(objTestObject.GetROProperty(ROProperty),,,,0)
			descript = "enter number <b>["& inputVal & "]</b> completely "
			call logging("strValueToEnter " & SetVal,c_DEBUG,3)				
			call logging("inputVal : " & inputVal,c_DEBUG,3)	
		Else
	  		descript = Environment("TS_ErrorDescption")
	  		call logging(descript,c_DEBUG,3)
	  	End If	
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")	
		call logging(descript,c_DEBUG,3)  		
   	End If	
    set objTestObject= Nothing
	MC_SetNumberOnEdit_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SetFocusText()
'*  Purpose:        To set focus text on textbox 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   SetFocusOnEdit
'* 	Author By		SUEBSAKUL A. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetFocusText(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "set focus text on textbox" 	
   	Const sModule = "MC_SetFocusText"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	
	If objTestObject.exist(3) Then
		If InStr(mobile_object_t,".Page(") > 0 Then
			objTestObject.Click
		Else
			objTestObject.SetFocus	
		End if		
		iRet = CheckError()
	End If	
	
	If iRet = True Then
		descript="set focus on " &mobile_object_t
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If		
	call logging(descript,c_DEBUG,3)
    set objTestObject= Nothing
	MC_SetFocusText = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CompareTextContain_ANDROID()
'*  Purpose:        To compare some text in Excel data contain in app label   
'*  Arguments:      strParentObject,strChildobject,ValueToEnter,UIName
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CompareTextContain_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	Activity = "compare text contain"
	Const sModule = "MC_CompareTextContain"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Mobile_Object_t = Create_OR_Object(UIName,"")
	set objTestObject=Eval(Mobile_Object_t)	
	get_mcindex = MapUIName_ANDROID(UIName)
	If  get_mcindex <>"::N/A" Then
		objTestObject.SetTOProperty "mcindex",get_mcindex
		call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 	
	End If	
	
	Flag = CheckObjectExist(objTestObject)
		
	If Flag=1 Then
	
		If InStr(Mobile_Object_t,".Page(") = 0 Then
			ROProperty="text"				
		Else
			ROProperty="innertext"							
		End If
		get_text = objTestObject.GetROProperty(ROProperty)
		If Instr(get_text,strValueToEnter) > 0 then
			iRet = True
			descript = "found <b>["&strValueToEnter&"]</b> contain in <b>"&get_text&"</b>"
			call logging("found ["&strValueToEnter&"] contain in "&get_text,c_DEBUG,3) 
		Else
			iRet = False
			descript = "<font color=red>not found</font> <b>["&strValueToEnter&"]</b> contain in <b>"&get_text&"</b>"
			call logging("not found ["&strValueToEnter&"] contain in "&get_text,c_DEBUG,3)  	
		End if	
	Else
		iRet = False
		descript="not found object :"&Mobile_Object_t
		call logging(descript,c_DEBUG,2)
	End If
	set objTestObject = Nothing
	MC_CompareTextContain_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function 

'**********************************************************************************************************************************
'*  Function Name : MC_Business_FastEasyPinLogin()
'*  Purpose:        Input pin in login page  
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Business_FastEasyPinLogin(strValueToEnter,OSType)
	On Error Resume Next
	Activity = "login pin"
	Environment("CheckScenarioLogin") = strValueToEnter
'    call logging("Password : "& Password,c_DEBUG,3) 
	Const sModule = "MC_Business_FastEasyPinLogin"
  	Call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set objLoginObject=Eval(Create_OR_Object("Login_Number_Btn",""))
	For Iterator = 1 To len(strValueToEnter) Step 1	
		Pwd_Number=Mid(strValueToEnter,Iterator,1)
		If OSType="ANDROID" Then		
			objLoginObject.SetTOProperty "resourceid","button_"&Pwd_Number				
		Else
			objLoginObject.SetTOProperty "text",Pwd_Number
		End If		
						  										
		objLoginObject.Tap
		objLoginObject.RefreshObject
		call logging("Click Number : " & Pwd_Number,c_DEBUG,3)								
	Next
	set objLoginObject= Nothing	
	
	iRet = CheckError()
	If iRet = True Then
		wait 3
		'Check is still same page	  
		set objTestObject_check=Eval(Create_OR_Object("Login_1_Btn",""))
		Flag = CheckObjectExist(objTestObject_check)	
		If Flag = 1 Then
			iRet = False	
			descript="Error to input pin login"
		Else
			iRet = True	
			descript="login done"
			Environment("Login") = 1
		End If
		set objTestObject_check= nothing
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If
	
	MC_Business_FastEasyPinLogin = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

Function MC_Business_ConfirmDuplicate(strConfirm)
	Dim iRet ,UIName ,ExpectedResult, MaxLoop
	
	Const sModule = "MC_Business_ConfirmDuplicate"
  	call logging("sModule " & sMODULE,c_DEBUG,3)	
	
	UIName = "Payment_Billing_MessageConfirmDup"		  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	Flag = CheckObjectExist(objTestObject)	
	If Flag = 1 Then
	
		If lcase(strConfirm) = "yes" Then
			UIName = "Payment_Billing_AcceptDup"
		Else
			UIName = "Payment_Billing_CancleDup"
		End If
		
		set objTestObject=Eval(Create_OR_Object(UIName,""))	
		Flag = CheckObjectExist(objTestObject)
	
		
		objTestObject.tap
		
	End If
	
	set objTestObject= nothing
	set objTestObjectAccountNo= nothing

	iRet = True	
	descript="Accept when found confirm"

	MC_Business_ConfirmDuplicate = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function



Function MC_Business_PaymentSelectAccount(strSelectAccountNo)
	Dim iRet ,UIName ,ExpectedResult, MaxLoop
	Activity = "login pin"
	
	Const sModule = "MC_Business_PaymentSelectAccount"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
			
	UIName = "Payment_Billing_AccountSource"		  		
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	call logging("Environment ValidateText : " & strSelectAccountNo,c_DEBUG,3)
	Flag = CheckObjectExist(objTestObject)
	
	If Flag = 1 Then
		
		Environment("ValidateText") = strSelectAccountNo
		Environment("mcIndex") = 40
			
		set objTestObjectAccountNo=Eval(Create_OR_Object("Payment_Billing_AccountNo",""))
		
		MaxLoop = 20
		FountAccNo = False
		do		
			CheckAcc = objTestObjectAccountNo.Exist(2)
			
			If CheckAcc Then
				objTestObjectAccountNo.Tap
				FlagWaite = MC_WaitObject(strParentObjectAccountNo,strChildobjectAccountNo,strSelectAccountNo,"Payment_Billing_AccountNo",ExpectedResult)
				FountAccNo = True
			else
				'objTestObject.Swipe "left"
				objTestObject.Tap 300,50
				
				Environment("mcIndex") = Environment("mcIndex") + 2
			End If
			
			MaxLoop = MaxLoop - 1		

			call logging("Remain MaxLoop : " & MaxLoop,c_DEBUG,3)
		Loop until FountAccNo = True or MaxLoop <= 0 
		
		If FountAccNo Then
			iRet = True	
			descript="login done"
		Else
			iRet = False	
			descript="Not found object"			
		End If
		
	Else
		iRet = False	
		descript="Not found object"	

	End If
	
	set objTestObject= nothing
	set objTestObjectAccountNo= nothing
	
	MC_Business_PaymentSelectAccount = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_WaitObject()
'*  Purpose:        for check object is exist  
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_WaitObject(strValueToEnter,UIName,ExpectedResult)
	If UIName = "Register_checkyouracc_txt" Then
		wait 1
	End If
	Activity = "wait object"
	Const sModule = "MC_WaitObject"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	mobile_object_t=Create_OR_Object(UIName,"")	
	set objTestObject=Eval(mobile_object_t)
	call logging("Wait Object : " & mobile_object_t,c_DEBUG,3)
		
	If lcase(strValueToEnter)<>"yes" Then
		call logging(" Text To Search : " & strValueToEnter,c_DEBUG,3)
		If InStr(mobile_object_t,".Page(") > 0 Then	
			objTestObject.SetTOProperty "innertext",strValueToEnter			
		Else
			objTestObject.SetTOProperty "text",strValueToEnter
		End if
	End if	
	
	Flag = CheckObjectExist(objTestObject)	
	If  Flag = 1 then	
		If lcase(strValueToEnter)<>"yes" Then
			descript="found text ["&strValueToEnter&"] in object ["&mobile_object_t&"]"		
			call logging("found text ["&strValueToEnter&"] in object ["&mobile_object_t&"]",c_DEBUG,3)				
		Else		
			descript="found object <font color=blue>["&mobile_object_t&"]</font>"
			call logging("found object ["&mobile_object_t&"]",c_DEBUG,3)			
		End if
		iRet = True	
	Else
		If lcase(strValueToEnter)<>"yes" Then
			descript="not found text ["&strValueToEnter&"] in object ["&mobile_object_t&"]"	
			call logging("not found text ["&strValueToEnter&"] in object ["&mobile_object_t&"]",c_DEBUG,3)			
		Else
			descript="not found object ["&mobile_object_t&"]"
			call logging("not found object ["&mobile_object_t&"]",c_DEBUG,3)
		End if	
		iRet = False
	End If 
	
	set objTestObject = nothing
	call logging("functionName " & Environment("functionName"),c_DEBUG,3)
	If Environment("functionName")="MC_WaitObject" Then
		MC_WaitObject = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	Else
		If iRet = True Then
			MC_WaitObject=1
		Else
			MC_WaitObject=-1
		End If
	End If
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CheckVisibleObject()
'*  Purpose:        check visible object 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_CheckVisibleObject(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "check visible object"
	Const sModule = "MC_CheckVisibleObject"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	mobile_object_t=Create_OR_Object(UIName,"")	
  	set objTestObject=Eval(mobile_object_t)
  	
  	If instr(strValueToEnter,";") > 0 Then
  		splitVar = split(strValueToEnter,";")
  		ObjVisible = splitVar(0)
  		replaceText = splitVar(1)
  		If lcase(replaceText) <> "yes" Then
  			objTestObject.SetToProperty "text",replaceText
  		End If
  	Else
  		ObjVisible = strValueToEnter
  	End If
  	
  	Flag = CheckObjectExist(objTestObject)
  	
  	If lcase(ObjVisible)="yes" Then		
  		If Flag = 1 Then
  			descript="found object <font color=""blue"">["&mobile_object_t&"]</font>"
  			call logging("found object ["&mobile_object_t&"]",c_DEBUG,3)
		Else
		  	descript="not found object <font color=""red"">[" &mobile_object_t&"]</font>"
  			call logging("not found object ["&mobile_object_t&"]",c_DEBUG,3)
  		End If
  		iRet = Flag
  	ElseIf lcase(ObjVisible)="no" Then
  		If Flag = -1 Then
  			iRet=True
  			descript="not found object <font color=""blue"">["&mobile_object_t&"]</font>"
  			call logging("not found object ["&mobile_object_t&"]",c_DEBUG,3)			
  		Else
  		  	descript="found object <font color=""red"">["&strParentObject&"."&strChildobject &"]</font>"
  			call logging("found object ["&mobile_object_t&"]",c_DEBUG,3)
  			iRet=False
  		End If
  	Else
  		iRet=False
  		descript="invalid input data"
  		call logging("invalid input data",c_DEBUG,3)
  	End If
  	
  	MC_CheckVisibleObject = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_WaitHideObject()
'*  Purpose:        for check object is not exist  
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		SUEBSAKUL A. (AMS-GABLE)
'************************************************************************************************************************
Function MC_WaitHideObject(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On error resume next
	Activity = "wait hide object"
	Const sModule = "MC_WaitHideObject"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	Mobile_Object_t = Create_OR_Object(UIName,"")
	call logging("Wait hide Object : " &Mobile_Object_t,c_DEBUG,3)
	set objTestObject=Eval(Mobile_Object_t)
	
	MaxLoop = 20
	do
		existObj = objTestObject.Exist(1)
		If existObj Then
'			objTestObject.highlight
'			MC_HighlightObject(objTestObject)
		End If
		
		MaxLoop = MaxLoop - 1
	Loop Until existObj = False or MaxLoop <= 0 
	
	iRet = True

	set existObj = nothing		
	set objTestObject = nothing
	
	MC_WaitHideObject = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeRight()
'*  Purpose:        Swipe to right position 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeRight(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Const sModule = "MC_SwipeRight"
	Activity = "swipe right"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If lcase(strValueToEnter)="yes" or len(strValueToEnter)= 0 Then
		swipe_round=1
	Else
		swipe_round = strValueToEnter
	End If
	set Main_Object=Eval(Create_OR_Object(UIName,""))	
	direction = "right"		
	s_action = "SwipeAction_"&OSType&"(Main_Object,direction,swipe_round)"
	iRet = Eval(s_action)
		
	If iRet = True Then
		descript="swipe "&direction&" done" 
	Else
		descript=Environment("TS_ErrorDescption")
	End If  	
	set Main_Object = Nothing    
  	MC_SwipeRight = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  									
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeLeft()
'*  Purpose:        Swipe to left position 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeLeft(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Const sModule = "MC_SwipeLeft"
	Activity = "swipe left"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If lcase(strValueToEnter)="yes" or len(strValueToEnter)= 0 Then
		swipe_round=1
	Else
		swipe_round = strValueToEnter
	End If
	set Main_Object=Eval(Create_OR_Object(UIName,""))	
	direction = "left"		
	s_action = "SwipeAction_"&OSType&"(Main_Object,direction,swipe_round)"
	iRet = Eval(s_action)
		
	If iRet = True Then
		descript="swipe "&direction&" done" 
	Else
		descript=Environment("TS_ErrorDescption")
	End If  	
	set Main_Object = Nothing    
  	MC_SwipeLeft = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  									
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_SwipeUp()
'*  Purpose:        Swipe to up position 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeUp(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Const sModule = "MC_SwipeUp"
	Activity = "swipe up"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If lcase(strValueToEnter)="yes" or len(strValueToEnter)= 0 Then
		swipe_round=1
	Else
		swipe_round = strValueToEnter
	End If
	set Main_Object=Eval(Create_OR_Object(UIName,""))	
	direction = "up"		
	s_action = "SwipeAction_"&OSType&"(Main_Object,direction,swipe_round)"
	iRet = Eval(s_action)
	
'	If Main_Object.getroproperty("class") = "WebView" Then
'		iRet = True
'		Environment("TS_ErrorFound") = 0
'		descript="swipe "&direction&" done"
'	Else
		If iRet = True Then
			descript="swipe "&direction&" done" 
		Else
			descript=Environment("TS_ErrorDescption")
		End If  	
'	End If
	
	set Main_Object = Nothing    
  	MC_SwipeUp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  									
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeDown()
'*  Purpose:        Swipe to down position 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeDown(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Const sModule = "MC_SwipeDown"
	Activity = "swipe down"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If lcase(strValueToEnter)="yes" or len(strValueToEnter)= 0 Then
		swipe_round=1
	Else
		swipe_round = strValueToEnter
	End If
	set Main_Object=Eval(Create_OR_Object(UIName,""))	
	direction = "down"		
	s_action = "SwipeAction_"&OSType&"(Main_Object,direction,swipe_round)"
	iRet = Eval(s_action)
		
	If iRet = True Then
		descript="swipe "&direction&" done" 
	Else
		descript=Environment("TS_ErrorDescption")
	End If  	
	set Main_Object = Nothing    
  	MC_SwipeDown = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
  									
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeLeftFindObj()
'*  Purpose:        Swipe to left position until find expect object 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeLeftFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	direction = "left"
	Activity = "swipe "&direction&" until find object"
	Const sModule = "MC_SwipeLeftFindObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	MainObject_t = Create_OR_Object(UIName,"")
	set Main_Object = Eval(MainObject_t)
	
 	ExpectObject_t = Create_OR_Object(ExpectedResult,"")
 	set Mobile_Object = Eval(ExpectObject_t)

 	If lcase(strValueToEnter)<>"yes" Then
 		Mobile_Object.SetToProperty "text",strValueToEnter
 	End If
 	
	For start = 1 To 10 Step 1	
	 	If Mobile_Object.Exist(2) Then
	 		iRet=True
	 		descript ="found expect object ["&ExpectObject_t&"]"
			call logging( "found expect object ["&ExpectObject_t&"]",c_DEBUG,3)
			Exit For
	 	Else
	
  			s_action = "SwipeAction_"&OSType&"(Main_Object,direction,""1"")"
			iRet = Eval(s_action)
  			If iRet = False Then
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object ["&ExpectObject_t&"]"
				call logging(descript,c_DEBUG,3)
			End If
	 	End If
 	Next 
 	
 	set Mobile_Object=Nothing
  	set Main_Object = Nothing
	MC_SwipeLeftFindObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeRightFindObj()
'*  Purpose:        Swipe to right position until find expect object 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeRightFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	direction = "right"
	Activity = "swipe "&direction&" until find object"
	Const sModule = "MC_SwipeRightFindObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	MainObject_t = Create_OR_Object(UIName,"")
	set Main_Object = Eval(MainObject_t)
	
 	ExpectObject_t = Create_OR_Object(ExpectedResult,"")
 	set Mobile_Object = Eval(ExpectObject_t)

 	If lcase(strValueToEnter)<>"yes" Then
 		Mobile_Object.SetToProperty "text",strValueToEnter
 	End If
 	
	For start = 1 To 10 Step 1	
	 	If Mobile_Object.Exist(2) Then
	 		iRet=True
	 		descript ="found expect object ["&ExpectObject_t&"]"
			call logging( "found expect object ["&ExpectObject_t&"]",c_DEBUG,3)
			Exit For
	 	Else
	
  			s_action = "SwipeAction_"&OSType&"(Main_Object,direction,""1"")"
			iRet = Eval(s_action)
  			If iRet = False Then
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object ["&ExpectObject_t&"]"
				call logging(descript,c_DEBUG,3)
			End If
	 	End If
 	Next 
 	
 	set Mobile_Object=Nothing
  	set Main_Object = Nothing
	MC_SwipeRightFindObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeUpFindObj()
'*  Purpose:        Swipe to up position until find expect object 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeUpFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	direction = "up"
	Activity = "swipe "&direction&" until find object"
	Const sModule = "MC_SwipeUpFindObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	MainObject_t = Create_OR_Object(UIName,"")
	set Main_Object = Eval(MainObject_t)
	
 	ExpectObject_t = Create_OR_Object(ExpectedResult,"")
 	set Mobile_Object = Eval(ExpectObject_t)

 	If lcase(strValueToEnter)<>"yes" Then
 		Mobile_Object.SetToProperty "text",strValueToEnter
 	End If
 	
	For start = 1 To 10 Step 1	
	 	If Mobile_Object.Exist(2) Then
	 		iRet=True
	 		descript ="found expect object ["&ExpectObject_t&"]"
			call logging( "found expect object ["&ExpectObject_t&"]",c_DEBUG,3)
			Exit For
	 	Else
	
  			s_action = "SwipeAction_"&OSType&"(Main_Object,direction,""1"")"
			iRet = Eval(s_action)
  			If iRet = False Then
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object ["&ExpectObject_t&"]"
				call logging(descript,c_DEBUG,3)
			End If
	 	End If
 	Next 
 	
 	set Mobile_Object=Nothing
  	set Main_Object = Nothing
	MC_SwipeUpFindObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeDownFindObj()
'*  Purpose:        Swipe to down position until find expect object 
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeDownFindObj(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	direction = "down"
	Activity = "swipe "&direction&" until find object"
	Const sModule = "MC_SwipeDownFindObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	MainObject_t = Create_OR_Object(UIName,"")
	set Main_Object = Eval(MainObject_t)
	
 	ExpectObject_t = Create_OR_Object(ExpectedResult,"")
 	set Mobile_Object = Eval(ExpectObject_t)

 	If lcase(strValueToEnter)<>"yes" Then
 		Mobile_Object.SetToProperty "text",strValueToEnter
 	End If
 	
	For start = 1 To 10 Step 1	
	 	If Mobile_Object.Exist(2) Then
	 		iRet=True
	 		descript ="found expect object ["&ExpectObject_t&"]"
			call logging( "found expect object ["&ExpectObject_t&"]",c_DEBUG,3)
			Exit For
	 	Else
	
  			s_action = "SwipeAction_"&OSType&"(Main_Object,direction,""1"")"
			iRet = Eval(s_action)
  			If iRet = False Then
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object ["&ExpectObject_t&"]"
				call logging(descript,c_DEBUG,3)
			End If
	 	End If
 	Next 
 	
 	set Mobile_Object=Nothing
  	set Main_Object = Nothing
	MC_SwipeDownFindObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SwipeUpRelateObj()
'*  Purpose:        Swipe to down position relate object positon
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwipeUpRelateObj(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "swipe up relate object"
	Const sModule = "MC_SwipeUpRelateObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Mobile_Object_t = Create_OR_Object(UIName,"")
	set Main_Object=Eval(Mobile_Object_t)   	
	iRet = SwipeAction_ANDROID(Main_Object,"panup"&strValueToEnter,1)
	If iRet = True  Then
		descript ="swipe from object <b>[" &Mobile_Object_t&"]</b> to <b>["&strValueToEnter&"]</b> pixel"
		call logging("swipe from object [" &Mobile_Object_t&"] to ["&strValueToEnter&"] pixel",c_DEBUG,3)
	Else
	  	descript=Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)	  	
	End  If
	Set Main_Object = Nothing
	MC_SwipeUpRelateObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ChooseQuickAccount()
'*  Purpose:        Choose account on quick page (prelogin)
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseQuickAccount(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	Activity ="select quick account"
	Const sModule = "MC_ChooseQuickAccount"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	strValueToEnter = ".*"&strValueToEnter&".*"
'	strChildobject = replace(strChildobject,UIName,"text:="&strValueToEnter&""",""Index:=1")
	call logging("Object " & strParentObject&"."&strChildobject,c_DEBUG,3)		
	set objTestObject=Eval(Create_OR_Object(UIName,strValueToEnter))
	
	set mainObject=Eval(Create_OR_Object("AccSum_mainobject_obj",""))
	
	For Iterator = 1 To 30 Step 1
		If objTestObject.Exist(5) Then
'			objTestObject.highlight
'			MC_HighlightObject(objTestObject)
			descript ="Found account " & strValueToEnter
			call logging("Found account " & strValueToEnter,c_DEBUG,3)
			iRet = True
			objTestObject.Tap		
			Exit For	
		Else
			iRet = False
			descript ="error cannot Found account"
			mainObject.swipe "up"
		End If
	Next
	
	MC_ChooseQuickAccount = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
	
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_ChooseAddAccount()
'*  Purpose:        Choose account on add account page
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseAddAccount(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
Activity ="choose add account"
	Const sModule = "MC_ChooseAddAccount"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
'  	strValueToEnter = ".*"&strValueToEnter&".*"
  	strChildobject  = objDictChild.Item(UIName)
	strParentObject = objDictParent.Item(UIName)
	
'	Set WaitPage = Eval(strParentObject)
'	WaitPage.Sync
	'regular expression 
	strChildobject = replace(strChildobject,UIName,"innertext:="&strValueToEnter&""",""Index:=1")
	call logging("Object " & strParentObject&"."&strChildobject,c_DEBUG,3)

	set objTestObject=Eval(strParentObject&"."&strChildobject)	
	
	If objTestObject.Exist(5) Then
'		objTestObject.highlight
'		MC_HighlightObject(objTestObject)
		objTestObject.Click
		iRet = True
		descript ="choose account complete"
	Else
		iRet = False
		descript ="error cannot find object"
	End If
	
	MC_ChooseAddAccount = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_CheckLanguage()
'*  Purpose:        Check language on label 
'*  Arguments:      strValueToEnter,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckLanguage(strValueToEnter,UIName,ExpectedResult,OSType)
	Dim GetText,TextCompare
	Activity = "check language"
	Const sModule = "MC_CheckLanguage"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
		
	Set objRE = New RegExp		
		With objRE
		.Pattern    = "^[a-zA-Z]+[^ก-๙]$"
		.IgnoreCase = True
		.Global     = False
	End With
			
	' Test method returns TRUE if a match is found
	GetText = objTestObject.GetROProperty("text")
	iRet = CheckError()
	If iRet=True Then
		TextCompare = Replace(GetText," ","",1,-1)
		If objRE.Test(TextCompare)=True Then	
			strLanguage = "EN"
			descript="["&GetText&"] is English language ,Set Environment(""AppLanguage"") to ["&strLanguage&"]"			
		Else
			strLanguage = "TH"
			descript="["&GetText&"] is Thai language ,Set Environment(""AppLanguage"") to ["&strLanguage&"]"										
		End If
		Environment("AppLanguage") = strLanguage
	Else
		iRet = False
		descript=Environment("TS_ErrorDescption")
	End If
	
	call logging(descript,c_DEBUG,3)
	set objTestObject = nothing
  	MC_CheckLanguage =  StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult) 	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CloseAllApp()
'*  Purpose:        Close application
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CloseAllApp(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)

	Const sModule = "MC_CloseAllApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	Activity = "close all app" 
  	
  	set objTestObject=Eval(Create_OR_Object(UIName,""))
  	
  	Device("Device").RecentApps
  	If objTestObject.Exist Then
  		objTestObject.Tap
  		iRet = True
  		descipt ="Close Complete"
  	Else
  		iRet = False
  		descipt ="Error Not Found Object"
  	End If
	
	set objTestObject = nothing
  	MC_CloseAllApp = StampStatusToReport (iRet,Activity,descipt,UIName,ExpectedResult)
  	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetOTP()
'*  Purpose:        Get otp and store value in Environment("Activity_Result") sitehttp://bastet.se.scb.co.th
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_GetOTP(strValueToEnter)
	On Error Resume Next
	
	Activity = "get OTP from chrome" 
	Const sModule = "MC_GetOTP_Chrome"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	inputval=split(strValueToEnter,";")
	MobileNo=inputval(0)
	UserVal=inputval(1)
	If Err.Number = 0 Then
		      IsNotErr = True
	Else
		      IsNotErr = False
		      ErrorDescpt = Err.Description
	End If
		 
	If IsNotErr=True Then
	
		Dim WebURL : WebURL = Environment("SMS_URL")
		set objShell = CreateObject("Shell.Application")
		
		
		SystemUtil.CloseProcessByName "chrome.exe"
		wait 1		
		
		objShell.ShellExecute "chrome.exe", WebURL, "", "", 3
		
		username = Environment("SMS_USERNAME")
		password = Environment("SMS_PASSWORD")
		domain =Environment("SMS_DOMAIN")
		
		Wait 2
		
		Set a = createobject("wscript.shell")
		a.SendKeys "{F5}"
		
'		Browser("SMSLogView").Page("LoginPage").Sync
		call logging("Sync Browser ",c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebEdit("loginname").highlight
		Browser("SMSLogView").Page("LoginPage").WebEdit("loginname").Set username
		call logging("Set uername",c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebEdit("password").Set password
		call logging("Set password",c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebList("domain").Select domain
		call logging("Select domain",c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebButton("LoginBtn").Click
		wait(0.1)
		
		Browser("SMSLogView").Page("SMSPage").Sync
		call logging("Login Completed",c_DEBUG,3)
		Browser("SMSLogView").Page("SMSPage").WebEdit("mobile").Set MobileNo
		Browser("SMSLogView").Page("SMSPage").WebButton("SearchBtn").Click
		ReqVal = Browser("SMSLogView").Page("SMSPage").WebElement("DetailLabel").GetROProperty("innertext")
		PhoneNo = Browser("SMSLogView").Page("SMSPage").WebElement("PhoneLabel").GetROProperty("innertext")
		ReqType = Browser("SMSLogView").Page("SMSPage").WebElement("TypeLabel").GetROProperty("innertext")
		call logging("Get InnerText : "&ReqVal,c_DEBUG,3)
		reqstr=split(ReqVal,"<OTP")
		reqstr2=trim(reqstr(1))
		reqstr3=split(reqstr2,">")
		reqOTP=trim(reqstr3(0))
		call logging("reqOTP : "& reqOTP,c_DEBUG,3)
		call logging("PhoneNo : "& PhoneNo,c_DEBUG,3)
		call logging("ReqType : "& ReqType,c_DEBUG,3)
		
		Browser("SMSLogView").Page("SMSPage").WebButton("User").Click
		Browser("SMSLogView").Page("SMSPage").Link("Logout").Click
		Browser("SMSLogView").CloseAllTabs
		
		set objShell = Nothing
		Environment(UserVal) = reqOTP&","&PhoneNo&","&ReqType
		
		Environment("PhoneNo") = PhoneNo
		Environment("Contents") = ReqVal		
		Environment("Product") = ReqType
		
		If instr(1, Environment("Contents"), "<OTP: ") > 0 Then
			beforeotp = "<OTP: "
			afterotp = ">"			
		ElseIf instr(1, Environment("Contents"), "OTP ") > 0 Then 
			beforeotp = "OTP "
			afterotp = ">"	
		ElseIf instr(1, Environment("Contents"), "OTP=") Then
			beforeotp = "OTP="
			afterotp = " "
		End If 
		
		reqstr=split(ReqVal,beforeotp)
'		reqstr2=trim(reqstr(1))
		reqstr3=split(reqstr(1),afterotp)
		reqOTP=trim(reqstr3(0))
		Environment("OTP") = reqOTP
		
		call logging("Get OTP in Environment("""&UserVal&""") = "&reqOTP,c_DEBUG,3)
	Else
		call logging(ErrorDescpt,c_DEBUG,3)
	End  If
	  	
	iRet = CheckError()
	MC_GetOTP = 1

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CompareOTPDetail(PhoneNo, Contents, Product)
'*  Purpose:        Compare data in getOTP screen (PhoneNo, Contents, Product) sitehttp://bastet.se.scb.co.th
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CompareOTPDetail(strValueToEnter)

	Activity = "Compare data in getOTP screen" 
	Const sModule = "MC_CompareOTPDetail"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	inputval=split(strValueToEnter,";")
	PhoneNo  = inputval(0)
	Contents = inputval(1)
	Product  = inputval(2)
	
	
		If instr(1, Environment("Contents"), "<OTP: ") > 0 Then
			beforeotp = "<OTP: "
			afterotp = ">"			
		ElseIf instr(1, Environment("Contents"), "OTP ") > 0 Then 
			beforeotp = "OTP "
			afterotp = ">"	
		ElseIf instr(1, Environment("Contents"), "OTP=") Then
			beforeotp = "OTP="
			afterotp = " "
		End If 
		
		reqstr=split(ReqVal,beforeotp)
'		reqstr2=trim(reqstr(1))
		reqstr3=split(reqstr(1),afterotp)
		reqOTP=trim(reqstr3(0))
	
	If Err.Number = 0 Then
		      IsNotErr = True
	Else
		      IsNotErr = False
		      ErrorDescpt = Err.Description
	End If
	iret = 0
		 
	If IsNotErr=True Then
		If cstr(PhoneNo) = cstr(Environment("PhoneNo")) Then
			call logging("Compare excel data [" &PhoneNo& "] and on web data [" &Environment("PhoneNo")& "] is true" ,c_DEBUG,3)
		Else
			call logging("Compare excel data [" &PhoneNo& "] and on web data [" &Environment("PhoneNo")& "] is false" ,c_DEBUG,3)
			iret = iret + 1
		End If
		
		If instr(1,Environment("Contents"), Contents) > 0 Then
			call logging("Compare excel data : [" &Contents& "] is substring in on web data [" &Environment("Contents")& "] is true" ,c_DEBUG,3)
		Else 
			call logging("Compare excel data : [" &Contents& "] is substring in on web data [" &Environment("Contents")& "] is false" ,c_DEBUG,3)
			iret = iret + 1
		End If
		
		If cstr(Product) = cstr(Environment("Product")) Then
			call logging("Compare excel data [" &Product& "] and on web data [" &Environment("Product")& "] is true" ,c_DEBUG,3)
		Else
			call logging("Compare excel data [" &Product& "] and on web data [" &Environment("Product")& "] is false" ,c_DEBUG,3)
			iret = iret + 1
		End If
		
		If iret = 0 Then
			descipt = "Compare excel data and on web data is true"
			iret = true
		Else 
			iret = false
			descipt = "Compare excel data and on web data is false"
		End If	
	Else
		call logging(ErrorDescpt,c_DEBUG,3)
	End  If
		
  	MC_CompareOTPDetail = StampStatusToReport (iRet,Activity,descipt,UIName,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetOTP_IE()
'*  Purpose:        Get otp and store value in Environment("Activity_Result") sitehttp://bastet.se.scb.co.th
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_GetOTP_IE(strValueToEnter)

	Activity = "get OTP" 
	Const sModule = "MC_GetOTP_IE"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	
	'This function require setup trust on IE Browser
	'require trust sitehttp://bastet.se.scb.co.th
	inputval=split(strValueToEnter,";")
	MobileNo=inputval(0)
	UserVal=inputval(1)
	If Err.Number = 0 Then
		      IsNotErr = True
	Else
		      IsNotErr = False
		      ErrorDescpt = Err.Description
	End If
		 
	If IsNotErr=True Then
	
		SystemUtil.CloseProcessByName ("iexplore.exe") 
	  	Dim WebURL : WebURL = Environment("SMS_URL")
		Set IE = CreateObject("InternetExplorer.Application")
		IE.Visible = True	
		IE.Navigate WebURL
		
		call logging("Open IE : " & WebURL,c_DEBUG,3)
	
		'Loop defect when IE's busy
		While IE.Busy
			'print "IE's busy...."
		Wend
		
		'Adjust screen
		'Maximize the Browser Window
		'Window("hwnd:=" & IE.HWND).Maximize
		 
		'Minimize the Browser Window
		'Wait(1)
		'Window("hwnd:=" & IE.HWND).Minimize
		
	
		'Loop for defect when found certification on web OTP
		Do
			If Browser("name:=Certificate Error: Navigation Blocked").Exist then
				Browser("name:=Certificate Error: Navigation Blocked").Page("title:=Certificate Error: Navigation Blocked").Link("name:=Continue to this website.*").Click
				Exit Do
			Elseif Browser("SMSLogView").Exist then
				Exit Do
		    Else
				wait(0.5)
			End If
		Loop
		username = Environment("SMS_USERNAME")
		password = Environment("SMS_PASSWORD")
		domain =Environment("SMS_DOMAIN")
		
		Browser("SMSLogView").Page("LoginPage").Sync
		call logging("Sync Page",c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebEdit("loginname").Set username
		call logging("input username  " & username,c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebEdit("password").Set password
		call logging("input password " & password,c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebList("domain").Select domain
		call logging("select domain  " & domain,c_DEBUG,3)
		Browser("SMSLogView").Page("LoginPage").WebButton("LoginBtn").Click
		call logging("Login Web OTP",c_DEBUG,3)
		wait(0.1)
		
		Browser("SMSLogView").Page("SMSPage").Sync
		Browser("SMSLogView").Page("SMSPage").WebEdit("mobile").Set MobileNo
		call logging("Input Mobile Number "&MobileNo,c_DEBUG,3)
		Browser("SMSLogView").Page("SMSPage").WebButton("SearchBtn").Click
		reqval = Browser("SMSLogView").Page("SMSPage").WebElement("DetailLabel").GetROProperty("innertext")
	
		call logging("Get InnerText : "&reqval,c_DEBUG,3)
		reqstr=split(reqval,"<OTP")
		reqstr2=trim(reqstr(1))
		reqstr3=split(reqstr2,">")
		reqOTP=trim(reqstr3(0))
		call logging("reqOTP : "& reqOTP,c_DEBUG,3)
		
		Browser("SMSLogView").Page("SMSPage").WebButton("User").Click
		Browser("SMSLogView").Page("SMSPage").Link("Logout").Click
	
		
		'Comment by Suebsakul.A 2017-04-06
		'Fixed process IE still not quit
		'>>>> Start Statement
		'IE.Stop
		'IE.Quit
		
		IE.Stop()
		IE.Quit()
		'>>>> End Statement
		
		call logging("Close IE",c_DEBUG,3)
		set IE = nothing 		
		Environment(UserVal) = reqOTP
		call logging("Get OTP in Environment("""&UserVal&""") = "&reqOTP,c_DEBUG,3)
	Else
		call logging(ErrorDescpt,c_DEBUG,3)
	End  If
	
	MC_GetOTP_IE = 1

End Function

'**********************************************************************************************************************************
'*  Function Name : MC_SetOTP()
'*  Purpose:        set otp on check otp page
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetOTP(strValueToEnter)
Dim MyOTP ,UIName ,Action

	Activity = "set OTP"
	Const sModule = "MC_SetOTP"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	SplitVar=split(strValueToEnter,";")
  	Content = split(Eval(SplitVar(0)),",")
  	GetContent = SplitVar(1)
  	If lcase(GetContent) ="otp" Then
  		MyOTP = Content(0)
  	ElseIf lcase(GetContent) ="mobile" Then
  		MobileNo = Content(1)
  	ElseIf lcase(GetContent) ="product" Then
  		Product = Content(2)
  	End If
  	
'	If Left(SplitVar(0),11) = "Environment" Then
'		MyOTP = Eval(strValueToEnter) 
'	Else
'		MyOTP = strValueToEnter		    
'    End if	
    
	call logging("MyOTP : "& MyOTP,c_DEBUG,3)    
		
	If len(MyOTP)<=6 and IsNumeric(MyOTP) Then
	  	For Iterator = 1 To len(MyOTP) Step 1
	
			UIName = "ForgotOTPNumber"&Mid(MyOTP,Iterator,1)		  		
			set objTestObject=Eval(Create_OR_Object(UIName,""))
			If Iterator=1 Then		
				If InStr(strParentObject,".Page(") > 0 Then
						Action = 0					
				Else
						Action = 1
				End if
			End If	
		
			If Action = 0 Then
					objTestObject.Click
			Else
					objTestObject.Tap
			End if			
			
			call logging("Click UIName : " & UIName,c_DEBUG,3)
			set objTestObject= nothing
		Next
		iRet = True
		descript="success to set OTP "&MyOTP
	Else
		iRet = False
		descript=MyOTP&" is invalid OTP"
	End If	
	MC_SetOTP = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)		
			
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_EnterKey()
'*  Purpose:        Enter key on object
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_EnterKey(strValueToEnter)

	Activity = "enter key on object" 
	Const sModule = "MC_EnterKey"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	splitvar=split(strValueToEnter,";")
	InputValue=splitvar(1)
	If lcase(splitvar(0))="value" Then
		Device("Device").Enterkeys typeValue,InputValue
		call logging("enterkeys "&InputValue&" done",c_DEBUG,3)
		descript="enterkeys "&InputValue&" done"
		iRet = True
	ElseIf lcase(splitvar(0))="key" Then
		Device("Device").Enterkeys typeKey,InputValue
		call logging("enterkeys "&InputValue&" done",c_DEBUG,3)
		descript="enterkeys "&InputValue&" done"
		iRet = True
	Else
		call logging("input value is invalid",c_DEBUG,3)
		descript="input value is invalid"
		iRet = False
	End If
	
	If Environment("functionName")="MC_EnterKey" Then
		MC_EnterKey = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	End If 
	
	If iRet = True Then
		MC_EnterKey = 1
	Else
		MC_EnterKey = -1
	End If
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_Back()
'*  Purpose:        Back to previous page
'*  Arguments:      -
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Back()

	Const sModule = "MC_Back"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	Device("Device").Back
	MC_Back=1
	
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_ClickOnText()
'*  Purpose:        Click coordinate position with relate on text
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		SUEBSAKUL A. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ClickOnText(strValueToEnter)
	On Error Resume Next
	Activity = "click on text"
	Const sModule = "MC_ClickOnText"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	If instr(strValueToEnter,";") > 0 Then
	  	SplitVar=split(strValueToEnter,";")
	  	TextToFind = SplitVar(0)
	  	
	  	If SplitVar(1)<>"" or IsNull(SplitVar(1)) Then
	  		index = SplitVar(1)
	  	End If 	
	  	If SplitVar(2)<>"" or IsNull(SplitVar(2)) Then
	  		CoX = SplitVar(2)
	  	End If 	
	  	If SplitVar(3)<>"" or IsNull(SplitVar(3)) Then
	  		CoY = SplitVar(3)
	  	End If 	
		  	
		call logging("ClickOnText : "&TextToFind&"/ index : "&index&" /CoX :"&CoX&" /CoY: "&CoY,c_DEBUG,3) 	
		Device("Device").ClickOnText TextToFind,index,CoX,CoY

  	Else
  		TextToFind = strValueToEnter
  	  	Device("Device").ClickOnText TextToFind
  	End If
  	
  	iRet = CheckError()
	If iRet = True Then
		call logging("ClickOnText : "&TextToFind,c_DEBUG,3)
  	Else		
	  	descript = Environment("TS_ErrorDescption")
	  	call logging(descript,c_DEBUG,3) 
	End If
  	MC_ClickOnText = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_Find_AccountNO_ANDROID()
'*  Purpose:        Swipe to right position until find expect accountID or nickname on my deposits section
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Find_AccountNO_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "swipe left until find AccountNO"
	Const sModule = "MC_Find_AccountNO"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set Main_Object=Eval(Create_OR_Object("AccSum_maindepview_obj",""))
	Obj_right = Main_Object.GetRoproperty("right")
'	Obj_left = Main_Object.GetRoproperty("left")
'	Obj_top = Main_Object.GetRoproperty("top")
	
	Start_X = Obj_right-100
	Start_Y = 100
	End_X = 10
	End_Y = Start_Y
	x_velocity = -4086.855000
	y_velocity = 10.000730
				
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	    
	For Iterator = 1 To 30 Step 1

		AccountLabel = objTestObject.GetROProperty("text")
		call logging("Input Value " & strValueToEnter,c_DEBUG,3)
		call logging("AccountLabel " & AccountLabel,c_DEBUG,3)		
		iRet = Instr(AccountLabel,strValueToEnter)
		
		If iRet>0 Then
			iRet = True
			descript ="Found account label <b>" &strValueToEnter &"</b>"
			call logging( "Found account label : " &strValueToEnter,c_DEBUG,3)
			Exit for
		Else
			Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity
			iRet = False			
			descript ="not found expect object"
			call logging(descript,c_DEBUG,3)
			objTestObject.RefreshObject
			
		End If
	Next  	

	Flag_Err = CheckError()
	If Flag_Err = False Then
		iRet = False	
		descript = Environment("TS_ErrorDescption")
	End If	
	set objTestObject= Nothing
	set Main_Object= Nothing		
	MC_Find_AccountNO_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_Find_CreditCardNO()
'*  Purpose:        swipe to right position until find expect creditcard number on my cards section
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Find_CreditCardNO(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
Dim CreditCardNo
	Activity = "swipe left until find credit card number"
	Const sModule = "MC_Find_CreditCard"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	'Swipe up	
	AccSum_MainObject="AccSum_mainobject_obj"
	set accjMainObject=Eval(Create_OR_Object(AccSum_MainObject,""))
	
	'For Swipe Up	
	Obj_top = accjMainObject.GetROProperty("abs_y")		
	Obj_right = accjMainObject.GetROProperty("right")
		
	'For Swipe Up
	Start_X = Obj_right/2
	Start_Y = Obj_top+20
	End_X = Start_X	
	End_Y = Start_Y-200
			
	call logging("AccMain Start_X " & Start_X,c_DEBUG,3)
	call logging("AccMain Start_Y " & Start_Y,c_DEBUG,3)
	call logging("AccMain End_X " & End_X,c_DEBUG,3)
	call logging("AccMain End_Y " & End_Y,c_DEBUG,3)
	
	' Swipe Up	
	accjMainObject.Pan Start_X,Start_Y,End_X,End_Y,0.208741,0.208741
	
	AccSum_maincard="AccSum_maincardview_obj"
	set CardMainObject=Eval(Create_OR_Object(AccSum_maincard,""))
		
	'For Swipe left	
	ObjMainC_right = CardMainObject.GetROProperty("right")
	
	Start_Y = 100
	Start_X = ObjMainC_right-100
	End_X = ObjMainC_right-(ObjMainC_right-100)
	End_Y = Start_Y

	call logging("CCMain Start_X " & Start_X,c_DEBUG,3)
	call logging("CCMain Start_Y " & Start_Y,c_DEBUG,3)
	call logging("CCMain End_X " & End_X,c_DEBUG,3)
	call logging("CCMain End_Y " & End_Y,c_DEBUG,3)
	
	For Iterator = 1 To 30 Step 1
		CreditCardNo = objTestObject.GetROProperty("text")
		call logging("Input Value " & strValueToEnter,c_DEBUG,3)	
		call logging("CreditCardNo " & CreditCardNo,c_DEBUG,3)					
		iRet = Instr(CreditCardNo,strValueToEnter) 
		
		If iRet>0 Then
			iRet = True
			descript ="Found expect object" &strParentObject&"."&strChildobject
			call logging( "Found expect object : " &strValueToEnter,c_DEBUG,3)
			Exit for
		Else
			iRet = False
			CardMainObject.Pan Start_X,Start_Y,End_X,End_Y,0.208741,0.208741
			descript ="not found object"
			call logging( "not found object",c_DEBUG,3)
		End If
	Next
	
	set objTestObject= Nothing
	set objMainObject= Nothing
	MC_Find_CreditCardNO = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_Find_LoansNO()
'*  Purpose:        swipe to right position until find expect loan number on my loans section
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:    Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Find_LoansNO(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)

	Activity = "swipe left until find loans number"
	Const sModule = "MC_Find_LoansNO"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set objTestObject=Eval(Create_OR_Object(UIName,""))
  	set AccMain_Object=Eval(Create_OR_Object("AccSum_maindepview_obj","")) 
  	set objMainObject=Eval(Create_OR_Object("AccSum_mainloanview_obj",""))
  	 
  	For scrollup = 1 To 5 Step 1
  		If objMainObject.Exist(4) Then	
'			MC_HighlightObject(objMainObject)
  			objMainObject.Tap
  			If objMainObject.Exist(5) = False Then			
  				Exit For
  			Else
  				objMainObject.Tap
  				Exit For
  			End If
  		Else
			Flag_Err = SwipeAction_ANDROID(AccMain_Object,"up",1)  
			If Flag_Err = False Then
				iRet = Flag_Err
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object"
				call logging(descript,c_DEBUG,3)
			End If			
  		End If	
	Next
		 
	set objBackObject=Eval(Create_OR_Object("AccLoan_back_btn",""))
	For Iterator = 1 To 30 Step 1
		call logging("Input Value " & strValueToEnter,c_DEBUG,3)
		LoansNO = objTestObject.GetROProperty("text")
		call logging("LoansNO " & LoansNO,c_DEBUG,3)					
		iRet = Instr(LoansNO,strValueToEnter) 
		
		If iRet>0 Then
'			objTestObject.highlight	
'			MC_HighlightObject(objTestObject)
			iRet = True
			descript ="Found expect object" &strParentObject&"."&strChildobject
			call logging( "Found expect object : " &strValueToEnter,c_DEBUG,3)
			Exit for
		Else
			iRet = False
			descript ="not found object"
			call logging( "not found object",c_DEBUG,3)
			objBackObject.Tap
			Flag_Err = SwipeAction_ANDROID(objMainObject,"left",1)
			If Flag_Err = False Then
				iRet = Flag_Err 
				descript =Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object"
				call logging(descript,c_DEBUG,3)
			End If	
			objMainObject.Tap
		End If
	Next
	
	set objTestObject= Nothing
	set AccMain_Object= Nothing
	set objMainObject= Nothing
	set objBackObject= Nothing

	MC_Find_LoansNO = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_RefreshSummaryPage()
'*  Purpose:        swipe to top position for refresh page on account summary page
'*  Arguments:      OSType,strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_RefreshSummaryPage(OSType,strValueToEnter)
	On Error Resume Next
	Activity="refresh summary page"
	Const sModule = "MC_RefreshSummaryPage"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If OSType="IOS" Then
		TopUIName="AccSum_dep_txt"
	Else
		TopUIName="AccSum_maintotalamount_data"	
	End If
	set TopObject=Eval(Create_OR_Object(TopUIName,""))	
	
	mainUIName="AccSum_mainobject_obj"
	set objMainObject=Eval(Create_OR_Object(mainUIName,""))
	
	For Iterator = 1 To 5 Step 1
		Flag = CheckObjectExist(TopObject)
		If Flag = 1 Then
			objMainObject.Swipe "down"
			descript="refresh summary page done"
			call logging(descript,c_DEBUG,3)
			iRet = True
			Exit For
		Else
			
			Flag_Err = Eval("SwipeAction_"&OSType&"(objMainObject,""down"",1)")		
			If Flag_Err = False Then
				iRet = Flag_Err 
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			End If				
'			If OSType="IOS" Then
''				objMainObject.ScrollOnePage "down"
'				
'			Else
'				Flag_Err = SwipeAction_ANDROID(objMainObject,"down",1)
''				objMainObject.Swipe "down"
'			End If
		End If
	Next
'	iRet = CheckError()
'	If iRet = True Then
'			
'	Else
'	  	descript = Environment("TS_ErrorDescption")
'	End If	  	
	wait 2
	set TopObject= Nothing
	set objMainObject = Nothing
	RefreshSummaryPage = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
											
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_CompareNumber()
'*  Purpose:        compare number in excel data and label object with positive ,negative ,less , more or same number
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CompareNumber(strValueToEnter,UIName,ExpectedResult,OSType)
On Error Resume Next
	Dim ROPropertyNumber
	Activity="compare positive ,negative , zero number"
	Const sModule = "MC_CompareNumber"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	set objTestObject=Eval(Create_OR_Object(UIName,""))
	If OSType="ANDROID" Then
		get_mcindex = MapUIName_ANDROID(UIName)	
	Else
		get_mcindex = MapUIName_IOS(UIName)
	End If
		 	
	If  get_mcindex <>"::N/A" Then
		objTestObject.SetTOProperty "mcindex",get_mcindex
		call logging("Set Mc_Index ["&get_mcindex&"]" ,c_DEBUG,3) 				
	End If	
	
	Flag = CheckObjectExist(objTestObject)
	
	If Flag=1 Then
		ROPropertyNumber=FormatNumber(cdbl(objTestObject.GetROProperty("text")),,,,0)
		call logging("ROPropertyNumber " & ROPropertyNumber,c_DEBUG,3)
		If lcase(strValueToEnter) = "positive" Then		
			If ROPropertyNumber>0 Then
				iRet = True
				descript="App Value ["&ROPropertyNumber&"] is positive number"
			Else
				iRet = False
				descript="App Value ["&ROPropertyNumber&"] is not " &strValueToEnter &" number"
			End If
		ElseIf lcase(strValueToEnter)="negative" Then
			If ROPropertyNumber<0 Then
				iRet = True
				descript="App Value ["&ROPropertyNumber&"] is negative number"
			Else
				iRet = False
				descript="App Value ["&ROPropertyNumber&"] is not " &strValueToEnter &" number"
			End If
		ElseIf InStr(1,strValueToEnter, "less",vbTextCompare) > 0 or InStr(1,strValueToEnter, "more",vbTextCompare) > 0 Then
		
			If InStr(1,strValueToEnter, "Environment",vbTextCompare) > 0 Then
				splitvar=split(strValueToEnter," ")
				strValueEnvironment = Eval(splitvar(1))
				EnviFlag=1
			End If
			Set regCompare = New RegExp
			regCompare.IgnoreCase =True
		 	regCompare.Pattern    = "(?=.)([+-]?(0|([1-9](\d*|\d{0,2}(,\d{3})*)))?(\.([0-9]+))?)"
		    regCompare.Global = True
		   		If EnviFlag=1 Then
		   			Set matches = regCompare.Execute(strValueEnvironment)
		   		Else
		        	Set matches = regCompare.Execute(strValueToEnter)
		        End If
		        If matches.count > 0 Then
			        For Each getMatchStr In matches
			        	newstr = newstr  & getMatchStr.Value
			        Next
			        newstr=FormatNumber(cdbl(newstr),,,,0)
			        comparenumber=FormatNumber(ROPropertyNumber,,,,0)
			        call logging("input value : " &newstr,c_DEBUG,3)
			        call logging("compare value : " &comparenumber,c_DEBUG,3)
			         
			       	If InStr(1,strValueToEnter, "less",vbTextCompare) > 0  Then
				        If cdbl(newstr) > cdbl(comparenumber) Then
				        	iRet = True
				        	descript=comparenumber& " less than " &newstr
				        	call logging(comparenumber& " less than " &newstr,c_DEBUG,3)
				        Else
				        	iRet = False
				        	descript="fail, <b>" &comparenumber& "</b> <font color=""red""> more than </font> <b>" &newstr&"</b>"
				        	call logging("fail, " &comparenumber& "more than " &newstr,c_DEBUG,3)
				        End If
				     Else
				     	If cdbl(newstr) < cdbl(comparenumber) Then
				        	iRet = True
				        	descript=comparenumber& " more than " &newstr
				        	call logging(comparenumber& " more than " &newstr,c_DEBUG,3)
				        Else
				        	iRet = False
				        	descript="fail, <b>" &comparenumber& "</b> <font color=""red"">less than</font> <b>" &newstr&"</b>"
				        	call logging("fail, " &comparenumber& "less than " &newstr,c_DEBUG,3)
				        End If  	
			         End If
			        Set regCompare = Nothing
			    Else
			    	iRet = False
			    	descript="invalid input data"		    	
			    End If
		Else
			If ROPropertyNumber=FormatNumber(cdbl(strValueToEnter),,,,0) Then
				iRet = True
				descript ="Excel Data : <b>["&strValueToEnter&"]</b> match app label : <b>["&ROPropertyNumber&"]</b>"
			Else
				iRet = False
				descript ="Excel Data : <b>["&strValueToEnter&"]</b> <font color=""red"">not match </font> app label : <b>["&ROPropertyNumber&"]</b>"
			End If
		End If
	Else
		iRet = False
		descript="Not found Object"	
	End If
	
	set objTestObject= nothing
	call logging(descript,c_DEBUG,3)
	MC_CompareNumber = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseAccountTransferFrom_ANDROID()
'*  Purpose:        Choose account transfer from in transfer , topup and bill payment page 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseAccountTransferFrom_ANDROID(strValueToEnter,Device_ID)
	On Error Resume Next
	Dim AccFromNO,ExpectedResult
	Activity="choose transfer from account"
	Const sModule = "MC_ChooseAccountTranferFrom"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	wait 2
  	
  	If strValueToEnter<>"" or len(strValueToEnter)>0 Then
  		
  		UIName="Transfer_FromName"
  		set objTestObject=Eval(Create_OR_Object(UIName,""))
  		objTestObject.SetTOProperty "text",strValueToEnter
  		call logging("Account Input Value " & strValueToEnter,c_DEBUG,3) 				  		
'		Environment("swipe_round") = 0
  		MaximumRound = 30
  		
  		set main_object=Eval(Create_OR_Object("Transfer_FromMain_Obj",""))
  		c_top = main_object.getROProperty("top")
  		c_bottom = main_object.getROProperty("bottom")
  		
  		co_x = main_object.getROProperty("right") - 10
  		co_y = c_top + ((c_bottom - c_top) /2)
  		
  		call logging("co_x " & co_x,c_DEBUG,3)
  		call logging("co_y " & co_y,c_DEBUG,3)
  		
  		SDKPath=Environment("RootProject") & "\SDK-tools"
  		Set oShell = CreateObject ("WSCript.shell")
		Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell input tap "&co_x&" "&co_y&" & Exit"
			
  		For i = 1 To MaximumRound Step 1
  			Environment("swipe_round") = i
  			If objTestObject.Exist(3) Then
	  			objTestObject.Tap
	  			iRet=True
'				MC_HighlightObject(objTestObject)
	  			descript="Choose Account <b>["&strValueToEnter&"]</b>"
	  			call logging("Choose Account "&strValueToEnter,c_DEBUG,3)
	  			If i=1 Then
	  				Environment("swipe_round") = 1
	  			Else
	  				Environment("swipe_round") = Environment("swipe_round") +1
	  			End If
	  			
	  			Exit for
			Else
				oShell.Run Command,0,False
				call logging("not found account",c_DEBUG,3)
  				descript ="not found account"
				iRet=False				
  			End If
  		Next
  		Set oShell = main_object
  		Set oShell = Nothing
  		set objTestObject = Nothing
  	Else
  		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "invalid input data" 
  		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)
  	End If
	
  	MC_ChooseAccountTransferFrom_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseClientInvestmentFrom_ANDROID()
'*  Purpose:        Choose client no. in purchase investment page 
'*  Arguments:      strValueToEnter,Device_ID
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseClientInvestmentFrom_ANDROID(strValueToEnter,Device_ID)
	Activity="choose client no. from investment"
	Const sModule = "MC_ChooseClientInvestmentFrom"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	iRet=False	
  	
  	If strValueToEnter<>"" or len(strValueToEnter)>0 Then
  		
  		UIName="Invest_ClientNo_Txt"
  		set objTestObject=Eval(Create_OR_Object(UIName,""))
  		objTestObject.SetTOProperty "text",strValueToEnter
  		call logging("ClientNo. Input Value " & strValueToEnter,c_DEBUG,3) 

		set Main_Object=Eval(Create_OR_Object("Invest_PurchaseClientMain_Obj",""))
		
		'Initail Value
		descript ="not found client"
		MaximumRound = 20
		Obj_right = Main_Object.GetRoproperty("right")
		Obj_left = Main_Object.GetRoproperty("left")
		Obj_top = Main_Object.GetRoproperty("top")
		Obj_mid = Main_Object.GetRoproperty("right")/2
		
		Start_X = Obj_right-100
		Start_Y = 100
		End_X = Obj_left+10
		End_Y = Start_Y
		x_velocity = -4086.855000
		y_velocity = -117.190100
		
  		
  		For i = 1 To MaximumRound Step 1
  		  	If objTestObject.Exist(3) Then
  		  		left_position = objTestObject.GetRoproperty("left")
  		  		If left_position < Obj_mid Then
  		  			iRet=True
	  				descript="Choose Client <b>["&strValueToEnter&"]</b>"
	  				call logging("Choose Client "&strValueToEnter,c_DEBUG,3)
	  				Exit For
	  			Else
	  				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity
	  				iRet = CheckError()
  		  		End If
			Else
				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity
				iRet = CheckError()	
  			End If
  			
  			If iRet = False Then 'Check error in pan acction
			  	descript = Environment("TS_ErrorDescption")
			  	Exit For
			End If
			
			call logging(descript,c_DEBUG,3)
  		Next
  		
  		Set objTestObject = Nothing
  		set Main_Object = Nothing  		
  		
  	Else
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "invalid input data" 
  		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)
  	End If
  	
	MC_ChooseClientInvestmentFrom_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseAccountInvestmentFrom_ANDROID()
'*  Purpose:        Choose account id in purchase investment page 
'*  Arguments:      strValueToEnter,Device_ID
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseAccountInvestmentFrom_ANDROID(strValueToEnter,Device_ID)
	Activity="choose account from investment"
	Const sModule = "MC_ChooseAccountInvestmentFrom"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
 
  	iRet=False	
  	
  	If strValueToEnter<>"" or len(strValueToEnter)>0 Then
  		
  		UIName="Invest_PurchaseAccId_Txt"
  		set objTestObject=Eval(Create_OR_Object(UIName,""))
  		objTestObject.SetTOProperty "text",strValueToEnter
  		call logging("Account ID Input Value " & strValueToEnter,c_DEBUG,3) 

		set Main_Object=Eval(Create_OR_Object("Invest_PurchaseAccMain_Obj",""))
		
		'Initail Value
		descript ="not found Account"
		MaximumRound = 20
		Obj_right = Main_Object.GetRoproperty("right")
		Obj_left = Main_Object.GetRoproperty("left")
		Obj_top = Main_Object.GetRoproperty("top")
		Obj_mid = Main_Object.GetRoproperty("right")/2
		
		Start_X = Obj_right-100
		Start_Y = 100
		End_X = Obj_left+10
		End_Y = Start_Y
		x_velocity = -4086.855000
		y_velocity = -117.190100
		
  		
  		For i = 1 To MaximumRound Step 1
  		  	If objTestObject.Exist(3) Then
  		  		left_position = objTestObject.GetRoproperty("left")
  		  		If left_position < Obj_mid Then
  		  			iRet=True
	  				descript="Choose Account <b>["&strValueToEnter&"]</b>"
	  				call logging("Choose Account "&strValueToEnter,c_DEBUG,3)
	  				Exit For
	  			Else
	  				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity
	  				iRet = CheckError()
  		  		End If
			Else
				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity
				iRet = CheckError()	
  			End If
  			
  			If iRet = False Then 'Check error in pan acction
			  	descript = Environment("TS_ErrorDescption")
			  	Exit For
			End If
			
			call logging(descript,c_DEBUG,3)
  		Next
  		
  		Set objTestObject = Nothing
  		set Main_Object = Nothing  		
  		
  	Else
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "invalid input data" 
  		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)
  	End If
  	
	MC_ChooseAccountInvestmentFrom_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_CheckAccountNotInList()
'*  Purpose:        Check Account Not In List cardless
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		Thakoon L. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckAccountNotInList_ANDROID(strValueToEnter)

	Activity="Check Account Not In List cardless"
	Const sModule = "MC_CheckAccountNotInList"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	  		
  	If strValueToEnter<>"" or strValueToEnter<>null Then
  		iRet = true
  		Mobile_Object_t = Create_OR_Object("F-NotInList_AccountID_Txt","")
		set objTestObject=Eval(Mobile_Object_t)	
		objTestObject.SetTOProperty "text", strValueToEnter
		objTestObject.RefreshObject
'		set FavHeadObject=Eval(Create_OR_Object("FAV_HeaderFav_Txt",""))

		set main_object=Eval(Create_OR_Object("F-NotInList_FromMain_Obj",""))	
  		c_top = main_object.getROProperty("top")
  		c_bottom = main_object.getROProperty("bottom")
  		
  		co_x = main_object.getROProperty("right") - 10
  		co_y = c_top + ((c_bottom - c_top) /2)
  		
  		call logging("co_x " & co_x,c_DEBUG,3)
  		call logging("co_y " & co_y,c_DEBUG,3)
  		
  		SDKPath=Environment("RootProject") & "\SDK-tools"
  		Set oShell = CreateObject ("WSCript.shell")
		Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell input tap "&co_x&" "&co_y&" & Exit"		

		Set oDesc = description.Create()
		oDesc("class").value="Label"
		
		For i = 1 To 30 Step 1
			Set l =  main_object.ChildObjects(oDesc)
			If objTestObject.Exist(2) Then
				objTest_TopPosition = objTestObject.GetRoproperty("top")
				call logging("objTest_TopPosition : "&objTest_TopPosition,c_DEBUG,3)
				If objTest_TopPosition < c_bottom Then	
					iRet = false
					Exit for
				Else
					oShell.Run Command,0,False
				End If
			Else
				If l.count()>2 Then
					Get_OldAccountID = l(2).GetRoProperty("text")
				Else
					Get_OldAccountID = l(0).GetRoProperty("text")
				End If
				Set l = Nothing
				call logging("Current Account :["&Get_OldAccountID&"] not match ["&strValueToEnter&"]",c_DEBUG,3)
				oShell.Run Command,0,False
				wait 1
				Set l =  main_object.ChildObjects(oDesc)		
				If l.count()>2 Then
					Get_CurrentAccountID = l(2).GetRoProperty("text")
				Else
					Get_CurrentAccountID = l(0).GetRoProperty("text")
				End If
				Set l = Nothing
				
				If Get_OldAccountID = Get_CurrentAccountID Then
					call logging("finish select account list",c_DEBUG,3)
					Exit for
				End If
			End If
		Next
		
		If iRet=True Then
			descript="not found <b>["&strValueToEnter&"]</b> in list"
			call logging("not found "&strValueToEnter&" in list",c_DEBUG,3)
		Else
			descript="found <b>["&strValueToEnter&"]</b> in list"
			call logging("found "&strValueToEnter&" in list",c_DEBUG,3)
		End If
  	Else
  		iRet = false
  		descript ="invalid input data"
		call logging( "invalid input data",c_DEBUG,3)
  	End If
'	
  	MC_CheckAccountNotInList_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
'	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseAccountTransferTo_ANDROID()
'*  Purpose:        choose own transfer account to
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_ChooseAccountTransferTo_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	Activity="choose own transfer account to"
	Const sModule = "MC_ChooseAccountTransferTo"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  		
  	call logging("Account Input Value " & strValueToEnter,c_DEBUG,3) 	
	set accFromObject=Eval(Create_OR_Object(UIName,""))
	accFromObject.SetTOProperty "text",strValueToEnter
	
	set mainToObject=Eval(Create_OR_Object("Transfer_MainObject_Obj",""))
	
'		mainToObject.swipe "up"
	  	For Iterator = 1 To 30 Step 1
	  	 	If accFromObject.Exist(3) Then
'				MC_HighlightObject(accFromObject)
				wait 2
	  	 		accFromObject.longpress
'	  	 		accFromObject.Tap
	  	 		iRet=True
	 			descript ="Found account transfer to <b>[" &strValueToEnter &"]</b>"
				call logging("Found account transfer to " &strValueToEnter,c_DEBUG,3)
				mainToObject.Swipe "up" 
				Exit for
	  	 	Else
				mainToObject.Swipe "up" 
				iRet=False
				descript ="not found account"
				call logging( "not found account",c_DEBUG,3)			
	  	 	End If
	  	Next
'	  	accFromObject.Tap
	  	UIName=AccountTo
	  	set accFromObject=Nothing
	  	set mainToObject=Nothing
	  	
	  	MC_ChooseAccountTransferTo_ANDROID= StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

	  
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_HighlightObject()
'*  Purpose:        highlight object
'*  Arguments:      Object
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_HighlightObject(strValueToEnter,UIName,ExpectedResult)
	Activity="highlight object"
	Const sModule = "MC_HighlightObject"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	Mobile_Object_T = Create_OR_Object(UIName,"")
	set objTestObject=Eval(Mobile_Object_T)
	
	objTestObject.highlight	
	
	iRet = CheckError()
	If iRet = True Then
		descript ="success to highlight object "&Mobile_Object_T
		call logging("success to highlight object "&Mobile_Object_T,c_DEBUG,3)		
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If
	
	MC_HighlightObject = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_InputTopupAmount()
'*  Purpose:        input topup amount
'*  Arguments:      Object
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_InputTopupAmount(strValueToEnter)
	Dim UIName , ExpectedResult
	Activity="input topup amount"
	Const sModule = "MC_InputTopupAmount"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	Amount_UIName = "Topup_EditAmount_Data" 
	set AmountInput_Object=Eval(Create_OR_Object(Amount_UIName,""))
	
	AmountBtn_UIName = "Topup_Amount_Btn"
	set AmountBtn_Object=Eval(Create_OR_Object(AmountBtn_UIName,strValueToEnter))
	
	InputNumber	= strValueToEnter
'	InputNumber=FormatNumber(strValueToEnter,,,,0)
	call logging("InputNumber " & InputNumber,c_DEBUG,3)
		
	If AmountInput_Object.Exist(5) Then
		AmountInput_Object.Set InputNumber
		iRet = True
		descript="set value <b>["&strValueToEnter&"]</b> done"
	Else
	
		AmountMain_UIName="Topup_AmountMain_Obj"
		set AmountMain_Object=Eval(Create_OR_Object(AmountMain_UIName,""))
		
		For Iterator = 1 To 30 Step 1
				If AmountBtn_Object.Exist(5) Then
					AmountBtn_Object.Tap
					iRet = True
					descript="choosse topup value <b>["&strValueToEnter&"]</b> done"
					Exit For
				Else
					AmountMain_Object.swipe "left"
					iRet = False
					descript="not found object"
				End If
		Next

	End If
	
	MC_InputTopupAmount = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_CompareDate()
'*  Purpose:        compare date from input
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'*****************************************************************************************************************
Function MC_CompareDate(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	Activity="compare date"
	Const sModule = "MC_CompareDate"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	Set objRE = New RegExp
	objRE.Pattern    = "^[0-9]*[0-9]$"
	
	If objRE.Test(strValueToEnter) Then
		set objTestObject=Eval(Create_OR_Object(UIName,""))		
		Flag = CheckObjectExist(objTestObject)
		If Flag=1 Then
				
			AppGetDateLabel=objTestObject.GetROProperty("text")
			SplitVal=split(AppGetDateLabel," ")
			AppDayLabel=SplitVal(0)
			AppMonthLabel=SplitVal(1)
			If AppMonthLabel="Jan" Then
				AppMonthLabel="1"
			ElseIf AppMonthLabel="Feb" Then
				AppMonthLabel="2"
			ElseIf AppMonthLabel="Mar" Then
				AppMonthLabel="3"
			ElseIf AppMonthLabel="Apr" Then
				AppMonthLabel="4"
			ElseIf AppMonthLabel="May" Then
				AppMonthLabel="5"
			ElseIf AppMonthLabel="Jun" Then
				AppMonthLabel="6"
			ElseIf AppMonthLabel="Jul" Then
				AppMonthLabel="7"
			ElseIf AppMonthLabel="Aug" Then
				AppMonthLabel="8"
			ElseIf AppMonthLabel="Sep" Then
				AppMonthLabel="9"
			ElseIf AppMonthLabel="Oct" Then
				AppMonthLabel="10"
			ElseIf AppMonthLabel="Nov" Then
				AppMonthLabel="11"
			ElseIf AppMonthLabel="Dec" Then
				AppMonthLabel="12"				
			End If
			
			AppDateLabel=CDate(AppDayLabel&"/"&AppMonthLabel&"/"&Year(date))
			DateNow=Day(date)&"/"&Month(date)&"/"&Year(date)
			NumDateDiff=DateDiff("d",AppDateLabel,DateNow,vbMonday)
			If strValueToEnter>=NumDateDiff Then
				iRet=True
				descript="Excel date <b>"&strValueToEnter&"</b> compare match AppLabelDate"
				call logging("Excel date "&strValueToEnter&" compare match AppLabelDate",c_DEBUG,3)
			Else
				iRet=False
				descript="Excel date "&strValueToEnter&" compare not match AppLabelDate ,datediff <b>"&NumDateDiff &"</b>"
				call logging("Excel date "&strValueToEnter&" compare not match AppLabelDate ,datediff "&NumDateDiff ,c_DEBUG,3)
			End If
			
		Else
			descript="not fould object"
			call logging("not fould object",c_DEBUG,3)
			iRet=False
		End If
	Else
		descript="invalid input data"
		call logging("invalid input data",c_DEBUG,3)
		iRet=False
  	End If
	
	MC_CompareDate = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
  	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_CheckCashBack()
'*  Purpose:        check credit card cashback on BLP system
'*  Arguments:      Object
'* 	Return Value:   Flag True or False
'* 	Author By		THAKOON L. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_CheckCashBack(strValueToEnter)

	'strValueToEnter = "4966151080000743" 'HardCode
	SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe", "http://10.21.16.38:9090/oneloyalty/web"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebEdit("userid").WaitProperty "visible", "True"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebEdit("userid").Set "OLS005"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebEdit("pwd").SetSecure "scb1234!"
	Browser("BLP").Page("BLP").Frame("fmiddle").Image("submit_norm").Click
	Browser("BLP").Page("BLP").Frame("fmiddle").WebList("clubCode").WaitProperty "visible", "True"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebList("clubCode").Select "SCB CLUB"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebList("clubCode").WaitProperty "value", "SCB CLUB"
	Browser("BLP").Page("BLP").Frame("fmiddle").WebEdit("cardNo").Set strValueToEnter
	Browser("BLP").Page("BLP").Frame("upper").Image("find_over").Click
	Browser("BLP").Page("BLP").Frame("fmiddle").WebElement("PoolBalValue").WaitProperty "visible", "True"
	text = Browser("BLP").Page("BLP").Frame("fmiddle").WebElement("PoolBalValue").GetROProperty("innertext")
	If instr(text,"C01") > 0 Then
		iRet = true
	Else 
		iRet = false
	End If	
	Browser("BLP").Close
	MC_CheckCashBack = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_WaitIdle()
'*  Purpose:        Set timeout for wait  
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		THAKOON L. (AMS-GABLE)
'************************************************************************************************************************
Public Function MC_WaitIdle(strValueToEnter)
	Activity="set timeout for wait"
	Const sModule = "MC_WaitIdle"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	wait strValueToEnter
	iRet = CheckError()
	If iRet = True Then
		descript="wait <b>["&strValueToEnter& "]</b> second"
		call logging("wait " & strValueToEnter & " second",c_DEBUG,3)
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If

	MC_WaitIdle = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function


Public Function MC_DetectiveFindObject(strNativeClassObject,strTextObject,UIName)
	Dim ObjectDec
	Dim ListMCIndex()
	Const sModule = "MC_DetectiveFindObject"
	
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set MainObject=Eval(Create_OR_Object(UIName,""))
	
	Set ObjectDec = Description.Create
	
	ObjectDec("nativeclass").value = strNativeClassObject
	ObjectDec("text").value        = strTextObject
	
	Set ListObj = MainObject.ChildObjects(ObjectDec)
	
	call logging("Count Object : " & ListObj.count,c_DEBUG,3)
	
	If ListObj.count > 0 Then
		iRet = true
		call logging("Found Object : " & ListObj.count & " Object(s)",c_DEBUG,3)
		
		set MC_DetectiveFindObject = ListObj
	else
		iRet = false
		call logging("Error not found Object Sequence : " & Seq,c_DEBUG,3)
		
		set MC_DetectiveFindObject = Nothing
	End If
		
End Function

Public Function MC_Business_CheckEditFavoritePayment(strValueToEnter,ExpectedResult)

	Const sModule = "MC_Business_CheckEditFavoritePayment"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Set ArrayButton = Nothing
	Set ArrayButton = MC_DetectiveFindObject("UIButton","Button","Payment_Billing_TH_MainObject")
	
	If ArrayButton is Nothing Then
		call logging("Error Not found object... ",c_DEBUG,3)
		iRet = False
		Description = "Not found query from function : MC_DetectiveFindObject"
		MC_Business_CheckEditFavoritePayment = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
		Exit Function
	End If
	
	strTitleFavorite = "Payment_Billing_TH_TitleFavorite"
	set objTestObject=Eval(Create_OR_Object(strTitleFavorite,""))
	
	strBackBtn = "Payment_Billing_TH_BackBtn"
	set objBackBtn=Eval(Create_OR_Object(strBackBtn,""))
	
	For I = 0 To ArrayButton.Count - 1 Step 1
		
'		ArrayButton(I).highlight
'		MC_HighlightObject(ArrayButton(I))
		ArrayButton(I).Tap
		
		'Setup text findding
		Environment("ValidateText") = strValueToEnter
		
		Check = objTestObject.Exist(5)
		
		If Check Then
'			objTestObject.highlight
'			MC_HighlightObject(objTestObject)
			
			call logging("Found page : " & strValueToEnter,c_DEBUG,3)
			iRet = True
			
			descript ="Found page : " &strValueToEnter
			MC_Business_CheckEditFavoritePayment = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
			Exit Function
		else
			'Click back button
			objBackBtn.Tap
		End If
		
	Next
	
	iRet = False
	descript ="not Found page : " &strValueToEnter
	call logging(descript,c_DEBUG,3)
	MC_Business_CheckEditFavoritePayment = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End function

'**********************************************************************************************************************************
'*  Function Name :	MC_PlusNumber()
'*  Purpose:        Plus 2 number and add value in Environment("UserDefineVariable") 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_PlusNumber(strValueToEnter)
	On Error Resume Next
	Activity="plus number"
	Const sModule = "MC_PlusNumber"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	Operation="+"
	ResultVal = OperationNumber(strValueToEnter,Operation)
	Flag = split(ResultVal,";")
	If Flag(0) = "True" Then
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=True
	Else
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=False
	End If
	
	MC_PlusNumber = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_MinusNumber()
'*  Purpose:        Minus 2 number and add value in Environment("UserDefineVariable") 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_MinusNumber(strValueToEnter)
	On Error Resume Next
	Activity="minus number"
	Const sModule = "MC_MinusNumber"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	Operation="-"
	ResultVal = OperationNumber(strValueToEnter,Operation)
	Flag = split(ResultVal,";")
	If Flag(0) = "True" Then
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=True
	Else
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=False
	End If
	
	MC_MinusNumber = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_MultiplyNumber()
'*  Purpose:        Multiply 2 number and add value in Environment("UserDefineVariable") 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_MultiplyNumber(strValueToEnter)
	On Error Resume Next
	Activity="multiply number"
	Const sModule = "MC_MultiplyNumber"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	Operation="*"
	ResultVal = OperationNumber(strValueToEnter,Operation)
	Flag = split(ResultVal,";")	
	If Flag(0) = "True" Then
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=True
	Else
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=False
	End If
	
	MC_MultiplyNumber = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_DivideNumber()
'*  Purpose:        Divide 2 number and add value in Environment("UserDefineVariable")
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_DivideNumber(strValueToEnter)
	On Error Resume Next
	Activity="divide number"
	Const sModule = "MC_DivideNumber"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	Operation="/"
	ResultVal = OperationNumber(strValueToEnter,Operation)
	Flag = split(ResultVal,";")		
	If Flag(0) = "True" Then
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=True	
	Else
		call logging(Flag(1),c_DEBUG,3)
		descript=Flag(1)
		iRet=False
	End If
		
	MC_DivideNumber = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_SetToggle()
'*  Purpose:        set toggle on / off
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_SetToggle(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)

	Activity="set toggle on / off"
	Const sModule = "MC_SetToggle"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	mobile_object_t	= Create_OR_Object(UIName,"")
	set objTestObject=Eval(mobile_object_t)
	objTestObject.RefreshObject
	wait 1
'	Flag = CheckObjectExist(objTestObject)
	
	If objTestObject.exist(3) Then	
		If instr(mobile_object_t,".Page(") > 0 Then
			If lcase(UIName)="Manage_ReqMoneyToggle_Btn" Then
				set ToggleCheckObject=Eval(Create_OR_Object("Manage_RTPToggleValue_Data",""))
				ToggleValue=ToggleCheckObject.GetROProperty("Checked")
				If ToggleValue=1 Then
					ToggleValue="ON"
				Else
					ToggleValue="OFF"
				End If
				set ToggleCheckObject = Nothing


			ElseIf lcase(UIName)="managenoti_pushnoti_obj" Then
				set ToggleCheckObject=Eval(Create_OR_Object("ManageNoti_push_toggle",""))
				ToggleValue=ToggleCheckObject.GetROProperty("Checked")				

				If ToggleValue=1 Then
					ToggleValue="ON"
				Else
					ToggleValue="OFF"
				End If
				set ToggleCheckObject = Nothing
			ElseIf lcase(UIName)="managenoti_emailnoti_obj" Then
				set ToggleCheckObject=Eval(Create_OR_Object("ManageNoti_email_toggle",""))
				ToggleValue=ToggleCheckObject.GetROProperty("Checked")
				If ToggleValue=1 Then
					ToggleValue="ON"
				Else
					ToggleValue="OFF"
				End If
				set ToggleCheckObject = Nothing
			Else
				ToggleValue=objTestObject.GetROProperty("class")	
				If instr(lcase(ToggleValue),"checked") > 0 Then
					ToggleValue="ON"			
				Else
					ToggleValue="OFF"
				End If				
			End If
			



			call logging("Toggle value ["&ToggleValue&"]",c_DEBUG,3)
			If lcase(strValueToEnter) = "on" or lcase(strValueToEnter) = "off" Then
				If lcase(ToggleValue) <> lcase(strValueToEnter) Then
					objTestObject.RefreshObject
					objTestObject.Click
					descript="set toggle value <b>"&strValueToEnter&"</b> done"
					call logging("set toggle value "&strValueToEnter,c_DEBUG,3)
				Else
					descript="skip set toggle value"
			 		call logging("skip set toggle value "&strValueToEnter,c_DEBUG,3)
				End If
				iRet = True
			Else
		 			descript="data input <font color=""red"">["&strValueToEnter&"]</font> is invalid"
			 		call logging("data input ["&strValueToEnter&"] is invalid",c_DEBUG,3)
			 		iRet=False
			End If
		Else	
			ToggleVal=objTestObject.GetROProperty("value")
			call logging("Toggle value "&ToggleVal,c_DEBUG,3)
			If StrComp(ToggleVal, strValueToEnter, vbTextCompare)<>0  Then
				If StrComp("ON", strValueToEnter, vbTextCompare)=0  Then
					objTestObject.Set "ON"
					descript="set toggle value <b>"&strValueToEnter&"</b> done"
					call logging("set toggle value "&strValueToEnter,c_DEBUG,3)
					iRet=True
				ElseIf	StrComp("OFF", strValueToEnter, vbTextCompare)=0  Then	
					objTestObject.Set "OFF"
					descript="set toggle value <b>"&strValueToEnter&"</b> done"
					call logging("set toggle value "&strValueToEnter,c_DEBUG,3)
					iRet=True
				Else
			 		descript="data input <font color=""red"">["&strValueToEnter&"]</font> is invalid"
			 		call logging("data input ["&strValueToEnter&"] is invalid",c_DEBUG,3)
			 		iRet=False
				End If
			Else
			 	iRet=False
			 	descript="skip set toggle value"
			 	call logging("skip set toggle value "&strValueToEnter,c_DEBUG,3)
			End If
		End If
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&mobile_object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
	End If
	wait 5
	MC_SetToggle = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_Find_ShareApp_ANDROID()
'*  Purpose:        Swipe to right position until find expect share app on share slip section
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_Find_ShareApp_ANDROID(strValueToEnter)
	On Error Resume Next
	Activity = "swipe left until find share app"
	Const sModule = "MC_Find_ShareApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set Main_Object=Eval(Create_OR_Object("Share_MainObject_obj",""))   
	set objTestObject=Eval(Create_OR_Object("Share_App_obj",""))	
	objTestObject.SetTOProperty "text",strValueToEnter
	For Iterator = 1 To 10 Step 1
		If objTestObject.Exist(4) Then
			objTestObject.Tap
			iRet = True
			descript ="found share app label <b>[" &strValueToEnter &"]</b>"
			call logging( "found share app label " &strValueToEnter,c_DEBUG,3)
			Exit for
		Else
			Flag_Err = SwipeAction_ANDROID(Main_Object,"left",1)
			If Flag_Err = False Then
				iRet = Flag_Err
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object"
				call logging(descript,c_DEBUG,3)
			End If	
		End If
	Next
	
	set objTestObject= Nothing
	set Main_Object= Nothing
	MC_Find_ShareApp_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseItemFromRadio()
'*  Purpose:        choose value in item  from radio
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_ChooseItemFromRadio(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "choose value in item  from radio"
	Const sModule = "MC_ChooseItemFromRadio"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Environment("ValidateText") = strValueToEnter
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	wait 1
	objTestObject.setToProperty "value", strValueToEnter
	objTestObject.RefreshObject
	
	objTestObject.Select strValueToEnter
		 
	'check error after action
	iRet = CheckError() 
	If iRet = True Then
		call logging("select item " & strValueToEnter,c_DEBUG,3)
		descript="select item <b>" &strValueToEnter &"</b>"		
	Else
		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)
	End If

	set objTestObject= Nothing
	
	MC_ChooseItemFromRadio = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseItemFromList()
'*  Purpose:        choose value in item  from dropdown list
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_ChooseItemFromList(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "choose value in item  from list"
	Const sModule = "MC_ChooseItemFromList"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
		objTestObject.Select strValueToEnter
		
		'check error after action
		iRet = CheckError()
		If iRet = True Then
			descript="select item <b>" &strValueToEnter &"</b>"
			call logging("select item " & strValueToEnter,c_DEBUG,3)				
		Else
			descript = Environment("TS_ErrorDescption")
			call logging(descript,c_DEBUG,3)
		End If		
		
		set objTestObject= Nothing

  		MC_ChooseItemFromList = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_ChooseItemFromRadioPromptpay()
'*  Purpose:        choose value in item  from radio Promptpay
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		Thakoon L. (AMS-GABLE)
'**********************************************************************************************************************************
Public Function MC_ChooseItemFromRadioPromptpay(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "choose value in item  from radio Promptpay"
	Const sModule = "MC_ChooseItemFromRadioPromptpay"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
'  	UIName = "Manage_PPAccId_Btn"
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	account = ".*" &strValueToEnter& ".*"
	objTestObject.SetTOProperty "value", account
	objTestObject.click
		 
		'check error after action
	iRet = CheckError()
	If iRet = True Then
		call logging("select item " & strValueToEnter,c_DEBUG,3)
		descript="select item <b>" &strValueToEnter &"</b>"		
	Else
		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3)
	End If
		
	set objTestObject= Nothing

	MC_ChooseItemFromRadioPromptpay = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_SetPin()
'*  Purpose:        set new pin 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		Thakoon L. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetPin(strValueToEnter)

	Activity = "set new pin " 
	Const sModule = "MC_SetPin"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	
	call logging("MyPin : "& strValueToEnter,c_DEBUG,3)    
	
	lenpin = len(strValueToEnter)
	UIName = "ForgotPin_setpin_data"
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	If lenpin <=6 and IsNumeric(strValueToEnter) Then
	  	For Iterator = 1 To lenpin Step 1
			'UIName = "ForgotSetPin"&Mid(MyPin,Iterator,1)		  	
			
			pin = mid(strValueToEnter, Iterator, 1)
			objTestObject.set pin
			
			call logging("Set Pin : " & pin,c_DEBUG,3)
			
		Next
		iRet = True
		descript="success to set Pin "&strValueToEnter
	Else
		iRet = False
		descript=strValueToEnter&" is an invalid Pin"
	End If	
	MC_SetPin = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)		
	
End Function  	

'**********************************************************************************************************************************
'*  Function Name :	MC_CheckTypeObject()
'*  Purpose:        check object type is number or text
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckTypeObject(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	Activity = "check object type" 
	Const sModule = "MC_CheckTypeObject"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	If InStr(strParentObject,".Page(") > 0 Then
		AppLabel=objTestObject.GetROProperty("innertext")
	Else
		AppLabel=objTestObject.GetROProperty("text")
	End if
	Flag = CheckObjectExist(objTestObject)
	
	If Flag = 1 Then
		Set objRE = New RegExp
		If lcase(strValueToEnter)= "number" Then		
			objRE.Pattern    = "^(?=.)([+-]?(0|([1-9](\d*|\d{0,2}(,\d{3})*)))?(\.([0-9]+))?)$"
		ElseIf lcase(strValueToEnter) = "text" Then
			objRE.Pattern    = "^[^0-9]*[^0-9]$"
		Else
			descript="not support other type"
			Flag = 0
			iRet = False
		End If
		
		If Flag = 1 Then
			If objRE.Test(AppLabel) Then
				iRet = True
				call logging("["& AppLabel & "] is match "&strValueToEnter&" type" ,c_DEBUG,3)
				descript ="<b>["& AppLabel & "]</b> is match <b>"&strValueToEnter&"</b> type"
			Else
				iRet = False
				call logging("["& AppLabel & "] is not match "&strValueToEnter&" type" ,c_DEBUG,3)		
				descript ="<b>["& AppLabel & "]</b> is <font color=""red"">not match</font> <b>"&strValueToEnter&"</b> type"
			End If
		End If
	Else
			descript="not fould object"
			iRet = False
	End If	
	MC_CheckTypeObject = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function  


'**********************************************************************************************************************************
'*  Function Name :	MC_CheckEnable()
'*  Purpose:        check property object is disable / enable 
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckEnable(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)

	Activity = "check enable / disable object" 
	Const sModule = "MC_CheckEnable"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
'	Flag = CheckObjectExist(objTestObject)

  	If instr(strValueToEnter,";") > 0 Then
  		splitVar = split(strValueToEnter,";")
  		ObjVisible = splitVar(0)
  		replaceText = splitVar(1)
  		If lcase(replaceText) <> "yes" Then
  			objTestObject.SetToProperty "text",replaceText
  		End If
  	Else
  		ObjVisible = strValueToEnter
  	End If
  	
	If objTestObject.exist(3)  Then
	
		If lcase(ObjVisible)="yes" Then
  			If lcase(objTestObject.GetROProperty("isenabled"))="true" Then
  				iRet = True
  				descript="object <b>["&UIName&"]</b> is <font color=blue>enabled</font>"
  				call logging("object ["&UIName&"] is enabled",c_DEBUG,3)
  			Else
  				iRet = False
  				descript="object <b>["&UIName&"]</b> is <font color=red>disabled</font>"
  				call logging("object ["&UIName&"] is disabled",c_DEBUG,3)
  			End If
		ElseIf lcase(ObjVisible)="no" Then
			If lcase(objTestObject.GetROProperty("isenabled"))="false" Then
  				iRet = True
  				descript="object <b>["&UIName&"]</b> is <font color=blue>disabled</font>"
  				call logging("object ["&UIName&"] is disabled",c_DEBUG,3)
  			Else
  				iRet = False
  				descript="object <b>["&UIName&"]</b> is <font color=red>enabled</font>"
  				call logging("object ["&UIName&"] is enabled",c_DEBUG,3)
  			End If
		
		Else
			descript="input invalid data"
			call logging("input invalid data",c_DEBUG,3)
			iRet = False		
	  	End If
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
	End If	
	MC_CheckEnable = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	

End Function  

'**********************************************************************************************************************************
'*  Function Name :	MC_CheckIsFocus()
'*  Purpose:        check status object is focus or not
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckIsFocus(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)

	Activity = "check focus / not focus object" 
	Const sModule = "MC_CheckIsFocus"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	
	set objTestObject=Eval(Create_OR_Object(UIName,""))
'	Flag = CheckObjectExist(objTestObject)
	
	If objTestObject.exist(3) Then
	
		If lcase(strValueToEnter)="yes" Then
  			If objTestObject.GetROProperty("isfocusable")="true" Then
  				iRet = True
  				descript="object <b>["&UIName&"]</b> is <font color=blue>focus</font>"
  				call logging("object ["&UIName&"] is focus",c_DEBUG,3)
  			Else
  				iRet = False
  				descript="object <b>["&UIName&"]</b> is <font color=red>not focus</font>"
  				call logging("object ["&UIName&"] is not focus",c_DEBUG,3)
  			End If
		ElseIf lcase(strValueToEnter)="no" Then
			If objTestObject.GetROProperty("isfocusable")="false" Then
  				iRet = True
  				descript="object <b>["&UIName&"]</b> is <font color=blue>not focus</font>"
  				call logging("object ["&UIName&"] is not focus",c_DEBUG,3)
  			Else
  				iRet = False
  				descript="object <b>["&UIName&"]</b> is <font color=red>focus</font>"
  				call logging("object ["&UIName&"] is focus",c_DEBUG,3)
  			End If
		
		Else
			descript="input invalid data"
			call logging("input invalid data",c_DEBUG,3)
			iRet = False		
	  	End If
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
	End If	
	MC_CheckIsFocus = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	

End Function  

'**********************************************************************************************************************************
'*  Function Name :	MC_SetSliderBar_ANDROID()
'*  Purpose:        Set slider bar (value input length is 0-100)
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SetSliderBar_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "Set slider bar" 
	Const sModule = "MC_SetSliderBar"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Mobile_Object_t = Create_OR_Object(UIName,"")
	set objTestObject=Eval(Mobile_Object_t)	
	
	If objTestObject.exist(3) Then	
		If strValueToEnter="100" Then
			Slide_left = objTestObject.GetROProperty("left")
			Slide_right = objTestObject.GetROProperty("right")
			Slide_right = Slide_right - 44
			call logging("Slide_left : "&Slide_left,c_DEBUG,3)
			call logging("Slide_right : "&Slide_right,c_DEBUG,3)
			objTestObject.Pan Slide_left,24,Slide_right,24,0.256963,0.004729
		ElseIf strValueToEnter="0" Then	
			Slide_left = objTestObject.GetROProperty("left")
			Slide_right = objTestObject.GetROProperty("right")
			Slide_right = Slide_right/2
			Slide_left = Slide_left-14
			call logging("Slide_left : "&Slide_left,c_DEBUG,3)
			call logging("Slide_right : "&Slide_right,c_DEBUG,3)
			objTestObject.Pan Slide_right,24,Slide_left,24,-0.122902,0
		Else
			strValueToEnter = FormatNumber(strValueToEnter,6)
			objTestObject.SetPosition strValueToEnter
		End If
		iRet = CheckError()	
	Else	
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
  	End If	
	
	If iRet = True Then
		descript="SetPosition Value <b>["&strValueToEnter&"]</b> in slider object <font color=green>["&Mobile_Object_t&"]</font> complate"
		call logging("SetPosition Value ["&strValueToEnter&"] in slider object ["&Mobile_Object_t&"] complate",c_DEBUG,3)
	Else
		descript = Environment("TS_ErrorDescption")
		call logging(descript,c_DEBUG,3) 
	End If	
	set objTestObject = Nothing
	MC_SetSliderBar_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	

End Function 


'**********************************************************************************************************************************
'*  Function Name : MC_SwitchDevice()
'*  Purpose:        Switch to other devices
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'*******************************************************************************************************************
Function MC_SwitchDevice(strValueToEnter)
	Activity = "switch other devices" 
	Const sModule = "MC_SwitchDevice"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	Device("Device").CloseViewer
  	Device("Device").SetTOProperty id,strValueToEnter
  	call logging("Device ID " & strValueToEnter,c_DEBUG,3)
  	descript="switch to device id <b>["&strValueToEnter&"]</b>"
	iRet = True
  	Device("Device").OpenViewer
  	MC_SwitchDevice = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ReinstallApp()
'*  Purpose:        Reinstall App 
'*  Arguments:      strValueToEnter,Device_ID,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_ReinstallApp(strValueToEnter,Device_ID,OSType)
	Activity = "Reinstall App" 
	Const sModule = "MC_ReinstallApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	If len(strValueToEnter) = 0 or strValueToEnter ="" or lcase(strValueToEnter) ="yes" Then
		strValueToEnter = Device("Device").App("Common").GetROProperty("identifier")
	End If
	
	'For Uninstall App
	If OSType="ANDROID" Then
		SDKPath=Environment("RootProject") & "\SDK-tools"
		Dim oShell 
		Set oShell = CreateObject ("WSCript.shell")
		Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" uninstall "&strValueToEnter&" & Exit"
		oShell.Run Command,1,True
		Set oShell = Nothing 	
	Else
		libimobiledevice=Environment("RootProject") & "\libimobiledevice\windows-amd64\"
		SystemUtil.Run libimobiledevice & "ideviceinstaller.exe", "-U " & strValueToEnter
	End If
	
	Device("Device").App("Common").Launch Install, Restart
	wait 10
	
 	iRet = CheckError()
	If iRet = True Then
		descript="Reinstall <b>["&strValueToEnter&"]</b> complete"
		call logging("Reinstall ["&strValueToEnter&"] complete",c_DEBUG,3)
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If	  	
	call logging( descript,c_DEBUG,3)
	
 	MC_ReinstallApp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_UninstallApp()
'*  Purpose:        Uninstall App
'*  Arguments:      strValueToEnter,Device_ID,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_UninstallApp(strValueToEnter,Device_ID,OSType)
	Activity = "Uninstall App" 
	Const sModule = "MC_UninstallApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	If len(strValueToEnter) = 0 or strValueToEnter ="" or lcase(strValueToEnter) ="yes" Then
		strValueToEnter = Device("Device").App("Common").GetROProperty("identifier")
	End If
	
	If OSType="ANDROID" Then
		SDKPath=Environment("RootProject") & "\SDK-tools"
		Dim oShell 
		Set oShell = CreateObject ("WSCript.shell")
		Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" uninstall "&strValueToEnter&" & Exit"
		oShell.Run Command,1,True
		Set oShell = Nothing 	
	Else
		libimobiledevice=Environment("RootProject") & "\libimobiledevice\windows-amd64\"
		SystemUtil.Run libimobiledevice & "ideviceinstaller.exe", "-U " & strValueToEnter
	End If
 
  	iRet = CheckError()
	If iRet = True Then
		descript="Uninstall <b>["&strValueToEnter&"]</b> complete"
		call logging("Uninstall ["&strValueToEnter&"] complete",c_DEBUG,3)
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If	  	
	call logging( descript,c_DEBUG,3)
	
 	MC_UninstallApp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
 	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_LaunchApp()
'*  Purpose:        Launch App 
'*  Arguments:      strValueToEnter,Device_ID
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_LaunchApp(strValueToEnter,Device_ID)
	Activity = "Launch App" 
	Const sModule = "MC_LaunchApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	If len(strValueToEnter) = 0 or strValueToEnter ="" or lcase(strValueToEnter) ="yes" Then
		strValueToEnter = Device("Device").App("Common").GetROProperty("identifier")
	End If
	Device("Device").App("Common").Launch DoNotInstall, DoNotRestart
	
   	iRet = CheckError()
	If iRet = True Then
		descript="Launch Application <b>["&strValueToEnter&"]</b> complete"
		call logging("Launch Application ["&strValueToEnter&"] complete",c_DEBUG,3)
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If	  	
	call logging( descript,c_DEBUG,3)
	
 	MC_LaunchApp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
 	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CloseApp()
'*  Purpose:        Close App **For Android 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_CloseApp(strValueToEnter,Device_ID)
	Activity = "Close App" 
	Const sModule = "MC_CloseApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	SDKPath=Environment("RootProject") & "\SDK-tools"
	If len(strValueToEnter) = 0 or strValueToEnter ="" Then
		strValueToEnter = Device("Device").App("Common").GetROProperty("identifier")
'		strValueToEnter = "com.scb.phone_uat_cr"
	End If
	
	Set oShell = CreateObject ("WSCript.shell")
	Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell am force-stop "&strValueToEnter&" & Exit"
	oShell.Run Command,0,False
	Set oShell = Nothing 
	iRet = True
	descript="Close Application <b>["&strValueToEnter&"]</b> complete"
	call logging("Close Application ["&strValueToEnter&"] complete",c_DEBUG,3)
 
 	MC_CloseApp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_RestartApp()
'*  Purpose:        Restart App
'*  Arguments:      strValueToEnter,Device_ID
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_RestartApp(strValueToEnter,Device_ID)
	Activity = "Restart App" 
	Const sModule = "MC_RestartApp"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	If len(strValueToEnter) = 0 or strValueToEnter ="" or lcase(strValueToEnter) ="yes" Then
		strValueToEnter = Device("Device").App("Common").GetROProperty("identifier")
	End If
	Device("Device").App("Common").Launch DoNotInstall, Restart

   	iRet = CheckError()
	If iRet = True Then
		descript="Restart Application <b>["&strValueToEnter&"]</b> complete"
		call logging("Restart Application ["&strValueToEnter&"] complete",c_DEBUG,3)
	Else
	  	descript = Environment("TS_ErrorDescption")
	End If	  	
	call logging( descript,c_DEBUG,3)
	
 	MC_RestartApp = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
 	
End Function


'**********************************************************************************************************************************
'*  Function Name : MC_TurnOnOff3G()
'*  Purpose:        Turn On-Off 3G **For Android 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_TurnOnOff3G(strValueToEnter,Device_ID)
	Activity = "Turn On Off 3G" 
	Const sModule = "MC_TurnOnOff3G"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	If lcase(strValueToEnter)="on" Then
		status = "enable"
		descript="Turn On 3G complete"
		call logging("Turn On 3G complete",c_DEBUG,3)
	Else
		status = "disable"
		descript="Turn Off 3G complete"
		call logging("Turn Off 3G complete",c_DEBUG,3)
	End If
	
	SDKPath=Environment("RootProject") & "\SDK-tools"
	Set oShell = CreateObject ("WSCript.shell")
	Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell svc data "&status&" & Exit"
	oShell.Run Command,0,False
	Set oShell = Nothing 
	iRet = True
		
 	MC_TurnOnOff3G = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_CheckGreeting()
'*  Purpose:        check welcome text
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_CheckGreeting(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "check welcome text" 
	Const sModule = "MC_CheckGreeting"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set Main_Object=Eval(Create_OR_Object("Common_MainObject_Obj",""))   
	set objTestObject=Eval(Create_OR_Object(UIName,""))
	
	For Iterator = 1 To 3 Step 1
		Flag = CheckObjectExist(objTestObject)
		If Flag = 1 Then
				GreetingText = objTestObject.GetROProperty("text")
				timenow = Fix(Timer()/60)
				print timenow
				diff = DateDiff("n","00:00:00", "23:59:00")
				print "diff date : " & diff
				
				 If timenow>= 300 and timenow <= 719 Then
				 	If GreetingText = "Good morning" or GreetingText = "อรุณสวัสดิ์" Then
				 		iRet = True
				 		descript="["&GreetingText&"] match 'Good morning' or 'อรุณสวัสดิ์'"
					Else
						iRet = False
						descript="["&GreetingText&"] <font color = red>not match</font> 'Good morning' or 'อรุณสวัสดิ์'"				
				 	End If
				 ElseIf timenow>= 720 and timenow <= 1019 Then
				 	If GreetingText = "Good afternoon" or GreetingText = "สวัสดี" Then
				 		iRet = True	
				 		descript="["&GreetingText&"] match 'Good afternoon' or 'สวัสดี'"
					Else
						iRet = False	
						descript="["&GreetingText&"] <font color = red>not match</font> 'Good afternoon' or 'สวัสดี'"
				 	End If
				 ElseIf timenow>= 1020 and timenow <= 1440 Then
				 	If GreetingText = "Good evening" or GreetingText = "สวัสดี" Then
				 		iRet = True	
				 		descript="["&GreetingText&"] match 'Good evening' or 'สวัสดี'"
					Else
						iRet = False	
						descript="["&GreetingText&"] <font color = red>not match</font> 'Good evening' or 'สวัสดี'"
				 	End If
				 Else
				 	If GreetingText = "Hello" or GreetingText = "สวัสดี" Then
				 		iRet = True
						descript="["&GreetingText&"] match 'Hello' or 'สวัสดี'"		 		
					Else
						iRet = False	
						descript="["&GreetingText&"] <font color = red>not match</font> 'Hello' or 'สวัสดี'"
				 	End If
				 End If
				 call logging(descript,c_DEBUG,3)
				 Exit For
		Else
			Flag_Err = SwipeAction_ANDROID(Main_Object,"down",1)				
			If Flag_Err = False Then
				iRet = Flag_Err
				descript = Environment("TS_ErrorDescption")
				call logging(descript,c_DEBUG,3)
				Exit For
			Else
				iRet = False			
				descript ="not found expect object"
				call logging(descript,c_DEBUG,3)
			End If				
		End If
	Next
	set Main_Object = Nothing
	set objTestObject = Nothing
	MC_CheckGreeting = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
	
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetBalance()
'*  Purpose:        get balance from money movement
'*  Arguments:      strValueToEnter,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_GetBalance(strValueToEnter,OSType)
	On Error Resume Next
	Activity = "get balance from money movement" 
	Const sModule = "MC_GetBalance"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
 
	 SplitVar=split(strValueToEnter,";")
	 AccNo=SplitVar(0)
	 UserVariable=SplitVar(1)
	 	
	Set oDesc = description.Create()
	oDesc("class").value="Label"
	If OSType="ANDROID" Then
		Set l = Device("Device").App("Transfer").MobileObject("Transfer_FromMain_Obj").ChildObjects(oDesc)
	Else
		Set l = Device("Device").App("Transfer").ChildObjects(oDesc)
	End If
	
	Flag = MC_WaitUntilFindObj("10;yes","Transfer_From_Txt","")
	If Flag=1 Then
		n = l.count()
		For i = 0 To n-1
			judge = l(i).GetRoProperty("text")
			call logging("found text ["&judge&"]",c_DEBUG,3)
			If judge = AccNo Then
				balanceVal=i+1	
  				value = l(balanceVal).GetRoProperty("text")
				Environment(UserVariable) = value				
				iRet=True
				Exit for
			ElseIf lcase(judge)="to" or judge="ไปยัง" Then
			    iRet=False
			    Exit for
			End If    
		Next
	Else
		iRet=False
	End If

'	Flag_Err = CheckError()	
'  	If Flag_Err = True Then
  		If iRet=True Then
			descript="success get balance : <b>"&Environment(UserVariable)&"</b>"
			call logging("balance in Environment("&UserVariable&"): "&Environment(UserVariable),c_DEBUG,3)
		Else
			Environment("TS_ErrorFound") = 1
			descript="cannot get balance from Account ["&AccNo&"]"
			call logging("cannot get balance from Account ["&AccNo&"]",c_DEBUG,3)	
  		End If
'	Else
'		descript = Environment("TS_ErrorDescption")
'		call logging(descript,c_DEBUG,3) 
'	End If	
	Set l = Nothing
  	MC_GetBalance = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  		
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetBalanceRemain()
'*  Purpose:        get balance from remain
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_GetBalanceRemain(strValueToEnter)
	Activity = "get balance from remain" 
	Const sModule = "MC_GetBalanceRemain"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	UIName="Success_YourRemain_Txt"
  	set objTestObject=Eval(Create_OR_Object(UIName,""))	
  	RemainMsg = objTestObject.GetROProperty("text")
  	
	Set regCompare = New RegExp
		regCompare.IgnoreCase =True
	 	regCompare.Pattern    = "(?=.)([+-]?(0|([1-9](\d*|\d{0,2}(,\d{3})*)))?(\.([0-9]+))?)"
	    regCompare.Global = True
	   	Set matches = regCompare.Execute(RemainMsg)
	   	
	   	If matches.count > 0 Then
		    For Each getMatchStr In matches
		       newstr = newstr  & getMatchStr.Value
		    Next
		    newstr=FormatNumber(cdbl(newstr),,,,0)
		    print "balance : " &newstr
		    iRet=True
		    descript="get balance value <b>["&newstr&"]</b>"
		    call logging("get balance value ["&newstr&"]",c_DEBUG,3)
		    Environment(strValueToEnter)  = newstr
		 Else
		 	iRet=False
		 	descript="can not get balance from remaining message ["&RemainMsg&"]"
		 	call logging("can not get balance from remaining message ["&RemainMsg&"]",c_DEBUG,3)
		 End  If
		 MC_GetBalanceRemain = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function
'**********************************************************************************************************************************
'*  Function Name : SwipeAction_ANDROID()
'*  Purpose:        main action for swipe (ANDROID)
'*  Arguments:      UIName,S_Action
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function SwipeAction_ANDROID(Main_Object,S_Action,swipe_round)
	On Error Resume Next
'	set Main_Object=Eval(Create_OR_Object(UIName,""))	
	For Iterator = 1 To swipe_round Step 1
				
		If lCase(S_Action)="left" or lCase(S_Action)="right" or left(S_Action,5)="panup" or left(S_Action,7)="pandown" Then			
			If instr(Main_Object.GetROProperty("resourceid"),"account_select_view_pager") > 0 Then
				  	c_top = Main_Object.getROProperty("top")
			  		c_bottom = Main_Object.getROProperty("bottom")
			  		
			  		If S_Action="left" Then
				  		co_x = Main_Object.getROProperty("right") - 10
				  		co_y = c_top + ((c_bottom - c_top) /2)
			  		ElseIf S_Action="right" Then
			  			co_x = 10
				  		co_y = c_top + ((c_bottom - c_top) /2)
			  		End If
			  		
			  		call logging("Main_Object co_x " & co_x,c_DEBUG,3)
					call logging("Main_Object co_y " & co_y,c_DEBUG,3)
				
			  		SDKPath=Environment("RootProject") & "\SDK-tools"
			  		Set oShell = CreateObject ("WSCript.shell")
					Command = "cmd /K pushd "&SDKPath&" & adb -s "&Device_ID&" shell input tap "&co_x&" "&co_y&" & Exit"					
					oShell.Run Command,0,False
					Set oShell = Nothing
			Else
				
				Obj_left = Main_Object.GetROProperty("left")		
				Obj_right = Main_Object.GetROProperty("right")
				If S_Action="left" Then
					Start_X = Obj_right-100
					Start_Y = 100
					End_X = Obj_left+10
					End_Y = Start_Y
					x_velocity = -4086.855000
	'				y_velocity = -117.190100
					y_velocity = 10.000730
				ElseIf S_Action="right" Then
					Start_X = Obj_left+100
					Start_Y = 100
					End_X = Obj_right-100
					End_Y = Start_Y
					x_velocity = 5221.578000
					y_velocity = 10.000730
				ElseIf left(S_Action,5)="panup" Then
					Obj_top = Main_Object.GetROProperty("abs_y")
					inputVal=cint(right(len(S_Action)-5))
						
					Start_X = Obj_right/2
					Start_Y = Obj_top/2
					End_X = Start_X
					End_Y = Start_Y+inputVal
					x_velocity = 0.208741
					y_velocity = 0.208741
				ElseIf left(S_Action,7)="pandown" Then
					Obj_top = Main_Object.GetROProperty("top")
					Obj_bottom = Main_Object.GetROProperty("bottom")
					Obj_height= Obj_bottom - Obj_top
					x_velocity = 0.208741
					y_velocity = 0.208741
					inputVal=cint(right(len(S_Action)-5))
						
					Start_X = Obj_right/2
					Start_Y = Obj_height+inputVal
					End_X = Start_X
					End_Y = Obj_height
				End If
					
				call logging("AccMain Start_X " & Start_X,c_DEBUG,3)
				call logging("AccMain Start_Y " & Start_Y,c_DEBUG,3)
				call logging("AccMain End_X " & End_X,c_DEBUG,3)
				call logging("AccMain End_Y " & End_Y,c_DEBUG,3)
			
					' Swipe left or right Action	
	'			Main_Object.Pan Start_X,Start_Y,End_X,End_Y,0.208741,0.208741
				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity	
			End If						
		Else
			If lCase(S_Action)="up" Then
				Main_Object.Swipe "up"
			Else
				Main_Object.Swipe "down"
			End If
		End If
	Next
	iRet = CheckError() 
	SwipeAction_ANDROID = iRet
		
End Function

 
Function LinkPromptPay(strValueToEnter)

	Device("Device").App("Common").MobileObject("Main_Home_Btn").Tap
	Device("Device").App("LifeStyle").MobileLabel("LifeStyle_BankServices_Txt").Tap
	Device("Device").App("Common").MobileLabel("SCB_MenuSelect_Btn").SetToProperty "text","Manage SCB PromptPay|จัดการ SCB พร้อมเพย์"
	Device("Device").App("Common").MobileLabel("SCB_MenuSelect_Btn").Tap


End Function

'**********************************************************************************************************************************
'*  Function Name : MC_ClickMainMenu
'*  Purpose:        seperate click footer main menu [IOS] and [Android]
'*  Arguments:      strValueToEnter,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_ClickMainMenu(strValueToEnter,OSType)

	Activity = "click footer main menu ["&OSType&"]"
	Const sModule = "MC_ClickMainMenu"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	If OSType="IOS" Then
  		wait 1
		UIName="Main_MenuBar_Obj"  
		set objTestObject=Eval(Create_OR_Object(UIName,""))
		iRet=True	
		wait 2
'		If Err.Number <> 0 Then
'			environmentVarExists = False
'		Else
'			environmentVarExists = True
'		End If
		
	  	If instr(lcase(strValueToEnter),"home") > 0 Then
	  		objTestObject.Select 0
	  		descript="Select home menu"
	  		call logging(descript,c_DEBUG,3)
	  		Environment("selected_mainmenu")=strValueToEnter
	  	ElseIf instr(lcase(strValueToEnter),"account") > 0 Then
	  		objTestObject.Select 1
	  		descript="Select account menu"
	  		call logging(descript,c_DEBUG,3)
	  		Environment("selected_mainmenu")=strValueToEnter
	  	ElseIf instr(lcase(strValueToEnter),"money") > 0 Then
	  	'Edit by THA
	  		If lcase(Environment("selected_mainmenu"))="account" Then
	  			objTestObject.Select 1
	  		Else
	  			objTestObject.Select 2
	  		End If
			
	  		descript="Select money movement menu"
	  		call logging(descript,c_DEBUG,3)
	  		Environment("selected_mainmenu")=strValueToEnter
	  	ElseIf instr(lcase(strValueToEnter),"noti") > 0 Then
	  		objTestObject.Select 3
	  		descript="Select notification menu"
	  		call logging(descript,c_DEBUG,3)
	  		Environment("selected_mainmenu")=strValueToEnter
	  	ElseIf instr(lcase(strValueToEnter),"setting") > 0 Then
	  		If lcase(Environment("selected_mainmenu"))="account" Then
	  			objTestObject.Select 3
	  		Else
	  			objTestObject.Select 4
	  		End If	  		
	  		descript="Select setting menu"
	  		call logging(descript,c_DEBUG,3)
	  		Environment("selected_mainmenu")=strValueToEnter
	  	Else
			iRet=False 
			descript="input invalid data"
	  		call logging(descript,c_DEBUG,3)		
	  	End If
	Else
	
		iRet=True
	  	If instr(lcase(strValueToEnter),"home") > 0 Then
	  		UIName="Main_Home_Btn"  
			set objTestObject=Eval(Create_OR_Object(UIName,""))	
	  		objTestObject.Tap
	  		descript="Select home menu"
	  		call logging(descript,c_DEBUG,3)
	  		
	  	ElseIf instr(lcase(strValueToEnter),"account") > 0 Then
	  		UIName="Main_Acc_Btn"  
			set objTestObject=Eval(Create_OR_Object(UIName,""))	
	  		objTestObject.Tap
	  		descript="Select account menu"
	  		call logging(descript,c_DEBUG,3)
	  		
	  	ElseIf instr(lcase(strValueToEnter),"money") > 0 Then
	  		UIName="Main_ReqPay_Btn"  
			set objTestObject=Eval(Create_OR_Object(UIName,""))	
	  		objTestObject.Tap
	  		descript="Select money movement menu"
	  		call logging(descript,c_DEBUG,3)
	  		
	  	ElseIf instr(lcase(strValueToEnter),"noti") > 0 Then
	  		UIName="Main_Noti_Btn"  
			set objTestObject=Eval(Create_OR_Object(UIName,""))	
	  		objTestObject.Tap
	  		descript="Select notification menu"
	  		call logging(descript,c_DEBUG,3)
	  		
	  	ElseIf instr(lcase(strValueToEnter),"setting") > 0 Then
	  		UIName="Main_Settings_Btn"  
			set objTestObject=Eval(Create_OR_Object(UIName,""))	
	  		objTestObject.Tap
	  		descript="Select setting menu"
	  		call logging(descript,c_DEBUG,3)
	  		
	  	Else
			iRet=False 
			descript="input invalid data"
	  		call logging(descript,c_DEBUG,3)		
	  	End If	
  	End If
  	set objTestObject= Nothing
  	MC_ClickMainMenu = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function
 
'**********************************************************************************************************************************
'*  Function Name : MC_WaitUntilFindObj
'*  Purpose:        wait until find obj
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_WaitUntilFindObj(strValueToEnter,UIName,ExpectedResult)
	Dim check_ObjExist
	On Error Resume Next
	Activity = "wait until find obj" 
	Const sModule = "MC_WaitUntilFindObj"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	iRet = False
  	If instr(strValueToEnter,";")>0 Then	
  		iRet = True
  		SplitVar = split(strValueToEnter,";")
  		waittime = SplitVar(0) 
  		replacetext = SplitVar(1) 
  		
	  	Mobile_Object_t = Create_OR_Object(UIName,"")
		set objTestObject=Eval(Mobile_Object_t)	
		
		If len(replacetext) > 0 and lcase(replacetext) <>"yes" Then
			If instr(lcase(Mobile_Object_t),".page(") > 0 Then
				RoProperty="innertext"
			Else
				RoProperty="text"
			End If
			
			objTestObject.SetToProperty RoProperty,replacetext
			call logging("Set ROProperty "&RoProperty&" to ["&replacetext&"]",c_DEBUG,3)
		End If
		
		call logging("Wait object ["&Mobile_Object_t&"] for "&waittime&" second",c_DEBUG,3)
		
		For wait_second = 1 To waittime Step 1
			objTestObject.RefreshObject()
			check_ObjExist = objTestObject.exist(1)			
			waittime_countdown = (waittime+1) - wait_second
			call logging("wait time "&waittime_countdown &" second",c_DEBUG,3)
			If  check_ObjExist Then
				Flag = 1
				iRet = True
				Exit for
			End If	
		Next
	End If	
	
	If iRet = True Then
		If Flag = 1 Then
			descript= "found expect object ["&Mobile_Object_t&"]"
		Else
			iRet = False
			descript= "not found expect object ["&Mobile_Object_t&"]"
		End If	
	Else		
		descript= "invalid input value"	
		Environment("TS_ErrorFound")=1
	End If
	
	call logging(descript,c_DEBUG,3)	
	
	Set objTestObject = Nothing
	
	If Environment("functionName")="MC_WaitUntilFindObj" Then
		MC_WaitUntilFindObj = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	Else
		If iRet = True Then
			MC_WaitUntilFindObj=1
		Else
			MC_WaitUntilFindObj=-1
		End If
	End If
	  
End Function

'**********************************************************************************************************************************
'*  Function Name : MC_GetLimitValue
'*  Purpose:        get limit from limit msg
'*  Arguments:      trValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_GetLimitValue(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "get limit from limit msg" 
	Const sModule = "MC_GetLimitValue"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set objTestObject=Eval(Create_OR_Object(UIName,""))	
  	If objTestObject.exist(5) Then
  		RemainMsg = objTestObject.GetROProperty("text")
		iRet = CheckError()
	Else
		iRet = False
		Environment("TS_ErrorDescption") = "Not Found Object"
  	End If

	If iRet = True Then	
		Set regCompare = New RegExp
		regCompare.IgnoreCase =True
		regCompare.Pattern    = "(?=.)([+-]?(0|([1-9](\d*|\d{0,2}(,\d{3})*)))?(\.([0-9]+))?)"
		regCompare.Global = True
		Set matches = regCompare.Execute(RemainMsg)
		   	
		If matches.count > 0 Then
			    For Each getMatchStr In matches
			       newstr = newstr  & getMatchStr.Value
			    Next
			    newstr=FormatNumber(cdbl(newstr),,,0)
			    print "Limit Value : " &newstr
			    iRet=True
			    descript="get balance value <b>["&newstr&"]</b>"
			    call logging("get balance value ["&newstr&"]",c_DEBUG,3)
			    Environment(strValueToEnter)  = newstr
		Else
			 	iRet=False
			 	descript="can not get balance from remaining message ["&RemainMsg&"]"
			 	call logging("can not get balance from remaining message ["&RemainMsg&"]",c_DEBUG,3)
		End  If	
	Else
		descript = Environment("TS_ErrorDescption")
	End If
	
	MC_GetLimitValue = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function

Function HandleError ()
	Environment("TS_ErrorFound")=1
	Environment("TS_ErrorDescption")=Err.Description
	IsNotErr = False&","&Err.Description
                print Environment("TS_ErrorFound")
                print Environment("TS_ErrorDescption")
	Err.Clear
End Function 


'**********************************************************************************************************************************
'*  Function Name : MC_DeleteThisItem_ANDROID
'*  Purpose:        Delete favourite icon in lifestyle page or friend & family page
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_DeleteThisItem_ANDROID(strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Activity = "delete favourite from lifestyle" 
	Const sModule = "MC_DeleteThisItem"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	mobile_object_t = Create_OR_Object(UIName,"")
	set mobile_object=Eval(mobile_object_t)	
		
	If mobile_object.exist(3) Then
	
		del_object_t = Create_OR_Object("DialogConfirm_Txt","")
		set del_object=Eval(del_object_t)	
		
		page_object_t = Create_OR_Object("LifeStyle_BankServices_Txt","")
		set page_object=Eval(page_object_t)	
		
		If page_object.Exist(2) Then
		
			main_object_t = Create_OR_Object("Common_MainObject_Obj","")
			set main_object=Eval(main_object_t)
			icontop_position = mobile_object.GetRoProperty("top")			
			bot_position = main_object.GetRoProperty("bottom")	
			high_screen = 	bot_position - (icontop_position+180)
			pan_duration = 0.025

		Else
		
		  	main_object_t = Create_OR_Object("FFamily_Main_Obj","")
			set main_object=Eval(main_object_t)
			top_position = main_object.GetRoProperty("top")			
			bot_position = main_object.GetRoProperty("bottom")	
			high_screen = 	bot_position - (top_position+180)
			pan_duration = 0.035
			
		End If
		
		mobile_object.Pan 10,300,10,high_screen,0, pan_duration
		
		If del_object.exist(3) Then
			del_object.Tap	
			iRet = true
		else
			iRet = false
		End If
		
			
'		iRet = CheckError()
		If iRet = True Then
			descript="delete favourite icon done"	
		Else
		  	descript = "not found object"
		End If	
		
		set page_object = nothing
		Set del_object_t = nothing
		set main_object= nothing
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
		descript="not found object"	
	End If
  	
	call logging( descript,c_DEBUG,3)
	
	Set mobile_object = nothing

	MC_DeleteThisItem_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
End Function 

'**********************************************************************************************************************************
'*  Function Name : MC_SetQuickMenuOff
'*  Purpose:        turn off quick menu
'*  Arguments:      strValueToEnter,UIName,ExpectedResult,OSType
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function MC_SetQuickMenuOff(strValueToEnter,UIName,ExpectedResult,OSType)
	On Error Resume Next
	Activity = "turn off quick menu" 
	Const sModule = "MC_SetQuickMenuOff"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	QMenu_Object_t = Create_OR_Object(UIName,"")
  	set QuickMenu_Object = Eval(QMenu_Object_t)
  	
  	If lcase(strValueToEnter) <> "yes" Then
  		QuickMenu_Object.SetToProperty "text",strValueToEnter	
  	End If
  	Flag_QMenu = CheckObjectExist(QuickMenu_Object)
  	
  	If Flag_QMenu =1 Then 
		QuickMenu_Object.Tap  	
		Flag_Modify = MC_WaitUntilFindObj("10;yes","Modify_QMenu_Btn","")	
		
		If Flag_Modify = 1 Then
			set Modify_Object = Eval(Create_OR_Object("Modify_QMenu_Btn",""))
			
			If OSType="ANDROID" Then 
				MainUIName="Common_MainObject_Obj"
			Else
				MainUIName="Modify_MainObject_Obj"
			End If	  	
			
			set Main_Object=Eval(Create_OR_Object(MainUIName,""))	
		  	set Toggle_Object = Eval(Create_OR_Object("Modify_Toggle_Btn",""))
		  	set Confirm_Object = Eval(Create_OR_Object("Modify_Confirm_Btn",""))
		  	
		  	Modify_Object.Tap
		  	For Iterator = 1 To 10 Step 1
				If Toggle_Object.Exist(3) Then
				
					If OSType="ANDROID" Then
						Toggle_Object.Tap
					Else
						Toggle_Object.Set "Off"
					End If
					
					Confirm_Object.Tap
					descript ="Turn off done"
					call logging(descript,c_DEBUG,3)
					iRet=True
					Exit For
				Else			
					Flag_Err = Eval("SwipeAction_"&OSType&"(Main_Object,""up"",1)")					
					If Flag_Err = False Then
						iRet = Flag_Err
						descript = Environment("TS_ErrorDescption")
						call logging(descript,c_DEBUG,3)
						Exit For
					Else
						iRet = False			
						descript ="not found expect object"
						call logging(descript,c_DEBUG,3)
					End If	
				End If
			Next			
			set Main_Object = Nothing
			Set Toggle_Object = nothing	
			Set Confirm_Object = nothing			
		Else	  	
			descript ="This feature is already off"
			call logging(descript,c_DEBUG,3)
			If OSType="ANDROID" Then 
				BackUIName="SCB_NavBack_Btn"
			Else
				BackUIName="Modify_Back_Btn"
			End If	  			
		  	set Back_Object = Eval(Create_OR_Object(BackUIName,""))
		  	Back_Object.Tap
		  	Set Back_Object = Nothing
		End If
		iRet = True
	Else
		descript ="not found object ["&QMenu_Object_t&"]"
		call logging(descript,c_DEBUG,3)	
		iRet = False
		Environment("TS_ErrorFound") = 1
	End If
	
	Flag_Err = CheckError()
	If Flag_Err = False Then
		iRet = Flag_Err
	  	descript = Environment("TS_ErrorDescption")
	End If	  	
	Set QuickMenu_Object = Nothing
	MC_SetQuickMenuOff = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function 

Function MapUIName_ANDROID(UIName)

	SearchText_Red = array("The Thai Red Cross Society","สภากาชาดไทย")
	SearchText_Child = array("The Children’s Hospital Foundation","มูลนิธิโรงพยาบาลเด็ก")	
	SearchText_Seub = array("Seub Nakhasathien Foundation","มูลนิธิสืบนาคะเสถียร")	
	SearchText_Smile = array("Operation Smile Thailand","มูลนิธิสร้างรอยยิ้ม")	
	SearchText_INVFooter = array("*Fund balance does not include client added today.","*มูลค่าลงทุนรวมยังไม่รวมเลขทะเบียนที่เพิ่มในวันนี้")
	StartText_Amount = array("AMOUNT","จำนวนเงิน")
	StartText_Fee = array("FEE","ค่าธรรมเนียม")
	SearchText_cardavailvalue = array("Available","วงเงินคงเหลือ")
	SearchText_cardused = array("Used","วงเงินที่ใช้ไป")

	Select Case Trim(UIName)
			
			'Get Mc_index From Donation
			Case lcase("Donate_ThaiRedList_Txt")
				  i_value = GetObject_McIndex_ANDROID("donation",SearchText_Red,"Donate_FoudationMainList_Obj","foundation_name")
			Case lcase("Donate_ChildList_Txt")
				  i_value = GetObject_McIndex_ANDROID("donation",SearchText_Child,"Donate_FoudationMainList_Obj","foundation_name")
			Case lcase("Donate_SeubList_Txt")
				  i_value = GetObject_McIndex_ANDROID("donation",SearchText_Seub,"Donate_FoudationMainList_Obj","foundation_name")
			Case lcase("Donate_SmileList_Txt")
				  i_value = GetObject_McIndex_ANDROID("donation",SearchText_Smile,"Donate_FoudationMainList_Obj","foundation_name")	
			Case lcase("AccSum_InvestFootterMsg_Txt")
				  i_value = GetObject_McIndex_ANDROID("",SearchText_INVFooter,"","")		
			Case lcase("MyFund_MFName_Txt")
				  i_value = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")					  
			Case lcase("MyFund_MFAccID_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 1
			Case lcase("MyFund_MFCurval_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 2
			Case lcase("MyFund_MFCurvalValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 3
			Case lcase("MyFund_MFNetgain_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 4	
			Case lcase("MyFund_MFNetgainValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 6		
			Case lcase("MyFund_MFUnits_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 5	
			Case lcase("MyFund_MFUnitsValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
				  i_value = Ini_McIndex + 7	
			Case lcase("MyFund_MFLowCurrentval_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_average_unit_text_view","MyFund_ExpandMain_Obj","")
				  i_value = Ini_McIndex + 5	
			Case lcase("MyFund_MFLowCurrentvalValue_txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_average_unit_text_view","MyFund_ExpandMain_Obj","")
				  i_value = Ini_McIndex + 6	
			Case lcase("AccSum_LastTransDate1_Txt")
				  i_value = GetObject_McIndex_ANDROID("mydeposit","recent_transaction_date_text_view","AccSum_maindepview_obj","")
			Case lcase("AccSum_LastTransAmount1_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("mydeposit","recent_transaction_date_text_view","AccSum_maindepview_obj","")	
				  i_value = Ini_McIndex + 1	
			Case lcase("AccSum_LastTransDate2_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("mydeposit","recent_transaction_date_text_view","AccSum_maindepview_obj","")	
				  i_value = Ini_McIndex + 2		
			Case lcase("AccSum_LastTransAmount2_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("mydeposit","recent_transaction_date_text_view","AccSum_maindepview_obj","")	
				  i_value = Ini_McIndex + 3	
				  
			'Get Data From Review Transfer	  

			Case lcase("Review_AMOUNT_Txt")
				  i_value = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")	
				  
			Case lcase("Review_AmountValue_Txt"),lcase("REV_Amount_Data")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 1		

			Case lcase("Review_FEES_Txt"),lcase("Review_FEE_Txt")
				  i_value = GetObject_McIndex_ANDROID("transfer",StartText_Fee,"Review_FeesMain_Obj","transfer_and_pay_item_title_text_view")
				  
			Case lcase("Review_FeesValue_Txt"),lcase("Review_FeeValue_Txt"),lcase("REV_Fee_Data")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Fee,"Review_FeesMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 1

			Case lcase("REV_BillAmount_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 13	
				  
			Case lcase("REV_BillAmountValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 12		

			Case lcase("REV_OperateFee_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 15	
				  
			Case lcase("REV_OperateFeeValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 14		

			Case lcase("REV_Overdue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 17
				  
			Case lcase("REV_OverdueValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 16		

			Case lcase("REV_Address_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 18
				  
			Case lcase("REV_AddressValue_Txt")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Review_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 19		
				  
			'Get Data From Success Transfer	  
			Case lcase("Success_AmountValue_Txt"),lcase("SUCC_Amount_Data")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Amount,"Success_AmountMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 1
				  
			Case lcase("Success_FeesValue_Txt"),lcase("Success_FeeValue_Txt"),lcase("SUCC_Fee_Data")
				  Ini_McIndex = GetObject_McIndex_ANDROID("transfer",StartText_Fee,"Success_FeesMain_Obj","transfer_and_pay_item_title_text_view")
				  i_value = Ini_McIndex + 1
				  
			'Get Data From Account Summary
			
			
			Case lcase("AccSum_cardavailvalue_btn")
				  Ini_McIndex = GetObject_McIndex_ANDROID("accsum",SearchText_cardavailvalue,"AccSum_maincardview_obj","available_title_text_view")
				  i_value = Ini_McIndex + 2

			Case lcase("AccSum_cardusedvalue_btn")
				  Ini_McIndex = GetObject_McIndex_ANDROID("accsum",SearchText_cardused,"AccSum_maincardview_obj","used_title_text_view")
				  i_value = Ini_McIndex + 2
				  
				  
			Case Else
				  i_value ="::N/A"
				  call logging("Not found UIName for assign mcindex",c_DEBUG,3)
	End Select
	
	Erase SearchText_Red
	Erase SearchText_Child 
	Erase SearchText_Seub
	Erase SearchText_Smile 
	Erase StartText_Amount
	Erase StartText_Fee
	MapUIName_ANDROID = i_value
	
End Function

Function GetObject_McIndex_ANDROID(section,searchtext,MainUIName,resourceid_search)

	If MainUIName = "" or MainUIName = null Then
		MainUIName="Common_MainObject_Obj"
	End If
	
	Main_Object_t = Create_OR_Object(MainUIName,"")
	Set oDesc = description.Create()
	oDesc("class").value="Label"
	set Main_Object = Eval(Main_Object_t&".ChildObjects(oDesc)")
	n = Main_Object.count()
	
	Set Obj_width = Eval(Main_Object_t)
	r_width = Obj_width.GetROProperty("right")
	r_max = cint(r_width)*0.95
	iRet = False
	For i = 0 To n-1
	    judge = Main_Object(i).GetRoProperty("text")
	    mc_index = Main_Object(i).GetRoProperty("mcindex")
	    resourceid = Main_Object(i).GetRoProperty("resourceid")

		If resourceid_search<>"" Then
			If lcase(resourceid) = lcase(resourceid_search) Then
				skip = False
			Else
				skip = True
			End If
		Else
				skip = False
		End If
		
		If skip = False Then		
			Select Case Trim(section)
					
				Case"myfund","mydeposit"
					 
					position_left = Main_Object(i).GetRoProperty("left")
					skip = False
					
					If position_left >=r_max Then
						skip = True
					End If
					
					If position_left > 0 and skip = False Then
						If instr(resourceid,searchtext) > 0 Then
							iRet = True
							GetMCIndex = mc_index
							Exit For
						End If
					End If
					
				Case Else
				
					call logging("["&i&"] - "&judge&" - mc_index = ["&mc_index&"]" ,c_DEBUG,3) 		 	
					For iSearchtext = 0 To Ubound(searchtext)
						If lcase(judge) = lcase(searchtext(iSearchtext)) Then
							   iRet = True
							   GetMCIndex = mc_index
							   Exit For		
						End If
					Next
				
			End Select	
		End If
	    
	    If iRet = True Then
	    	Exit for
	    End If    
	Next
	
	
	If IsArray(searchtext) Then
		Erase searchtext
	End If
	
	GetObject_McIndex_ANDROID = GetMCIndex

End Function 


Function MC_IOSSkipStep(OSType,skipstep)
	If OSType = "IOS" Then
		Select Case lcase(skipstep)
			Case "wifi"
				If Device("Device").App("RegisterWebView").MobileButton("Register_wifi_btn").exist(5) Then
					Device("Device").App("RegisterWebView").MobileButton("Register_wifi_btn").Tap	
				End If	
			Case "touchid"
				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Touch").WebElement("Register_touchid_txt").exist(5) Then
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Touch").WebButton("Register_touchidnotnow_btn").Click
				End If
			Case "location"
				If Device("Device").App("RegisterMobile").MobileLabel("Register_location_txt").exist(5) Then
				 	Device("Device").App("RegisterMobile").MobileButton("Register_locationallow_btn").Tap
				End If
			Case "maxdevice"
				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Review Device Registration").WebElement("Register_reviewdevices_txt").exist(5) Then			
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Review Device Registration").WebElement("Register_deletedevice_btn").Click
					wait 1
					Device("Device").App("RegisterWebView").MobileButton("Register_confirm_btn").Tap
					wait 1
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration Confirmation").WebButton("Register_nextcompleted_btn").Click
				End If
			Case "otp"
			    If not Device("Device").App("Settings").MobileWebView("MobileWebView").Page("Enter OTP Prompt Pay").Link("Register_resend_btn").exist(5) Then
			     Device("Device").App("Settings").MobileWebView("MobileWebView").Page("Enter OTP Prompt Pay").WebElement("Register_Back_btn").Click
			     wait 2
			     Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration No Language").WebButton("Register_next_btn").Click
			     wait 2
			    End If

			Case "email"
				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Email (5a470fd1-ffc").WebElement("Register_emailnotified_txt").exist(5) Then
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Email (5a470fd1-ffc").WebButton("Register_notnowemail_btn").Click
				End If
		End Select		
	Else
		Select Case lcase(skipstep)
			Case "wifi"
				If Device("Device").App("RegisterMobile").MobileLabel("Register_wifi_btn").exist(5) Then
					Device("Device").App("RegisterMobile").MobileLabel("Register_wifi_btn").Tap	
				End If	
			Case "touchid"
'				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Touch").WebElement("Register_touchid_txt").exist(5) Then
'					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Touch").WebButton("Register_touchidnotnow_btn").Click
'				End If
			Case "location2"
				If Device("Device").App("PermissionPopup").MobileLabel("Common_permission_txt").exist(5) Then
				 	Device("Device").App("PermissionPopup").MobileButton("Common_yes_btn").Tap
				End If
			Case "maxdevice"
				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration").WebElement("Register_deletedevice_btn").exist(5) Then			
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration").WebElement("Register_deletedevice_btn").Click
					wait 5
					Device("Device").App("RegisterMobile").MobileButton("Register_confirm_btn").Tap
					wait 5
					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration").WebButton("Register_nextcompleted_btn").Click
				End If

			Case "otp"
'			    If not Device("Device").App("Settings").MobileWebView("MobileWebView").Page("Enter OTP Prompt Pay").Link("Register_resend_btn").exist(5) Then
'			     Device("Device").App("Settings").MobileWebView("MobileWebView").Page("Enter OTP Prompt Pay").WebElement("Register_Back_btn").Click
'			     wait 2
'			     Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("Registration No Language").WebButton("Register_next_btn").Click
'			     wait 2
'			    End If
			Case "email"
'				If Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Email (5a470fd1-ffc").WebElement("Register_emailnotified_txt").exist(5) Then
'					Device("Device").App("RegisterWebView").MobileWebView("MobileWebView").Page("OnBoarding Email (5a470fd1-ffc").WebButton("Register_notnowemail_btn").Click
'				End If
		End Select		
	End If
	 MC_IOSSkipStep = 1
End Function

Function TimeoutHandler
	On Error Resume Next
	Activity ="timout handler"
	Const sModule = "TimeoutHandler"
	strValueToEnter = "Yes"
	
	call logging("sModule " & sMODULE,c_DEBUG,2)
	Call logging("strValueToEnter : " & strValueToEnter,c_DEBUG,3)
  	
	If Device("Device").App("Common").MobileLabel("DialogMsgTimeout_Txt").Exist(2) Then 'Please change to Timeout txt obj
		Device("Device").App("Common").MobileButton("DialogConfirm_Txt").Tap 'Please change to Timeout btn obj
		iRet = CheckError()
		If iRet = True Then
			descript="click done"
		Else
	  		descript = Environment("TS_ErrorDescption")
	  	End If
		TimeoutHandler = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	End If
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_CheckProperty()
'*  Purpose:        check object property 
'*  Arguments:      strParentObject,strChildobject,strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		Thakoon L. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_CheckProperty(strValueToEnter,UIName,ExpectedResult)

	Activity = "check object' property" 
	Const sModule = "MC_CheckProperty"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	arrayval = split(strValueToEnter,";")
  	propertyname = arrayval(0)
  	propertyval = arrayval(1)
  	iRet = false

	set objTestObject=Eval(Create_OR_Object(UIName,""))
'	Set objEmail = Eval(Create_OR_Object("ManageNoti_email_toggle",""))


	If objTestObject.exist(3)  Then
		Select Case lcase(propertyname)
			Case  "checked"
				If UIName = "managenoti_emailnoti_obj" Then
					Set objCheck = Eval(Create_OR_Object("ManageNoti_email_toggle",""))
				ElseIf UIName = "managenoti_pushnoti_obj" Then
					Set objCheck = Eval(Create_OR_Object("ManageNoti_push_toggle",""))
				End If
				
				If lcase(propertyval)= 0 Then
					if lcase(objCheck.GetROProperty("checked"))= 0 Then
		  				iRet = True
		  				descript="object <b>["&UIName&"]</b> is <font color=blue>unchecked</font>"
		  				call logging("object ["&UIName&"] is unchecked",c_DEBUG,3)
		  			Else
		  				descript="object <b>["&UIName&"]</b> is <font color=red>checked</font>"
		  				call logging("object ["&UIName&"] is checked",c_DEBUG,3)
		  			End If
				ElseIf lcase(propertyval)= 1 Then
					If lcase(objCheck.GetROProperty("checked"))= 1 Then
		  				iRet = True
		  				descript="object <b>["&UIName&"]</b> is <font color=blue>checked</font>"
		  				call logging("object ["&UIName&"] is checked",c_DEBUG,3)
		  			Else
		  				iRet = False
		  				descript="object <b>["&UIName&"]</b> is <font color=red>unchecked</font>"
		  				call logging("object ["&UIName&"] is unchecked",c_DEBUG,3)
		  			End If
		  		End If
		  	Case "isenabled"
		End Select
	Else
		iRet = False
		Environment("TS_ErrorFound") = 1
	    Environment("TS_ErrorDescption") = "Not Found Object ["&Mobile_Object_t&"]" 
  		descript = Environment("TS_ErrorDescption")
	End If	
	MC_CheckProperty = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	

End Function  

'**********************************************************************************************************************************
'*  Function Name :	MC_changelimitadmin()
'*  Purpose:        Change Limit On Admin Web
'*  Arguments:      strValueToEnter
'* 	Return Value:   -
'* 	Author By		Thakoon L. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_changelimitadmin(strValueToEnter)
  		Activity="Change limit on FastEasy Admin"
	Const sModule = "MC_changelimitadmin"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	SystemUtil.CloseProcessByName "chrome.exe"
	SystemUtil.Run "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", "https://panelbeanu.se.scb.co.th/", "", "", 3
	Browser("FastEasyAdmin").Page("SCB Admin Panel").WebEdit("FastEasyAdmin_username_data").WaitProperty "visible", True
	Browser("FastEasyAdmin").Page("SCB Admin Panel").WebEdit("FastEasyAdmin_username_data").Set "UATFast14"
	Browser("FastEasyAdmin").Page("SCB Admin Panel").WebEdit("FastEasyAdmin_password_data").Set "Easy1234!"
	Browser("FastEasyAdmin").Page("SCB Admin Panel").WebButton("FastEasyAdmin_login_btn").Click
	Browser("FastEasyAdmin").Page("SCB Admin Panel_2").WebElement("FastEasyAdmin_welcome_txt").WaitProperty "visible", True
	Browser("FastEasyAdmin").Page("SCB Admin Panel_2").Link("FastEasyAdmin_searchcustomer_lnk").Click
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_searchcustomer_txt").WaitProperty "visible", True
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebEdit("FastEasyAdmin_cardid_data").Set "EP5912194"
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebButton("FastEasyAdmin_search_btn").Click
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_nameenglish_txt").WaitProperty "visible", True
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_nameenglish_data").WaitProperty "visible", True
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").Link("FastEasyAdmin_limits_lnk").Click
	Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").WaitProperty "visible", True
	
	arrayval = split(strValueToEnter,";")
	
	If ubound(arrayval) = 1 Then
		acctype = arrayval(0)
		amountval = arrayval(1)
		
		If lcase(acctype) = "own scb" Then
			rowsubfunction = "OWN"
		ElseIf lcase(acctype) = "other scb" Then
			rowsubfunction = "SCB"
		ElseIf lcase(acctype) = "other bank" Then
			rowsubfunction = "OTHER"
		ElseIf lcase(acctype) = "promptpay" Then
			rowsubfunction = "PROMPTPAY"
		End If
		
		
		rowfunction = "TRANSFER"
		rowlimittype = cint(Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").GetRowWithCellText("PER_CUSTOMER_PER_DAY"))
		rowcount = cint(Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").RowCount)
		colcount = Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").ColumnCount(rowcount)
		rowfound = colfound = 0
		
		
		For Itr = rowlimittype To rowcount Step 1
			If Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").GetCellData(Itr,2) = rowfunction Then		
				If Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebTable("FasyEasyAdmin_limittype_tab").GetCellData(Itr,3) = rowsubfunction Then
					Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebEdit("FastEasyAdmin_limitamout_data").SetTOProperty "xpath", "//TABLE[@id=""customerLimitsTable""]/TBODY[1]/TR["&Itr-1&"]/TD[5]/INPUT[1]"
					Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebEdit("FastEasyAdmin_limitamout_data").RefreshObject
					Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebEdit("FastEasyAdmin_limitamout_data").Set amountval
					Exit for
				End If	
			End If
		Next
		Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebButton("FastEasyAdmin_savechanges_btn").Click
		
		If Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_nochanges_txt").exist(2) Then
			descript="cannot input data"
			call logging("cannot input data",c_DEBUG,3)
		Else
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_newlimit_txt").SetTOProperty "innertext", amountval
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_newlimit_txt").RefreshObject
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_newlimit_txt").WaitProperty "visible", true
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebButton("FastEasyAdmin_ok_btn").Click
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebElement("FastEasyAdmin_savedsuccessfull_txt").WaitProperty "visible", true
			Browser("FastEasyAdmin").Page("SCB Admin Panel_3").WebButton("FastEasyAdmin_ok_btn").Click
			descript="change limit successfully"
			call logging("change limit successfully",c_DEBUG,3)
			iret = true
		End If
		SystemUtil.CloseProcessByName "chrome.exe"

	Else
		descript="invalid input data"
		call logging("invalid input data",c_DEBUG,3)
		iRet=False
  	End If
	
	MC_changelimitadmin = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
  	
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_EasynetLogin()
'*  Purpose:        Log in to easynet
'*  Arguments:      strValueToEnter
'* 	Return Value:   -
'* 	Author By		Chanarak P. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_EasynetLogin(strValueToEnter)
	On Error Resume Next

	Activity = "Log in to easynet" 
	Const sModule = "MC_EasynetLogin"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	 SplitVar=split(strValueToEnter,";")
	 id=SplitVar(0)
	 password=SplitVar(1)

	 
	SystemUtil.Run "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", "https://www.scbeasy.com/v1.4/site/presignon/index.asp", "", "", 3
	Browser("SCBEASY").Page("SCBEasy.com").WebEdit("ezn_LOGIN").WaitProperty "visible", True
	Browser("SCBEASY").Page("SCBEasy.com").Sync
'	Browser("SCBEASY").Page("SCBEasy.com").Link("EnglishLang_Btn").Click

	Browser("SCBEASY").Page("SCBEasy.com").WebEdit("ezn_LOGIN").Set id
	Browser("SCBEASY").Page("SCBEasy.com").WebEdit("ezn_PASSWD").Set password
	Browser("SCBEASY").Page("SCBEasy.com").Image("EZNLogin_Btn").Click
	Browser("SCBEASY").Page("SCBEasy.com").Image("EZNinvestment_Btn").Click
	
	ClientID = Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").WebList("eznClientID_Txt").GetItem(1)
	
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_1").Image("eznClientID_OK_Btn").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_2").Image("DisclaimerHeader_Txt").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_2").Image("DisclaimerConfirm_Btn").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Image("SCBEasyFundHeader_Txt").WaitProperty "visible", True

	iret = true
	descript = "Login to easynet successful"
  	MC_EasynetLogin = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)  	
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_PurchaseFund()
'*  Purpose:        Purchase fund on easynet
'*  Arguments:      strValueToEnter
'* 	Return Value:   -
'* 	Author By		Chanarak P. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_PurchaseFund(strValueToEnter)
	On Error Resume Next

	Activity = "Purchase fund on easynet" 
	Const sModule = "MC_PurchaseFund"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	 SplitVar=split(strValueToEnter,";")
	 OrderType=SplitVar(0)
	 FundName=SplitVar(1)
	 PurchaseAmount=SplitVar(2)
	 PurchaseAmount=FormatNumber(PurchaseAmount,,,,0)
	 
	 If lcase(OrderType) = "today" Then
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Today_Purchase_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("PurchasePage").WebList("eznPurchasedFund_ddl").Select FundName
		Browser("SCBEASY").Page("SCBEasy.com").Frame("PurchasePage").Image("eznPurchaseNext_Btn").Click
	 else 
	 	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Awaiting_Purchase_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_4_AwaitingPage").WebRadioGroup("AwaitingDateSet_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_4_AwaitingPage").Image("AwaitingDate_OK_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("PurchasePage").WebList("eznPurchasedFund_ddl").Select FundName
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
	 End If 	

'		Browser("SCBEASY").Page("SCBEasy.com").Frame("PurchasePage").WebList("eznAcc4Purchase_Obj").Select "ออมทรัพย์ - 4050498841"
		Browser("SCBEASY").Page("SCBEasy.com").Frame("PurchasePage").WebEdit("eznPurchaseAmount_Obj").Set PurchaseAmount
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("z2_ReviewPage").WebRadioGroup("ConfirmSlip_Rad").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("z3_Success").WebElement("ezn_SuccessHeader_Txt").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("z3_Success").Image("eznBackToMain_Btn").Click	
	 
	SystemUtil.CloseProcessByName "chrome.exe"		

	iret = true
	descript = "Purchase fund from scbeasy successful"
	MC_PurchaseFund = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult) 
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_RedeemFund()
'*  Purpose:        Redeem fund on easynet
'*  Arguments:      strValueToEnter
'* 	Return Value:   -
'* 	Author By		Chanarak P. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_RedeemFund(strValueToEnter)
	On Error Resume Next

	Activity = "Redeem fund on easynet" 
	Const sModule = "MC_RedeemFund"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	
	SplitVar=split(strValueToEnter,";")
	OrderType=SplitVar(0)
	FundName=SplitVar(1)
	RedeemType=SplitVar(2)
	PurchaseAmount=SplitVar(3)
	PurchaseAmount=FormatNumber(PurchaseAmount,,,,0)
	 
	If lcase(OrderType) = "today" Then
	 	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Today_Redeem_Btn").Click
	else
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Awaiting_Redeem_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_4_AwaitingPage").Image("AwaitingDate_OK_Btn").Click
	End If
	
''----------cooltime for slow webpage----------,,
		timeout = 90
	For Iterator = 1 To timeout Step 1
		if Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebElement("eznRedemptionHeader_Txt").Exist(1) then
		Exit for
		end if
		print "waiting obj time "&Iterator
	Next
''----------cooltime for slow webpage----------,,

	If lcase(RedeemType) = "unit" Then
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebList("eznRedeemFundName_Obj").Select FundName
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebRadioGroup("eznRedeemByUnit_Rad").Select(2)
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebEdit("eznRedeemByUnit_Obj").Set PurchaseAmount
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").Image("eznRedeem_Next_Btn").Click	
	else
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebList("eznRedeemFundName_Obj").Select FundName
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebRadioGroup("eznRedeemByTHB_Rad").Select(1)
		Browser("SCBEASY").Page("SCBEasy.com").Frame("RedeemPage").WebEdit("eznRedeemByTHB_Obj").Set PurchaseAmount
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
	End If
	
	Browser("SCBEASY").Page("SCBEasy.com").Frame("z2_ReviewPage").WebRadioGroup("ConfirmSlip_Rad").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("z3_Success").WebElement("ezn_SuccessHeader_Txt").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("z3_Success").Image("eznBackToMain_Btn").Click	
SystemUtil.CloseProcessByName "chrome.exe"

	iret = true
	descript = "Redeem fund from scbeasy successful"
	MC_RedeemFund = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult) 	
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_SwitchFund()
'*  Purpose:        Switch fund on easynet
'*  Arguments:      strValueToEnter
'* 	Return Value:   -
'* 	Author By		Chanarak P. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_SwitchFund(strValueToEnter)
	On Error Resume Next

	Activity = "Switch fund on easynet" 
	Const sModule = "MC_SwitchFund"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	SplitVar=split(strValueToEnter,";")
	OrderType=SplitVar(0)
	FundOut=SplitVar(1)
	FundIn=SplitVar(2)
	SwitchType=SplitVar(3)
	SwitchAmount=SplitVar(4)
	SwitchAmount=FormatNumber(SwitchAmount,,,,5)
  	
  	If lcase(OrderType) = "today" Then
	 	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Today_Switch_Btn").Click
	else
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_3_FundPage").Link("Awaiting_Switch_Btn").Click
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_4_AwaitingPage").Image("AwaitingDate_OK_Btn").Click
	End If
	
		''----------cooltime for slow webpage----------,,
		 If Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebElement("eznSwitchHeader_Txt").Exist(90)  Then
		 End If
		 ''----------cooltime for slow webpage----------,,

		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebList("eznSwitchOutName_Obj").Select FundOut
			For j = 1 To 90
				count = Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebList("eznSwitchInName_Obj").GetROProperty("items count")
				If count > 1 Then
					Exit for
				Else 
					wait 1
				End If				
			Next			
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebList("eznSwitchInName_Obj").Select FundIn

	If lcase(SwitchType) = "unit" Then
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebRadioGroup("eznSwitchByUnit_Rad").Select(2)
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebEdit("eznSwitchByUnit_Obj").Set SwitchAmount
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").Image("eznSwitch_Next_Btn").Click
	else
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebRadioGroup("eznSwitchByTHB_Rad").Select(1)
		Browser("SCBEASY").Page("SCBEasy.com").Frame("SwitchPage").WebEdit("eznSwitchByTHB_Obj").Set SwitchAmount
		Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
	End If

	Browser("SCBEASY").Page("SCBEasy.com").Frame("z2_ReviewPage").WebRadioGroup("ConfirmSlip_Rad").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("body_0").Image("eznFund_Next_Btn").Click
	Browser("SCBEASY").Page("SCBEasy.com").Frame("z3_Success").WebElement("ezn_SuccessHeader_Txt").Click
SystemUtil.CloseProcessByName "chrome.exe"
  	
	iret = true
	descript = "Switch fund from scbeasy successful"
	MC_RedeemFund = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult) 	
End Function


