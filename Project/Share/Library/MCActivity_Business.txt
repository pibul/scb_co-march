
'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_LoginCCB()
'*  Purpose:        Login to CCB Application
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_LoginCCB(strValueToEnter,ExpectedResult)
	On Error Resume Next
	Activity="Login to CCB App"
	Const sModule = "MC_BIZ_LoginCCB"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	PinLogin = trim(strValueToEnter)
  	If len(PinLogin) = 6 Then
 		OSType = Environment("OSType")
  		set objLoginObject=Create_OR_Object("MC_PinNumber_Btn")
  		Flag = CheckObjectExist(objLoginObject,"")
  		If Flag = 1 Then 		
  		
			For i = 1 To len(PinLogin) Step 1	
				Pwd_Number=Mid(PinLogin,i,1)
				objLoginObject.SetTOProperty "text",Pwd_Number			  										
				objLoginObject.Tap
				objLoginObject.RefreshObject
				call logging("Click Number : " & Pwd_Number,c_DEBUG,3)			
			Next
			
			If lcase(OSType) = "android" Then
				set NextLoginObject = Create_OR_Object("MC_PinNext_Btn")
				NextLoginObject.Click
				set NextLoginObject = Nothing
			End If
			
			iRet = True	
			descript="login done"
			set objLoginObject= nothing
		Else
			c_DEBUG = "error"
			iRet = False	
			Environment("TS_ErrorFound") = 1
			Environment("TS_ErrorDescption") = "not found object"
			descript = Environment("TS_ErrorDescption")		
		End If
  	Else
  		c_DEBUG = "error"
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Input invalid data" 
  		descript = Environment("TS_ErrorDescption")
		iRet=False		
	End If
	
	call logging(descript,c_DEBUG,3)
	
	MC_BIZ_LoginCCB = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_EnterPin()
'*  Purpose:        Enter PIN 
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_EnterPin(strValueToEnter,ExpectedResult)
	On Error Resume Next
	Activity="Enter PIN"
	Const sModule = "MC_BIZ_EnterPin"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	PinLogin = trim(strValueToEnter)
  	If len(PinLogin) = 6 Then
  		set objLoginObject=Create_OR_Object("Approve_PinNumber_Btn")
  		Flag = CheckObjectExist(objLoginObject,"")
  		If Flag = 1 Then 		
  		
			For i = 1 To len(PinLogin) Step 1	
				Pwd_Number=Mid(PinLogin,i,1)
				objLoginObject.SetTOProperty "text",Pwd_Number	
'				objLoginObject.SetTOProperty "resourceid","com.mobile.banking.scb:id/key"&Pwd_Number			  										
				objLoginObject.Tap
				objLoginObject.RefreshObject
				call logging("Click Number : " & Pwd_Number,c_DEBUG,3)			
			Next
						
			iRet = True	
			descript="Enter PIN ["&strValueToEnter&"]done"
			set objLoginObject= nothing
		Else
			c_DEBUG = "error"
			iRet = False	
			Environment("TS_ErrorFound") = 1
			Environment("TS_ErrorDescption") = "not found object"
			descript = Environment("TS_ErrorDescption")		
		End If
  	Else
  		c_DEBUG = "error"
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Input invalid data" 
  		descript = Environment("TS_ErrorDescption")
		iRet=False		
	End If
	
	call logging(descript,c_DEBUG,3)
	
	MC_BIZ_EnterPin = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_EnableFilterBtn_ANDROID()
'*  Purpose:        Enable filter state button
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_EnableFilterBtn_ANDROID(strValueToEnter,UIName,ExpectedResult)
'	On Error Resume Next
	Activity="enable filter state button"
	Const sModule = "MC_BIZ_EnableFilterBtn"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set objTestObject=Create_OR_Object(UIName)
  	Flag = CheckObjectExist(objTestObject,"")
  	If Flag = 1 Then 	
  		strFilePath = Environment("RootProject")&"\Share\ExternalLibrary\Screenshot01.png"
  		
  		'Check File Exist
  		Set fso = CreateObject("Scripting.FileSystemObject")
		If (fso.FileExists(strFilePath)) Then
		   fso.DeleteFile strFilePath
		End If
		Set fso = Nothing
		
		'Capture Screenshot
  		Set Parent_Object = Eval(Environment("ParentOR"))

  		Parent_Object.CaptureBitmap ""&strFilePath&""	
  		Set Parent_Object = Nothing

		'Get Coordinate Top and Left
  		co_y = CInt(objTestObject.GetRoProperty("top")+10)
  		co_x = CInt((objTestObject.GetRoProperty("right") - objTestObject.GetRoProperty("left"))/2)+CInt(objTestObject.GetRoProperty("left"))
  		
  		call logging("co_x : "&co_x,c_DEBUG,3)
  		call logging("co_y : "&co_y,c_DEBUG,3)
  		
  		'Define Expect Color
  		ColorExpect = "Orange"
  		
  		'Call Image Check color Func.
  		ret_Ext = CheckImageColor(strFilePath,co_x,co_y,ColorExpect)
		
		'If not match => Click Btn
  		If ret_Ext = 1 Then
  			objTestObject.Tap
  			descript = "enabled filter button ["&UIName&"] done"	
		Else
			descript = "this filter button ["&UIName&"] is already enabled"			
  		End If
		iRet = True
  	Else
  		c_DEBUG = "error"
		iRet = False	
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "not found object"
		descript = Environment("TS_ErrorDescption")		
	End If	
	
	set objTestObject=Nothing
	call logging(descript,c_DEBUG,3)
	
  	MC_BIZ_EnableFilterBtn_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_DisableFilterBtn_ANDROID()
'*  Purpose:        Disable filter state button
'*  Arguments:      strValueToEnter,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_DisableFilterBtn_ANDROID(strValueToEnter,UIName,ExpectedResult)
	On Error Resume Next
	Dim Activity
	Activity="disable filter state button"
	Const sModule = "MC_BIZ_DisableFilterBtn"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	set objTestObject=Create_OR_Object(UIName)
  	Flag = CheckObjectExist(objTestObject,"")
  	If Flag = 1 Then 	
  		strFilePath = Environment("RootProject")&"\Share\ExternalLibrary\Screenshot01.png"
  		
  		'Check File Exist
  		Set fso = CreateObject("Scripting.FileSystemObject")
		If (fso.FileExists(strFilePath)) Then
		   fso.DeleteFile strFilePath
		End If
		Set fso = Nothing
		
		'Capture Screenshot
		Set Parent_Object = Eval(Environment("ParentOR"))
  		Parent_Object.CaptureBitmap ""&strFilePath&""
  		Set Parent_Object = Nothing

		'Get Coordinate Top and Left
  		co_y = CInt(objTestObject.GetRoProperty("top")+10)
  		co_x = CInt((objTestObject.GetRoProperty("right") - objTestObject.GetRoProperty("left"))/2)+CInt(objTestObject.GetRoProperty("left"))
  		
  		call logging("co_x : "&co_x,c_DEBUG,3)
  		call logging("co_y : "&co_y,c_DEBUG,3)
  		
  		'Define Expect Color
  		ColorExpect = "Orange"
  		
  		'Call Image Check color Func.
  		ret_Ext = CheckImageColor(strFilePath,co_x,co_y,ColorExpect)
		
		'If not match => Click Btn
  		If ret_Ext = 0 Then
  			objTestObject.Tap
  			descript = "disabled filter button ["&UIName&"] done"	
		Else
			descript = "this filter button ["&UIName&"] is already disabled"			
  		End If
		iRet = True
  	Else
  		c_DEBUG = "error"
		iRet = False	
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "not found object"
		descript = Environment("TS_ErrorDescption")		
	End If	
	
	call logging(descript,c_DEBUG,3)
	set objTestObject=Nothing
  	MC_BIZ_DisableFilterBtn_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  	
End Function


'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_SelectDatePicker_ANDROID()
'*  Purpose:        Select Date Picker (ANDROID)
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_SelectDatePicker_ANDROID(strValueToEnter,ExpectedResult)
	On Error Resume Next
	Activity="select Date Picker"
	Const sModule = "MC_BIZ_SelectDatePicker"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	strValueToEnter = trim(strValueToEnter)
  	If strValueToEnter <> "" Then	

		splitVar = split(trim(strValueToEnter),"/") 
		DatePicker_Day = trim(splitVar(0))
		DatePicker_Month = trim(splitVar(1))
		DatePicker_Year = trim(splitVar(2))
		
		If len(trim(DatePicker_Day)) < 2 Then
			DatePicker_Day = "0"&DatePicker_Day
		End If		
		
		set Main_Object=Create_OR_Object("DatePicker_MainObject_Obj")
		set ShowDate_Object=Create_OR_Object("DatePicker_SelectDay_Txt")
		set ShowMonth_Object=Create_OR_Object("DatePicker_ShowSelectMonth_Txt")
		set ShowYear_Object=Create_OR_Object("DatePicker_ShowYear_Txt")
		set Ok_Object=Create_OR_Object("DatePicker_OK_Btn")
		
		'if date not same input data
		If DatePicker_Year <> ShowYear_Object.GetRoProperty("text") Then
			ShowYear_Object.Tap
			set SelectYear_Object = Create_OR_Object("DatePicker_SelectYear_Btn")
			SelectYear_Object.SetToProperty "text",DatePicker_Year
			SelectYear_Object.Tap
			set SelectYear_Object = Nothing
		End If
		
		Current_Month = lcase(trim(ShowMonth_Object.GetRoProperty("text")))
		Date_Language = CheckLanguage(Current_Month)
		call logging("DatePicker Current Month : "&Current_Month,c_DEBUG,3)
		
			Select Case DatePicker_Month
				Case "1","01"
				If Date_Language="EN" Then
					Month_Select="January"
				Else
					Month_Select="มกราคม"
				End If

				Case "2","02"
				If Date_Language="EN" Then
					Month_Select="February"
				Else
					Month_Select="กุมภาพันธ์"
				End If

				Case "3","03"
				If Date_Language="EN" Then
					Month_Select="March"
				Else
					Month_Select="มีนาคม"
				End If

				Case "4","04"
				If Date_Language="EN" Then
					Month_Select="April"
				Else
					Month_Select="เมษายน"
				End If

				Case "5","05"
				If Date_Language="EN" Then
					Month_Select="May"
				Else
					Month_Select="พฤษภาคม"
				End If

				Case "6","06"
				If Date_Language="EN" Then
					Month_Select="June"
				Else
					Month_Select="มิถุนายน"
				End If

				Case "7","07"
				If Date_Language="EN" Then
					Month_Select="July"
				Else
					Month_Select="กรกฎาคม"
				End If

				Case "8","08"
				If Date_Language="EN" Then
					Month_Select="August"
				Else
					Month_Select="สิงหาคม"
				End If

				Case "9","09"
				If Date_Language="EN" Then
					Month_Select="September"
				Else
					Month_Select="กันยายน"
				End If

				Case "10"
				If Date_Language="EN" Then
					Month_Select="December"
				Else
					Month_Select="ตุลาคม"
				End If

				Case "11"
				If Date_Language="EN" Then
					Month_Select="November"
				Else
					Month_Select="พฤศจิกายน"
				End If

				Case "12"
				If Date_Language="EN" Then
					Month_Select="December"
				Else
					Month_Select="ธันวาคม"
				End If
				
			End Select 	
		
			Select Case Current_Month
		
				Case "jan","ม.ค."						
						Month_Select_Num = 1			
				Case "feb","ก.พ."
						Month_Select_Num = 2
				Case "mar","มี.ค."
						Month_Select_Num = 3
				Case "apr","เม.ย."
						Month_Select_Num = 4
				Case "may","พ.ค."
						Month_Select_Num = 5
				Case "jun","มิ.ย."
						Month_Select_Num = 6					
				Case "jul","ก.ค."
						Month_Select_Num = 7
				Case "aug","ส.ค."
						Month_Select_Num = 8
				Case "sep","ก.ย."
						Month_Select_Num = 9
				Case "oct","ต.ค."
						Month_Select_Num = 10
				Case "nov","พ.ย."
						Month_Select_Num = 11
				Case "dec","ธ.ค."
						Month_Select_Num = 12
			End Select 		

		If cint(DatePicker_Month) <> cint(Month_Select_Num) Then
		
			set Main_Object=Create_OR_Object("DatePicker_MainObject_Obj")						
			If cint(DatePicker_Month) > cint(Month_Select_Num) Then	
				swipe_round = cint(DatePicker_Month) - Cint(Month_Select_Num)	
				Start_X = Main_Object.GetRoProperty("right")/2
				Start_Y = Main_Object.GetRoProperty("bottom") - Main_Object.GetRoProperty("top")-50
				End_X = Start_X
				End_Y = "90"
				x_velocity ="0.029061"
				y_velocity ="-0.185544"				
			Else
				swipe_round = cint(Month_Select_Num) - Cint(DatePicker_Month)	
				Start_X = Main_Object.GetRoProperty("right")/2
				Start_Y = "90"
				End_X = Start_X
				End_Y = Main_Object.GetRoProperty("bottom") - Main_Object.GetRoProperty("top")-50
				x_velocity ="0.029061"
				y_velocity ="-0.322122"
			End If

			For i = 1 To swipe_round Step 1
				Main_Object.Pan Start_X,Start_Y,End_X,End_Y,x_velocity,y_velocity	
			Next
			
			iRet = True
			set Main_Object=Nothing			
		End If
		
		ShowDate_Object.SetToProperty "accessibilityid" ,".*"&DatePicker_Day&" "&Month_Select&" "&DatePicker_Year&".*"
		ShowDate_Object.Tap	
		Ok_Object.Tap	
		'Check Error Before Input Text
		iRet = CheckError()
		If iRet = False Then
			c_DEBUG = "error"
			Environment("TS_ErrorFound") = 1
			descript = Environment("TS_ErrorDescption")
		Else
			descript = "select date ["&strValueToEnter&"] done"
		End If 
		set Main_Object=Nothing
		set ShowDate_Object=Nothing
		set ShowMonth_Object=Nothing
		set ShowYear_Object=Nothing
		set Ok_Object=Nothing
	Else
		c_DEBUG = "error"
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Input invalid data" 
  		descript = Environment("TS_ErrorDescption")
		iRet=False	
  	End If
  	
  	call logging(descript,c_DEBUG,3)
  		
  	MC_BIZ_SelectDatePicker_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
  	
End Function

'**********************************************************************************************************************************
'*  Function Name :	MC_BIZ_ChangeLanguage_ANDROID()
'*  Purpose:        Change Language Login Page
'*  Arguments:      strValueToEnter
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function MC_BIZ_ChangeLanguage_ANDROID(strValueToEnter,ExpectedResult)
	On Error Resume Next
	Activity="change language"
	Const sModule = "MC_BIZ_ChangeLanguage"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
  	UIName="Login_LanguageSwitch_Btn"
	set objTestObject=Create_OR_Object(UIName)
	
	'Check Object Exist
	Flag = CheckObjectExist(objTestObject,"")

	If Flag=1 Then
		Current_Language = objTestObject.GetRoProperty("text")
		If lcase(trim(strValueToEnter)) = lcase(trim(Current_Language))  Then
			objTestObject.Tap
			descript = "Change Language to ["& strValueToEnter & "] done"
		Else
			descript = "Current Language is already ["& strValueToEnter & "]"
		End If
		iRet = True
	Else
		c_DEBUG = "error"
		Environment("TS_ErrorFound") = 1
		Environment("TS_ErrorDescption") = "Not Found Object ["&UIName&"]" 
	  	descript = Environment("TS_ErrorDescption")
		iRet=False
	End If	

	set objTestObject = Nothing
	call logging(descript,c_DEBUG,3)
	MC_BIZ_ChangeLanguage_ANDROID = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function


Function MapUIName_ANDROID(UIName)

'	SearchText_Red = array("The Thai Red Cross Society","สภากาชาดไทย")
'	AppPackageName="com.mobile.banking.scb"
	AppPackageName = Environment("AppPackage")
	Select Case Trim(UIName)
			
			'Get Mc_index From AccountDetail Page
			Case lcase("AccDetail_TransType_Txt")
				  i_value = GetObject_McIndex_ANDROID("AccDetail_MainList_Obj","resourceid",AppPackageName&":id/operationType")

			Case lcase("AccDetail_Amount_Txt")
				  i_value = GetObject_McIndex_ANDROID("AccDetail_MainList_Obj","resourceid",AppPackageName&":id/accountOperationBalance")

			Case lcase("AccDetail_TransCurrency_Txt")
				  i_value = GetObject_McIndex_ANDROID("AccDetail_MainList_Obj","resourceid",AppPackageName&":id/accountOperationCurrency")

			Case lcase("Auth_CounterPartyName_Txt")
				  i_value = GetObject_McIndex_ANDROID("Auth_MainList_Obj","resourceid",AppPackageName&":id/item_header1")

			Case lcase("Auth_CounterPartyDescript_Txt")
				  i_value = GetObject_McIndex_ANDROID("Auth_MainList_Obj","resourceid",AppPackageName&":id/item_header2")

			Case lcase("Auth_TransactionStatus_Txt")
				  i_value = GetObject_McIndex_ANDROID("Auth_MainList_Obj","resourceid",AppPackageName&":id/listItemStatusIcon")
				  
'			Case lcase("MyFund_MFAccID_Txt")
'				  Ini_McIndex = GetObject_McIndex_ANDROID("myfund","fund_code_text_view","MyFund_MainObject_Obj","")
'				  i_value = Ini_McIndex + 1
				  				  
			Case Else
				  i_value ="::N/A"
				  call logging("Not found UIName for assign mcindex",c_DEBUG,3)
	End Select
	
'	Erase SearchText_Red
	MapUIName_ANDROID = i_value
	
End Function

Function GetObject_McIndex_ANDROID(Main_UIName,ExpectType,ExpectValue)
	
	Set Main_Object = Create_OR_Object(Main_UIName)
	Set oDesc = description.Create()
	oDesc("class").value="Label"
	Set l =  Main_Object.ChildObjects(oDesc)
	n = l.count()
	print "total Child Object : "&n
	
	iRet = False
	For i = 0 To n-1
'	 		l(i).highlight  
			If lcase(ExpectType)="text" Then
				ROProperty="text"
			ElseIf lcase(ExpectType)="resourceid" or lcase(SearchType)="id" Then
				ROProperty="resourceid"
			End If
	 	    judge = l(i).GetRoProperty(ROProperty)
		    mc_index = l(i).GetRoProperty("mcindex")
'		    resourceid = l(i).GetRoProperty("resourceid")
		    If len(judge)>0 Then
		    	call logging("["&i&"] - " &judge &" - " & mc_index,c_DEBUG,3)
		    End If
	 
	 		If lcase(judge) = lcase(ExpectValue) Then
	 			GetMCIndex = mc_index
	 			iRet = True
	 			Exit for
	 		End If     
	Next
	
	Set oDesc = Nothing
	Set Main_Object = Nothing 
	
	GetObject_McIndex_ANDROID = GetMCIndex

End Function 

Function MC_BIZ_CHECK_BALANCE(strValueToEnter,ExpectedResult)

	On Error Resume Next
	Activity="check balance"
	Const sModule = "MC_BIZ_CHECK_BALANCE"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	iRet = False

	SplitVar = Split(strValueToEnter,";")
	Timeout = trim(SplitVar(0))
	PayAmount = trim(SplitVar(1))
	DebitAcc = trim(SplitVar(2))
	BeforeBalance = trim(SplitVar(3))
	
	expect_time = DateAdd("s",Timeout,time())
	
	FlagMyBalanceChange = False
	
	For i = 1 To Timeout Step 1
				
		call logging("wait time "&DateDiff("s",time(),expect_time) &" second",c_DEBUG,3)
		
		If DateDiff("s",time(),expect_time) <=0 Then
			iRet=False
										
			If FlagMyBalanceChange = True Then
				ExpectedResult = "Available Balance(After) = Available Balance(Before)-Total Amount Package-Total Fee Package"
				descript = "Available Account Balance Check Successfully ["&FormatNumber(AfterBalance,2)&"] Not Match ["&FormatNumber(MyAvailableBalance,2)&"]"								
			ElseIf FlagMyBalanceChange = False Then
				ExpectedResult = "Available Balance Comarch(Before) = Available Balance Comarch(After)"
				descript= "Available Balance Comarch(After) ["&ormatNumber(AfterBalance,2)&"] Not Change in ["&Timeout&"] Second"						
			End If

			Exit for
			
		Else
		
			Call MC_SwipeDown_Obj("yes","Account_MainList_Obj","")
			
			AfterBalance = Device("Device").App("Account").MobileLabel("Account_Balance_Txt").GetROProperty("text")
			

			If CDBL(AfterBalance) < CDBL(BeforeBalance) or FormatNumber(PayAmount) = 0.00  Then
			
				FlagMyBalanceChange = True
				
				MyAvailableBalance = CDBL(BeforeBalance)-CDBL(PayAmount)
			
				If FormatNumber(AfterBalance,2) = FormatNumber(MyAvailableBalance,2) Then
					
					Call TE_GetBalanceFromHost(DebitAcc&";CoreBalance","")
					
					If Environment("CoreBalance") = AfterBalance Then
						iRet = True
									
						ExpectedResult = "Check Available Balance Corebank = Available Balance Comarch (After)"
						descript = "Available Balance CoreBank ["&Environment("CoreBalance")&"] match Available Balance Comarch ["&FormatNumber(AfterBalance,2)&"]"							
									
						MC_BIZ_CHECK_BALANCE = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
									
						ExpectedResult = "Available Balance["&FormatNumber(MyAvailableBalance,2)&"] = ["&FormatNumber(BeforeBalance,2)&"]-["&FormatNumber(PayAmount,2)&"]"
						descript = "Available Account Balance Successfully ["&FormatNumber(AfterBalance,2)&"] Match ["&FormatNumber(MyAvailableBalance,2)&"]"								
										
						Exit for
						
					End If
					
				End If
					
			End If
		
		End If
		
		wait 10
		
	Next
	
	call logging(descript,c_DEBUG,3)
	
	MC_BIZ_CHECK_BALANCE = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
	
	
End Function

Function MC_BIZ_Check_StatusAndBalance(strValueToEnter,ExpectedResult)

	On Error Resume Next
	Activity="check Status and Balance"
	Const sModule = "MC_BIZ_Check_StatusAndBalance"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
	iRet = False

	SplitVar = Split(strValueToEnter,";")
	Timeout1 = trim(SplitVar(0))
	Timeout2 = trim(SplitVar(0))
	PayDesc = trim(SplitVar(1))
	PayStatus = trim(SplitVar(2))
	PayAmount = trim(SplitVar(3))
	DebitAcc = trim(SplitVar(4))
	BeforeBalance = trim(SplitVar(5))
		
	expect_time = DateAdd("s",Timeout1,time())
	
'	Device("Device").App("Tutorial_3").MobileLabel("Home_Approval_Tab").Tap
	Device("Device").App("Authorization").MobileLabel("Auth_Search_Btn").Tap
	Device("Device").App("Authorization").MobileEdit("Auth_Search_Inp").Set PayDesc
	
	For i = 1 To Timeout1 Step 1
	
	If iRet = true Then
		Exit for
	End If
			
		call logging("wait time "&DateDiff("s",time(),expect_time) &" second",c_DEBUG,3)
		
		If DateDiff("s",time(),expect_time) <=0 Then
			iRet=False
		
			ExpectedResult = "Check status Change"
			descript = "status excel input ["&PayStatus&"] Not Match ["&GetStatus&"]"								
		
			Exit for
					
		Else

			Call MC_SwipeDown_Obj("yes","Account_MainList_Obj","")
			
			GetStatus = Device("Device").App("Authorization").MobileLabel("Auth_TransactionStatus_Txt").GetROProperty("text")

			If lcase(GetStatus) = lcase(PayStatus) Then
			
				Device("Device").App("MainTabs").MobileLabel("Home_Accounts_Tabs").Tap
				Device("Device").App("Account").MobileLabel("Account_Search_Btn").Tap
				Device("Device").App("Account").MobileEdit("Account_SearchBar_Inp").Set DebitAcc

				FlagMyBalanceChange = False
				
				For j = 1 To Timeout2 Step 1
							
					call logging("wait time "&DateDiff("s",time(),expect_time) &" second",c_DEBUG,3)
					
					If DateDiff("s",time(),expect_time) <=0 Then
						iRet=False
													
						If FlagMyBalanceChange = True Then
							ExpectedResult = "Available Balance(After) = Available Balance(Before)-Total Amount Package-Total Fee Package"
							descript = "Available Account Balance Check Successfully ["&FormatNumber(AfterBalance,2)&"] Not Match ["&FormatNumber(MyAvailableBalance,2)&"]"								
						ElseIf FlagMyBalanceChange = False Then
							ExpectedResult = "Available Balance Comarch(Before) = Available Balance Comarch(After)"
							descript= "Available Balance Comarch(After) ["&ormatNumber(AfterBalance,2)&"] Not Change in ["&Timeout&"] Second"						
						End If
			
						Exit for
						
					Else
					
						Call MC_SwipeDown_Obj("yes","Account_MainList_Obj","")
						
						AfterBalance = Device("Device").App("Account").MobileLabel("Account_Balance_Txt").GetROProperty("text")
						
			
						If CDBL(AfterBalance) < CDBL(BeforeBalance) or FormatNumber(PayAmount) = 0.00  Then
						
							FlagMyBalanceChange = True
							
							Device("Device").App("Tutorial_3").MobileLabel("Home_Approval_Tab").Tap
							Device("Device").App("Authorization").MobileLabel("Auth_Search_Btn").Tap
							Device("Device").App("Authorization").MobileEdit("Auth_Search_Inp").Set PayDesc				
							Device("Device").App("Authorization").MobileLabel("Auth_TransactionStatus_Txt").Tap
							
							Call MC_SwipeUp_Obj("2","Account_MainList_Obj","")
							
							GetFee = Device("Device").App("Tutorial_3").MobileLabel("Amount_Free_Payment_Txt").GetROProperty("text")
							
							Call MC_ConvertToNumber(GetFee&";MyFeeConvert","")
							
							Summary_Amount = PayAmount + CDBL(Environment("MyFeeConvert"))						
							
							MyAvailableBalance = CDBL(BeforeBalance)-CDBL(Summary_Amount)
						
							Device("Device").App("Tutorial_3").MobileButton("GetFree_Close_Btn").Tap

						
							If FormatNumber(AfterBalance,2) = FormatNumber(MyAvailableBalance,2) Then
								
								Call TE_GetBalanceFromHost(DebitAcc&";CoreBalance","")
								
								If cdbl(Environment("CoreBalance")) = cdbl (AfterBalance)Then
									iRet = True
												
									ExpectedResult = "Check Available Balance Corebank = Available Balance Comarch (After)"
									descript = "Available Balance CoreBank ["&Environment("CoreBalance")&"] Match Available Balance Comarch ["&FormatNumber(AfterBalance,2)&"]"							
												
									MC_BIZ_Check_StatusAndBalance = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)
												
									ExpectedResult = "Available Balance["&FormatNumber(MyAvailableBalance,2)&"] = ["&FormatNumber(BeforeBalance,2)&"]-["&FormatNumber(PayAmount,2)&"]"
									descript = "Available Account Balance Successfully ["&FormatNumber(AfterBalance,2)&"] Match ["&FormatNumber(MyAvailableBalance,2)&"]"								
													
									Exit for
								Else
									iRet = False
												
									ExpectedResult = "Check Available Balance Corebank = Available Balance Comarch (After)"
									descript = "Available Balance CoreBank ["&Environment("CoreBalance")&"] Not Match Available Balance Comarch ["&FormatNumber(AfterBalance,2)&"]"							
									
									Exit for
								End If
							Else
									iRet = False
																							
									ExpectedResult = "Available Balance["&FormatNumber(MyAvailableBalance,2)&"] = ["&FormatNumber(BeforeBalance,2)&"]-["&FormatNumber(PayAmount,2)&"]"
									descript = "Available Account Balance Successfully ["&FormatNumber(AfterBalance,2)&"] Not Match ["&FormatNumber(MyAvailableBalance,2)&"]"								
													
									Exit for
										
							End If
								
						End If
					
					End If
					
					wait 10
					
				Next
				
			End If
		
		
		End If 
		
	    wait 10

	Next

	call logging(descript,c_DEBUG,3)
	
	MC_BIZ_Check_StatusAndBalance = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

End Function
