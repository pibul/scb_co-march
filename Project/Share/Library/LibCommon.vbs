'Option Explicit

' ************************************* MAINTAIN THIS HEADER *************************************
' Library Name.......: libCommon
' Library Type.......: Module
' Purpose............: To handle on Common function
' Author By..........: SUEBSAKUL A. (AMS-GABLE)
' Created on.........: 2016-08-31
' ---------------------------------------------------------
'
'   Copyright (c) 2016 By GABLE Company Limited
'   Confidential - All Rights Reserved
'
' ---------------------------------------------------------
'
'  ###### HISTORY INFORMATION ######
'  Date            ByWho		Description
'  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'   2016-08-31		K			1) Created a new function
'
' ************************************* END MAINTAIN THIS HEADER *********************************

' ******************************************
' *** PRIVATE CONSTANT VARIABLES MODULE  ***
' ******************************************
Private Const m_sMODULE_NAME_FUNC = "LIBF"
Private m_oFso ', m_oStream
Private re

' ******************************************
' *** PUBLIC CONSTANT VARIABLES MODULE  ***
' ******************************************
Public Const m_sDEBUG = 1
Public Const m_sLevel = 3
Public Const c_DEBUG  = "D"
Public Const c_INFORM = "I"
Public s_LogMsg : s_LogMsg = ""


Public sub logging(Message, iType, iLevel)
	On Error Resume Next
	Const sModule = "logging"
	
	'Log to Console
'	If (m_sDEBUG) and (m_sLevel >= iLevel) Then
	
		logTCName = " ["&Environment("TestCaseName")&"]"
		If Err.Number <> 0 Then
			logTCName = ""
			Err.Clear
		End If
		
		If lcase(iType) = "error" Then
			logType = " [ERROR]"
		Else
			logType = " [INFO]"
		End If
		
		LogMsg = formatdatetime(now) &logTCName & logType&" : " & Message
		print LogMsg

		logfileName = Environment("TestScenarioName")		
		If Err.Number <> 0 Then
			logfileName = ""
			WriteAction = False
			Err.Clear
		Else
			WriteAction = True
		End If
		
		
'		If WriteAction = True and len(logfileName) > 0 Then
'			Call WriteLogToFile(LogMsg,logfileName)
'		End If
		
'	End If

end sub

Public Function excel_DataSheetExists(sheetName)
	Const sModule = "excel_DataSheetExists"
    call logging("sModule " & sMODULE,c_DEBUG,2)
	
	Dim Flag : Flag = True
	On error resume next

	Err.clear
	Set objSheet= DataTable.GetSheet(sheetName)
	'In case error occured sheet does not exist
	If err.number <> 0 then
		call logging("Not found Sheet : " & sheetName,c_DEBUG,3)
		Flag = FALSE
	End if
	
	Set objSheet = nothing
	excel_DataSheetExists = Flag
	
End Function

Public Function excel_DataSheetDelete(sheetName)
	Const sModule = "excel_DataSheetDelete"
    call logging("sModule " & sMODULE,c_DEBUG,2)	
    
    Dim Flag : Flag = True
    
    Flag = excel_DataSheetExists(sheetName)
    If Flag = True Then
    
    	Err.clear
    	
    	DataTable.DeleteSheet(sheetName)
    	
    	'Error Delete Sheet
	 	If err.number <> 0 then
			Flag = FALSE
		Else 
			Flag = True
		End if
    End If
    
    Set objSheet = nothing
    excel_DataSheetDelete = Flag
End Function

Public Sub MC_Wait()
	Const sModule = "MC_Wait"
    call logging("sModule " & sMODULE,c_DEBUG,2)	
	
	call logging("DefaultWait :  " & Environment("DefaultWait") &" sec(s)..",c_DEBUG,3)
	
	Wait(Environment("DefaultWait"))
	
End Sub 

Public Sub RenameFile(oldFilename,newFilename)
	Const sModule = "MoveFile"
    call logging("sModule " & sMODULE,c_DEBUG,2)
    
	Set ObjFSO = CreateObject("Scripting.FileSystemObject")
	
	call logging("Rename file : " & oldFilename & " to " & newFilename,c_DEBUG,2)
	ObjFSO.MoveFile oldFilename, newFilename
	
	Set ObjFSO = nothing

End Sub

'**********************************************************************************************************************************
'*  Function Name : CreateFormTestCase()
'*  Purpose:        Create new form testcase template from testscenario    
'*  Arguments:      ScenarioPath,SheetName
'* 	Return Value:   none
'* 	Author By		ANUPOL H. (AMS-GABLE)
'* 	How to use		CreateFormTestCase (Testscenario_path,Testscenario_SheetName)
'*****************************************************************************************************************

Public Function CreateFormTestCase(ScenarioPath,SheetName)

	print "ScenarioPath " &ScenarioPath
	Set objFileExcel= CreateObject("Excel.Application")
	Set objWB_x = objFileExcel.Workbooks.Open (ScenarioPath) 
	objFileExcel.Application.Visible = True
	objFileExcel.Application.DisplayAlerts = False
	
 		set mysheet_x = objFileExcel.ActiveWorkbook.Worksheets(SheetName)
 		
 		cols= mysheet_x.UsedRange.Columns.Count
		row = mysheet_x.UsedRange.Rows.Count
		
		TestCaseNameCol =  0
		
		Set TestCaseNameArr = createObject("System.Collections.ArrayList")

		'Get column from testcase name 
		For i = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,i).value) = lcase("TestCaseName") Then
				print mysheet_x.cells(1,i).value
				TestCaseNameCol=i
				Exit For
			End If 
			
		Next
		
		'Add testcasename in Array List
		For j = 2 To row Step 1		
			testcasename = mysheet_x.cells(j,TestCaseNameCol).value
			If testcasename<>"EndOfRow" and TestCaseNameArr.Contains(testcasename)=False Then
				TestCaseNameArr.Add(testcasename)
			End If		
		Next
				
		'Close Test Scenario
'		objFileExcel.ActiveWorkbook.Close
'		Set mysheet_x = Nothing
'		Set objWB_x = Nothing
		
		'Get folder path from testScenario path
		segments = Split(ScenarioPath,"\")
		For k = 0 To UBound(segments)-1 Step 1
			If k = 0  Then
				TS_FolderPath = segments(k)
			Else
				TS_FolderPath=TS_FolderPath&"\"& segments(k)
			End If			
		    
		Next
		
		'Get module from TestScenario path
		segment_module = Split(segments(UBound(segments)),"_")
		module = segment_module(1)
		
		'Set TestCase and TestData Name
		TC_Path=TS_FolderPath&"\TestCase_"&module
		
		print TS_FolderPath
		
		'Create file system for check file exist in path
		Set FileSys = CreateObject("Scripting.FileSystemObject")
				
		If FileSys.FileExists(TC_Path) Then
			print "Found exist file in path "&TC_Path
			TC_CreateAction = False
		Else
			print "Create TestCase file in path "&TC_Path
			TC_CreateAction = True
		End If
			
		'Create TestCase File
		If TC_CreateAction=True Then
		
			TC_Num=0
			AddSheetAndFillForm objFileExcel,TestCaseNameArr,"Create",0	
			'Remove path .xls			
			TC_Path_New = left(TC_Path,len(TC_Path)-4)
			
			'Set the save location
			objFileExcel.ActiveWorkbook.SaveAs TC_Path_New,18
			Print "create testcase done in path "&TC_Path	
			objFileExcel.ActiveWorkbook.Close()
			
		Else
		
			objFileExcel.Workbooks.Open (TC_Path) 
			Set objExistWorkbook = objFileExcel.ActiveWorkbook
			sheetexistcount = objExistWorkbook.Sheets.Count	
			
			count_TestCaseNameArr = TestCaseNameArr.Count		
			print "Total Test Scenario "&count_TestCaseNameArr	
			
			For x = 1 To sheetexistcount
			 	exist_sheetname = objExistWorkbook.Sheets(x).Name	 	
			 	If TestCaseNameArr.Contains(exist_sheetname)=True Then
			 		print "found sheet exist name " &exist_sheetname	 		
					TestCaseNameArr.Remove(exist_sheetname)	
					removecount=removecount+1
			 	End If
			 	
			Next
			
'		For Each extractTCName In TestCaseNameArr
'		       print "After Remove : "& extractTCName
'		Next
		
			TC_Num=count_TestCaseNameArr-removecount
			print "Add TestCase "&TC_Num
			print "Exist TestCase "&removecount
			TestCaseNameArr.TrimToSize()
			
			If TestCaseNameArr.Count>0 Then
											
				AddSheetAndFillForm objExistWorkbook,TestCaseNameArr,"Edit",sheetexistcount											
				TC_Path_New = left(TC_Path,len(TC_Path)-4)
				objExistWorkbook.Save
				objExistWorkbook.Close
				Set objExistWorkbook = Nothing

			End If
			
		End If
	objFileExcel.Application.Quit
	Set objFileExcel=nothing
	Set FileSys = nothing

End Function

Private Function AddSheetAndFillForm(ByRef objFileExcel,TestCaseNameArr,Action,SheetStart)
			
			For SheetLoop = 1 To (TestCaseNameArr.Count)-1 Step 1								
				If SheetLoop = 1 Then
					
					'Set Active
						ActiveSheet=1					
						TC_Num = 0
						
					If Action="Create" Then							
						'Create a new workbook.	
						Set objWorkbook = objFileExcel.Workbooks.Add
						Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet"&ActiveSheet)
					Else
						objFileExcel.Sheets(SheetStart).Activate
						Set objWorkbook = objFileExcel
						objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						Set objWorkSheet = objFileExcel.ActiveSheet
						
					End If
									
				Else
				
					If Action="Create" Then
						'Add Sheet	
						objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet"&ActiveSheet)
					Else
					
						If SheetLoop>=2 Then
							objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						End If
						objWorkbook.Sheets(objWorkbook.Worksheets.Count).Activate
						Set objWorkSheet = objWorkbook.ActiveSheet
					
					End If
				End If
										
					'Create Header Column
					objWorkSheet.cells(1,1).value = "TestCaseName"
					objWorkSheet.cells(1,2).value = "UIName"
					objWorkSheet.cells(1,3).value = "Activity"
					objWorkSheet.cells(1,4).value = "ExpectedResult"
					objWorkSheet.cells(1,5).value = "SkipTestStep"
					objWorkSheet.cells(1,6).value = "Severity"
					objWorkSheet.cells(1,7).value = "TestData1_UAT_EN"
					objWorkSheet.cells(1,8).value = "TestData2_UAT_TH"
					objWorkSheet.cells(1,9).value = "EndOfCol"
					objWorkSheet.cells(2,1).value = TestCaseNameArr.Item(TC_Num)
					objWorkSheet.cells(2,5).value = "No"
					objWorkSheet.cells(2,6).value = "Normal"
					objWorkSheet.cells(3,3).value = "EndOfRow"
					
					'Fill BackGround Color
					objWorkSheet.Range("A1:H1").Interior.Color = RGB(153, 204, 255)
					objWorkSheet.Range("A1:H1").Font.Bold = True
						
					For header_col = 1 To 9 Step 1
						
						'Set Font
						objWorkSheet.cells(3,3).Font.Color = RGB(153, 51, 0)
						objWorkSheet.cells(1,9).Font.Color = RGB(153, 51, 0)
						objWorkSheet.Columns(header_col).Font.Size = 9
						objWorkSheet.Columns(header_col).Font.Name= "Arial"
						'Set AutoFit 
						objWorkSheet.Columns(header_col).AutoFit()
					Next
								
					'Name the worksheet
					objWorkSheet.Name = TestCaseNameArr.Item(TC_Num)
					print "Add Sheet Name : " &TestCaseNameArr.Item(TC_Num)
				
				TC_Num=	TC_Num+1
				ActiveSheet = ActiveSheet+1
				Set objWorkSheet = Nothing
			Next
			
	print "Add Sheet TestCase done" 		
	TestCaseNameArr.Clear()
	Set TestCaseNameArr = Nothing
	
End Function


'**********************************************************************************************************************************
'*  Function Name : GetTextAllChildObject
'*  Purpose:        For get text all child object 
'*  Arguments:      Main_Object,ExpectText,LimitLeft,LimitRight
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function GetTextAllChildObject(Main_Object,ExpectText,LimitLeft,LimitRight)

	Set oDesc = description.Create()

	oDesc("class").value="Label"
	Device("Device").OpenViewer
	
	If LimitRight<>"" or len(LimitRight)>0 Then
		percentwidth = LimitRight/100
		r_width = Main_Object.GetROProperty("right")
		r_max = (cint(r_width)*percentwidth) 
		print "r_width :"&r_width
		print "r_max :"&r_max
	End If
	Set l =  Main_Object.ChildObjects
'	Set l =  Main_Object.ChildObjects(oDesc)
	n = l.count()
	print "total Child Object : "&n
	
	iRet = False
	For i = 0 To n-1

		skip = False
		If LimitRight<>"" or len(LimitRight)>0 Then
			position_left = l(i).GetRoProperty("left")
			If LimitLeft="" or len(LimitLeft)=0 Then
				LimitLeft = 0
			End If
			If position_left >=r_max or position_left < LimitLeft Then
				skip = True
			End If			
		End If
  
	 	If  skip = False Then
	 		l(i).highlight  
	 	    judge = l(i).GetRoProperty("text")
		    mc_index = l(i).GetRoProperty("mcindex")
		    resourceid = l(i).GetRoProperty("resourceid")
'		    If len(judge)>0 Then
		    	print "["&i&"] - " &judge &" - " & mc_index&" - " & resourceid
'		    End If
	 		
	 		If ExpectText<>"" Then
	 			If lcase(judge) = ExpectText Then
	 				iRet = True
	 				Exit for
	 			End If
	 		End If

	 	End If		 	      
	Next
	
	Set oDesc = Nothing
	Set Main_Object = Nothing 
End Function

'**********************************************************************************************************************************
'*  Function Name : CheckORAndLoad()
'*  Purpose:        Choose correct repositories and load
'*  Arguments:      -
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'*******************************************************************************************************************
Function CheckORAndLoad()

	Const sModule = "CheckORAndLoad"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If repositoriescollection.Count > 0 Then
		repositoriescollection.RemoveAll	
	End If
	
	'Define ObjectRepository For Android Version UAT
	Environment("ObjectRepository") = Environment("RootProject") & "\Share\ObjectRepository\FastEasy_Android_UAT_CR.tsr"
	'Add runtime repository file
	repositoriescollection.Add Environment("ObjectRepository")
	
	OSType = Device("Device").GetROProperty("ostype")
	identifier = Device("Device").App("Common").GetROProperty("identifier")
	
	call logging("OSType : " & OSType,c_DEBUG,1)
	call logging("identifier : " & identifier,c_DEBUG,1)
	
	repositoriescollection.RemoveAll
	If OSType="ANDROID" Then
		If instr(lcase(identifier),"uat_cr")>0 Then  'Define ObjectRepository For Android Version UAT
			ORPath = "\Share\ObjectRepository\FastEasy_Android_UAT_CR.tsr"
		ElseIf instr(lcase(identifier),"sit_cr")>0 Then 'Define ObjectRepository For Android Version SIT
			ORPath = "\Share\ObjectRepository\FastEasy_Android_SIT_CR.tsr"
		ElseIf instr(lcase(identifier),"perf")>0 Then  'Define ObjectRepository For Android Version PERF
			ORPath = "\Share\ObjectRepository\FastEasy_Android_UAT_PERF.tsr"
		End If
	Else
		'Define ObjectRepository For IOS Version UAT
		ORPath = "\Share\ObjectRepository\FastEasy_IOS_UAT_CR.tsr"
	End If	
	
	Environment("ObjectRepository") = Environment("RootProject") & ORPath
	call logging("Loading repository file : " & Environment("ObjectRepository"),c_DEBUG,1)
	repositoriescollection.Add Environment("ObjectRepository")	
	
End Function

Public Function SendEmailWithAttachment(Emailfrom, Emailto, Emailpassword, attachmentfile)
  	Activity="Send email with an attachment"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	Set MyEmail=CreateObject("CDO.Message")
	Const cdoBasic=0 'Do not Authenticate
	Const cdoAuth=1 'Basic Authentication
	
	MyEmail.Subject = "Run Result : Fast Easy App"
	MyEmail.From    = Emailfrom
	MyEmail.To      = Emailto
	MyEmail.TextBody= "TEST MAIL BODY"
	
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
	
	'SMTP Server
	'hotmail = smtp.live.com
	'gmail = smtp.gmail.com
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="smtp.live.com"
	
	'SMTP Port
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25	
	
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	
	'Your UserID on the SMTP server
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") = Emailfrom
	
	'Your password on the SMTP server
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Emailpassword
	
	
	'Use SSL for the connection (False or True)
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true
	
	'Add attachment in full path
	'example = "D:\Automate\FastEasy\Result\iOS\Wave2DropBLot1\Notification_EN\iOS\TS_Notification_015\HTML\Notification_EN.html"
	MyEmail.AddAttachment attachmentfile
	
	MyEmail.Configuration.Fields.Update
	MyEmail.Send
	
	If err.number = 0 Then
  		call logging("Success to send email" & sMODULE,c_DEBUG,3)
	Else
		call logging("Failed to send email" & sMODULE,c_DEBUG,3)
	End If	

	Set MyEmail=nothing
	SendEmailWithAttachment = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
End Function

'**********************************************************************************************************************************
'*  Function Name : CheckObjectExist()
'*  Purpose:        Create Object Responsibility 
'*  Arguments:      object_exp,timeout
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function CheckObjectExist(object_exp,timeout)
On Error Resume Next
		If timeout=null or timeout="" Then
			timeout = 3
		End If
	 	If object_exp.Exist(timeout) Then
	 		Flag = 1
	 	Else
			Flag = -1	
	 	End If
	 	
		Flag_Err = CheckError()
		If Flag_Err = False Then
			Flag = -1
		End If	  		 	
		CheckObjectExist = Flag
	 
End Function

'**********************************************************************************************************************************
'*  Function Name : StampStatusToReport()
'*  Purpose:        Stamp test result status on HTML report
'*  Arguments:      iRet,Action,Descript,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function StampStatusToReport(sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

	If iRet = True then
		Flag = 1
		If UIName<>"" or UIName<>null Then
			Reporter.ReportEvent micPass,"Click :'"&UIName &"'" ,"Success to "&Action&" :"&UIName
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter,"Success to "&Activity&" <font color=""blue"">["&UIName &"]</font> ,"&Descript , "Passed")
		Else
			Reporter.ReportEvent micPass,"Click :'"&UIName &"'" ,"Success to "&Action
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter,"Success to "&Activity&" ,"&Descript , "Passed")
		End If
			
	Else	
	
		Flag = -1
		If UIName<>"" or UIName<>null Then
			Reporter.ReportEvent micFail,"Click on button :'"&UIName &"'" ,"<font color=red>Failed</font> to "&Action&" :"&UIName
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter ,"<font color=red>Failed</font> to "&Activity&" <font color=""red"">["&UIName &"]</font> ,"&Descript , "Failed")
		Else
			Reporter.ReportEvent micFail,"Click on button :'"&UIName &"'" ,"Failed to "&Action
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter ,"<font color=red>Failed</font> to "&Activity&" ,"&Descript , "Failed")
		End If 
	End If
	StampStatusToReport = Flag
	
End Function

Function GetDeviceProperty(property_type)

    Const sModule = "GetDeviceProperty"
    call logging("sModule " & sMODULE,c_DEBUG,2)
    
    
	GetDeviceProperty = vbNullString	

		
		call logging("Parent_Obj : " & Environment("ParentOR"),c_DEBUG,2)

		Set DeviceObject = Eval(Environment("ParentOR"))
		
		Property_Value = DeviceObject.GetROProperty(property_type)
		call logging("Property_Value : " & Property_Value,c_DEBUG,2)
		Set DeviceObject = Nothing
		
		If Property_Value="" Then
			Property_Value ="N/A"
		End If
		GetDeviceProperty = Property_Value
		
'	else
'		'Not found
'		GetDeviceProperty = "N/A"
'	End If
			
End Function
  
'**********************************************************************************************************************************
'*  Function Name :	reloginApp()
'*  Purpose:        Restart app and relogin
'*  Arguments:      OSType
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
'Function reloginApp(OSType)
'On Error Resume Next
'		iRet = False
'		waitOK = 10
'		
'		set DialogConfirmObj = Eval(Create_OR_Object("DialogConfirm_Txt",""))		
'		For r_waitOK = 1 To waitOK Step 1
'			DialogConfirmObj
'			If DialogConfirmObj.Exist(1) Then
'				DialogConfirmObj.Tap
'				call logging("Found Confirm Button",c_DEBUG,3)
'				Exit For
'			Else
'				call logging("wait for Confirm Button "&(waitOK-r_waitOK)+1&" second",c_DEBUG,3)
'			End If	
'		Next
'		set DialogConfirmObj = Nothing
'		
'		Device("Device").CloseViewer
'		Device("Device").OpenViewer
'		Device("Device").App("Common").Launch DoNotInstall, Restart
'		waittime = 30
'		Set SCB_HeaderObj = Eval(Create_OR_Object("SCB_Header_Txt",""))
'		For wait_page_r = 1 To waittime Step 1
'			wait_time_countdown = (waittime+1) - wait_page_r
'			call logging("wait for load landding page "&wait_time_countdown&" second",c_DEBUG,3)
'			check_exist_obj = SCB_HeaderObj.exist(1)
'			If check_exist_obj Then
'				iRet = True
'				Exit for
'			End If
'		Next
'		Set SCB_HeaderObj = Nothing
'		
'		If iRet = True Then	
'			If Environment("CheckScenarioLogin") <> 0 Then
'									
'				If OSType="ANDROID" Then
'					Set Main_Acc_BtnObj = Eval(Create_OR_Object("Main_Acc_Btn",""))
'					Main_Acc_BtnObj.Tap
'					Set Main_Acc_BtnObj = Nothing
'				Else
'					Flag_Click = Select_MainMenuBar("acc")							
'				End If
'				
'				strValueToEnter = Environment("CheckScenarioLogin")
'				set objLoginObject = Eval(Create_OR_Object("Login_Number_Btn",""))
'				For Iterator = 1 To len(strValueToEnter) Step 1	
'				
'					Pwd_Number=Mid(strValueToEnter,Iterator,1)	
'					If OSType="ANDROID" Then		
'						objLoginObject.SetTOProperty "resourceid","button_"&Pwd_Number				
'					Else
'						objLoginObject.SetTOProperty "text",Pwd_Number
'					End If																	
'					objLoginObject.Tap
'					objLoginObject.RefreshObject
'					call logging("Click Number : " & Pwd_Number,c_DEBUG,3)								
'				Next
'				
'				set objLoginObject = Nothing	
'				Flag_Err = CheckError()
'				
'				set AccSumPageObj = Eval(Create_OR_Object("AccSum_dep_txt",""))
'				If AccSumPageObj.Exist(5) and iRet=True Then
'					iRet = True
'				Else
'					iRet = False			
'				End If
'				set AccSumPageObj = Nothing
'			End If					
'		End If
'
'		reloginApp = iRet
'		
'End Function

'**********************************************************************************************************************************
'*  Function Name : CheckError()
'*  Purpose:        check error 
'*  Arguments:      none
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function CheckError()
On Error Resume Next	
	 If Err.Number = 0 Then
	      Flag = True
	      Environment("TS_ErrorDescption") = "::N/A"
	 Else 	
		  Flag = False	 
		  Environment("TS_ErrorFound") = 1
	      Environment("TS_ErrorDescption") = Err.Description
	      call logging(Err.Description ,c_DEBUG,3)
	      Err.Clear
	 End If
	 CheckError = Flag
	 
End Function

Function CreateTestCaseFile(ScenarioPath,SheetName,TemplateFilePath,DestinationFolder)
	
	'Copy Excel TestCase Template
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	SampleTC_Name = "TestCase_Sample.xls"
	
	If right(DestinationFolder,1)<>"\" Then
		DestinationFolder = DestinationFolder &"\"
	End If
	
	DestinationFilePath = DestinationFolder & SampleTC_Name
	
	print "TemplateFilePath : "&TemplateFilePath
	print "DestinationFolder : "&DestinationFolder
	print "DestinationFilePath : "&DestinationFilePath
	
	objFSO.CopyFile TemplateFilePath, DestinationFilePath
	Set objFSO = Nothing	
	
	'open and count row in test scenario
	SampleTC_Name = "TestCase_Sample.xls"

	Set objFileExcel= CreateObject("Excel.Application")
	Set objWB_x = objFileExcel.Workbooks.Open (ScenarioPath) 
	objFileExcel.Application.Visible = True
	objFileExcel.Application.DisplayAlerts = False
	
 		set mysheet_x = objFileExcel.ActiveWorkbook.Worksheets(SheetName)
 		
 		cols= mysheet_x.UsedRange.Columns.Count
		row = mysheet_x.UsedRange.Rows.Count
		
		TestCaseNameCol =  0
		
		
		Set TestCaseNameArr = createObject("System.Collections.ArrayList")

		'Get column from testcase name 
		For i = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,i).value) = "testcasename" Then
				print "Found "& mysheet_x.cells(1,i).value &" Column"
				TestCaseNameCol=i
				Exit For
			End If 
			
		Next
		
		RunStatusCol = 0
		
		'Get column from RunStatus 
		For i = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,i).value) = "runstatus" Then
				print "Found "& mysheet_x.cells(1,i).value &" Column"
				RunStatusCol=i
				Exit For
			End If 
			
		Next

		'Add testcasename in Array List
		For j = 2 To row Step 1		
			testcasename = mysheet_x.cells(j,TestCaseNameCol).value
			runstatus = mysheet_x.cells(j,RunStatusCol).value
			
			If lcase(trim(testcasename)) = "endofrow" Then
				Exit For
			Else
				If lcase(trim(runstatus)) = "yes" Then
					TestCaseNameArr.Add(testcasename)
				End If
				
			End If		
		Next
		
		'Get column from TestCaseFilePath
		For k = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,k).value) = "testcasefilepath" Then
					print "Found "& mysheet_x.cells(1,k).value &" Column"
						If mysheet_x.cells(2,k).value <>"" Then
							RenameTestcase = mysheet_x.cells(2,k).value
						Else
							RenameTestcase = SampleTC_Name
						End If
					Exit For
			End If 		
		Next
		
		RootProject = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
		print "RootProject "&RootProject
			
		'Set TestCase and TestData Name
		TC_Path= RootProject&"\TestCases\"&RenameTestcase
		print TC_Path

		'Create file system for check file exist in path
		Set FileSys = CreateObject("Scripting.FileSystemObject")
				
		If FileSys.FileExists(TC_Path) Then
			print "Found exist file in path "&TC_Path
			TC_CreateAction = False
		Else
			print "Create TestCase file in path "&TC_Path
			TC_CreateAction = True
		End If

		'Create TestCase File
		If TC_CreateAction=True Then
			CreateTestCaseSheet objFileExcel,DestinationFilePath,TestCaseNameArr,"Create",1	
		End  If
	
		set mysheet_x = Nothing
		objWB_x.Save
		objWB_x.Close
		set objFileExcel = Nothing
		
		'Rename TestCase File
		Set objFso= CreateObject("Scripting.FileSystemObject")  
		objFso.MoveFile DestinationFilePath, TC_Path
		Set objFso= Nothing
		print "Rename File To ["&RenameTestcase&"]"
		print "Create TestCase ["&RenameTestcase&"] Complete"
End Function		


Function CreateTestCaseSheet(ByRef objFileExcel,DestinationFilePath,TestCaseNameArr,Action,SheetStart) 

	objFileExcel.Workbooks.Open (DestinationFilePath) 
	Set objExistWorkbook = objFileExcel.ActiveWorkbook
	
	For SheetLoop = 0 To (TestCaseNameArr.Count)-1 Step 1	
		
		ActiveSheet = SheetLoop + 1
	
		If SheetLoop = 0 Then
			Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Template_001")
			
		Else
			Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets(TestCaseNameArr.Item(SheetLoop-1)&" (2)")	
		End If
		
'		'Rename Sheet
		objWorkSheet.Name = TestCaseNameArr.Item(SheetLoop)
		objWorkSheet.cells(2,1).value = TestCaseNameArr.Item(SheetLoop)

		'Create Sheet1 
		If SheetLoop = 0 Then
			objFileExcel.Sheets.Add , objFileExcel.Worksheets(objFileExcel.Worksheets.Count)
		End If
		
		'Set Object for sheet1
		Set objNewWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet1")	
		
		If SheetLoop <> (TestCaseNameArr.Count)-1 Then					
			'Clone Sheet
			objWorkSheet.Copy objNewWorkSheet
		Else
			objFileExcel.ActiveWorkbook.Worksheets("Sheet1").Delete
		End If
		
		print "Create Sheet ["&TestCaseNameArr.Item(SheetLoop) &"] complete"	
	Next

	objExistWorkbook.Save
	objExistWorkbook.Close
	Set objWorkSheet = Nothing
	Set objExistWorkbook = Nothing
End Function 



Function MergeTC_ToFileName(ScenarioFilePath,ScenarioSheet,TemplateFilePath,TestCaseFilePath,Des_filepath,Des_filename) 

	  ' Create the Excel object
	  On Error Resume Next

	DbConn = ConnectToDB(ScenarioFilePath)
	ArrIgnoreCase = array("xxxxx.xls")

	StrQuery="SELECT * FROM [" & ScenarioSheet & "$] where RunStatus ='Yes' and TestCaseFilePath <>'"&ArrIgnoreCase(0)&"' "

	Set RsRecord = CreateObject("ADODB.Recordset")
	RsRecord.Open StrQuery, DbConn, 1, 3						
	Set oRsExcel= RsRecord
	
	call logging("Found total record : " & oRsExcel.RecordCount,c_DEBUG,3)	
		
	
	If oRsExcel.RecordCount > 0 Then 
	
	  Set objExcel = CreateObject("Excel.Application")
	  objExcel.visible = False
	  objExcel.DisplayAlerts = False	

	  If right(Des_filepath,1) <> "\" Then
			Des_filepath=Des_filepath&"\"
	  End If

		Set objFSO = CreateObject("Scripting.FileSystemObject")
		fullpath_des = Des_filepath&Des_filename
		objFSO.CopyFile TemplateFilePath,fullpath_des
		Set objFSO = Nothing	
	  
		oRsExcel.MoveFirst
		For i = 1 To oRsExcel.RecordCount Step 1
			print "Round "&i
			print "TestCaseName : "&oRsExcel.Fields(3).Value
			TestCaseSheetsName= oRsExcel.Fields(3).Value
			print "TestCaseFilePath : "&oRsExcel.Fields(5).Value
			TestCaseFileName = oRsExcel.Fields(5).Value
			
			If right(TestCaseFilePath,1) <> "\" Then
				TestCaseFilePath=TestCaseFilePath&"\"
			End If
			
			fullpathTC = TestCaseFilePath&oRsExcel.Fields(5).Value
			print "fullpath :"&fullpathTC 
					
			Set objWorkbook = objExcel.Workbooks.Open (fullpathTC)	
			Set mysheet = objWorkbook.Worksheets(TestCaseSheetsName)
			mysheet.Activate
			mysheet.Range("A1:T250").Copy

			
			Set objNewWorkbook = objExcel.Workbooks.Open(Des_filepath&Des_filename)
	  		Set mysheet_des = objNewWorkbook.Worksheets(TestCaseSheetsName)  
	  		mysheet_des.Activate
			mysheet_des.Range("A1:T250").PasteSpecial
			
			replacelink="["&TestCaseFileName&"]"
			mysheet_des.Range("A1:T250").Replace replacelink, vbNullString
			'Close Destination Excel
			objNewWorkbook.save
			objNewWorkbook.Close
			
			'Close Copy Excel
			objWorkbook.Close
			objExcel.Quit
			
			oRsExcel.MoveNext

		Next
	End If

Set objWorkbook = Nothing		
Set mysheet = Nothing	
Set objExcel = Nothing	
print "Finish Merge TestCase"		
'objWorkbook.Close
'objExcel.Quit

End Function 

'**********************************************************************************************************************************
'*  Function Name :	SendLineNotify()
'*  Purpose:        Send line notification By JAVA
'*  Arguments:      Message
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function SendLineNotify_JAVA(Message)
	  	Set oShell = CreateObject ("WSCript.shell")
'	  	LineTokenID="FkjBMQCSipYni33vRGak03B8Ala9yTJksfAjUFYY0vd"
		LineTokenID = Environment("LineTokenID")
	  	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
	  	Command = "cmd /K pushd "&Environment("RootProject")&"\Share\ExternalLibrary\LineNotify\& ""%java_home%""\bin\java.exe -jar GLineNotify.jar "&LineTokenID&" "&Message &"& exit"
		Ret = oShell.Run (Command,0,True)
		Set oShell = Nothing
End Function

'**********************************************************************************************************************************
'*  Function Name :	SendLineNotify()
'*  Purpose:        Send line notification By CURL
'*  Arguments:      Message
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function SendLineNotify(Message)
	  	Set oShell = CreateObject ("WSCript.shell")
'	  	LineTokenID="qhDjrHVhWaVO0lHfnfhjygP0K4cVh6PiX27MeQEI57f,1m7Nxpp58tI27YQ4wpa3ljeZwTiNQ4jDgqYv9Nlydbn"
		LineTokenID = Environment("LineTokenID")
	  	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
	  	Command = "cmd /K pushd "&Environment("RootProject")&"\Share\ExternalLibrary\LineNotify\ & LineNotify.bat "&LineTokenID&" "&Message &"& exit"
		Ret = oShell.Run (Command,0,True)
		Set oShell = Nothing
End Function

'**********************************************************************************************************************************
'*  Function Name :	CheckImageColor()
'*  Purpose:        Check image coordinate color
'*  Arguments:      ImgPath,co_x,co_y
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function CheckImageColor(ImgPath,co_x,co_y,ColorExpect)
'		Environment("RootProject")="D:\SCB_CoMarch\Project"
	  	Set oShell = CreateObject ("WSCript.shell")
	  	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
	  	Command = "cmd /K pushd "&Environment("RootProject")&"\Share\ExternalLibrary\& ""%java_home%""\bin\java.exe -jar GCheckColor.jar -test """&ImgPath&""" "&co_x&","&co_y&" "&ColorExpect&"& exit"
		ret = oShell.Run (Command,0,True)
'		print "return value : "&ret
		Set oShell = Nothing
		CheckImageColor = ret
		  
		'java -jar 		
		
End Function

'**********************************************************************************************************************************
'*  Function Name :	SendLineNotify_PS()
'*  Purpose:        Send line notification
'*  Arguments:      Message
'* 	Return Value:   Flag True or False
'* 	Author By		
'**********************************************************************************************************************************
'Function SendLineNotify_PS(Message)
'
'	  	Set oShell = CreateObject ("WSCript.shell")
'	  	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
'	  	Command = "cmd /K pushd C:\Windows\system32\WindowsPowerShell\v1.0\PowerShell -ExecutionPolicy Unrestricted -File "&Environment("RootProject")&"\LineNotify\LineNotify.ps1 "&Message
''		print Command
'		oShell.Run Command,1,False
'		
'End Function

Function SetEnvironmentVariable(UserVariable,UserValue)
	On Error Resume Next
	Environment(UserVariable) = UserValue
	call logging("Set Environment("""&UserVariable&""") : " &Environment(UserVariable),c_DEBUG,3) 
	
End Function

Function PopupAlert(PopupTitle,PopupMsg,Timeout)

	If lcase(Environment("ShowPopupWainting")) ="yes" Then
		Set PopupObject = CreateObject("WScript.Shell")
		
		If Timeout="" or len(Timeout) = 0 Then
			Timeout =1
		End If
		
		If PopupTitle="" or len(PopupTitle) = 0 Then
			PopupTitle =""
		End If

		If PopupMsg="" or len(PopupMsg) = 0 Then
			PopupMsg =""
		End If
		
		PopupObject.Popup PopupMsg,Timeout,PopupTitle
		
		Set PopupObject = Nothing
	End If
	
End Function

Function SwitchTestCaseProfile(ProfilePath,TScrenario_Path,TScrenario_Sheetname)

  	Const sModule = "SwitchTestCaseProfile"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	TScrenario_Path=Environment("RootProject")&"\"&TScrenario_Path
	DbConn = ConnectToDB(TScrenario_Path)
	
	StrQuery="SELECT distinct TestCaseFilePath FROM [" & TScrenario_Sheetname & "$] where RunStatus ='Yes'"

	Set RsRecord = CreateObject("ADODB.Recordset")
	RsRecord.Open StrQuery, DbConn, 1, 3						
	Set oRsExcel= RsRecord
	
	call logging("Found total record : " & oRsExcel.RecordCount,c_DEBUG,3)	
		
	
	If oRsExcel.RecordCount > 0 Then	
	
		'Create excel object and open excel
		Set objFileExcel= CreateObject("Excel.Application")
		Set objWB_x = objFileExcel.Workbooks.Open (ProfilePath) 
		objFileExcel.Application.Visible = False
		objFileExcel.Application.DisplayAlerts = False
		
		'Set Sheet Active
		set mysheet_x = objFileExcel.ActiveWorkbook.Worksheets("profile")
			
		'loop for check data in profile
		For j = 2 To 14 Step 1		
			call logging(mysheet_x.Range("A"&j).value &" : "&mysheet_x.Range("B"&j).value,c_DEBUG,3)	 
		Next
		
		'Set Excel Copy Range
		CopyExcelRange = "A1:FZ1000"
				
			oRsExcel.MoveFirst
			For i = 1 To oRsExcel.RecordCount Step 1
					TestCaseFilePath = oRsExcel.Fields(0).Value
					
		'			print TestCaseFilePath
					
						TC_Path = Environment("RootProject")&"\TestCases\"&TestCaseFilePath
						call logging("Replace profile in testcase : " & TestCaseFilePath,c_DEBUG,3)	
						
						Set objTC_x = objFileExcel.Workbooks.Open (TC_Path) 
					
						'Set Sheet Active
						set dessheet_x = objFileExcel.ActiveWorkbook.Worksheets("profile")
						
						'Copy cell from profile to testcase
						mysheet_x.Range(CopyExcelRange).Copy
						dessheet_x.Range(CopyExcelRange).PasteSpecial						
							
						'Save and Close Destination Excel 
						objTC_x.Save
						objTC_x.Close
						set dessheet_x = Nothing
						set objTC_x = Nothing						
						oRsExcel.MoveNext
			Next
			Set oRsExcel = Nothing
			Set RsRecord = Nothing
			
			
			ReplaceFilesPath =  array(Environment("RootProject")&"\CommonStep\CommonStep.xls" , Environment("RootProject")&"\CommonStep\PostAction.xls",Environment("RootProject")&"\CommonStep\MapUIName.xls")
			For k = 0 To Ubound(ReplaceFilesPath) Step 1
			
				'Replace Commonstep sheet
				TC_Common_Path = ReplaceFilesPath(k)
				call logging("Replace profile in testcase : "&TC_Common_Path,c_DEBUG,3)	
				
				Set objTC_Common_x = objFileExcel.Workbooks.Open (TC_Common_Path) 
				
				'Set Sheet Active
				set des_common_sheet_x = objFileExcel.ActiveWorkbook.Worksheets("profile")
				
				'Copy cell from profile to common step
				mysheet_x.Range(CopyExcelRange).Copy
				des_common_sheet_x.Range(CopyExcelRange).PasteSpecial
							
								
				'Save and Close Destination Excel 
				objTC_Common_x.Save
				objTC_Common_x.Close
				set des_common_sheet_x = Nothing
				set objTC_Common_x = Nothing			
			Next
			
			Erase ReplaceFilesPath
'			'Replace Commonstep sheet
'			TC_Common_Path = Environment("RootProject")&"\CommonStep\CommonStep.xls"
'			call logging("Replace profile in testcase : CommonStep.xls",c_DEBUG,3)	
						
'			Set objTC_Common_x = objFileExcel.Workbooks.Open (TC_Common_Path) 
'					
'			'Set Sheet Active
'			set des_common_sheet_x = objFileExcel.ActiveWorkbook.Worksheets("profile")
'			
'			'Copy cell from profile to common step
'			mysheet_x.Range("A1:BZ1000").Copy
'			des_common_sheet_x.Range("A1:BZ1000").PasteSpecial
'						
'							
'			'Save and Close Destination Excel 
'			objTC_Common_x.Save
'			objTC_Common_x.Close
'			set des_common_sheet_x = Nothing
'			set objTC_Common_x = Nothing
						
			
			'Save and Close Profile Excel 
			objWB_x.Save
			objWB_x.Close
			set objWB_x = Nothing
			
			objFileExcel.Quit
			set objFileExcel = Nothing
			call logging("replace profile complete",c_DEBUG,3)	
	End If
	Set RsRecord = Nothing	
	Set oRsExcel= Nothing
End Function

Function CheckLanguage(StrChar)
On Error Resume Next
		Set objReg = New RegExp		
			With objReg
			.Pattern    = "^[a-zA-Z]"
			.IgnoreCase = True
			.Global     = False
		End With

		StrChar = Trim(Replace(StrChar," ",""))
		If objReg.Test(StrChar) Then	
			GetLang = "EN"	
		Else
			GetLang = "TH"		
		End If
		
		Set objReg = Nothing
		call logging("["&StrChar&"] is ["&GetLang&"] Language",c_DEBUG,3)
		CheckLanguage = GetLang

			
End Function

Function MapUIName_Excel(Inp_UIName,FilePath,SheetName)

	If lcase(Environment("MapUINameExcel"))="yes" Then	
		DbConn = ConnectToDB(FilePath)
		Inp_UIName = lcase(trim(Inp_UIName))
		StrQuery="SELECT * FROM [" & SheetName & "$] where UIName ='"&Inp_UIName&"' and PropertyType <>'' and PropertyValue <>'' "
	
		Set RsRecord = CreateObject("ADODB.Recordset")
		RsRecord.Open StrQuery, DbConn, 1, 3						
		Set oRsExcel= RsRecord
		
		call logging("Found total record : " & oRsExcel.RecordCount,c_DEBUG,3)	
		
		If oRsExcel.RecordCount > 0 Then
				oRsExcel.MoveFirst
				UIName_ExVal = lcase(trim(oRsExcel.Fields(1).Value))
				Type_ExVal = oRsExcel.Fields(2).Value
				Value_ExVal = oRsExcel.Fields(3).Value
						
				call logging("UIName : "&UIName_ExVal,c_DEBUG,3)	
				call logging("Type : "&Type_ExVal,c_DEBUG,3)
				call logging("Value : "&Value_ExVal,c_DEBUG,3)						
				Flag_Found = True
				Environment("MapUI_Type")=Type_ExVal
				Environment("MapUI_Value")=Value_ExVal
		Else
			Flag_Found = False
			call logging("Skip replace value in UIName ["&Inp_UIName&"]",c_DEBUG,3)	
		End If 
		Set oRsExcel = Nothing
		Set RsRecord = Nothing
	Else
		Flag_Found = False
	End If
	
	MapUIName_Excel = Flag_Found
	
End Function

Function DefineExternalParam()
	On Error Resume Next 
	Environment("InpTCName") = Parameter("TC_Name")
	If Err.Number <> 0 Then
		Environment("InpTCName") = ""
		Err.Clear
	End If
	
	Environment("ScenarioRunType") = Parameter("RunTC_Type")
	If Err.Number <> 0 Then
		Environment("ScenarioRunType") = "All"
		Err.Clear
	End If
	
End Function

Function WriteLogToFile(Message_Inp,Out_FileName_Param)
	On Error Resume Next
	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
	Folder_Path=Environment("RootProject") &"\log"
	
'	Folder_Path="D:\Test project\log"
	Set FileSys = CreateObject("Scripting.FileSystemObject")
	If FileSys.FolderExists(Folder_Path) Then
		Set objFolder = FileSys.GetFolder(Folder_Path)
		
		Out_FileName="_"&Out_FileName_Param
		outFileName = "log"&Out_FileName&"_"&Year(now)&Month(now)&Day(now)&".txt"
	
		outFile= Folder_Path &"\"& outFileName
		If FileSys.FileExists(outFile) Then
			Set objFile = FileSys.OpenTextFile(outFile , 8)
		Else
			Set objFile = FileSys.CreateTextFile(outFile,True)	
		End If		
		objFile.Write Message_Inp & vbCrLf
		objFile.Close
		Set objFolder = Nothing
		Set objFile = Nothing
	Else
		print "Not found folder path"
	End If 
	Set FileSys = Nothing
	
End Function

Function AddZeoNum(num)
	AddZeoNum = right("0"&num,2)
End Function


Function QuickClip(input)
'  If IsNull(input) Then
'    QuickClip = CreateObject("HTMLFile").parentWindow.clipboardData.getData("Text")
'    If IsNull(QuickClip) Then QuickClip = ""
'  Else
    CreateObject("WScript.Shell").Run _
      "mshta.exe javascript:eval(""document.parentWindow.clipboardData.setData('text','" _
      & Replace(Replace(input, "'", "\\u0027"), """", "\\u0022") & "');window.close()"")", _
      0,True
'  End If
End Function

Function CheckUINameExistInReposities(TSRFile,TScrenario_Path,TScrenario_Sheetname,ExportFilePath)

  	Const sModule = "CheckUINameExistInReposities"
  	call logging("sModule " & sMODULE,c_DEBUG,3)

	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
	TSR_Path=Environment("RootProject")&"\Share\ObjectRepository\"&TSRFile
	TScrenario_Path=Environment("RootProject")&"\"&TScrenario_Path
	
	DbConn = ConnectToDB(TScrenario_Path)	
	StrQuery="SELECT * FROM [" & TScrenario_Sheetname & "$] where RunStatus ='Yes'"
	Set RsRecord = CreateObject("ADODB.Recordset")
	RsRecord.Open StrQuery, DbConn, 1, 3						
	Set oRsExcel= RsRecord
	
	call logging("Found total record : " & oRsExcel.RecordCount,c_DEBUG,3)	
	Set FileSys = CreateObject("Scripting.FileSystemObject")
	bolFlag = True	
	
	If oRsExcel.RecordCount > 0 and FileSys.FileExists(TSR_Path) Then	
	
		ExecSheet = "TSRCheck"
		datatable.AddSheet ExecSheet
		CurrentDTRow = 1
		
		'Remove TSR File
		If repositoriescollection.Count > 0 Then
			repositoriescollection.RemoveAll
			call logging("Remove TSR File",c_DEBUG,3)			
		End If
		
		'ADD TSR File
		repositoriescollection.Add TSR_Path
		call logging("Add TSR File : "&TSR_Path,c_DEBUG,3)	

		'Get App Map
		GetAppMap TSR_Path
		call logging("Get App Map : "&TSR_Path,c_DEBUG,3)	
		
		'Get Column index
		Flag_TCName = False
		Flag_TCPath = False
		For iCol = 0 to oRsExcel.Fields.Count
			strColName = oRsExcel.Fields(iCol).Name
			If lcase(strColName) = "testcasename" Then
				Col_TCName = iCol
				Flag_TCName = True
'				call logging("Found TestCaseName Column : "&Col_TCName,c_DEBUG,3)	
			End If
			
			If lcase(strColName) = "testcasefilepath" Then
				Col_TCPath = iCol
				Flag_TCPath = True
'				call logging("Found TestCaseFilePath Column : "&Col_TCPath,c_DEBUG,3)	
			End If
			
			If Flag_TCName = True and  Flag_TCPath = True Then
				Exit For
			End If
		Next

		oRsExcel.MoveFirst
		For i = 1 To oRsExcel.RecordCount Step 1
			call logging("Row : "&i,c_DEBUG,3)	
			
			If  LCase(Trim(oRsExcel.Fields(Col_TCName).Value)) = "endofrow" or _
			len(Trim(oRsExcel.Fields(Col_TCName).Value)) = 0 Then
					Exit For
			End If 
				
			TC_Name = oRsExcel.Fields(Col_TCName).Value
			TC_File = oRsExcel.Fields(Col_TCPath).Value
			TC_FullPath = Environment("RootProject")&"\TestCases\"&TC_File
			call logging("Process TestCase Sheet : "&TC_Name,c_DEBUG,3)	
			call logging("Process TestCase File Path : "&TC_FullPath,c_DEBUG,3)	

			DbTC_Conn = ConnectToDB(TC_FullPath)	
			StrQuery="SELECT * FROM [" & TC_Name & "$] where SkipTestStep ='No'"
			Set TC_RsRecord = CreateObject("ADODB.Recordset")
			TC_RsRecord.Open StrQuery, DbTC_Conn, 1, 3	
			
			Flag_TCUIName = False
			Flag_TCSkipTestStep = False
			Flag_TCSkipTestStep = False
			For iCol_TC = 0 to TC_RsRecord.Fields.Count
				strColName = TC_RsRecord.Fields(iCol_TC).Name
'				call logging("TestCase Column : "&strColName,c_DEBUG,3)	
				If lcase(strColName) = "uiname" Then
					Col_TCUIName = iCol_TC
					Flag_TCUIName = True
'					call logging("Found UIName Column : "&Col_TCUIName,c_DEBUG,3)	
				End If
				
				If lcase(strColName) = "activity" Then
					Col_Activity = iCol_TC
					Flag_TCActivity = True
'					call logging("Found Activity Column : "&Col_Activity,c_DEBUG,3)	
				End If

				If lcase(strColName) = "skipteststep" Then
					Col_SkipTestStep = iCol_TC
					Flag_TCSkipTestStep = True
'					call logging("Found SkipTestStep Column : "&Col_SkipTestStep,c_DEBUG,3)	
				End If
				
				If Flag_TCUIName = True and  Flag_TCActivity = True and Flag_TCSkipTestStep = True or lcase(strColName) ="endofcol" or _
				len(trim(strColName)) = 0 Then
					Exit For
				End If
			Next		
			
			TC_RsRecord.MoveFirst			
			For j = 1 To TC_RsRecord.RecordCount Step 1
				call logging("Test Step : "&j,c_DEBUG,3)
				
				If  LCase(Trim(TC_RsRecord.Fields(Col_Activity).Value)) = "endofrow" or _
				len(Trim(TC_RsRecord.Fields(Col_Activity).Value)) = 0 Then
					Exit For
				End If 
				
				UIName = TC_RsRecord.Fields(Col_TCUIName).Value
				Activity = TC_RsRecord.Fields(Col_Activity).Value
				SkipTestStep = TC_RsRecord.Fields(Col_SkipTestStep).Value				
				call logging("UIName : "&UIName,c_DEBUG,3)
				call logging("Activity : "&Activity,c_DEBUG,3)
				call logging("SkipTestStep : "&SkipTestStep,c_DEBUG,3)

				If  len(Trim(UIName)) > 0 and not objDictChild.Exists(lcase(UIName)) and LCase(SkipTestStep)="no" Then
					Val_Failure = TC_File&";"&TC_Name&";"&j&";"&Activity&";"&UIName
					DataTable.SetCurrentRow(CurrentDTRow)
					If CurrentDTRow = 1 Then
						DataTable.GetSheet(ExecSheet).AddParameter "TestCaseFilePath",TC_File
						DataTable.GetSheet(ExecSheet).AddParameter "TestCaseName",TC_Name
						DataTable.GetSheet(ExecSheet).AddParameter "TestStep",j
						DataTable.GetSheet(ExecSheet).AddParameter "Activity",Activity
						DataTable.GetSheet(ExecSheet).AddParameter "UIName",UIName	
					Else
						DataTable.GetSheet(ExecSheet).GetParameter("TestCaseFilePath").Value = TC_File	
						DataTable.GetSheet(ExecSheet).GetParameter("TestCaseName").Value = TC_Name	
						DataTable.GetSheet(ExecSheet).GetParameter("TestStep").Value = j	
						DataTable.GetSheet(ExecSheet).GetParameter("Activity").Value = Activity	
						DataTable.GetSheet(ExecSheet).GetParameter("UIName").Value = UIName	
					End If

					str_Failure = "Test Step ["&j&"] - Activity ["&Activity&"] - UIName ["&UIName&"] is not in ObjectRopository"
					CurrentDTRow = CurrentDTRow + 1
					call logging(str_Failure ,c_DEBUG,1)
					bolFlag = False
				End If		

			TC_RsRecord.MoveNext
			Next
			TC_RsRecord.Close
			call logging("Close TestCase ADODB Connection ",c_DEBUG,3)	
			Set TC_RsRecord = Nothing
		oRsExcel.MoveNext
		Next
	
	If bolFlag = False Then
		If FileSys.FileExists(ExportFilePath) Then
			FileSys.DeleteFile ExportFilePath
			call logging("Delete exist file in path "&ExportFilePath,c_DEBUG,3)	
		End If
		datatable.ExportSheet ExportFilePath ,ExecSheet
		call logging("Export not found object ["&TSRFile&"] in path "&ExportFilePath,c_DEBUG,3)
		systemutil.Run 	ExportFilePath
	End If
	
	datatable.DeleteSheet ExecSheet
	End If
	RsRecord.Close
	call logging("Close TestScrenario ADODB Connection ",c_DEBUG,3)	
	Set RsRecord = Nothing	
	Set oRsExcel= Nothing
	Set FileSys = Nothing
	CheckUINameExistInReposities = bolFlag
	
End Function

Function CountCreatePayment(ExcelFileName)
	
	Set objExcel = CreateObject("Excel.Application")
	
'	objExcel.Application.Visible = True
	objExcel.Application.DisplayAlerts = False		
	
	Set objWorkbook = objExcel.Workbooks.Open(Environment("RootProject")&"\Profile\Profile.xls")

'	get row count columnA WorkSheet:round_test
	RowColA = objExcel.Worksheets("round_test").Cells(1,3).Value

	For ColA = 2 To RowColA Step 1
	
		GetTCName = objExcel.Worksheets("round_test").Cells(ColA,2).Value
		
		If instr(ExcelFileName,GetTCName) > 0 Then
			
			GetTCCount = objExcel.Worksheets("round_test").Cells(ColA,3).Value
			Environment("GetTCCount") = GetTCCount
			objExcel.Worksheets("round_test").Cells(ColA,3).Value = GetTCCount+1
			
		'	save the existing excel file. use SaveAs to save it as something else
			objWorkbook.Save
					
			Exit For 'Exit loob
			
		End If
		
	Next
	
'close the workbook
	objWorkbook.Close 
			
'exit the excel program
	objExcel.Quit
			
'release objects
	Set objExcel = Nothing
	Set objWorkbook = Nothing
	
	
	
End Function


Function CheckRunningProcess(FindProc)
	strComputer = "."
'	FindProc="pcsws.exe"
	iRet = False
	Set objWMIService = GetObject("winmgmts:" _
	     & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	 Set colProcessList = objWMIService.ExecQuery _
	     ("Select Name from Win32_Process WHERE Name LIKE '" & FindProc & "%'")
	
	 If colProcessList.count>0 then
'	     print FindProc & " is running"
	     iRet = True
'	 else
'	     print FindProc & " is not running"
	 End if 
	  Set colProcessList = Nothing
	 Set objWMIService = Nothing
	 CheckRunningProcess = iRet
	 
End Function