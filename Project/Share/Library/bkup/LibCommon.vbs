'Option Explicit

' ************************************* MAINTAIN THIS HEADER *************************************
' Library Name.......: libCommon
' Library Type.......: Module
' Purpose............: To handle on Common function
' Author By..........: SUEBSAKUL A. (AMS-GABLE)
' Created on.........: 2016-08-31
' ---------------------------------------------------------
'
'   Copyright (c) 2016 By GABLE Company Limited
'   Confidential - All Rights Reserved
'
' ---------------------------------------------------------
'
'  ###### HISTORY INFORMATION ######
'  Date            ByWho		Description
'  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'   2016-08-31		K			1) Created a new function
'
' ************************************* END MAINTAIN THIS HEADER *********************************

' ******************************************
' *** PRIVATE CONSTANT VARIABLES MODULE  ***
' ******************************************
Private Const m_sMODULE_NAME_FUNC = "LIBF"
Private m_oFso ', m_oStream
Private re

' ******************************************
' *** PUBLIC CONSTANT VARIABLES MODULE  ***
' ******************************************
Public Const m_sDEBUG = 1
Public Const m_sLevel = 3
Public Const c_DEBUG  = "D"
Public Const c_INFORM = "I"
Public s_LogMsg : s_LogMsg = ""


Public sub logging(Message, iType, iLevel)
	Const sModule = "logging"
	
	'Log to Console
	If (m_sDEBUG) and (m_sLevel >= iLevel) Then
		print formatdatetime(now) & " : " & Message
	End If

end sub

Public Function excel_DataSheetExists(sheetName)
	Const sModule = "excel_DataSheetExists"
    call logging("sModule " & sMODULE,c_DEBUG,2)
	
	Dim Flag : Flag = True
	On error resume next

	Err.clear
	Set objSheet= DataTable.GetSheet(sheetName)
	'In case error occured sheet does not exist
	If err.number <> 0 then
		call logging("Not found Sheet : " & sheetName,c_DEBUG,3)
		Flag = FALSE
	End if
	
	Set objSheet = nothing
	excel_DataSheetExists = Flag
	
End Function

Public Function excel_DataSheetDelete(sheetName)
	Const sModule = "excel_DataSheetDelete"
    call logging("sModule " & sMODULE,c_DEBUG,2)	
    
    Dim Flag : Flag = True
    
    Flag = excel_DataSheetExists(sheetName)
    If Flag = True Then
    
    	Err.clear
    	
    	DataTable.DeleteSheet(sheetName)
    	
    	'Error Delete Sheet
	 	If err.number <> 0 then
			Flag = FALSE
		Else 
			Flag = True
		End if
    End If
    
    Set objSheet = nothing
    excel_DataSheetDelete = Flag
End Function

Public Sub MC_Wait()
	Const sModule = "MC_Wait"
    call logging("sModule " & sMODULE,c_DEBUG,2)	
	
	call logging("DefaultWait :  " & Environment("DefaultWait") &" sec(s)..",c_DEBUG,3)
	
	Wait(Environment("DefaultWait"))
	
End Sub 

Public Sub RenameFile(oldFilename,newFilename)
	Const sModule = "MoveFile"
    call logging("sModule " & sMODULE,c_DEBUG,2)
    
	Set ObjFSO = CreateObject("Scripting.FileSystemObject")
	
	call logging("Rename file : " & oldFilename & " to " & newFilename,c_DEBUG,2)
	ObjFSO.MoveFile oldFilename, newFilename
	
	Set ObjFSO = nothing

End Sub

'**********************************************************************************************************************************
'*  Function Name : CreateFormTestCase()
'*  Purpose:        Create new form testcase template from testscenario    
'*  Arguments:      ScenarioPath,SheetName
'* 	Return Value:   none
'* 	Author By		ANUPOL H. (AMS-GABLE)
'* 	How to use		CreateFormTestCase (Testscenario_path,Testscenario_SheetName)
'*****************************************************************************************************************

Public Function CreateFormTestCase(ScenarioPath,SheetName)

	print "ScenarioPath " &ScenarioPath
	Set objFileExcel= CreateObject("Excel.Application")
	Set objWB_x = objFileExcel.Workbooks.Open (ScenarioPath) 
	objFileExcel.Application.Visible = True
	objFileExcel.Application.DisplayAlerts = False
	
 		set mysheet_x = objFileExcel.ActiveWorkbook.Worksheets(SheetName)
 		
 		cols= mysheet_x.UsedRange.Columns.Count
		row = mysheet_x.UsedRange.Rows.Count
		
		TestCaseNameCol =  0
		
		Set TestCaseNameArr = createObject("System.Collections.ArrayList")

		'Get column from testcase name 
		For i = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,i).value) = lcase("TestCaseName") Then
				print mysheet_x.cells(1,i).value
				TestCaseNameCol=i
				Exit For
			End If 
			
		Next
		
		'Add testcasename in Array List
		For j = 2 To row Step 1		
			testcasename = mysheet_x.cells(j,TestCaseNameCol).value
			If testcasename<>"EndOfRow" and TestCaseNameArr.Contains(testcasename)=False Then
				TestCaseNameArr.Add(testcasename)
			End If		
		Next
				
		'Close Test Scenario
'		objFileExcel.ActiveWorkbook.Close
'		Set mysheet_x = Nothing
'		Set objWB_x = Nothing
		
		'Get folder path from testScenario path
		segments = Split(ScenarioPath,"\")
		For k = 0 To UBound(segments)-1 Step 1
			If k = 0  Then
				TS_FolderPath = segments(k)
			Else
				TS_FolderPath=TS_FolderPath&"\"& segments(k)
			End If			
		    
		Next
		
		'Get module from TestScenario path
		segment_module = Split(segments(UBound(segments)),"_")
		module = segment_module(1)
		
		'Set TestCase and TestData Name
		TC_Path=TS_FolderPath&"\TestCase_"&module
		
		print TS_FolderPath
		
		'Create file system for check file exist in path
		Set FileSys = CreateObject("Scripting.FileSystemObject")
				
		If FileSys.FileExists(TC_Path) Then
			print "Found exist file in path "&TC_Path
			TC_CreateAction = False
		Else
			print "Create TestCase file in path "&TC_Path
			TC_CreateAction = True
		End If
			
		'Create TestCase File
		If TC_CreateAction=True Then
		
			TC_Num=0
			AddSheetAndFillForm objFileExcel,TestCaseNameArr,"Create",0	
			'Remove path .xls			
			TC_Path_New = left(TC_Path,len(TC_Path)-4)
			
			'Set the save location
			objFileExcel.ActiveWorkbook.SaveAs TC_Path_New,18
			Print "create testcase done in path "&TC_Path	
			objFileExcel.ActiveWorkbook.Close()
			
		Else
		
			objFileExcel.Workbooks.Open (TC_Path) 
			Set objExistWorkbook = objFileExcel.ActiveWorkbook
			sheetexistcount = objExistWorkbook.Sheets.Count	
			
			count_TestCaseNameArr = TestCaseNameArr.Count		
			print "Total Test Scenario "&count_TestCaseNameArr	
			
			For x = 1 To sheetexistcount
			 	exist_sheetname = objExistWorkbook.Sheets(x).Name	 	
			 	If TestCaseNameArr.Contains(exist_sheetname)=True Then
			 		print "found sheet exist name " &exist_sheetname	 		
					TestCaseNameArr.Remove(exist_sheetname)	
					removecount=removecount+1
			 	End If
			 	
			Next
			
'		For Each extractTCName In TestCaseNameArr
'		       print "After Remove : "& extractTCName
'		Next
		
			TC_Num=count_TestCaseNameArr-removecount
			print "Add TestCase "&TC_Num
			print "Exist TestCase "&removecount
			TestCaseNameArr.TrimToSize()
			
			If TestCaseNameArr.Count>0 Then
											
				AddSheetAndFillForm objExistWorkbook,TestCaseNameArr,"Edit",sheetexistcount											
				TC_Path_New = left(TC_Path,len(TC_Path)-4)
				objExistWorkbook.Save
				objExistWorkbook.Close
				Set objExistWorkbook = Nothing

			End If
			
		End If
	objFileExcel.Application.Quit
	Set objFileExcel=nothing
	Set FileSys = nothing

End Function

Private Function AddSheetAndFillForm(ByRef objFileExcel,TestCaseNameArr,Action,SheetStart)
			
			For SheetLoop = 1 To (TestCaseNameArr.Count)-1 Step 1								
				If SheetLoop = 1 Then
					
					'Set Active
						ActiveSheet=1					
						TC_Num = 0
						
					If Action="Create" Then							
						'Create a new workbook.	
						Set objWorkbook = objFileExcel.Workbooks.Add
						Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet"&ActiveSheet)
					Else
						objFileExcel.Sheets(SheetStart).Activate
						Set objWorkbook = objFileExcel
						objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						Set objWorkSheet = objFileExcel.ActiveSheet
						
					End If
									
				Else
				
					If Action="Create" Then
						'Add Sheet	
						objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet"&ActiveSheet)
					Else
					
						If SheetLoop>=2 Then
							objWorkbook.Sheets.Add , objWorkbook.Worksheets(objWorkbook.Worksheets.Count)
						End If
						objWorkbook.Sheets(objWorkbook.Worksheets.Count).Activate
						Set objWorkSheet = objWorkbook.ActiveSheet
					
					End If
				End If
										
					'Create Header Column
					objWorkSheet.cells(1,1).value = "TestCaseName"
					objWorkSheet.cells(1,2).value = "UIName"
					objWorkSheet.cells(1,3).value = "Activity"
					objWorkSheet.cells(1,4).value = "ExpectedResult"
					objWorkSheet.cells(1,5).value = "SkipTestStep"
					objWorkSheet.cells(1,6).value = "Severity"
					objWorkSheet.cells(1,7).value = "TestData1_UAT_EN"
					objWorkSheet.cells(1,8).value = "TestData2_UAT_TH"
					objWorkSheet.cells(1,9).value = "EndOfCol"
					objWorkSheet.cells(2,1).value = TestCaseNameArr.Item(TC_Num)
					objWorkSheet.cells(2,5).value = "No"
					objWorkSheet.cells(2,6).value = "Normal"
					objWorkSheet.cells(3,3).value = "EndOfRow"
					
					'Fill BackGround Color
					objWorkSheet.Range("A1:H1").Interior.Color = RGB(153, 204, 255)
					objWorkSheet.Range("A1:H1").Font.Bold = True
						
					For header_col = 1 To 9 Step 1
						
						'Set Font
						objWorkSheet.cells(3,3).Font.Color = RGB(153, 51, 0)
						objWorkSheet.cells(1,9).Font.Color = RGB(153, 51, 0)
						objWorkSheet.Columns(header_col).Font.Size = 9
						objWorkSheet.Columns(header_col).Font.Name= "Arial"
						'Set AutoFit 
						objWorkSheet.Columns(header_col).AutoFit()
					Next
								
					'Name the worksheet
					objWorkSheet.Name = TestCaseNameArr.Item(TC_Num)
					print "Add Sheet Name : " &TestCaseNameArr.Item(TC_Num)
				
				TC_Num=	TC_Num+1
				ActiveSheet = ActiveSheet+1
				Set objWorkSheet = Nothing
			Next
			
	print "Add Sheet TestCase done" 		
	TestCaseNameArr.Clear()
	Set TestCaseNameArr = Nothing
	
End Function


'**********************************************************************************************************************************
'*  Function Name : GetTextAllChildObject
'*  Purpose:        For get text all child object 
'*  Arguments:      Main_Object,ExpectText,LimitLeft,LimitRight
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function GetTextAllChildObject(Main_Object,ExpectText,LimitLeft,LimitRight)

	Set oDesc = description.Create()

	oDesc("class").value="Label"
	Device("Device").OpenViewer
	
	If LimitRight<>"" or len(LimitRight)>0 Then
		percentwidth = LimitRight/100
		r_width = Main_Object.GetROProperty("right")
		r_max = (cint(r_width)*percentwidth) 
		print "r_width :"&r_width
		print "r_max :"&r_max
	End If
	Set l =  Main_Object.ChildObjects(oDesc)
	n = l.count()
	print "total Child Object : "&n
	
	iRet = False
	For i = 0 To n-1

		skip = False
		If LimitRight<>"" or len(LimitRight)>0 Then
			position_left = l(i).GetRoProperty("left")
			If LimitLeft="" or len(LimitLeft)=0 Then
				LimitLeft = 0
			End If
			If position_left >=r_max or position_left < LimitLeft Then
				skip = True
			End If			
		End If
  
	 	If  skip = False Then
'	 		l(i).highlight  
	 	    judge = l(i).GetRoProperty("text")
		    mc_index = l(i).GetRoProperty("mcindex")
		    resourceid = l(i).GetRoProperty("resourceid")
		    If len(judge)>0 Then
		    	print "["&i&"] - " &judge &" - " & mc_index&" - " & resourceid
		    End If
	 		
	 		If ExpectText<>"" Then
	 			If lcase(judge) = ExpectText Then
	 				iRet = True
	 				Exit for
	 			End If
	 		End If

	 	End If		 	      
	Next
	
	Set oDesc = Nothing
	Set Main_Object = Nothing 
End Function

'**********************************************************************************************************************************
'*  Function Name : CheckORAndLoad()
'*  Purpose:        Choose correct repositories and load
'*  Arguments:      -
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'*******************************************************************************************************************
Function CheckORAndLoad()

	Const sModule = "CheckORAndLoad"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	If repositoriescollection.Count > 0 Then
		repositoriescollection.RemoveAll	
	End If
	
	'Define ObjectRepository For Android Version UAT
	Environment("ObjectRepository") = Environment("RootProject") & "\Share\ObjectRepository\FastEasy_Android_UAT_CR.tsr"
	'Add runtime repository file
	repositoriescollection.Add Environment("ObjectRepository")
	
	OSType = Device("Device").GetROProperty("ostype")
	identifier = Device("Device").App("Common").GetROProperty("identifier")
	
	call logging("OSType : " & OSType,c_DEBUG,1)
	call logging("identifier : " & identifier,c_DEBUG,1)
	
	repositoriescollection.RemoveAll
	If OSType="ANDROID" Then
		If instr(lcase(identifier),"uat_cr")>0 Then  'Define ObjectRepository For Android Version UAT
			ORPath = "\Share\ObjectRepository\FastEasy_Android_UAT_CR.tsr"
		ElseIf instr(lcase(identifier),"sit_cr")>0 Then 'Define ObjectRepository For Android Version SIT
			ORPath = "\Share\ObjectRepository\FastEasy_Android_SIT_CR.tsr"
		ElseIf instr(lcase(identifier),"perf")>0 Then  'Define ObjectRepository For Android Version PERF
			ORPath = "\Share\ObjectRepository\FastEasy_Android_UAT_PERF.tsr"
		End If
	Else
		'Define ObjectRepository For IOS Version UAT
		ORPath = "\Share\ObjectRepository\FastEasy_IOS_UAT_CR.tsr"
	End If	
	
	Environment("ObjectRepository") = Environment("RootProject") & ORPath
	call logging("Loading repository file : " & Environment("ObjectRepository"),c_DEBUG,1)
	repositoriescollection.Add Environment("ObjectRepository")	
	
End Function

Public Function SendEmailWithAttachment(Emailfrom, Emailto, Emailpassword, attachmentfile)
  	Activity="Send email with an attachment"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	Set MyEmail=CreateObject("CDO.Message")
	Const cdoBasic=0 'Do not Authenticate
	Const cdoAuth=1 'Basic Authentication
	
	MyEmail.Subject = "Run Result : Fast Easy App"
	MyEmail.From    = Emailfrom
	MyEmail.To      = Emailto
	MyEmail.TextBody= "TEST MAIL BODY"
	
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
	
	'SMTP Server
	'hotmail = smtp.live.com
	'gmail = smtp.gmail.com
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver")="smtp.live.com"
	
	'SMTP Port
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=25	
	
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
	
	'Your UserID on the SMTP server
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") = Emailfrom
	
	'Your password on the SMTP server
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Emailpassword
	
	
	'Use SSL for the connection (False or True)
	MyEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true
	
	'Add attachment in full path
	'example = "D:\Automate\FastEasy\Result\iOS\Wave2DropBLot1\Notification_EN\iOS\TS_Notification_015\HTML\Notification_EN.html"
	MyEmail.AddAttachment attachmentfile
	
	MyEmail.Configuration.Fields.Update
	MyEmail.Send
	
	If err.number = 0 Then
  		call logging("Success to send email" & sMODULE,c_DEBUG,3)
	Else
		call logging("Failed to send email" & sMODULE,c_DEBUG,3)
	End If	

	Set MyEmail=nothing
	SendEmailWithAttachment = StampStatusToReport (sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)	
End Function

'**********************************************************************************************************************************
'*  Function Name : CheckObjectExist()
'*  Purpose:        Create Object Responsibility 
'*  Arguments:      object_exp,timeout
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function CheckObjectExist(object_exp,timeout)
On Error Resume Next
		If timeout=null or timeout="" Then
			timeout = 3
		End If
	 	If object_exp.Exist(timeout) Then
	 		Flag = 1
	 	Else
			Flag = -1	
	 	End If
	 	
		Flag_Err = CheckError()
		If Flag_Err = False Then
			Flag = -1
		End If	  		 	
		CheckObjectExist = Flag
	 
End Function

'**********************************************************************************************************************************
'*  Function Name : StampStatusToReport()
'*  Purpose:        Stamp test result status on HTML report
'*  Arguments:      iRet,Action,Descript,UIName,ExpectedResult
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function StampStatusToReport(sModule,iRet,Activity,descript,UIName,strValueToEnter,ExpectedResult)

	If iRet = True then
		Flag = 1
		If UIName<>"" or UIName<>null Then
			Reporter.ReportEvent micPass,"Click :'"&UIName &"'" ,"Success to "&Action&" :"&UIName
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter,"Success to "&Activity&" <font color=""blue"">["&UIName &"]</font> ,"&Descript , "Passed")
		Else
			Reporter.ReportEvent micPass,"Click :'"&UIName &"'" ,"Success to "&Action
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter,"Success to "&Activity&" ,"&Descript , "Passed")
		End If
			
	Else	
	
		Flag = -1
		If UIName<>"" or UIName<>null Then
			Reporter.ReportEvent micFail,"Click on button :'"&UIName &"'" ,"<font color=red>Failed</font> to "&Action&" :"&UIName
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter ,"<font color=red>Failed</font> to "&Activity&" <font color=""red"">["&UIName &"]</font> ,"&Descript , "Failed")
		Else
			Reporter.ReportEvent micFail,"Click on button :'"&UIName &"'" ,"Failed to "&Action
			Call ReportHTMLResult  ("Function:"&sModule,UIName, ExpectedResult, strValueToEnter ,"<font color=red>Failed</font> to "&Activity&" ,"&Descript , "Failed")
		End If 
	End If
	StampStatusToReport = Flag
	
End Function

Function GetDeviceProperty(property_type)

    Const sModule = "GetDeviceProperty"
    call logging("sModule " & sMODULE,c_DEBUG,2)
    
    call logging("AUT : " & Environment("AUT"),c_DEBUG,3)
    
	GetDeviceProperty = vbNullString
	
	If  objDictParent.Exists(lcase(Environment("AUT"))) Then
		Parent_Obj = objDictParent.Item(lcase(Environment("AUT")))
		call logging("Parent_Obj : " & Parent_Obj,c_DEBUG,2)
'		SplitVar=split(Parent_Obj,".")
		Set DeviceObject = Eval(Parent_Obj)
		
		Property_Value = DeviceObject.GetROProperty(property_type)
		call logging("Property_Value : " & Property_Value,c_DEBUG,2)
		Set DeviceObject = Nothing
		
		GetDeviceProperty = Property_Value
		
	else
		'Not found
		GetDeviceProperty = "N/A"
	End If
			
End Function

'**********************************************************************************************************************************
'*  Function Name :	reloginApp()
'*  Purpose:        Restart app and relogin
'*  Arguments:      OSType
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function reloginApp(OSType)
On Error Resume Next
		iRet = False
		waitOK = 10
		
		set DialogConfirmObj = Eval(Create_OR_Object("DialogConfirm_Txt",""))		
		For r_waitOK = 1 To waitOK Step 1
			DialogConfirmObj
			If DialogConfirmObj.Exist(1) Then
				DialogConfirmObj.Tap
				call logging("Found Confirm Button",c_DEBUG,3)
				Exit For
			Else
				call logging("wait for Confirm Button "&(waitOK-r_waitOK)+1&" second",c_DEBUG,3)
			End If	
		Next
		set DialogConfirmObj = Nothing
		
		Device("Device").CloseViewer
		Device("Device").OpenViewer
		Device("Device").App("Common").Launch DoNotInstall, Restart
		waittime = 30
		Set SCB_HeaderObj = Eval(Create_OR_Object("SCB_Header_Txt",""))
		For wait_page_r = 1 To waittime Step 1
			wait_time_countdown = (waittime+1) - wait_page_r
			call logging("wait for load landding page "&wait_time_countdown&" second",c_DEBUG,3)
			check_exist_obj = SCB_HeaderObj.exist(1)
			If check_exist_obj Then
				iRet = True
				Exit for
			End If
		Next
		Set SCB_HeaderObj = Nothing
		
		If iRet = True Then	
			If Environment("CheckScenarioLogin") <> 0 Then
									
				If OSType="ANDROID" Then
					Set Main_Acc_BtnObj = Eval(Create_OR_Object("Main_Acc_Btn",""))
					Main_Acc_BtnObj.Tap
					Set Main_Acc_BtnObj = Nothing
				Else
					Flag_Click = Select_MainMenuBar("acc")							
				End If
				
				strValueToEnter = Environment("CheckScenarioLogin")
				set objLoginObject = Eval(Create_OR_Object("Login_Number_Btn",""))
				For Iterator = 1 To len(strValueToEnter) Step 1	
				
					Pwd_Number=Mid(strValueToEnter,Iterator,1)	
					If OSType="ANDROID" Then		
						objLoginObject.SetTOProperty "resourceid","button_"&Pwd_Number				
					Else
						objLoginObject.SetTOProperty "text",Pwd_Number
					End If																	
					objLoginObject.Tap
					objLoginObject.RefreshObject
					call logging("Click Number : " & Pwd_Number,c_DEBUG,3)								
				Next
				
				set objLoginObject = Nothing	
				Flag_Err = CheckError()
				
				set AccSumPageObj = Eval(Create_OR_Object("AccSum_dep_txt",""))
				If AccSumPageObj.Exist(5) and iRet=True Then
					iRet = True
				Else
					iRet = False			
				End If
				set AccSumPageObj = Nothing
			End If					
		End If

		reloginApp = iRet
		
End Function

'**********************************************************************************************************************************
'*  Function Name :	restartWeb()
'*  Purpose:        Restart and relogin
'*  Arguments:      
'* 	Return Value:   -
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function restartWeb()

	If lcase(Environment("GetPlatform")) = "ie" Then
		
	End If
	
	
End Function


'**********************************************************************************************************************************
'*  Function Name : CheckError()
'*  Purpose:        check error 
'*  Arguments:      none
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'************************************************************************************************************************
Function CheckError()
On Error Resume Next	
	 If Err.Number = 0 Then
	      Flag = True
	      Environment("TS_ErrorDescption") = "::N/A"
	 Else 	
		  Flag = False	 
		  Environment("TS_ErrorFound") = 1
	      Environment("TS_ErrorDescption") = Err.Description
	      call logging(Err.Description ,c_DEBUG,3)
	      Err.Clear
	 End If
	 CheckError = Flag
	 
End Function

Function CreateTestCaseFile(ScenarioPath,SheetName,TemplateFilePath,DestinationFolder)
	
	'Copy Excel TestCase Template
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	SampleTC_Name = "TestCase_Sample.xls"
	
	If right(DestinationFolder,1)<>"\" Then
		DestinationFolder = DestinationFolder &"\"
	End If
	
	DestinationFilePath = DestinationFolder & SampleTC_Name
	
	print "TemplateFilePath : "&TemplateFilePath
	print "DestinationFolder : "&DestinationFolder
	print "DestinationFilePath : "&DestinationFilePath
	
	objFSO.CopyFile TemplateFilePath, DestinationFilePath
	Set objFSO = Nothing	
	
	'open and count row in test scenario
	SampleTC_Name = "TestCase_Sample.xls"

	Set objFileExcel= CreateObject("Excel.Application")
	Set objWB_x = objFileExcel.Workbooks.Open (ScenarioPath) 
	objFileExcel.Application.Visible = True
	objFileExcel.Application.DisplayAlerts = False
	
 		set mysheet_x = objFileExcel.ActiveWorkbook.Worksheets(SheetName)
 		
 		cols= mysheet_x.UsedRange.Columns.Count
		row = mysheet_x.UsedRange.Rows.Count
		
		TestCaseNameCol =  0
		
		Set TestCaseNameArr = createObject("System.Collections.ArrayList")

		'Get column from testcase name 
		For i = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,i).value) = "testcasename" Then
				print "Found "& mysheet_x.cells(1,i).value &" Column"
				TestCaseNameCol=i
				Exit For
			End If 
			
		Next

		'Add testcasename in Array List
		For j = 2 To row Step 1		
			testcasename = mysheet_x.cells(j,TestCaseNameCol).value
			If lcase(trim(testcasename)) = "endofrow" Then
				Exit For
			Else
				TestCaseNameArr.Add(testcasename)
			End If		
		Next
		
		'Get column from TestCaseFilePath
		For k = 1 To cols Step 1	
			If lcase(mysheet_x.cells(1,k).value) = "testcasefilepath" Then
					print "Found "& mysheet_x.cells(1,k).value &" Column"
						If mysheet_x.cells(2,k).value <>"" Then
							RenameTestcase = mysheet_x.cells(2,k).value
						Else
							RenameTestcase = SampleTC_Name
						End If
					Exit For
			End If 		
		Next
		
		RootProject = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")
		print "RootProject "&RootProject
			
		'Set TestCase and TestData Name
		TC_Path= RootProject&"\TestCases\"&RenameTestcase
		print TC_Path

		'Create file system for check file exist in path
		Set FileSys = CreateObject("Scripting.FileSystemObject")
				
		If FileSys.FileExists(TC_Path) Then
			print "Found exist file in path "&TC_Path
			TC_CreateAction = False
		Else
			print "Create TestCase file in path "&TC_Path
			TC_CreateAction = True
		End If

		'Create TestCase File
		If TC_CreateAction=True Then
			CreateTestCaseSheet objFileExcel,DestinationFilePath,TestCaseNameArr,"Create",1	
		End  If
	
		set mysheet_x = Nothing
		objWB_x.Save
		objWB_x.Close
		set objFileExcel = Nothing
		
		'Rename TestCase File
		Set objFso= CreateObject("Scripting.FileSystemObject")  
		objFso.MoveFile DestinationFilePath, TC_Path
		Set objFso= Nothing
		print "Rename File To ["&RenameTestcase&"]"
		print "Create TestCase ["&RenameTestcase&"] Complete"
End Function		


Function CreateTestCaseSheet(ByRef objFileExcel,DestinationFilePath,TestCaseNameArr,Action,SheetStart) 

	objFileExcel.Workbooks.Open (DestinationFilePath) 
	Set objExistWorkbook = objFileExcel.ActiveWorkbook
	
	For SheetLoop = 0 To (TestCaseNameArr.Count)-1 Step 1	
		
		ActiveSheet = SheetLoop + 1
	
		If SheetLoop = 0 Then
			Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Template_001")
			
		Else
			Set objWorkSheet = objFileExcel.ActiveWorkbook.Worksheets(TestCaseNameArr.Item(SheetLoop-1)&" (2)")	
		End If
		
'		'Rename Sheet
		objWorkSheet.Name = TestCaseNameArr.Item(SheetLoop)
		objWorkSheet.cells(2,1).value = TestCaseNameArr.Item(SheetLoop)

		'Create Sheet1 
		If SheetLoop = 0 Then
			objFileExcel.Sheets.Add , objFileExcel.Worksheets(objFileExcel.Worksheets.Count)
		End If
		
		'Set Object for sheet1
		Set objNewWorkSheet = objFileExcel.ActiveWorkbook.Worksheets("Sheet1")	
		
		If SheetLoop <> (TestCaseNameArr.Count)-1 Then					
			'Clone Sheet
			objWorkSheet.Copy objNewWorkSheet
		Else
			objFileExcel.ActiveWorkbook.Worksheets("Sheet1").Delete
		End If
		
		print "Create Sheet ["&TestCaseNameArr.Item(SheetLoop) &"] complete"	
	Next

	objExistWorkbook.Save
	objExistWorkbook.Close
	Set objWorkSheet = Nothing
	Set objExistWorkbook = Nothing
End Function 

'**********************************************************************************************************************************
'*  Function Name :	SendLineNotify()
'*  Purpose:        Send line notification
'*  Arguments:      Message
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function SendLineNotify(Message)

	  	Set oShell = CreateObject ("WSCript.shell")
	  	Environment("RootProject")      = replace(replace(Environment.Value("TestDir"),"\Scripts\",""),Environment.Value("TestName"),"")
	  	Command = "cmd /K pushd "&Environment("RootProject")&"\LineNotify\ &java -jar GLineNotify.jar FkjBMQCSipYni33vRGak03B8Ala9yTJksfAjUFYY0vd """&Message&""""
		oShell.Run Command,1,False
		
End Function

'**********************************************************************************************************************************
'*  Function Name :	restartWeb()
'*  Purpose:        Close browser and open again
'*  Arguments:      Browser
'* 	Return Value:   Flag True or False
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function restartWeb(Browser_Input)
	On Error Resume Next
	Dim browser
	'Close Browser
	Set WshShell = CreateObject("WScript.Shell")
	If lcase(Browser_Input) = "ie" Then
		browser = "iexplore.exe"
	ElseIf lcase(Browser_Input) = "chrome" Then
		browser = "chrome.exe"
	ElseIf lcase(Browser_Input) = "firefox" Then
		browser = "firefox.exe"
	End If

	Set oExec = WshShell.Exec("taskkill /fi ""imagename eq "&browser&"""")
	
'	Do While oExec.Status = 0
'	     wait 100
'	Loop

	restartWeb = True
	call logging("Close Browser "&Browser_Input,c_DEBUG,3)
	
	'Open Browser
	URL="http://ccbsit.se.scb.co.th/"
	
	If lcase(Browser_Input) = "ie" Then
		iRet = LaunchIE(URL)
		call logging("IE is launched with url : '"& URL & " Done",c_DEBUG,3)
	ElseIf lcase(Browser_Input) = "chrome" Then
		iRet = LaunchChrome(URL)
		call logging("Chrome is launched with url : '"& URL & " Done",c_DEBUG,3)
	ElseIf lcase(Browser_Input) = "firefox" Then
		iRet = LaunchFirefox(URL)
		call logging("Firefox is launched with url : '"& URL & " Done",c_DEBUG,3)
	End If
	
	wait 5
	
End Function
