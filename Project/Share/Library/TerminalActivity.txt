Function TE_CheckData(strValueToEnter,ExpectedResult)

	On Error Resume Next

	Dim mySendKeys , LoginAction ,ExcelData ,UserVariable
  	Const sModule = "TE_CheckData"
  	
  	Activity="Get Data from host"
  	
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	SplitVar = split(strValueToEnter,";")
	ExcelData = trim(SplitVar(0))
	UserVariable = trim(SplitVar(1))
	BalanceType = "available"
	
	LoginAction = TE_Login("11114","one1776")
	
	If LoginAction Then
	
	End If 


	set mySendKeys = CreateObject("WScript.shell")

End Function


Function TE_Login(UserName,Password)

	On Error Resume Next
	
	Dim  strSheetName
	set mySendKeys = CreateObject("WScript.shell")
	Environment("RootProject")      = replace(replace(Environment("TestDir"),"\Scripts\",""),Environment("TestName"),"")
'	SystemUtil.CloseProcessByName "pcsws.exe"
	wait 3

	if Not (CheckRunningProcess("pcsws.exe")) then
		SystemUtil.Run Environment("RootProject")&"\Share\ExternalLibrary\ibm3270.ws"
		
		if not(TeWindow("short name:=A").TeScreen("label:=screen.*").TeField("field id:=1762").Exist(300)) then
			TE_Login = False
			Exit Function
		End if
		
	End If

	Wait 3	

	If not(TeWindow("short name:=A").TeScreen("label:=screen.*").TeField("field id:=1762").Exist(5)) then
		Window("regexpwndclass:=PCSWS:Main:00400000").Activate
		Window("regexpwndclass:=PCSWS:Main:00400000").WinMenu("menuobjtype:=2").Select "Communication;Connect"	
	End If 
'		TeWindow("short name:=A").Activate

		'Get Data From Profile Excel
		ExcelFilePath = Environment("RootProject")&"\Profile\Profile.xls"
		DbConn = ConnectToDB(ExcelFilePath)
		strSheetName="profile"
		StrQuery="SELECT * FROM [" & strSheetName & "$]"
		Set RsRecord = CreateObject("ADODB.Recordset")
		RsRecord.Open StrQuery, DbConn, 1, 3		
		Set oRsExcel= RsRecord
				
		X_Environment="Environment"
				
		oRsExcel.MoveFirst
		For i = 1 To oRsExcel.RecordCount Step 1
			If oRsExcel.Fields(0).Value = X_Environment Then
				TestEnvironment = oRsExcel.Fields(1).Value
				call logging("Test Environment : "&TestEnvironment,c_DEBUG,3)
				Exit For
			End If
		oRsExcel.MoveNext
		Next
				
		oRsExcel.Close
		Set RsRecord = Nothing
	
		If lcase(TestEnvironment)="sit" Then	
			'SIT
			TeWindow("short name:=A").TeScreen("label:=screen.*").TeField("field id:=1762").Set "l cpskfnf1"	
		Else
			'UAT
			TeWindow("short name:=A").TeScreen("label:=screen.*").TeField("field id:=1762").Set "l cpshfnf1"
		End If	
		TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey TE_ENTER
		TeWindow("short name:=A").TeScreen("label:=Si*").TeField("attached text:=Userid").Set UserName
		TeWindow("short name:=A").TeScreen("label:=Si").TeField("attached text:=Password").Set Password
		TeWindow("short name:=A").TeScreen("label:=Si").SendKey TE_ENTER
		TeWindow("short name:=A").Activate
		mySendKeys.SendKeys("{BREAK}")
		iRet = True
'	Else
'		iRet = False
'		print "pcomm not found"	
'	End  If
	Set mySendKeys = Nothing
	TE_Login = iRet
End Function

'**********************************************************************************************************************************
'*  Function Name : TE_GetBalanceFromHost()
'*  Purpose:        Get Available balance from host
'*  Arguments:      strValueToEnter
'* 	Return Value:   AcctBal
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function TE_GetBalanceFromHost(strValueToEnter,ExpectedResult)

On Error Resume Next

Dim mySendKeys , LoginAction ,AcctNumber ,UserVariable , AccountType
  	Const sModule = "TE_GetBalanceFromHost"
  	Activity="Get Available balance from host"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	SplitVar = split(strValueToEnter,";")
	AcctNumber = trim(SplitVar(0))
	UserVariable = trim(SplitVar(1))
	'BalanceType = trim(SplitVar(1))
	BalanceType = "available"
	'BalanceType ="Current"

set mySendKeys = CreateObject("WScript.shell")

	tmpAcct=trim(AcctNumber)
	Branch=mid(tmpAcct,1,3)
	AcctType=mid(tmpAcct,4,1)
	AcctDigit=mid(tmpAcct,5,6)
	
	If (CInt(Branch)>=401 and CInt(Branch<=469)) Then  ' Branch1000 
		
		AcctType=Branch
		Branch="000"
		AcctDigit=mid(tmpAcct,4,7)  ' acct 7 digits
		
	else
	
		If AcctType = "2" or AcctType = "4" or AcctType = "5" or AcctType = "6" Then 'Saving
			
			AcctType = "10" & AcctType
		
		elseIf AcctType = "0" or AcctType = "8" then  ' Long Term
		
			AcctType = "50" & AcctType
		
		elseIf AcctType = "1" then  ' Fix
		
			AcctType="301"	
			
		elseIf AcctType = "3" then  'Current
		
			AcctType="0703"

		End If
	
	End If
		
		'Get TE User / Passs from excel
	TE_Value = 	GetUserPassTE()
	SpltVar = split(TE_Value,";")
	TE_User = SpltVar(0)
	TE_Pass = SpltVar(1)
	LoginAction = TE_Login(TE_User,TE_Pass)	
	
	If LoginAction Then
		
		If (CInt(AcctType)>=401 and CInt(AcctType)<=460) or AcctType="466" or AcctType="467" or AcctType = "102" or AcctType = "104" or AcctType = "105" or AcctType = "106"  or AcctType = "500" or AcctType = "508" Then  'Saving
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "sti1"
			TeWindow("short name:=A").TeScreen("label:=sti1").SendKey TE_ENTER
			AccountType="saving"
		ElseIf (CInt(AcctType)>=461 and CInt(AcctType)<=465) or AcctType = "301" Then ' Fix
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "stiq"
			TeWindow("short name:=A").TeScreen("label:=stiq").SendKey TE_ENTER
			AccountType="fix"
		ElseIf AcctType="468" or AcctType="469" or AcctType = "0703" Then 'Current
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "imi1"
			TeWindow("short name:=A").TeScreen("label:=imi1").SendKey TE_ENTER
			AccountType="current"
		End If
		
		
		
		If TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Account").exist(3) Then
			TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Account").Set AcctDigit
		else
			TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Customer Number").Set AcctDigit
		End If
		
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl2").Set "764"
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl3").Set Branch
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl4").Set AcctType
		TeWindow("short name:=A").TeScreen("label:=Command.*").SendKey TE_ENTER
		wait 1
		
		If AccountType="saving" Then		
			If lcase(BalanceType)="available" Then
				TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=464").Highlight
				AcctBal=trim(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=464").GetROProperty("text"))
			Else
				TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=544").Highlight
				AcctBal=trim(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=544").GetROProperty("text"))
			End If
		ElseIf AccountType="current" Then	
				TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=624").Highlight
				AcctBal=trim(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=624").GetROProperty("text"))
		ElseIf AccountType="fix" Then	
				TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=456").Highlight
				AcctBal=trim(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=456").GetROProperty("text"))
		End If

	Else
		iRet = False
		
		Environment("TestCaseStatusSummary") = "TERMINAL_FAIL"
	End if

	Set mySendKeys = Nothing	
	If len(AcctBal) > 0 Then
		Environment(UserVariable)=FormatNumber(AcctBal,2)
		descript="Get Balance from Host Value ["&FormatNumber(AcctBal,2)&"]"
		iRet = True
	Else
		Environment(UserVariable)="::N/A"
		descript="Can not get balance from Host"
		Environment("TestCaseStatusSummary") = "TERMINAL_FAIL"		
		iRet = False
	End If

	call logging(descript,c_DEBUG,3) 

	TE_GetBalanceFromHost = StampStatusToReport (sModule,iRet,Activity,descript,"",strValueToEnter,ExpectedResult) 
	
'	SystemUtil.CloseProcessByName "pcsws.exe"
	TeWindow("short name:=A").WinMenu("menuobjtype:=2").Select "Communication;Disconnect"
	hWnd = TeWindow("short name:=A").GetROProperty("hwnd")
	Window("hwnd:=" & hWnd).Minimize

'TE_GetBalanceFromHost=CDBL(AcctBal)   
	    
	wait 3
End Function

'**********************************************************************************************************************************
'*  Function Name : TE_GetLastTransValue()
'*  Purpose:        Get last transaction value from host
'*  Arguments:      strValueToEnter
'* 	Return Value:   retValue
'* 	Author By		ANUPOL H. (AMS-GABLE)
'**********************************************************************************************************************************
Function TE_GetLastTransValue(strValueToEnter,ExpectedResult)

On Error Resume Next

Dim mySendKeys ,LoginAction ,StartRow ,Transaction_List ,retValue ,AcctNumber ,TransColumn ,UserVariable
  	Const sModule = "TE_GetLastTransValue"
  	Activity="Get last transaction value from host"
  	call logging("sModule " & sMODULE,c_DEBUG,3)
  	
	SplitVar = split(strValueToEnter,";")
	AcctNumber = trim(SplitVar(0))
	TransColumn= trim(SplitVar(1))
	UserVariable = trim(SplitVar(2))

set mySendKeys = CreateObject("WScript.shell")

	tmpAcct=trim(AcctNumber)
	Branch=mid(tmpAcct,1,3)
	AcctType=mid(tmpAcct,4,1)
	AcctDigit=mid(tmpAcct,5,6)
	
	If (CInt(Branch)>=401 and CInt(Branch<=469)) Then  ' Branch1000 
		
		AcctType=Branch
		Branch="000"
		AcctDigit=mid(tmpAcct,4,7)  ' acct 7 digits
		
	else
	
		If AcctType = "2" or AcctType = "4" or AcctType = "5" or AcctType = "6" Then 'Saving
			
			AcctType = "10" & AcctType
		
		elseIf AcctType = "0" or AcctType = "8" then  ' Long Term
		
			AcctType = "50" & AcctType
		
		elseIf AcctType = "1" then  ' Fix
		
			AcctType="301"	
			
		elseIf AcctType = "3" then  'Current
		
			AcctType="0703"

		End If
	
	End If
	
	'Get TE User / Passs from excel
	TE_Value = 	GetUserPassTE()
	SpltVar = split(TE_Value,";")
	TE_User = SpltVar(0)
	TE_Pass = SpltVar(1)
	LoginAction = TE_Login(TE_User,TE_Pass)		
	If LoginAction Then
		
'		TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "sti3"
'		TeWindow("short name:=A").TeScreen("label:=sti3").SendKey TE_ENTER
			
		If (CInt(AcctType)>=401 and CInt(AcctType)<=460) or AcctType="466" or AcctType="467" or AcctType = "102" or AcctType = "104" or AcctType = "105" or AcctType = "106"  or AcctType = "500" or AcctType = "508" Then  'Saving
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "sti3"
			TeWindow("short name:=A").TeScreen("label:=sti3").SendKey TE_ENTER
		ElseIf (CInt(AcctType)>=461 and CInt(AcctType)<=465) or AcctType = "301" Then ' Fix
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "stiq"
			TeWindow("short name:=A").TeScreen("label:=stiq").SendKey TE_ENTER
		ElseIf AcctType="468" or AcctType="469" or AcctType = "0703" Then 'Current
			TeWindow("short name:=A").TeScreen("label:=screen.*").SendKey "imi1"
			TeWindow("short name:=A").TeScreen("label:=imi1").SendKey TE_ENTER
		End If
		
		If TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Account").exist(3) Then
			TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Account").Set AcctDigit
		else
			TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Customer Number").Set AcctDigit
		End If
		
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl2").Set "764"
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl3").Set Branch
		TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("attached text:=Ctl4").Set AcctType
		TeWindow("short name:=A").TeScreen("label:=Command.*").SendKey TE_ENTER
		wait 1
		
		Do 
			mySendKeys.SendKeys("{F1}")	
			wait 0.5	
		Loop While instr(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("field id:=1842").GetRoProperty("text"),"CANNOT PAGE FORWARD") = 0 
		print "Found last page"			

			StartRow = 11
		    Set RegObj = CreateObject("vbscript.regexp")
		    RegObj.Pattern = "(\d{2})+[\/-](\d{2})+[\/-](\d{2})+"
		    RegObj.Global = True
			
			If RegObj.test(left(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("start row:=12","start column:=2").GetRoProperty("text"),9)) = False Then
				mySendKeys.SendKeys("{F2}")	
			End If	
			
			For i = 1 To 11 Step 1
				If RegObj.test(left(TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("start row:="&StartRow+i,"start column:=2").GetRoProperty("text"),9)) Then
'					TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("start row:="&StartRow+i,"start column:=2").Highlight
					Transaction_List = TeWindow("short name:=A").TeScreen("label:=Command.*").TeField("start row:="&StartRow+i,"start column:=2").GetRoProperty("text")
					call logging("Transaction list ["&Transaction_List&"]",c_DEBUG,3)  
				Else 
					Exit For
				End If	
			Next		
			Set RegObj = Nothing
	End if

	Set mySendKeys = Nothing	
'	SystemUtil.CloseProcessByName "pcsws.exe"
	
	lastTextNum =  instr(Transaction_List,"  ")
	NextText = Mid(Transaction_List,lastTextNum,len(Transaction_List))

	If lcase(TransColumn) = "creditamount" Then
		retValue = trim(Mid(NextText,1,len(NextText)/2))
	ElseIf lcase(TransColumn) = "balance" Then
		retValue = trim(Mid(NextText,len(NextText)/2,len(NextText)))
	End If

	If len(retValue) > 0 Then
		Environment(UserVariable)=FormatNumber(retValue,2)
		descript="Get Last transaction value from Host ["&FormatNumber(retValue,2)&"]"	
		iRet = True
	Else
		Environment(UserVariable)="::N/A"
		descript="Can not get value from Host"	

		Environment("TestCaseStatusSummary") = "TERMINAL_FAIL"
		
		iRet = False
	End If	
	call logging(descript,c_DEBUG,3) 
	TE_GetLastTransValue = StampStatusToReport (sModule,iRet,Activity,descript,"",strValueToEnter,ExpectedResult)
'	SystemUtil.CloseProcessByName "pcsws.exe"
	TeWindow("short name:=A").WinMenu("menuobjtype:=2").Select "Communication;Disconnect"
	hwnd = TeWindow("short name:=A").GetROProperty("hwnd")
	Window("hwnd:=" & hWnd).Minimize
	wait 3
End Function

Function GetUserPassTE()

				On Error Resume Next

				'Get Data From Profile Excel
				ExcelFilePath = Environment("RootProject")&"\Profile\Profile.xls"
				DbConn = ConnectToDB(ExcelFilePath)
				strSheetName="profile"
				StrQuery="SELECT * FROM [" & strSheetName & "$]"
				Set RsRecord = CreateObject("ADODB.Recordset")
				RsRecord.Open StrQuery, DbConn, 1, 3		
				Set oRsExcel= RsRecord
				
				X_TE_User="TE_USERNAME"
				X_TE_Pass="TE_PASSWORD"
				
				oRsExcel.MoveFirst
				For i = 1 To oRsExcel.RecordCount Step 1
					If oRsExcel.Fields(0).Value = X_TE_User Then
						TE_User = trim(oRsExcel.Fields(1).Value)
					End If
					
					If oRsExcel.Fields(0).Value = X_TE_Pass Then
						TE_Pass = trim(oRsExcel.Fields(1).Value)
					End If
					
					oRsExcel.MoveNext
				Next
			GetUserPassTE = TE_User&";"&TE_Pass	
End Function
