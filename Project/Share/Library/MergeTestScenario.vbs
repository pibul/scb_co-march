'
' Owner by Suebsakul.A
' Description : Script for groupping data from TestSecenario_xxx.xls to TestScenario_run.xls
'               Perform for Automate engine instead human
'
'
' How run script on command line?
'    - Open command line
'    - Goto path keep script
'    - <Path>\Cscript G-Able_Get_ScenarioRun.vbs folderfilepath\
'
'Sub Function


Sub CopyFormatValue(NoOfRow,NoOfColume,ObjCellScr, ObjCellDes)
	'ObjCellScr.Copy
	'ObjCellDes.Paste
	'ObjCellDes.Format = ObjCellScr.Format



	If NoOfColume = 1 And NoOfRow <> 1 Then
		ObjCellDes.Value = NoOfRow - 1
	else
		ObjCellDes.Value = ObjCellScr.Value
	End If
End Sub

Function ListFile (Path)
	ReDim  FolderList(200)
	Dim i
	'List filename process
	Set oRE = New RegExp
	oRE.Pattern = "TestScenario_.*.xls$"

	'List ignore filename
	Set oRENot = New RegExp
	oRENot.Pattern = "TestScenario_(Common|Run).xls$"

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(Path)
	Set colFiles = objFolder.Files

	i = 0
	For Each objFile in colFiles

		'Filter filename
		If oRE.Test(objFile.Name) = True And oRENot.Test(objFile.Name) = False then
			FolderList(i) = Path & objFile.Name
			'Wscript.Echo objFile.Name
			i = i + 1
		End if
	Next
	
	'Resize Array for dynamic
	ReDim Preserve FolderList(i-1)

	ListFile = FolderList
End Function

Sub CloseAllExcel()
	Dim objWMIService, objProcess, colProcess

	Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\.\root\cimv2") 
	Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = " & "'EXCEL.EXE'")

	For Each objProcess in colProcess
		objProcess.Terminate()
	Next
	
End Sub

Sub CreateHeader(ObjnewWorkSheetDes)
	Dim ColumeNo : ColumeNo = 1
	
	For Each listArr In ArrHeader

		If listArr <> "" Then	
			ObjnewWorkSheetDes.Cells(1,ColumeNo) = listArr
			ObjnewWorkSheetDes.Cells(1,ColumeNo).Font.Color = RGB(255, 255, 255)
			ObjnewWorkSheetDes.Cells(1,ColumeNo).Interior.Color = RGB(51, 102, 204)
			ObjnewWorkSheetDes.Cells(1,ColumeNo).Font.FontStyle = Bold
			ColumeNo = ColumeNo + 1
		End If
	Next

End Sub

Sub MappingtoCol(ByRef WorkSheet,Row,C_GtestScenario)
	'Mapping to new WorkSheet
	With C_GtestScenario
		WorkSheet.Cells(Row,GetColume("No",ArrHeader)).Value           = .TestScenario_No
		WorkSheet.Cells(Row,GetColume("TestCaseID",ArrHeader)).Value   = .TestScenario_TestCaseID
		WorkSheet.Cells(Row,GetColume("Description",ArrHeader)).Value  = .TestScenario_Description
		WorkSheet.Cells(Row,GetColume("ScenarioFile",ArrHeader)).Value = .TestScenario_ScenarioFile
		WorkSheet.Cells(Row,GetColume("TestCaseName",ArrHeader)).Value = .TestScenario_TestCaseName
		WorkSheet.Cells(Row,GetColume("RunStatus",ArrHeader)).Value    = .TestScenario_RunStatus
		WorkSheet.Cells(Row,GetColume("TestCaseFilePath",ArrHeader)).Value = .TestScenario_TestCaseFilePath
		WorkSheet.Cells(Row,GetColume("Platform",ArrHeader)).Value           = .TestScenario_Platform
		WorkSheet.Cells(Row,GetColume("Prerequisite",ArrHeader)).Value           = .TestScenario_Prerequisite
		WorkSheet.Cells(Row,GetColume("DataDriven",ArrHeader)).Value       = .TestScenario_DataDriven
		WorkSheet.Cells(Row,GetColume("Keyword",ArrHeader)).Value          = .TestScenario_Keyword
	End With
	
End Sub

Sub ClearValue(ByRef C_GtestScenario)

	With C_GtestScenario
		.TestScenario_No = ""
		.TestScenario_TestCaseID   = ""
		.TestScenario_TestCaseName = ""
		.TestScenario_RunStatus    = ""
		.TestScenario_TestCaseFilePath = ""
		.TestScenario_ScenarioFile = ""
		.TestScenario_DataDriven  = ""
		.TestScenario_Keyword     = ""
		.TestScenario_Description = ""
		.TestScenario_Platform      = ""
		.TestScenario_Prerequisite      = ""
	End With

End Sub

Function GetColume(ColumeName,ByRef ArrHeaderPerSheet)
	Dim ColumeNo : ColumeNo = 0
	Dim Flag : Flag = False
	
	'Debug
	'Wscript.Echo "ColumeName : " & ColumeName

	For Each listColume In ArrHeaderPerSheet
		
		If listColume = ColumeName Then
			Flag = True
			Exit for
		End If

		ColumeNo = ColumeNo + 1
		
	Next
	
	If Flag = True Then
		GetColume = ColumeNo
	Else
		GetColume = 0
	End If
End Function

Class G_TESTSCENARIO

	Private No
	Private TestCaseID
	Private Description
	Private TestCaseName
	Private RunStatus
	Private TestCaseFilePath
	Private Platform
	Private Prerequisite
	Private DataDriven
	Private Keyword
	Private ScenarioFile
 
	Public Property Let TestScenario_No(value)
		No = value
	End Property

	Public Property Get TestScenario_No()
		TestScenario_No = No
	End Property

	Public Property Let TestScenario_TestCaseID(value)
		TestCaseID = value
	End Property

	Public Property Get TestScenario_TestCaseID()
		TestScenario_TestCaseID = TestCaseID
	End Property

	Public Property Let TestScenario_Description(value)
		Description = value
	End Property

	Public Property Get TestScenario_Description()
		TestScenario_Description = Description
	End Property
 
	Public Property Let TestScenario_TestCaseName(value)
		TestCaseName = value
	End Property

	Public Property Get TestScenario_TestCaseName()
		TestScenario_TestCaseName = TestCaseName
	End Property

	Public Property Let TestScenario_RunStatus(value)
		RunStatus = value
	End Property

	Public Property Get TestScenario_RunStatus()
		TestScenario_RunStatus = RunStatus
	End Property

	Public Property Let TestScenario_TestCaseFilePath(value)
		TestCaseFilePath = value
	End Property

	Public Property Get TestScenario_TestCaseFilePath()
		TestScenario_TestCaseFilePath = TestCaseFilePath
	End Property

	Public Property Let TestScenario_Platform(value)
		Platform = value
	End Property

	Public Property Get TestScenario_Platform()
		TestScenario_Platform = Platform
	End Property

	Public Property Let TestScenario_Prerequisite(value)
		Prerequisite = value
	End Property

	Public Property Get TestScenario_Prerequisite()
		TestScenario_Prerequisite = Prerequisite
	End Property

	Public Property Let TestScenario_DataDriven(value)
		DataDriven = value
	End Property

	Public Property Get TestScenario_DataDriven()
		TestScenario_DataDriven = DataDriven
	End Property

	Public Property Let TestScenario_Keyword(value)
		Keyword = value
	End Property

	Public Property Get TestScenario_Keyword()
		TestScenario_Keyword = Keyword
	End Property

	Public Property Let TestScenario_ScenarioFile(value)
		ScenarioFile = value
	End Property

	Public Property Get TestScenario_ScenarioFile()
		TestScenario_ScenarioFile = ScenarioFile
	End Property
End Class

'Main Script
Dim CopyFrom 'As Object
Dim CopyTo 'As Object
Dim CopyThis 'As Object
Dim xl
Dim filesys
Set filesys = CreateObject("Scripting.FileSystemObject")

'Dim strInputPath : strInputPath = "D:\GGroupScenarios\"
'Dim strNewFileName : strNewFileName = "D:\GGroupScenarios\TestScenario_Run.xls"

Dim strInputPath : strInputPath = Wscript.Arguments(0)
Dim strNewFileName : strNewFileName = strInputPath&"TestScenario_Run.xls"
 

Dim oREFilterSheet
Set oREFilterSheet = New RegExp
	oREFilterSheet.Pattern = "(CoverPage|Index|List-Devices|TestGoogle|Definition)"

Dim RowProcess
Dim RowRange : RowRange = 1000
Dim newRowTH : newRowTH = 1
Dim newRowEN : newRowEN = 1
Dim RunStatus : RunStatus = "No"
Dim TestCaseName : TestCaseName = "EndOfRow"
ReDim ArrHeaderPerSheet(30)
Dim ArrHeader
	ArrHeader = array( "","No","TestCaseID","Description","TestCaseName","RunStatus","TestCaseFilePath","ScenarioFile","Platform","Prerequisite","DataDriven","Keyword")


'Close all excel program
Call CloseAllExcel

Set xl = CreateObject("Excel.Application")

'Setup excel working on back group
'xl.Visible = False
xl.Visible = false
xl.DisplayAlerts = False

Set GtestScenario = New G_TESTSCENARIO
Set objWorkbook = xl.Workbooks.Add()

Wscript.echo "input file path :"&strInputPath

'Delete file
'On Error Resume Next
filesys.DeleteFile strNewFileName

'Save as file extension .xls
objWorkbook.SaveAs strNewFileName, 56
Set CopyTo   = xl.Workbooks.Open(strNewFileName)

CopyTo.Worksheets.Add

'Set object new Sheet
Set CopyTonewsheet_TH = CopyTo.Worksheets(1)
Set CopyTonewsheet_EN = CopyTo.Worksheets(2)

CopyTonewsheet_TH.Name = "CoMarch_Web_TH"
CopyTonewsheet_EN.Name = "CoMarch_Web_EN"

'Create new Header 
CreateHeader(CopyTonewsheet_TH)
CreateHeader(CopyTonewsheet_EN)
newRowTH = newRowTH + 1
newRowEN = newRowEN + 1

WScript.Sleep(10000)

'Add Prerequisite scenarios

With GtestScenario
	.TestScenario_No = newRowTH - 1
	.TestScenario_TestCaseID   = "LaunchWeb_TH"
	.TestScenario_TestCaseName = "LaunchWeb_TH"
	.TestScenario_RunStatus    = "Yes"
	.TestScenario_ScenarioFile = "D:\SCB_CoMarch\Project\TestCases\TestCase_WB_CMT_Common.xls"
	.TestScenario_TestCaseFilePath = "TestCase_WB_CMT_Common.xls"
	.TestScenario_DataDriven  = "Yes"
	.TestScenario_Keyword     = "UAT_TH"
	.TestScenario_Description = "Command Scenario Step"
	.TestScenario_Platform      = "Web"
	.TestScenario_Prerequisite      = "Yes"
End With

Call MappingtoCol(CopyTonewsheet_TH,newRowTH,GtestScenario)



With GtestScenario
	.TestScenario_No = newRowTH - 1
	.TestScenario_TestCaseID   = "LaunchWeb_EN"
	.TestScenario_TestCaseName = "LaunchWeb_EN"
	.TestScenario_RunStatus    = "Yes"
	.TestScenario_ScenarioFile = "D:\SCB_CoMarch\Project\TestCases\TestCase_WB_CMT_Common.xls"
	.TestScenario_TestCaseFilePath = "TestCase_WB_CMT_Common.xls"
	.TestScenario_DataDriven  = "Yes"
	.TestScenario_Keyword     = "UAT_EN"
	.TestScenario_Description = "Command Scenario Step"
	.TestScenario_Platform      = "Web"
	.TestScenario_Prerequisite      = "Yes"
End With

Call MappingtoCol(CopyTonewsheet_EN,newRowEN,GtestScenario)

newRowTH = newRowTH + 1
newRowEN = newRowEN + 1


ListFilesExcelProcess = ListFile(strInputPath)

'List all file excel prefix : TestScenario_*.xls
For Each listSrcFilename in ListFilesExcelProcess

	'comment by num 19/1/2018
	wscript.Echo listSrcFilename

	Set CopyFrom = xl.Workbooks.Open(listSrcFilename)
	Wscript.Echo "All Sheet = " & CopyFrom.Worksheets.Count

	'Loop for all sheet for excel file
	For SheetNumber = 1 To CopyFrom.Worksheets.Count
		
		WorkingSheet = CopyFrom.Worksheets(SheetNumber).Name

		'Filter sheet
		'If  (WorkingSheet <> "CoverPage" and _
		'	 WorkingSheet <> "Index" and _
		'	 WorkingSheet <> "List-Devices") Then
		
		If (oREFilterSheet.Test(WorkingSheet) = False) Then

			Wscript.Echo "Sheet : " & SheetNumber & " = " & WorkingSheet

			'CopyFrom.Sheets(SheetNumber).Activate()				

			With CopyFrom.Worksheets(SheetNumber)
				
				'List all colume of header record
				Set rng = .Range("A1:Z" & RowRange)
				
				RowProcess = 1
				For Each row In rng.Rows
					
					Colume_Count = 0
					RunStatus = "No"
					'Wscript.Echo "Row : " & newRowTH
					

					CellValue = row.Cells(1)
					
					'Define header per sheet
					If CellValue = "No" Then
						ReDim Preserve ArrHeaderPerSheet(30)

						Colume_Count = 0

						For Each cell In row.Cells
							Colume_Count = Colume_Count + 1
							ArrHeaderPerSheet(Colume_Count) = cell.value

							If cell.value = "" Then
								Exit for
							End if
			
						Next
						
						ReDim Preserve ArrHeaderPerSheet(Colume_Count - 1)

					Else
						'None header

'						For Each listArrHeader In ArrHeader
'							NoSrcCol = GetColume("No",ArrHeaderPerSheet)
'						Next
						
						ClearValue(GtestScenario)

						With GtestScenario
							'.TestScenario_No = row.Cells(GetColume("No",ArrHeaderPerSheet))
							
							.TestScenario_TestCaseID   = row.Cells(GetColume("TestCaseID",ArrHeaderPerSheet))						

							.TestScenario_TestCaseName = row.Cells(GetColume("TestCaseName",ArrHeaderPerSheet))
							.TestScenario_RunStatus    = row.Cells(GetColume("RunStatus",ArrHeaderPerSheet))
							.TestScenario_TestCaseFilePath = row.Cells(GetColume("TestCaseFilePath",ArrHeaderPerSheet))
							.TestScenario_DataDriven = row.Cells(GetColume("DataDriven",ArrHeaderPerSheet))
							.TestScenario_Keyword    = row.Cells(GetColume("Keyword",ArrHeaderPerSheet))

							If (GetColume("Description",ArrHeaderPerSheet) <> 0) then
								.TestScenario_Description  = row.Cells(GetColume("Description",ArrHeaderPerSheet))
							End If

							If (GetColume("Platform",ArrHeaderPerSheet) <> 0) then
								.TestScenario_Platform     = row.Cells(GetColume("Platform",ArrHeaderPerSheet))
							End If

							If (GetColume("Prerequisite",ArrHeaderPerSheet) <> 0) then
								.TestScenario_Prerequisite     = row.Cells(GetColume("Prerequisite",ArrHeaderPerSheet))
							End If

							If .TestScenario_TestCaseName = "EndOfRow" then
								Exit For
							'ElseIf .TestScenario_RunStatus = "" And .TestScenario_TestCaseName = "" Then
							'	Exit For
							End If
							
							.TestScenario_ScenarioFile = listSrcFilename

							If LCase(.TestScenario_RunStatus) = "yes" And LCase(.TestScenario_Prerequisite) <> "yes" Then
								If InStr(LCase(.TestScenario_Keyword),"_en") > 0 Then  
									.TestScenario_No = newRowEN - 1
									Call MappingtoCol(CopyTonewsheet_EN,newRowEN,GtestScenario)
										'Increase newRowEN
										newRowEN = newRowEN + 1
								Else
									.TestScenario_No = newRowTH - 1
									Call MappingtoCol(CopyTonewsheet_TH,newRowTH,GtestScenario)
										'Increase newRowTH
										newRowTH = newRowTH + 1
								End If

							End If
							
						End With
						
						'Debug
						'Wscript.Echo "Colume_RunStatus = " & Colume_RunStatus
						'Wscript.Echo "Colume_TestCaseName = " & Colume_TestCaseName
						
						'Debug				
						'Wscript.Echo "RunStatus = " & RunStatus
						'Wscript.Echo "TestCaseName = " & TestCaseName

					End If

'					For Each listArr In ArrHeaderPerSheet
'						NoCol = GetColume(listArr,ArrHeaderPerSheet)
'						Wscript.Echo listArr & " = " & NoCol
'					Next
					
					RowProcess = RowProcess + 1
				Next
				

				'Filter only RunStatus = "Yes"
				'.Range("A1").AutoFilter Colume_RunStatus, "Yes", True


				Wscript.Echo "Sheet : " & SheetNumber & " = " & WorkingSheet & " Process = " & RowProcess & " row(s)"
				
			End With
			

		End if

	Next
	
	'Close workbook for Source Excel
	CopyFrom.Close
Next


'Add Post Tst scenarios
With GtestScenario
	.TestScenario_No = ""
	.TestScenario_TestCaseID   = ""
	.TestScenario_TestCaseName = "EndOfRow"
	.TestScenario_RunStatus    = ""
	.TestScenario_TestCaseFilePath = ""
	.TestScenario_DataDriven  = ""
	.TestScenario_Keyword     = ""
	.TestScenario_Description = ""
	.TestScenario_Platform      = ""
	.TestScenario_Prerequisite      = ""
End With

Call MappingtoCol(CopyTonewsheet_TH,newRowTH,GtestScenario)
Call MappingtoCol(CopyTonewsheet_EN,newRowEN,GtestScenario)

CopyTo.Worksheets(1).Cells(newRowTH,4).Font.Color = RGB(255, 0, 0)
CopyTo.Worksheets(1).Cells(newRowTH,4).Interior.Color = RGB(255, 255, 0)

CopyTo.Worksheets(2).Cells(newRowEN,4).Font.Color = RGB(255, 0, 0)
CopyTo.Worksheets(2).Cells(newRowEN,4).Interior.Color = RGB(255, 255, 0)

newRowTH = newRowTH + 1
newRowEN = newRowEN + 1


'AutoFit Cell & Row
CopyTo.Worksheets(1).Range("A1:Z" & RowRange).Columns.AutoFit
CopyTo.Worksheets(1).Range("A1:Z" & RowRange).EntireRow.AutoFit

'Adjust Layout
'CopyTo.Worksheets(1).Columns("A1:Z1").HorizontalAlignment = xlCenter

CopyTo.Worksheets(1).Range("A1:Z1").Font.Color = vbWhite
CopyTo.Worksheets(1).Range("A1:Z1").Font.Bold = True
CopyTo.Worksheets(1).Range("A1:Z" & RowRange).Font.Size = 9

'AutoFit Cell & Row
CopyTo.Worksheets(2).Range("A1:Z" & RowRange).Columns.AutoFit
CopyTo.Worksheets(2).Range("A1:Z" & RowRange).EntireRow.AutoFit

'Adjust Layout
'CopyTo.Worksheets(1).Columns("A1:Z1").HorizontalAlignment = xlCenter

CopyTo.Worksheets(2).Range("A1:Z1").Font.Color = vbWhite
CopyTo.Worksheets(2).Range("A1:Z1").Font.Bold = True
CopyTo.Worksheets(2).Range("A1:Z" & RowRange).Font.Size = 9

'Sleep 5 Sec.
WScript.Sleep(10000)

CopyTo.Sheets(1).Activate()
'CopyTo.Save()
CopyTo.SaveAs(strNewFileName)
'xl.Quit()




'Close all Workbook
CopyTo.Close

xl.Quit()

Wscript.Echo "All Scenario test generate on file " & strNewFileName
Wscript.Echo "Success Generate File Success.."
