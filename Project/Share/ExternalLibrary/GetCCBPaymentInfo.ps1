﻿param (
[string]$inputfile = ".\GetCCBPaymentInfo_Input.txt",
[string]$outputfile = ".\GetCCBPaymentInfo_Output.txt"
)

$file = Get-Content($inputfile)

IF (Test-Path($outputfile))
{
    Remove-Item $outputfile
}

$excludeList = "CloseBrowser","LaunchWeb_EN_Recovery","CoMarch_Web_EN","LaunchWeb_EN","LaunchWeb_TH_Recovery","CoMarch_Web_TH","LaunchWeb_TH","CloseBrowser_Recovery"

$CompMapping = @{}

$CompMapping.Add('makone9241','001')
$CompMapping.Add('maktwo0975','002')
$CompMapping.Add('makthr8165','003')
$CompMapping.Add('makfou7975','004')
$CompMapping.Add('makfiv3573','005')
$CompMapping.Add('maksix9536','006')
$CompMapping.Add('maksev0478','007')
$CompMapping.Add('makeig4640','008')
$CompMapping.Add('maknin6354','009')
$CompMapping.Add('makten8924','010')
$CompMapping.Add('makele0955','011')
$CompMapping.Add('maktwe7772','012')
$CompMapping.Add('makthi7992','013')
$CompMapping.Add('makfou2453','014')
$CompMapping.Add('makfif1415','015')
$CompMapping.Add('maksix9487','016')
$CompMapping.Add('maksev9108','017')
$CompMapping.Add('makeig0742','018')
$CompMapping.Add('maknin0755','019')
$CompMapping.Add('maktwe3749','020')
$CompMapping.Add('maktwe5786','021')

$PaymentMapping = @{}

$PaymentMapping.Add("PYM_3RDParty_TC","3rd Party Transfer")
$PaymentMapping.Add("PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("PYM_OAT_TC","Own Account Transfer")
$PaymentMapping.Add("PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("PYM_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("PYM_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("PYM_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("PYM_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("PYM_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("PYM_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("PYM_PMPTPY_NXDY_TC","PromptPay (Next Day)")
$PaymentMapping.Add("PYM_PMPTPY_RLTM_TC","PromptPay (Realtime)")
$PaymentMapping.Add("PYM_PMPTPY_SMDY_TC","PromptPay (Same Day)")
$PaymentMapping.Add("PYM_SMRT_CDT_NXDY_TC","SMART (Next day)")
$PaymentMapping.Add("PYM_SMRT_CDT_SMDY_TC","SMART (Same day)")
$PaymentMapping.Add("ACS_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("APV_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("BEN_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("CUR_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("SAV_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("MFU_BAHTNET_TC","BAHTNET")
$PaymentMapping.Add("PAY_BIL_PM_OFF_REV_","Bill Payment")
$PaymentMapping.Add("ACS_BIL_PM_OFF_MEA_","Bill Payment")
$PaymentMapping.Add("ACS_BIL_PM_OFF_REV_","Bill Payment")
$PaymentMapping.Add("APV_BIL_PM_OFF_MEA_","Bill Payment")
$PaymentMapping.Add("APV_BIL_PM_OFF_REV_","Bill Payment")
$PaymentMapping.Add("CUR_BIL_PM_OFF_MEA_","Bill Payment")
$PaymentMapping.Add("CUR_BIL_PM_OFF_REV_","Bill Payment")
$PaymentMapping.Add("SAV_BIL_PM_OFF_MEA_","Bill Payment")
$PaymentMapping.Add("SAV_BIL_PM_OFF_REV_","Bill Payment")
$PaymentMapping.Add("BEN_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("SAV_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("CUR_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("ACS_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("APV_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("MFU_PYM_DRCDT_TC","Direct Credit")
$PaymentMapping.Add("SAV_PYM_OAT_TC","Own Account Transfer")
$PaymentMapping.Add("CUR_PYM_OAT_TC","Own Account Transfer")
$PaymentMapping.Add("ACS_PYM_OAT_TC","Own Account Transfer")
$PaymentMapping.Add("APV_PYM_OAT_TC","Own Account Transfer")
$PaymentMapping.Add("BEN_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("SAV_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("CUR_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("ACS_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("APV_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("MFU_PYM_ORFT_TC","Transfer Other Bank (Realtime)")
$PaymentMapping.Add("BEN_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("SAV_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("CUR_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("ACS_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("APV_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("MFU_PYRL_DRCDT_MGT_","Payroll 2")
$PaymentMapping.Add("BEN_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("SAV_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("CUR_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("ACS_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("APV_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("MFU_PYRL_DRCDT_OTH_","Payroll 3")
$PaymentMapping.Add("BEN_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("SAV_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("CUR_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("ACS_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("APV_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("MFU_PYRL_DRCDT_STF_","Payroll")
$PaymentMapping.Add("ACS_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("APV_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_NXDY_MGT_","SMART Payroll 2 (Next day)")
$PaymentMapping.Add("ACS_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("APV_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_NXDY_OTH_","SMART Payroll 3 (Next day)")
$PaymentMapping.Add("ACS_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("APV_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_NXDY_STF_","SMART Payroll (Next day)")
$PaymentMapping.Add("ACS_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("APV_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_SMDY_MGT_","SMART Payroll 2 (Same day)")
$PaymentMapping.Add("ACS_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("APV_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_SMDY_OTH_","SMART Payroll 3 (Same day)")
$PaymentMapping.Add("ACS_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("APV_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("BEN_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("CUR_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("SAV_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("MFU_PYRL_SMRT_SMDY_STF_","SMART Payroll (Same day)")
$PaymentMapping.Add("ACS_PMPTPY_NXDY_TC","PromptPay (Next Day)")
$PaymentMapping.Add("APV_PMPTPY_NXDY_TC","PromptPay (Next Day)")
$PaymentMapping.Add("CUR_PMPTPY_NXDY_TC","PromptPay (Next Day)")
$PaymentMapping.Add("SAV_PMPTPY_NXDY_TC","PromptPay (Next Day)")
$PaymentMapping.Add("ACS_PMPTPY_RLTM_TC","PromptPay (Realtime)")
$PaymentMapping.Add("APV_PMPTPY_RLTM_TC","PromptPay (Realtime)")
$PaymentMapping.Add("CUR_PMPTPY_RLTM_TC","PromptPay (Realtime)")
$PaymentMapping.Add("SAV_PMPTPY_RLTM_TC","PromptPay (Realtime)")
$PaymentMapping.Add("ACS_PMPTPY_SMDY_TC","PromptPay (Same Day)")
$PaymentMapping.Add("APV_PMPTPY_SMDY_TC","PromptPay (Same Day)")
$PaymentMapping.Add("CUR_PMPTPY_SMDY_TC","PromptPay (Same Day)")
$PaymentMapping.Add("SAV_PMPTPY_SMDY_TC","PromptPay (Same Day)")
$PaymentMapping.Add("ACS_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("APV_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("BEN_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("CUR_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("SAV_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("MFU_SMRT_CDT_NXDY_","SMART (Next day)")
$PaymentMapping.Add("ACS_SMRT_CDT_SMDY_","SMART (Same day)")
$PaymentMapping.Add("APV_SMRT_CDT_SMDY_","SMART (Same day)")
$PaymentMapping.Add("BEN_SMRT_CDT_SMDY_","SMART (Same day)")
$PaymentMapping.Add("CUR_SMRT_CDT_SMDY_","SMART (Same day)")
$PaymentMapping.Add("SAV_SMRT_CDT_SMDY_","SMART (Same day)")
$PaymentMapping.Add("MFU_SMRT_CDT_SMDY_","SMART (Same day)")

$srchusername = "login_username_inp|Function:WB_TypeTextOnEdit"
$TextToSearch1 = "pm_addtopt_approvesummary_search_inp"
$srchDebitAcct1 = "pm_searchacc_inp"
$srchCounterparty = "pm_oat_choosecreditacc_lst|pm_3rd_party_account_inp|pm_direct_credit_account_inp|pm_payroll_direct_credit_management_account_inp|pm_payroll_direct_credit_staff_account_inp|pm_payroll_smart_nextday_manager_account_inp|pm_payroll_smart_nextday_other_account_inp|pm_payroll_smart_nextday_staff_account_inp|pm_payroll_smart_sameday_manager_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_smart_sameday_staff_account_inp|pm_smart_credit_nextday_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_direct_credit_other_account_inp"
$srchPymAmt = "pm_3rd_party_amount_inp|pm_direct_credit_amount_inp|pm_oat_amount_inp|pm_orft_amount_inp|pm_payroll_direct_credit_management_amount_inp|pm_payroll_direct_credit_other_amount_inp|pm_payroll_direct_credit_staff_amount_inp|pm_payroll_smart_nextday_manager_amount_inp|pm_payroll_smart_nextday_other_amount_inp|pm_payroll_smart_nextday_staff_amount_inp|pm_payroll_smart_sameday_manager_amount_inp|pm_payroll_smart_sameday_other_amount_inp|pm_payroll_smart_sameday_staff_amount_inp|pm_promptpay_nextday_amount_inp|pm_promptpay_realtime_amount_inp|pm_promptpay_sameday_amount_inp|pm_smart_credit_nextday_amount_inp|pm_payroll_smart_sameday_other_amount_inp"

$url2getchk = [string]::Empty

foreach ($rec in $file)
{
    $URL = $rec
    Write-Host "### URL: $URL"
    Add-Content -Path $outputfile -Value "### URL: $URL" -Encoding UTF8
    Write-Host "Test Case Name,Test Case Status,Test Case Type,IN_REF,DB_ACCT,CR_ACCT,DR_AMT,CR_AMT,IPS Batch Ref,Fee Amt,UniqueValueForPaymentSearching"
    Add-Content -Path $outputfile -Value "Test Case Name,Test Case Status,Test Case Type,IN_REF,DB_ACCT,CR_ACCT,DR_AMT,CR_AMT,IPS Batch Ref,Fee Amt,UniqueValueForPaymentSearching"

    $result =  Invoke-WebRequest $URL

    foreach ($r in $result.Links)
    {
        if (-Not ($excludeList | Where-Object {$_ -like $r.innerText.Trim()}))
        {
            try
            {
                $testcase = $r.innerText.Trim()
                $testcasetype = $PaymentMapping[$testcase.Substring(0,$testcase.Length - 3)]
                $caselineno = ($result.ToString() -split "[`n]" | Select-String -Pattern "$testcase" | Select-Object -Expand LineNumber)[1]
                $casestatus = ($result.ToString() -split "[`n]")[$caselineno + 4].Trim()

                $url2get = "http://$(([System.Uri]$URL).Authority)/"
                For ($i = 1; $i -lt ([System.Uri]$URL).Segments.Count - 1; $i++) {$url2get += ([System.Uri]$URL).Segments[$i]}
                #$url2get += $result.links.href[$idx].Replace("\","/")
                $url2get += ($r.href.Replace("\","/")).Split("#",2)[0]

                if ($url2getchk -ne $url2get)
                {
                    $WebResponse = Invoke-WebRequest $url2get -ContentType 'text/html; charset=utf-8'
                    $url2getchk = $url2get
                }

                $caselinearr = ($WebResponse.ToString() -split "[`n]" | Select-String -Pattern "$testcase" | Select-Object -Expand LineNumber)
                $startline = $caselinearr[0]
                $endline = $caselinearr[$caselinearr.Count- 1]

                #Write-Host "### $startline : $endline ###"

                if ($casestatus -ne "INCOMPLETED")
                {
###########################################################################
                    Try
                    {
                        $srchusernamelinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchusername).LineNumber)
                        
                        for ($i = 0; $i -lt $srchusernamelinenumber.Count; $i++)
                        {
                            if (($startline + $srchusernamelinenumber[$i]) -lt $endline)
                            {
                                if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i] - 2] -match "login_username_inp") -and (($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 2] -match "Function:WB_TypeTextOnEdit"))
                                {
                                    if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 1])-match "<td style='color:Black;'>(?<username>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 1])-match "<td style='color:Red;'>(?<username>.*)</td>"))
                                    {
                                        $username = $matches['username']
                                        $compcode = $CompMapping[$username]
                                    }
                                    Break
                                }
                            }
                        }

                    }
                    Catch
                    {
                        $username = "<NOT FOUND>"
                    }

###########################################################################
                    Try
                    {
                        $TextToSearch1linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $TextToSearch1).LineNumber)[0]
                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
                        {
                            $result1 = ($matches['content'],"<NOT FOUND>")[(($startline + $TextToSearch1linenumber) -gt $endline)]
                        }
                        else
                        {
                            $result1 = "<NOT FOUND>"
                        }
                    }
                    Catch
                    {
                        $result1 = "<NOT FOUND>"
                    }

                    Try
                    {
                        $srchDebitAcct1LineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchDebitAcct1).LineNumber)

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchDebitAcct1LineNo[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchDebitAcct1LineNo[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {
                            $debitAcct1 = ($matches['content'],"<NOT FOUND>")[(($startline + $srchDebitAcct1LineNo[2]) -gt $endline)]
                        }
                        else
                        {
                            $debitAcct1 = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $debitAcct1 = "<NOT FOUND>"
                    }

                    Try
                    {
                        $srchCounterpartyLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchCounterparty).LineNumber)

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[0]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[0]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {

                            $chkifcounterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchCounterpartyLineNo[0]) -gt $endline)]
                            if ($chkifcounterparty -match "^[\d\.]+$")
                            {
                                $counterparty = $chkifcounterparty
                            }
                            else
                            {
                                if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                                {
                                    $counterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchCounterpartyLineNo[2]) -gt $endline)]
                                }
                                else
                                {
                                    $counterparty = "<NOT FOUND>"
                                }
                            }
                        }
                        else
                        {
                            $counterparty = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $counterparty = "<NOT FOUND>"
                    }

                    Try
                    {
                        $srchPymAmtlinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchPymAmt).LineNumber)

                        $chkmodified = 0

                        for ($i=0;$i -lt $srchPymAmtlinenumber.Count;$i++) {
                            if (($startline + $srchPymAmtlinenumber[$i]) -lt $endline)
                            {
                                $chkmodified += 1
                            }
                        }

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchPymAmtlinenumber[$chkmodified - 2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchPymAmtlinenumber[$chkmodified - 2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {
                            $pymamt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchPymAmtlinenumber[$chkmodified - 2]) -gt $endline)]
                        }
                        else
                        {
                            $pymamt = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $pymamt = "<NOT FOUND>"
                    }

                    Write-Host "$testcase,$casestatus,$testcasetype,,$debitAcct1,$counterparty,$pymamt,$pymamt,,,$result1"
                    Add-Content -Path $outputfile -Value "$testcase,$casestatus,$testcasetype,,$debitAcct1,$counterparty,$pymamt,$pymamt,,,$result1"
                }
            }
            catch
            {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
            
                Write-Host "Error(0): $FailedItem - $ErrorMessage"
                Add-Content -Path $outputfile -Value "Error(0): $FailedItem - $ErrorMessage" -Encoding UTF8 
            }
        }
    }
}