@ECHO OFF

setlocal EnableDelayedExpansion

SET defaultinputfile=SearchPaymentInfoWebInput.txt
SET defaultoutputfile=SearchPaymentInfoWebOutput.csv

rem for %%x in (%*) do echo %%x
rem set str1=abcde
rem if not x%str1:bcd=%==x%str1% echo It contains bcd

SET /A args_count=0    
FOR %%A in (%*) DO SET /A args_count+=1

If %args_count%==0 (GOTO NO_ARGUMENTS)
If %args_count%==1 if /I "%1"=="-help" (GOTO SHOW_USAGE)
If %args_count%==2 (GOTO PARAMCHK1)
If %args_count%==4 (GOTO PARAMCHK2)
GOTO SHOW_USAGE

GOTO :END

:NO_ARGUMENTS
REM ECHO No Arguments
CALL PowerShell -ExecutionPolicy Unrestricted -File SearchPaymentInfoWeb.ps1
GOTO :END

:PARAMCHK1
If /I "%1" neq "-i" (If /I "%1" neq "-o" GOTO SHOW_USAGE)

If /I "%1"=="-i" (
	SET INPUTFILE=%2
	IF NOT EXIST !INPUTFILE! (
		SET alertmsg=!INPUTFILE!
		GOTO INPUTFILE_DOES_NOT_EXIST
	)

	CALL PowerShell -ExecutionPolicy Unrestricted -File SearchPaymentInfoWeb.ps1 -inputfile !INPUTFILE!
	GOTO END
)

If /I "%1"=="-o" (
	SET INPUTFILE=%2
	FOR %%i IN ("!INPUTFILE!") DO (
		SET OPATH=%%~di%%~pi
		SET OFNAME=%%~ni
		SET OFEXT=%%~xi
	)
	SET ofile="!OPATH!!OFNAME!!OFEXT!"
	IF NOT EXIST !OPATH! (
	 	SET alertmsg=!OPATH!
	 	GOTO PATHFILE_DOES_NOT_EXIST
	)
	IF "!OFNAME!"=="" (SET ofile="!OPATH!!defaultoutputfile!")

	CALL PowerShell -ExecutionPolicy Unrestricted -File SearchPaymentInfoWeb.ps1 -outputfile !ofile!
)

GOTO :END

:PARAMCHK2
If /I "%1"=="-i" If /I "%3" neq "-o" (GOTO SHOW_USAGE)
If /I "%1"=="-o" If /I "%3" neq "-i" (GOTO SHOW_USAGE)

If /I "%1"=="-i" (
	SET INPUTFILE=%2
	IF NOT EXIST !INPUTFILE! (
		SET alertmsg=!INPUTFILE!
		GOTO INPUTFILE_DOES_NOT_EXIST
	)

	SET OUTPUTFILE=%4
	FOR %%i IN ("!OUTPUTFILE!") DO (
		SET OPATH=%%~di%%~pi
		SET OFNAME=%%~ni
		SET OFEXT=%%~xi
	)
	SET ofile="!OPATH!!OFNAME!!OFEXT!"
	IF NOT EXIST !OPATH! (
	 	SET alertmsg=!OPATH!
	 	GOTO PATHFILE_DOES_NOT_EXIST
	)
	IF "!OFNAME!"=="" (SET ofile="!OPATH!!defaultoutputfile!")

	CALL PowerShell -ExecutionPolicy Unrestricted -File SearchPaymentInfoWeb.ps1 -inputfile !INPUTFILE! -outputfile !ofile!
	GOTO END
)

If /I "%1"=="-o" DO (
	SET OUTPUTFILE=%2
	FOR %%i IN ("!OUTPUTFILE!") DO (
		SET OPATH=%%~di%%~pi
		SET OFNAME=%%~ni
		SET OFEXT=%%~xi
	)
	SET ofile="!OPATH!!OFNAME!!OFEXT!"
	IF NOT EXIST !OPATH! (
	 	SET alertmsg=!OPATH!
	 	GOTO PATHFILE_DOES_NOT_EXIST
	)
	IF "!OFNAME!"=="" (SET ofile="!OPATH!!defaultoutputfile!")

	SET INPUTFILE=%4
	IF NOT EXIST !INPUTFILE! (
		ECHO Input file (!INPUTFILE!) does not exist!!!
		GOTO END
	)

	CALL PowerShell -ExecutionPolicy Unrestricted -File SearchPaymentInfoWeb.ps1 -outputfile !ofile! -inputfile !INPUTFILE!
)

GOTO :END

:SHOW_USAGE
ECHO.[Usage]
ECHO.Type: %0 --help for more information
ECHO.
ECHO.%0 [-i [Path]SourceFile] [-o [Path]TargetFile]
ECHO.
ECHO.Example:
ECHO.
ECHO.    %0 -i D:\Temp\sourcefile.txt -o D:\Temp\outputfile.csv
ECHO.    %0 -i D:\Temp\sourcefile.txt
ECHO.    %0 -o D:\Temp\outputfile.csv
ECHO.    %0
ECHO.
ECHO.NOTE: 
ECHO.	Without parameter -i
ECHO.	the source file ("!defaultinputfile!")
ECHO.	must be placed at ^<Current directory^>
ECHO.	
ECHO.	Without parameter -o
ECHO.	the output file ("!defaultoutputfile!")
ECHO.	will be placed at ^<Current directory^>

GOTO :END

:INPUTFILE_DOES_NOT_EXIST
ECHO.Input file (%alertmsg%) does not exist!!!.
GOTO :END

:PATHFILE_DOES_NOT_EXIST
ECHO.Destination path (%alertmsg%) does not exist!!!.
GOTO :END

:END