@ECHO OFF

SET AllArgs=%*

REM SET /A argCount=0
REM FOR %%A in (%*) DO SET /A argCount+=1

SET execstr=curl -X POST -k https://notify-api.line.me/api/notify

for /f "tokens=1,2 delims==" %%a in (connection.cfg) do (
if "%%a"=="proxySet" (SET proxySet=%%b)
if "%%a"=="proxyHost" (SET proxyHost=%%b)
if "%%a"=="proxyPort" (SET proxyPort=%%b)
)

FOR /F "tokens=1* delims= " %%A IN ("%AllArgs%") DO (
   SET tokenlist=%%A
   SET message=%%B
)

if /I "%proxySet%" EQU "True" (SET execstr=%execstr% -x %proxyHost%:%proxyPort%)

FOR %%A IN ("%tokenlist:;=" "%") DO (
   SET token=%%~A
   CALL :EXEC
)

GOTO :END

:EXEC
   SET execcmd=%execstr% -H "Authorization: Bearer %token%" -F "message=%message%"
   %execcmd%

:END