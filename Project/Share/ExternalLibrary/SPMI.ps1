﻿ param (
    [string]$testenv = "UAT",
    [string]$inputfile = ".\SPMI_Input.txt",
    [string]$outputfile = "$(Get-Item -Path '.\')\SPMI_Output.xlsx"
 )


function GetPMElement()
{
    param(
    [string]$wr,
    [int]$sline,
    [int]$eline,
    [int]$sType, 
    [string]$sText,
    [int]$sIdx
    )

    Try
    {
        $TextToSearchline = (($wr -split "[`n]" | Select-Object -Skip ($sline - 1) | Select-String -Pattern $sText).LineNumber)
        
        if (((($wr -split "[`n]")[$sline + $TextToSearchline[$sIdx] + $sType]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($wr -split "[`n]")[$sline + $TextToSearchline[$sIdx] + $sType]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
        {
            $srchresult = ($matches['content'],"<NOT FOUND0>")[(($sline + $TextToSearchline[$sIdx] + $sType) -gt $eline)]
        }
        else
        {
            $srchresult = "<NOT FOUND1>"
        }
    }
    Catch
    {
        $srchresult = "<NOT FOUND2>"
    }

    return $srchresult
}

Function Convert-NumberToA1 { 
  <# 
  .SYNOPSIS 
  This converts any integer into A1 format. 
  .DESCRIPTION 
  See synopsis. 
  .PARAMETER number 
  Any number between 1 and 2147483647 
  #> 
   
  Param([parameter(Mandatory=$true)] 
        [int]$number) 
 
  $a1Value = $null 
  While ($number -gt 0) { 
    $multiplier = [int][system.math]::Floor(($number / 26)) 
    $charNumber = $number - ($multiplier * 26) 
    If ($charNumber -eq 0) { $multiplier-- ; $charNumber = 26 } 
    $a1Value = [char]($charNumber + 64) + $a1Value 
    $number = $multiplier 
  } 
  Return $a1Value 
}

$file = Get-Content($inputfile)

$TempateFilePath = ".\SPMI_Template.xlsx"
#$outputfile = ".\SPMI_Out.xlsx"

$TempateFilePath = (Resolve-Path $TempateFilePath).Path

$CompMapping = @{}

#$CompMapping.Add('makone9241','001')
#$CompMapping.Add('maktwo0975','002')
#$CompMapping.Add('makthr8165','003')
#$CompMapping.Add('makfou7975','004')
#$CompMapping.Add('makfiv3573','005')
#$CompMapping.Add('maksix9536','006')
#$CompMapping.Add('maksev0478','007')
#$CompMapping.Add('makeig4640','008')
#$CompMapping.Add('maknin6354','009')
#$CompMapping.Add('makten8924','010')
#$CompMapping.Add('makele0955','011')
#$CompMapping.Add('maktwe7772','012')
#$CompMapping.Add('makthi7992','013')
#$CompMapping.Add('makfou2453','014')
#$CompMapping.Add('makfif1415','015')
#$CompMapping.Add('maksix9487','016')
#$CompMapping.Add('maksev9108','017')
#$CompMapping.Add('makeig0742','018')
#$CompMapping.Add('maknin0755','019')
#$CompMapping.Add('maktwe3749','020')
#$CompMapping.Add('maktwe5786','021')

$CompMapping.Add('makone9241','นาย โคมาร์ช ทดสอบออโตเมท ซิท001')
$CompMapping.Add('maktwo0975','นาย โคมาร์ช ทดสอบออโตเมท ซิท002')
$CompMapping.Add('makthr8165','นาย โคมาร์ช ทดสอบออโตเมท ซิท003')
$CompMapping.Add('makfou7975','นาย โคมาร์ช ทดสอบออโตเมท ซิท004')
$CompMapping.Add('makfiv3573','นาย โคมาร์ช ทดสอบออโตเมท ซิท005')
$CompMapping.Add('maksix9536','นาย ทดสอบโคมาร์ช ทดสอบ ออโตเมท ซิท006')
$CompMapping.Add('maksev0478','นาย โคมาร์ท ทดสอบ ออโตเมท ซิท007')
$CompMapping.Add('makeig4640','นาย ทดสอบโคมาร์ช ทดสอบ ออโตเมต ซิท008')
$CompMapping.Add('maknin6354','นาย ทดสอบโคมาร์ช ทดสอบ ออโตเมท ซิท009')
$CompMapping.Add('makten8924','นาย ทดสอบโคมาร์ช ทดสอบ ออโตเมทซิท010')
$CompMapping.Add('makele0955','10400')
$CompMapping.Add('maktwe7772','10900')
$CompMapping.Add('makthi7992','33 หมู่ 3')
$CompMapping.Add('makfou2453','44 หมู่ 4')
$CompMapping.Add('makfif1415','55 หมู่ 5')
$CompMapping.Add('maksix9487','นาย ทดสอบโคมาร์ช ทดสอบออโตเมท ผู้รับเงิน')
$CompMapping.Add('maksev9108','บริษัท จำกัด บริษัทโคมาร์ช คอปเปอร์เรทออโตเมท แผนก006')
$CompMapping.Add('makeig0742','018')
$CompMapping.Add('maknin0755','019')
$CompMapping.Add('maktwe3749','020')
$CompMapping.Add('maktwe5786','021')


$srchusername = "login_username_inp|Function:WB_TypeTextOnEdit"
$srchPMUniqVal = "pm_addtopt_approvesummary_search_inp"
$srchPMUniqValRow = 0
$srchPMComp = "CP_Counterparty_CompanySettingsSelectCompanySearch_Inp"
$srchDate = "WB_BIZ_SelectDatePicker|WB_BIZ_SelectDatePeriodPicker"
$srchDebitAcct1 = "PM_Accounts_SearchAcc_Inp|WB_TypeTextOnEdit"
$srchDebitAcct2 = "PM_Accounts_CheckBox_Btn"
#$srchAmnt = "pm_3rd_party_amount_inp|pm_direct_credit_amount_inp|pm_oat_amount_inp|pm_orft_amount_inp|pm_payroll_direct_credit_management_amount_inp|pm_payroll_direct_credit_other_amount_inp|pm_payroll_direct_credit_staff_amount_inp|pm_payroll_smart_nextday_manager_amount_inp|pm_payroll_smart_nextday_other_amount_inp|pm_payroll_smart_nextday_staff_amount_inp|pm_payroll_smart_sameday_manager_amount_inp|pm_payroll_smart_sameday_other_amount_inp|pm_payroll_smart_sameday_staff_amount_inp|pm_promptpay_nextday_amount_inp|pm_promptpay_realtime_amount_inp|pm_promptpay_sameday_amount_inp|pm_smart_credit_nextday_amount_inp|pm_payroll_smart_sameday_other_amount_inp"
$srchHistDate = "PM_Accounts_History_TransactionDateList_Txt"
$srchHistAcct = "PM_Accounts_History_CompanyName-AccountList_Txt"
$srchHistAmnt = "PM_Accounts_History_AmountList_Txt"

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Started Reading Search Payment Info Template ##"

$SheetName = "Checkstatus"
$objExcel = New-Object -ComObject Excel.Application
$objExcel.Visible = $false
$WorkBook = $objExcel.Workbooks.Open($TempateFilePath)
$WorkSheet = $WorkBook.sheets.item($SheetName)
$WorkSheet.Activate()

$columns = $WorkSheet.UsedRange.Columns.Count
$lines = $WorkSheet.UsedRange.Rows.Count

$fields = @()
$TemplateData = @()

for ($column = 1; $column -le $columns - 1; $column ++) 
{
    $fieldName = $WorkSheet.Cells.Item.Invoke(1, $column).Value2
#    if ($fieldName -eq $null)
#    {
#        $fieldName = "Column" + $column.ToString()
#    }
    $fields += $fieldName
}

$line = 2

for ($line = 2; $line -le $lines; $line ++)
{
    $values = New-Object object[] $columns
    for ($column = 1; $column -le $columns - 1; $column++)
    {
        $values[$column - 1] = $WorkSheet.Cells.Item.Invoke($line, $column).Value2
#        if ($srchPMUniqVal -eq $WorkSheet.Cells.Item.Invoke($line, $column).Value2)
#        {
#            $srchPMUniqValRow = $line
#        }

    }

    $row = New-Object psobject
    $fields | foreach-object -begin {$col = 0} -process {
       $row | Add-Member -MemberType noteproperty -Name $fields[$col] -Value $values[$col]; $col++
     }
    $TemplateData += $row
}   

$WorkBook.Close()
$objExcel.Quit()
#[System.Runtime.InteropServices.Marshal]::FinalReleaseComObject($objExcel) | Out-Null

#Release COM Object
#[System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel) | Out-Null
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$WorkSheet)){}
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$WorkBook)){}
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel)){}
Remove-Variable WorkSheet
Remove-Variable workbook
Remove-Variable objExcel
[System.GC]::Collect()

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Started Writing Search Payment Info ##"

$objExcel = New-Object -ComObject Excel.Application
$objExcel.Visible = $false
$workbook = $objExcel.Workbooks.Add()

$uregwksht= $workbook.Worksheets.Item(1) 
$uregwksht.Name = $SheetName

for ($j = 0; $j -lt $fields.Count - 3; $j++)
{
    $uregwksht.Cells.Item(1,$j + 1) = $fields[$j]
    $uregwksht.Cells.Item(1,$j + 1).Font.Bold = $True
    $uregwksht.Cells.Item(1,$j + 1).Interior.ColorIndex = 48
}

for ($i = 0; $i -lt $TemplateData.Count; $i++)
{
    for ($j = 0; $j -lt $fields.Count - 3; $j++)
    {
        $uregwksht.Cells.Item($i + 2,$j + 1) = $TemplateData[$i].$($fields[$j])
    }
}

$coldata = $fields.Count - 2
foreach ($rec in $file)
{
    $url2getchk = [string]::Empty

    $TestCase = (($rec.Split("|"))[0]).Trim()
    if ($URL -ne (($rec.Split("|"))[1]).Trim())
    {
        $URL = (($rec.Split("|"))[1]).Trim()
        $result = Invoke-WebRequest $URL
    }

    $idx = [array]::indexof($result.links.innerText.Trim(),$testcase)
    $url2get = "http://$(([System.Uri]$URL).Authority)/"
    
    For ($i = 1; $i -lt ([System.Uri]$URL).Segments.Count - 1; $i++) {$url2get += ([System.Uri]$URL).Segments[$i]}
    $url2get += $result.links.href[$idx].Replace("\","/")
    $url2get = ($url2get.Split("#",2))[0]

    $linenumber = (($result.Content).ToString() -split "[`n]") | Select-String -Pattern "CoMarch_Web_EN|CoMarch_Web_TH" | Select-Object -Expand LineNumber
    $lngidx = ((($result.Content).ToString() -split "[`n]")[$linenumber[0] - 1]).IndexOf("CoMarch_Web")
    $lang = ((($result.Content).ToString() -split "[`n]")[$linenumber[0] - 1]).SubString(26,2)

    Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""):--> Test Case: $TestCase ($lang) - Started "

    $uregwksht.Cells.Item(1,$coldata) = $TestCase + "_" + $testenv + "_" + $lang

##############################################################
    try
    {
        if ($url2getchk -ne $url2get)
        {
            $WebResponse = Invoke-WebRequest $url2get -ContentType 'text/html; charset=utf-8'
            $url2getchk = $url2get
        }

        $caselinearr = ($WebResponse.ToString() -split "[`n]" | Select-String -Pattern "$TestCase" | Select-Object -Expand LineNumber)
        $startline = $caselinearr[0]
        $endline = $caselinearr[$caselinearr.Count- 1]
        
        #Write-Host "### startline: $startline, endline: $endline"
        # Search Maker Acct
        
        #$username = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchusername 2

###################################################################

        Try
        {
            $srchusernamelinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchusername).LineNumber)
                        
            for ($i = 0; $i -lt $srchusernamelinenumber.Count; $i++)
            {
                if (($startline + $srchusernamelinenumber[$i]) -lt $endline)
                {
                    if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i] - 2] -match "login_username_inp") -and (($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 2] -match "Function:WB_TypeTextOnEdit"))
                    {
                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 1])-match "<td style='color:Black;'>(?<username>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[$i + 1] - 1])-match "<td style='color:Red;'>(?<username>.*)</td>"))
                        {
                            $username = $matches['username']
                            #$compcode = $CompMapping[$username]
                        }
                        Break
                    }
                }
            }

        }
        Catch
        {
            $username = "<NOT FOUND>"
            #$compcode = "<NOT FOUND>"
        }


###################################################################

        #Write-Host "### username: $username"

        $srchPMUniqValstr = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchPMUniqVal 0
        
        for ($i = 0; $i -lt $TemplateData.Count; $i++)
        {
            if (($TemplateData[$i].SearchType) -eq "F")
            {
                $uregwksht.Cells.Item($i + 2,$coldata) = $TemplateData[$i].SearchText
            } elseif (($TemplateData[$i]."SearchType") -eq "S")
            {
                if ($TemplateData[$i].SearchText)
                {
                    $flag = ($TemplateData[$i].SearchText).Split(";")[0]
                    $adj = ($TemplateData[$i].SearchText).Split(";")[1]
                    $sidx = ($TemplateData[$i].SearchText).Split(";")[2]
                    $sStr = ($TemplateData[$i].SearchText).Split(";")[3]
                    
                    if ($flag -eq "0")
                    {
                        $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                        $retval = GetPMElement $WebResponse.ToString() $startline $endline $adj $sStr $sidx
                        if ($TemplateData[$i].Activity -eq "WB_BIZ_CheckAuthorizeTransaction")
                        {
                            $retval = ($retval.Substring(0, $retval.LastIndexOf(";") + 1) + " Success").Replace(" ","")
                        }

                        $uregwksht.Cells.Item($i + 2,$coldata) = $retval
                        
                    } elseif ($flag -eq "1")
                    {
                        $flag = ($TemplateData[$i].SearchText).Split(";")[0]
                        $sStr = ($TemplateData[$i].SearchText).Split(";")[1]
                        if (($flag -eq "1") -And ($sStr -eq "CP_Counterparty_CompanySettingsSelectCompanySearch_Inp"))
                        {
                            $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                            $uregwksht.Cells.Item($i + 2,$coldata) = $CompMapping[$username]

                            #Write-Host "#### $($CompMapping[$username]) ####"
                        }
                    }

                }
            } elseif (($TemplateData[$i]."SearchType") -eq "I")
            {
               
            } elseif (($TemplateData[$i]."SearchType") -eq "X")
            {
                #$uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "General"
                #$uregwksht.Cells.Item($i + 2,$coldata) = $TemplateData[$i].SearchText

                $fmlval = ($TemplateData[$i].SearchText) -replace "<CurrentColumn>", (Convert-NumberToA1 $coldata)
                $uregwksht.Cells.Item($i + 2,$coldata) = $fmlval
                $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "General"
            }
        }
    }
    catch
    {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
            
        Write-Host "Error(0): $FailedItem - $ErrorMessage"
        Add-Content -Path $outputfile -Value "Error(0): $FailedItem - $ErrorMessage" -Encoding UTF8 
    }

    Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""):--> Test Case: $TestCase ($lang) - Ended "

##############################################################

    $coldata++
}

$uregwksht.Cells.Item(1,$coldata) = "EndOfCol"

$usedRange = $uregwksht.UsedRange 
$usedRange.EntireColumn.AutoFit() | Out-Null

$objExcel.DisplayAlerts = $False
$workbook.SaveAs($outputfile) 

$WorkBook.Close()
$objExcel.Quit()
#[System.Runtime.InteropServices.Marshal]::FinalReleaseComObject($objExcel) | Out-Null

#Release COM Object
#[System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel) | Out-Null
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$uregwksht)){}
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$workbook)){}
while([System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel)){}
Remove-Variable uregwksht
Remove-Variable workbook
Remove-Variable objExcel
[System.GC]::Collect()
#Stop-Process -Name EXCEL -Force

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Ended Writing Search Payment Info ##"