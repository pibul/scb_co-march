﻿ param (
    [string]$inputfile = ".\SearchPaymentInfoWebInput.txt",
    [string]$outputfile = ".\SearchPaymentInfoWebOutput.csv"
 )

$srchusername = "login_username_inp|Function:WB_TypeTextOnEdit"
$srchdebitacct1 = "pm_searchacc_inp"
$srchdebitacct2 = "pm_chooseacc_lst"
$srchcounterparty = "pm_oat_choosecreditacc_lst|pm_3rd_party_account_inp|pm_direct_credit_account_inp|pm_payroll_direct_credit_management_account_inp|pm_payroll_direct_credit_staff_account_inp|pm_payroll_smart_nextday_manager_account_inp|pm_payroll_smart_nextday_other_account_inp|pm_payroll_smart_nextday_staff_account_inp|pm_payroll_smart_sameday_manager_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_smart_sameday_staff_account_inp|pm_smart_credit_nextday_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_direct_credit_other_account_inp"
$srchpymamt = "pm_3rd_party_amount_inp|pm_direct_credit_amount_inp|pm_oat_amount_inp|pm_orft_amount_inp|pm_payroll_direct_credit_management_amount_inp|pm_payroll_direct_credit_other_amount_inp|pm_payroll_direct_credit_staff_amount_inp|pm_payroll_smart_nextday_manager_amount_inp|pm_payroll_smart_nextday_other_amount_inp|pm_payroll_smart_nextday_staff_amount_inp|pm_payroll_smart_sameday_manager_amount_inp|pm_payroll_smart_sameday_other_amount_inp|pm_payroll_smart_sameday_staff_amount_inp|pm_promptpay_nextday_amount_inp|pm_promptpay_realtime_amount_inp|pm_promptpay_sameday_amount_inp|pm_smart_credit_nextday_amount_inp|pm_payroll_smart_sameday_other_amount_inp"

$TextToSearch1 = "pm_addtopt_approvesummary_search_inp"
$TextToSearch2 = "Function:WB_BIZ_CheckAuthorizeTransaction"
$TextToSearch3 = "Function:WB_BIZ_SelectDatePicker|Function:WB_BIZ_SelectDatePeriodPicker"

$srchhistamt = "pm_accounts_history_amountlist_txt"

#$file = Get-Content("D:\Shared\InputSearch.txt")
$file = Get-Content($inputfile)

#IF (Test-Path("D:\Shared\SearchResult.csv"))
#{
#    Remove-Item "D:\Shared\SearchResult.csv"
#}
IF (Test-Path($outputfile))
{
    Remove-Item $outputfile
}

$url2getchk = [string]::Empty

foreach ($rec in $file)
{
    $TestCase = (($rec.Split("|"))[0]).Trim()
    if ($URL -ne (($rec.Split("|"))[1]).Trim())
    {
        $URL = (($rec.Split("|"))[1]).Trim()
        $result = Invoke-WebRequest $URL
    }

    $idx = [array]::indexof($result.links.innerText.Trim(),$testcase)
    $url2get = "http://$(([System.Uri]$URL).Authority)/"
    
    For ($i = 1; $i -lt ([System.Uri]$URL).Segments.Count - 1; $i++) {$url2get += ([System.Uri]$URL).Segments[$i]}
    $url2get += $result.links.href[$idx].Replace("\","/")

    $linenumber = (($result.Content).ToString() -split "[`n]") | Select-String -Pattern "CoMarch_Web_EN|CoMarch_Web_TH" | Select-Object -Expand LineNumber
    $lngidx = ((($result.Content).ToString() -split "[`n]")[$linenumber[0] - 1]).IndexOf("CoMarch_Web")
    $lang = ((($result.Content).ToString() -split "[`n]")[$linenumber[0] - 1]).SubString(26,2)
        
    Try
    {
        if ($url2getchk -ne $url2get.Split("#",2)[0])
        {
            $WebResponse = Invoke-WebRequest $url2get -ContentType 'text/html; charset=utf-8'
            $url2getchk = $url2get.Split("#",2)[0]
        }        

        $caselinearr = ($WebResponse.ToString() -split "[`n]" | Select-String -Pattern "$TestCase" | Select-Object -Expand LineNumber)
        $startline = $caselinearr[0]
        $endline = $caselinearr[$caselinearr.Count- 1]
            
        Try
        {
            $srchusernamelinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchusername).LineNumber)
                
            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchusernamelinenumber[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {
                $username = ($matches['content'],"<NOT FOUND>")[(($startline + $srchusernamelinenumber[2]) -gt $endline)]
            }
            else
            {
                $username = "<NOT FOUND>"
            }

        }
        catch
        {
            $username = "<NOT FOUND>"
        }

        Try
        {
            $srchdebitacct1linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchdebitacct1).LineNumber)

            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchdebitacct1linenumber[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchdebitacct1linenumber[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {
                $debitacct1 = ($matches['content'],"<NOT FOUND>")[(($startline + $srchdebitacct1linenumber[2]) -gt $endline)]
            }
            else
            {
                $debitacct1 = "<NOT FOUND>"
            }
        }
        catch
        {
            $debitacct1 = "<NOT FOUND>"
        }

        Try
        {
            $srchdebitacct2linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchdebitacct2).LineNumber)
            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchdebitacct2linenumber[0]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchdebitacct2linenumber[0]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {
                $debitacct2 = ($matches['content'],"<NOT FOUND>")[(($startline + $srchdebitacct2linenumber[0]) -gt $endline)]
            }
            else
            {
                $debitacct2 = "<NOT FOUND>"
            }
        }
        catch
        {
            $debitacct2 = "<NOT FOUND>"
        }

        Try
        {
            $srchcounterpartylinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchcounterparty).LineNumber)

            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchcounterpartylinenumber[0]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchcounterpartylinenumber[0]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {

                $chkifcounterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchcounterpartylinenumber[0]) -gt $endline)]
                if ($chkifcounterparty -match "^[\d\.]+$")
                {
                    $counterparty = $chkifcounterparty
                }
                else
                {
                    if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchcounterpartylinenumber[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchcounterpartylinenumber[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                    {
                        $counterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchcounterpartylinenumber[2]) -gt $endline)]
                    }
                    else
                    {
                        $counterparty = "<NOT FOUND>"
                    }
                }
            }
            else
            {
                $counterparty = "<NOT FOUND>"
            }
        }
        catch
        {
            $counterparty = "<NOT FOUND>"
        }

        Try
        {
            $srchpymamtlinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchpymamt).LineNumber)

            $chkmodified = 0

            for ($i=0;$i -lt $srchpymamtlinenumber.Count;$i++) {
                if (($startline + $srchpymamtlinenumber[$i]) -lt $endline)
                {
                    if (($startline + $srchpymamtlinenumber[$i]) -lt $endline)
                    {
                        #if (-Not (($WebResponse.ToString() -split "[`n]")[$startline + ($srchpymamtlinenumber[$i]) - 2] -match "pm_addtopt_inv_invoiceamount_inp"))
                        #{
                            $chkmodified += 1
                        #}
                    }
                }
            }

            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchpymamtlinenumber[$chkmodified - 2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchpymamtlinenumber[$chkmodified - 2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {
                $pymamt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchpymamtlinenumber[$chkmodified - 2]) -gt $endline)]
            }
            else
            {
                $pymamt = "<NOT FOUND>"
            }
        }
        catch
        {
            $pymamt = "<NOT FOUND>"
        }

        Try
        {
            $TextToSearch1linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $TextToSearch1).LineNumber)[0]
            if (((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
            {
                $result1 = ($matches['content'],"<NOT FOUND>")[(($startline + $TextToSearch1linenumber) -gt $endline)]
            }
            else
            {
                $result1 = "<NOT FOUND>"
            }
        }
        Catch
        {
            $result1 = "<NOT FOUND>"
        }

        Try
        {
            $TextToSearch2linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $TextToSearch2).LineNumber)[0]
            if (((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch2linenumber - 1]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch2linenumber - 1]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
            {
                $result2 = ($matches['content'],"<NOT FOUND>")[(($startline + $TextToSearch2linenumber) -gt $endline)]
            }
            else
            {
                $result2 = "<NOT FOUND>"
            }
        }
        Catch
        {
            $result2 = "<NOT FOUND>"
        }

        Try
        {
            $TextToSearch3linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $TextToSearch3).LineNumber)[0]
            if (((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch3linenumber - 1]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch3linenumber - 1]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
            {
                $result3 = ($matches['content'],"<NOT FOUND>")[(($startline + $TextToSearch3linenumber) -gt $endline)]
            }
            else
            {
                $result3 = "<NOT FOUND>"
            }
        }
        Catch
        {
            $result3 = "<NOT FOUND>"
        }

        Try
        {
            $srchhistpymamtlinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchhistamt).LineNumber)

            if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchhistpymamtlinenumber[0]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchhistpymamtlinenumber[0]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
            {
                $histpymamt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchhistpymamtlinenumber[0]) -gt $endline)]

                if (-Not ($histpymamt -match '^[-,0-9.0-9]+$'))
                {
                    if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchhistpymamtlinenumber[1]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchhistpymamtlinenumber[1]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                    {
                        $histpymamt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchhistpymamtlinenumber[1]) -gt $endline)]
                    }
                }
            }
            else
            {
                $histpymamt = "<NOT FOUND>"
            }
        }
        catch
        {
            $histpymamt = "<NOT FOUND>"
        }

        if ($result2 -ne "<NOT FOUND>")
        {
            $result2 = $result2.Substring(0, $result2.LastIndexOf(";") + 1) + " Success"
        }

        if (($result2 -eq "<NOT FOUND>") -And ($result1 -ne "<NOT FOUND>"))
        {
            $result2 = ";;;Success"
        }

        $stline1 = (($result.Content.ToString() -split "[`n]" | Select-String -Pattern $testcase) | Select-Object -Expand LineNumber)[1]
        $stline2 = ($result.Content.ToString() -split "[`n]" | Select-Object -Skip ($stline1 - 1) | Select-String -Pattern "<TD")[0].LineNumber
        $casestatus = (($result.Content.ToString() -split "[`n]")[$stline1 + $stline2 - 1]).Replace("`r","")

        Write-Host "$testcase,$casestatus,$username,$debitacct1,$debitacct2,$counterparty,$result1,$pymamt,$result2,$result3,$histpymamt,$lang"
        Add-Content -Path $outputfile -Value "$testcase,$casestatus,$username,$debitacct1,$debitacct2,$counterparty,$result1,$pymamt,$result2,$result3,$histpymamt,$lang" -Encoding UTF8 
    }
    Catch
    {
        Write-Host "$testcase Information Extraction Failed !!!"
        Add-Content -Path $outputfile "$testcase Information Extraction Failed !!!"
    }
}