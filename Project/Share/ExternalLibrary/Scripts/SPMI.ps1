﻿ param (
    [string]$inputfile = ".\SPMI_Input.txt",
    [string]$outputfile = "$(Get-Item -Path '.\')\SPMI_Output.xlsx"
 )


function GetPMElement()
{
    param(
    [string]$wr,
    [int]$sline,
    [int]$eline,
    [int]$sType, 
    [string]$sText,
    [int]$sIdx
    )

    Try
    {
        $TextToSearchline = (($wr -split "[`n]" | Select-Object -Skip ($sline - 1) | Select-String -Pattern $sText).LineNumber)
        
        if (((($wr -split "[`n]")[$sline + $TextToSearchline[$sIdx] + $sType]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($wr -split "[`n]")[$sline + $TextToSearchline[$sIdx] + $sType]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
        {
            $srchresult = ($matches['content'],"<NOT FOUND0>")[(($sline + $TextToSearchline[$sIdx] + $sType) -gt $eline)]
        }
        else
        {
            $srchresult = "<NOT FOUND1>"
        }
    }
    Catch
    {
        $srchresult = "<NOT FOUND2>"
    }

    return $srchresult
}

$file = Get-Content($inputfile)

$TempateFilePath = ".\SPMI_Template.xlsx"
#$outputfile = ".\SPMI_Out.xlsx"

$TempateFilePath = (Resolve-Path $TempateFilePath).Path

$CompMapping = @{}
$CompMapping.Add('One','001')
$CompMapping.Add('Two','002')
$CompMapping.Add('Three','003')
$CompMapping.Add('Four','004')
$CompMapping.Add('Five','005')
$CompMapping.Add('Six','006')
$CompMapping.Add('Seven','007')
$CompMapping.Add('Eight','008')
$CompMapping.Add('Nine','009')
$CompMapping.Add('Ten','010')

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Started Reading Search Payment Info Template ##"

$SheetName = "Checkstatus"
$objExcel = New-Object -ComObject Excel.Application
$objExcel.Visible = $false
$WorkBook = $objExcel.Workbooks.Open($TempateFilePath)
$WorkSheet = $WorkBook.sheets.item($SheetName)

$columns = $WorkSheet.UsedRange.Columns.Count
$lines = $WorkSheet.UsedRange.Rows.Count

$srchPMUniqVal = "pm_addtopt_approvesummary_search_inp"
$srchPMUniqValRow = 0
$srchPMComp = "CP_Counterparty_CompanySettingsSelectCompanySearch_Inp"
$srchDate = "WB_BIZ_SelectDatePicker|WB_BIZ_SelectDatePeriodPicker"
$srchDebitAcct1 = "PM_Accounts_SearchAcc_Inp|WB_TypeTextOnEdit"
$srchDebitAcct2 = "PM_Accounts_CheckBox_Btn"
$srchDCAmnt = "PM_Direct_Credit_Amount_Inp"
$srchDCAmntStr = "pm_3rd_party_amount_inp|pm_direct_credit_amount_inp|pm_oat_amount_inp|pm_orft_amount_inp|pm_payroll_direct_credit_management_amount_inp|pm_payroll_direct_credit_other_amount_inp|pm_payroll_direct_credit_staff_amount_inp|pm_payroll_smart_nextday_manager_amount_inp|pm_payroll_smart_nextday_other_amount_inp|pm_payroll_smart_nextday_staff_amount_inp|pm_payroll_smart_sameday_manager_amount_inp|pm_payroll_smart_sameday_other_amount_inp|pm_payroll_smart_sameday_staff_amount_inp|pm_promptpay_nextday_amount_inp|pm_promptpay_realtime_amount_inp|pm_promptpay_sameday_amount_inp|pm_smart_credit_nextday_amount_inp|pm_payroll_smart_sameday_other_amount_inp"
$srchHistDate = "PM_Accounts_History_TransactionDateList_Txt"
$srchHistAcct = "PM_Accounts_History_CompanyName-AccountList_Txt"
$srchHistAmnt = "PM_Accounts_History_AmountList_Txt"

$fields = @()
$TemplateData = @()

for ($column = 1; $column -le $columns - 1; $column ++) 
{
    $fieldName = $WorkSheet.Cells.Item.Invoke(1, $column).Value2
    if ($fieldName -eq $null)
    {
        $fieldName = "Column" + $column.ToString()
    }
    $fields += $fieldName
}

$line = 2

for ($line = 2; $line -le $lines; $line ++)
{
    $values = New-Object object[] $columns
    for ($column = 1; $column -le $columns - 1; $column++)
    {
        $values[$column - 1] = $WorkSheet.Cells.Item.Invoke($line, $column).Value2
#        if ($srchPMUniqVal -eq $WorkSheet.Cells.Item.Invoke($line, $column).Value2)
#        {
#            $srchPMUniqValRow = $line
#        }
    }

    $row = New-Object psobject
    $fields | foreach-object -begin {$col = 0} -process {
       $row | Add-Member -MemberType noteproperty -Name $fields[$col] -Value $values[$col]; $col++
     }
    $TemplateData += $row
}   

$WorkBook.Close()
$objExcel.Quit()
#[System.Runtime.InteropServices.Marshal]::FinalReleaseComObject($objExcel) | Out-Null

#Release COM Object
[System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel) | Out-Null

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Started Writing Search Payment Info ##"

$objExcel = New-Object -ComObject Excel.Application
$objExcel.Visible = $false
$workbook = $objExcel.Workbooks.Add()

$uregwksht= $workbook.Worksheets.Item(1) 
$uregwksht.Name = $SheetName

for ($j = 0; $j -lt $fields.Count - 3; $j++)
{
    $uregwksht.Cells.Item(1,$j + 1) = $fields[$j]
    $uregwksht.Cells.Item(1,$j + 1).Font.Bold = $True
    $uregwksht.Cells.Item(1,$j + 1).Interior.ColorIndex = 48
}

for ($i = 0; $i -lt $TemplateData.Count; $i++)
{
    for ($j = 0; $j -lt $fields.Count - 3; $j++)
    {
        $uregwksht.Cells.Item($i + 2,$j + 1) = $TemplateData[$i].$($fields[$j])
    }
}

$coldata = $fields.Count - 2
foreach ($rec in $file)
{
    $url2getchk = [string]::Empty

    $TestCase = (($rec.Split("|"))[0]).Trim()
    if ($URL -ne (($rec.Split("|"))[1]).Trim())
    {
        $URL = (($rec.Split("|"))[1]).Trim()
        $result = Invoke-WebRequest $URL
    }

    $idx = [array]::indexof($result.links.innerText.Trim(),$testcase)
    $url2get = "http://$(([System.Uri]$URL).Authority)/"
    
    For ($i = 1; $i -lt ([System.Uri]$URL).Segments.Count - 1; $i++) {$url2get += ([System.Uri]$URL).Segments[$i]}
    $url2get += $result.links.href[$idx].Replace("\","/")
    $url2get = ($url2get.Split("#",2))[0]

    Write-Host "### Test Case: $TestCase - Started "

    $uregwksht.Cells.Item(1,$coldata) = $TestCase

##############################################################
    try
    {
        if ($url2getchk -ne $url2get)
        {
            $WebResponse = Invoke-WebRequest $url2get -ContentType 'text/html; charset=utf-8'
            $url2getchk = $url2get
        }

        $caselinearr = ($WebResponse.ToString() -split "[`n]" | Select-String -Pattern "$TestCase" | Select-Object -Expand LineNumber)
        $startline = $caselinearr[0]
        $endline = $caselinearr[$caselinearr.Count- 1]
        #Write-Host "startline: $startline, endline: $endline"

        $srchPMUniqValstr = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchPMUniqVal 0
        $srchDateVal = GetPMElement $WebResponse.ToString() $startline $endline -1 $srchDate 0

        for ($i = 0; $i -lt $TemplateData.Count; $i++)
        {
            if (($TemplateData[$i]."SearchType") -eq "F")
            {
                $uregwksht.Cells.Item($i + 2,$coldata) = $TemplateData[$i]."SearchText"
            } elseif (($TemplateData[$i]."SearchType") -eq "S")
            {
                if ($TemplateData[$i]."UIName" -eq $srchPMComp)
                {
                    if ($srchPMUniqValstr -match "Center(?<gcenter>.*)Auto")
                    {
                        $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                        $uregwksht.Cells.Item($i + 2,$coldata) = $CompMapping[$Matches['gcenter']]
                        
                    }
                    else
                    {
                        $uregwksht.Cells.Item($i + 2,$coldata) = "<NOT FOUND>"
                    }
                }

                if ($TemplateData[$i]."Activity" -match $srchDate)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata) = $srchDateVal
                }

                if ($TemplateData[$i]."UIName" -eq $srchPMUniqVal)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata) = $srchPMUniqValstr
                }
                
                #Write-Host "($($i))UIName: $($TemplateData[$i]."UIName") XXXXXX Activity:$($TemplateData[$i]."Activity")"

                if (($TemplateData[$i]."UIName" -eq $srchDebitAcct1.Split("|",2)[0]) -And ($TemplateData[$i]."Activity" -eq $srchDebitAcct1.Split("|",2)[1]))
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchDebitAcct1.Split("|",2)[0] 0
                }

                if ($TemplateData[$i]."UIName" -eq $srchDebitAcct2)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchDebitAcct2 0
                }

                if ($TemplateData[$i]."UIName" -eq $srchDCAmnt)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchDCAmntStr 0
                }

                if ($TemplateData[$i]."UIName" -eq $srchHistDate)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchHistDate 0
                }

                if ($TemplateData[$i]."UIName" -eq $srchHistAcct)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchHistAcct 0
                }
                
                if ($TemplateData[$i]."UIName" -eq $srchHistAmnt)
                {
                    $uregwksht.Cells.Item($i + 2,$coldata).NumberFormat = "@"
                    $uregwksht.Cells.Item($i + 2,$coldata) = GetPMElement $WebResponse.ToString() $startline $endline 0 $srchHistAmnt 0
                }
            }
        }
    }
    catch
    {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
            
        Write-Host "Error(0): $FailedItem - $ErrorMessage"
        Add-Content -Path $outputfile -Value "Error(0): $FailedItem - $ErrorMessage" -Encoding UTF8 
    }

    Write-Host "### Test Case: $TestCase - Ended "

##############################################################

    $coldata++
}

$uregwksht.Cells.Item(1,$coldata) = "EndOfCol"

$usedRange = $uregwksht.UsedRange 
$usedRange.EntireColumn.AutoFit() | Out-Null

$objExcel.DisplayAlerts = $False
$workbook.SaveAs($outputfile) 

$WorkBook.Close()
$objExcel.Quit()
#[System.Runtime.InteropServices.Marshal]::FinalReleaseComObject($objExcel) | Out-Null

#Release COM Object
[System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$objExcel) | Out-Null

Write-Host "## $(Get-Date -Format ""dd-MM-yyyy HH:mm:ss""): Ended Writing Search Payment Info ##"