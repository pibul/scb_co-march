﻿param (
[string]$inputfile = ".\GetCCBResult_Input.txt",
[string]$outputfile = ".\GetCCBResult_Output.txt"
)

$excludeList = "CloseBrowser","LaunchWeb_EN_Recovery","CoMarch_Web_EN","LaunchWeb_EN","LaunchWeb_TH_Recovery","CoMarch_Web_TH","LaunchWeb_TH"

$srchDebitAcct1 = "pm_searchacc_inp"
$srchCounterparty = "pm_oat_choosecreditacc_lst|pm_3rd_party_account_inp|pm_direct_credit_account_inp|pm_payroll_direct_credit_management_account_inp|pm_payroll_direct_credit_staff_account_inp|pm_payroll_smart_nextday_manager_account_inp|pm_payroll_smart_nextday_other_account_inp|pm_payroll_smart_nextday_staff_account_inp|pm_payroll_smart_sameday_manager_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_smart_sameday_staff_account_inp|pm_smart_credit_nextday_account_inp|pm_payroll_smart_sameday_other_account_inp|pm_payroll_direct_credit_other_account_inp"
$TextToSearch1 = "pm_addtopt_approvesummary_search_inp"
$srchPymAmt = "pm_3rd_party_amount_inp|pm_direct_credit_amount_inp|pm_oat_amount_inp|pm_orft_amount_inp|pm_payroll_direct_credit_management_amount_inp|pm_payroll_direct_credit_other_amount_inp|pm_payroll_direct_credit_staff_amount_inp|pm_payroll_smart_nextday_manager_amount_inp|pm_payroll_smart_nextday_other_amount_inp|pm_payroll_smart_nextday_staff_amount_inp|pm_payroll_smart_sameday_manager_amount_inp|pm_payroll_smart_sameday_other_amount_inp|pm_payroll_smart_sameday_staff_amount_inp|pm_promptpay_nextday_amount_inp|pm_promptpay_realtime_amount_inp|pm_promptpay_sameday_amount_inp|pm_smart_credit_nextday_amount_inp|pm_payroll_smart_sameday_other_amount_inp"

$srchAcctBlnc = "pm_accounts_available-balance_value_txt"
$srchAcctBlncChg = "Function:WB_WaitUntilValueChange|Function:WB_BIZ_WaitUntilStateChange"
$srchDeductAmnt = "pm_accounts_available-balance_value_txt|Function:WB_CheckTextOnLabel"
$srchPaymentStatus = "Function:WB_BIZ_CheckAuthorizeTransaction"
$srchAcctHist = "pm_accounts_history_transactiondatelist_txt"

#$URL = "http://localhost:8282/CoMarch_Web_TH/2018-04-18-11-58-23/CoMarch_Web_TH_FAIL.html"
#$URL = "http://localhost:8282/CoMarch_Web_EN/2018-03-28-14-20-46/CoMarch_Web_EN_FAIL.html"

$file = Get-Content($inputfile)

IF (Test-Path($outputfile))
{
    Remove-Item $outputfile
}

$url2getchk = [string]::Empty

foreach ($rec in $file)
{
    $URL = $rec
    Write-Host "### URL: $URL"
    Add-Content -Path $outputfile -Value "### URL: $URL" -Encoding UTF8 

    $result =  Invoke-WebRequest $URL

    foreach ($r in $result.Links)
    {
        if (-Not ($excludeList | Where-Object {$_ -like $r.innerText.Trim()}))
        {
            try
            {
                $testcase = $r.innerText.Trim()
                $caselineno = ($result.ToString() -split "[`n]" | Select-String -Pattern "$testcase" | Select-Object -Expand LineNumber)[1]
                $casestatus = ($result.ToString() -split "[`n]")[$caselineno + 4].Trim()
        
                $url2get = "http://$(([System.Uri]$URL).Authority)/"
                For ($i = 1; $i -lt ([System.Uri]$URL).Segments.Count - 1; $i++) {$url2get += ([System.Uri]$URL).Segments[$i]}
                #$url2get += $result.links.href[$idx].Replace("\","/")
                $url2get += ($r.href.Replace("\","/")).Split("#",2)[0]

                if ($url2getchk -ne $url2get)
                {
                    $WebResponse = Invoke-WebRequest $url2get -ContentType 'text/html; charset=utf-8'
                    $url2getchk = $url2get
                }

                $knownissuefound = $False

                $caselinearr = ($WebResponse.ToString() -split "[`n]" | Select-String -Pattern "$testcase" | Select-Object -Expand LineNumber)
                $startline = $caselinearr[0]
                $endline = $caselinearr[$caselinearr.Count- 1]

                #Write-Host "### $startline : $endline ###"

                if ($casestatus -ne "INCOMPLETED")
                {
                    Try
                    {
                        $srchDebitAcct1LineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchDebitAcct1).LineNumber)

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchDebitAcct1LineNo[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchDebitAcct1LineNo[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {
                            $debitAcct1 = ($matches['content'],"<NOT FOUND>")[(($startline + $srchDebitAcct1LineNo[2]) -gt $endline)]
                        }
                        else
                        {
                            $debitAcct1 = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $debitAcct1 = "<NOT FOUND>"
                    }


                    Try
                    {
                        $srchCounterpartyLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchCounterparty).LineNumber)

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[0]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[0]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {

                            $chkifcounterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchCounterpartyLineNo[0]) -gt $endline)]
                            if ($chkifcounterparty -match "^[\d\.]+$")
                            {
                                $counterparty = $chkifcounterparty
                            }
                            else
                            {
                                if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchCounterpartyLineNo[2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                                {
                                    $counterparty = ($matches['content'],"<NOT FOUND>")[(($startline + $srchCounterpartyLineNo[2]) -gt $endline)]
                                }
                                else
                                {
                                    $counterparty = "<NOT FOUND>"
                                }
                            }
                        }
                        else
                        {
                            $counterparty = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $counterparty = "<NOT FOUND>"
                    }

                    Try
                    {
                        $TextToSearch1linenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $TextToSearch1).LineNumber)[0]
                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $TextToSearch1linenumber]) -match "<td style='color:Red;'>(?<content>.*)</td>"))
                        {
                            $result1 = ($matches['content'],"<NOT FOUND>")[(($startline + $TextToSearch1linenumber) -gt $endline)]
                        }
                        else
                        {
                            $result1 = "<NOT FOUND>"
                        }
                    }
                    Catch
                    {
                        $result1 = "<NOT FOUND>"
                    }

                    Try
                    {
                        $srchPymAmtlinenumber = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchPymAmt).LineNumber)

                        $chkmodified = 0

                        for ($i=0;$i -lt $srchPymAmtlinenumber.Count;$i++) {
                            if (($startline + $srchPymAmtlinenumber[$i]) -lt $endline)
                            {
                                $chkmodified += 1
                            }
                        }

                        if (((($WebResponse.ToString() -split "[`n]")[$startline + $srchPymAmtlinenumber[$chkmodified - 2]]) -match "<td style='color:Black;'>(?<content>.*)</td>") -Or ((($WebResponse.ToString() -split "[`n]")[$startline + $srchPymAmtlinenumber[$chkmodified - 2]]) -match "<td style='color:Red;'>(?<content>.*)</td>")) 
                        {
                            $pymamt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchPymAmtlinenumber[$chkmodified - 2]) -gt $endline)]
                        }
                        else
                        {
                            $pymamt = "<NOT FOUND>"
                        }
                    }
                    catch
                    {
                        $pymamt = "<NOT FOUND>"
                    }

                    Write-Host "$testcase,$casestatus,$debitAcct1,$counterparty,$result1,$pymamt"
                    Add-Content -Path $outputfile -Value "$testcase,$casestatus,$debitAcct1,$counterparty,$result1,$pymamt" -Encoding UTF8 

                    if ($casestatus -eq "FAIL")
                    {

                        Try
                        {
                            $srchAcctBlncChgLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchAcctBlncChg).LineNumber)

                            for ($i = 0; $i -lt $srchAcctBlncChgLineNo.Count; $i++)
                            {
                                if (($startline + $srchAcctBlncChgLineNo[$i]) -lt $endline)
                                { 
                                    #$x = ($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncChgLineNo[$i] - 1]
                                    #Write-Host ("$($startline + $srchAcctBlncChgLineNo[$i] - 1) ### $x")

                                    if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncChgLineNo[$i] - 1] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                    {
                                        $errmsg = ($matches['content'],"<NOT FOUND>")[(($startline + $srchAcctBlncChgLineNo[$i] - 1) -gt $endline)]
                                        Write-Host "==> Balance - Not Changed: $errmsg"
                                        Add-Content -Path $outputfile -Value "==> Balance - Not Changed: $errmsg" -Encoding UTF8 

                                        $knownissuefound = $True
                                    }
                                }
                            }
                        }
                        catch
                        {
                            $ErrorMessage = $_.Exception.Message
                            $FailedItem = $_.Exception.ItemName
            
                            Write-Host "Error(3): $FailedItem - $ErrorMessage"
                            Add-Content -Path $outputfile -Value "Error(3): $FailedItem - $ErrorMessage" -Encoding UTF8 
                        }

                        try
                        {
                            $srchDeductAmntLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchDeductAmnt).LineNumber)

                            for ($i = 0; $i -lt $srchDeductAmntLineNo.Count; $i++)
                            {
                                if (($startline + $srchDeductAmntLineNo[$i]) -lt $endline)
                                {
                                    $chk1 = ($WebResponse.ToString() -split "[`n]")[$startline + $srchDeductAmntLineNo[$i] - 2]
                                    $chk2 = ($WebResponse.ToString() -split "[`n]")[$startline + $srchDeductAmntLineNo[$i] - 1]
                                    if (($chk1 -match "pm_accounts_available-balance_value_txt") -and ($chk1 -match "<td style='color:Red") -and ($chk2 -match "Function:WB_CheckTextOnLabel") -and ($chk2 -match "<td style='color:Red"))
                                    {
                                        #if (($WebResponse.ToString() -split "[`n]")[$startline + $srchDeductAmntLineNo[$i] + 2] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                        #{
                                            #Write-Host ("$($startline + $srchDeductAmntLineNo[$i] - 2) ### $chk1 ### $chk2")
                                            $deductErr = (($WebResponse.ToString() -split "[`n]")[$startline + $srchDeductAmntLineNo[$i] + 2]) -replace "<.*?>| View Screenshot" 
                                            #$deductErr = $matches['content']
                                            Write-Host "==> Deducted amount is not equal to actual amount: $deductErr"
                                            Add-Content -Path $outputfile -Value "==> Deducted amount is not equal to actual amount: $deductErr" -Encoding UTF8 

                                            $knownissuefound = $True
                                        #}
                                    }
                                }
                            }
                            
                        }
                        catch
                        {
                            $ErrorMessage = $_.Exception.Message
                            $FailedItem = $_.Exception.ItemName
            
                            Write-Host "Error(4): $FailedItem - $ErrorMessage"
                            Add-Content -Path $outputfile -Value "Error(4): $FailedItem - $ErrorMessage" -Encoding UTF8 
                        }


<########################
                        Try
                        {
                            $srchAcctBlncLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchAcctBlnc).LineNumber)
                
                            if ($srchAcctBlncLineNo)
                            {
                                for ($i=0; $i -lt $srchAcctBlncLineNo.Count; $i++)
                                {
                                    if (($startline + $srchAcctBlncLineNo[$i]) -lt $endline)
                                    {
                                        $x = ($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i] - 1]
                                        Write-Host "@@@ $($startline + $srchAcctBlncLineNo[$i] - 1) @@@ $x @@@"
                                        

                                        # Check account balance changing
                                        if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i] - 1] -match $srchAcctBlncChg)
                                        {
                                            #Write-Host "==> $(($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i]])"
                                            if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i]] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                            {
                                                $errmsg = ($matches['content'],"<NOT FOUND>")[(($startline + $srchAcctBlncLineNo[$i]) -gt $endline)]
                                                #Write-Host "==> Balance - Not Changed: $($matches['content'])"
                                                Write-Host "==> Balance - Not Changed: $errmsg"
                                                #Add-Content -Path $outputfile -Value "==> Balance - Not Changed: $($matches['content'])" -Encoding UTF8 
                                                Add-Content -Path $outputfile -Value "==> Balance - Not Changed: $errmsg" -Encoding UTF8 

                                                $knownissuefound = $True
                                            }
                                        }

                                        # Check Deducted Amount
                                        if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i] - 1] -match $srchDeductAmnt)
                                        {
                                            if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i]] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                            {
                                                $expectedDeductedAmt = $($matches['content'])
                                                
                                                for ($j = 1; $j -le $endline; $j++)
                                                {
                                                    if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i] - 1 + $j]) -match "<td style='color:Red;word-wrap: break-word'><font color=red>Failed</font>")
                                                    {
                                                        if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctBlncLineNo[$i] - 1 + $j]) -match ".*\[(?<ddamnt>.*)\].*")
                                                        {
                                                            #$appDeductedAmt = $Matches['ddamnt']
                                                            $appDeductedAmt = ($matches['content'],"<NOT FOUND>")[(($startline + $srchAcctBlncLineNo[$i]) -gt $endline)]
                                                        }

                                                        break
                                                    }
                                                }

                                                Write-Host "==> Deducted amount is not equal to actual amount: [Expected: $expectedDeductedAmt] [App Label: $appDeductedAmt]"
                                                Add-Content -Path $outputfile -Value "==> Deducted amount is not equal to actual amount: [Expected: $expectedDeductedAmt] [App Label: $appDeductedAmt]" -Encoding UTF8 

                                                $knownissuefound = $True
                                            }
                                        }
 
                                    }
                                }
                            }
                
                        }
                        catch
                        {
                            $ErrorMessage = $_.Exception.Message
                            $FailedItem = $_.Exception.ItemName
            
                            Write-Host "Error(3): $FailedItem - $ErrorMessage"
                            Add-Content -Path $outputfile -Value "Error(3): $FailedItem - $ErrorMessage" -Encoding UTF8 
                        }
################################>

                        Try
                        {
                            $srchPaymentStatusLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchPaymentStatus).LineNumber)

                            if ($srchPaymentStatusLineNo)
                            {
                                #if (($startline + $srchPaymentStatusLineNo[1] - 1) -lt $endline)
                                #{
                                    if (($WebResponse.ToString() -split "[`n]")[$startline + $srchPaymentStatusLineNo[1] - 1] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                    {
                                        for ($i=1; $i -le $endline; $i++)
                                        {
                                            if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchPaymentStatusLineNo[1] - 1 + $i]) -match "<td style='color:Red;word-wrap: break-word'><font color=red>Failed</font>")
                                            {
                                                if ((($WebResponse.ToString() -split "[`n]")[$startline + $srchPaymentStatusLineNo[1] - 1 + $i]) -match ".*\[(?<pmstatus>.*)\].*")
                                                {
                                                    $errmsg = ($matches['content'],"<NOT FOUND>")[(($startline + $srchPaymentStatusLineNo[1]) -gt $endline)]
                                                    #Write-Host "==> Status (""Success"") - Not Matched: $($Matches['pmstatus'])"
                                                    Write-Host "==> Status (""Success"") - Not Matched: $errmsg"
                                                    #Add-Content -Path $outputfile -Value "==> Status (""Success"") - Not Matched: $($Matches['pmstatus'])" -Encoding UTF8 
                                                    Add-Content -Path $outputfile -Value "==> Status (""Success"") - Not Matched: $errmsg" -Encoding UTF8 

                                                    $knownissuefound = $True
                                                }

                                                break
                                            }
                                        }
                                    }
                                #}
                            }
                        }
                        catch
                        {
                            $ErrorMessage = $_.Exception.Message
                            $FailedItem = $_.Exception.ItemName
            
                            Write-Host "Error(2): $FailedItem - $ErrorMessage"
                            Add-Content -Path $outputfile -Value "Error(2): $FailedItem - $ErrorMessage" -Encoding UTF8 
                        }

                        Try
                        {
                            $srchAcctHistLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern $srchAcctHist).LineNumber)

                            if ($srchAcctHistLineNo)
                            {
                                if (($startline + $srchAcctHistLineNo[0]) -lt $endline) {
                                    if (($WebResponse.ToString() -split "[`n]")[$startline + $srchAcctHistLineNo[0]] -match "<td style='color:Red;'>(?<content>.*)</td>")
                                    {
                                        Write-Host "==> Account History - Not Found: $($matches['content'])"
                                        Add-Content -Path $outputfile -Value "==> Account History - Not Found: $($matches['content'])" -Encoding UTF8 

                                        $knownissuefound = $True
                                    }
                                }
                            }
                        }
                        catch
                        {
                            $ErrorMessage = $_.Exception.Message
                            $FailedItem = $_.Exception.ItemName
            
                            Write-Host "Error(1): $FailedItem - $ErrorMessage"
                            Add-Content -Path $outputfile -Value "Error(1): $FailedItem - $ErrorMessage" -Encoding UTF8 
                        }

                        if (-Not $knownissuefound)
                        {
                            #$srchOtherErrorLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern "<td style='color:Red;word-wrap: break-word'>").LineNumber)
                            $srchOtherErrorLineNo = (($WebResponse.ToString() -split "[`n]" | Select-Object -Skip ($startline - 1) | Select-String -Pattern "color:Red").LineNumber)

                            if (($startline + $srchOtherErrorLineNo[$srchOtherErrorLineNo.Count - 1] - 2) -lt $endline)
                            {
                                if (($WebResponse.ToString() -split "[`n]")[$startline + $srchOtherErrorLineNo[$srchOtherErrorLineNo.Count - 1] - 2] -match ".*<font color=red>(?<errortext>.*)<br><a.*")
                                {
                                    $errtxt = $matches['errortext'] -replace "<\w+>|</\w+>|<font.*>"
                                    Write-Host "==> Error: $errtxt"
                                    Add-Content -Path $outputfile -Value "==> Error: $errtxt" -Encoding UTF8 
                                }
                            }
                        }
                    }
                }
                else
                {
                    Write-Host "$testcase,$casestatus"
                    Add-Content -Path $outputfile -Value "$testcase,$casestatus" -Encoding UTF8 
                }
            }
            catch
            {
                $ErrorMessage = $_.Exception.Message
                $FailedItem = $_.Exception.ItemName
            
                Write-Host "Error(0): $FailedItem - $ErrorMessage"
                Add-Content -Path $outputfile -Value "Error(0): $FailedItem - $ErrorMessage" -Encoding UTF8 
            }
        }
    }
}
